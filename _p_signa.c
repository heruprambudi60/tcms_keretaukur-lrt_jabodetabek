#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XFRONTR_WHT1_HMI;
   SC_BOOL                    XREARR_WHT2_HMI;
   SC_BOOL                    XFRONTR_RED1_HMI;
   SC_BOOL                    XREARR_RED2_HMI;
   SC_BOOL                    XFRONTR_SIDE1_HMI;
   SC_BOOL                    XREARR_SIDE2_HMI;
   SC_BOOL                    XFRONTR_GREEN_HMI;
   SC_BOOL                    XREARR_GREEN_HMI;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDE_RED_1;
   SC_BOOL                    XSIDE_RED_2;
   SC_BOOL                    XGREEN_1;
   SC_BOOL                    XGREEN_2;
}SIGNAL_LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XFRONT_ACTV;
   SC_BOOL                    XSIDE_ACTV;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDERED_1;
   SC_BOOL                    XSIDERED_2;
   SC_BOOL                    XFRWHITE_HMI;
   SC_BOOL                    XRRWHITE_HMI;
   SC_BOOL                    XFRSIDERED_HMI;
   SC_BOOL                    XRRSIDERED_HMI;
   SC_BOOL                    XFRRED_HMI;
   SC_BOOL                    XRRRED_HMI;
}P_SIGNAL_LAMP_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_SIGNAL_LAMP_00111(P_SIGNAL_LAMP_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_SIGNAL_LAMP_TYP, XRRRED_HMI) + sizeof(ip->XRRRED_HMI);
   /* non retains: */
   ip->XFRONT_ACTV = SC_C_BOOL(0);
   ip->XSIDE_ACTV = SC_C_BOOL(0);
   ip->XWHITE_1 = SC_C_BOOL(0);
   ip->XWHITE_2 = SC_C_BOOL(0);
   ip->XSIDERED_1 = SC_C_BOOL(0);
   ip->XSIDERED_2 = SC_C_BOOL(0);
   ip->XFRWHITE_HMI = SC_C_BOOL(0);
   ip->XRRWHITE_HMI = SC_C_BOOL(0);
   ip->XFRSIDERED_HMI = SC_C_BOOL(0);
   ip->XRRSIDERED_HMI = SC_C_BOOL(0);
   ip->XFRRED_HMI = SC_C_BOOL(0);
   ip->XRRRED_HMI = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_SIGNAL_LAMP_00044(void)
{
   P_SIGNAL_LAMP_TYP *ip = (P_SIGNAL_LAMP_TYP*) __OD(112);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(44, ip);
   ip->XFRONT_ACTV = (((1 & LD_I_X(3,1,2,22)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,3,25)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XSIDE_ACTV = (((1 & LD_I_X(3,1,2,23)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,3,26)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));

   if(((1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD) ^ (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV)))
   {
      ip->XWHITE_1 = ((1 & ip->XFRONT_ACTV) & (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD)) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV))));
      ip->XWHITE_2 = ((1 & ip->XFRONT_ACTV) & (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV)) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD))));
      ip->XSIDERED_1 = ((1 & ip->XSIDE_ACTV) & (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV)) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD))));
      ip->XSIDERED_2 = ((1 & ip->XSIDE_ACTV) & (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD)) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) 
      & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV))));
      ip->XFRWHITE_HMI = ((1 & ip->XFRONT_ACTV) & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD));
      ip->XRRWHITE_HMI = ((1 & ip->XFRONT_ACTV) & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV));
      ip->XFRRED_HMI = ((1 & ip->XFRONT_ACTV) & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV));
      ip->XRRRED_HMI = ((1 & ip->XFRONT_ACTV) & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD));
      ip->XFRSIDERED_HMI = ((1 & ip->XSIDE_ACTV) & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV));
      ip->XRRSIDERED_HMI = ((1 & ip->XSIDE_ACTV) & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD));
   }
   else
   {
      ip->XWHITE_1 = SC_C_BOOL(0);
      ip->XWHITE_2 = SC_C_BOOL(0);
      ip->XSIDERED_1 = SC_C_BOOL(0);
      ip->XSIDERED_2 = SC_C_BOOL(0);
      ip->XFRWHITE_HMI = SC_C_BOOL(0);
      ip->XRRWHITE_HMI = SC_C_BOOL(0);
      ip->XFRRED_HMI = SC_C_BOOL(0);
      ip->XRRRED_HMI = SC_C_BOOL(0);
      ip->XFRSIDERED_HMI = SC_C_BOOL(0);
      ip->XRRSIDERED_HMI = SC_C_BOOL(0);
   }

   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XWHITE_1 = ip->XWHITE_1;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XWHITE_2 = ip->XWHITE_2;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XSIDE_RED_1 = ip->XSIDERED_1;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XSIDE_RED_2 = ip->XSIDERED_2;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_WHT1_HMI = ip->XFRWHITE_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_WHT2_HMI = ip->XRRWHITE_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_RED1_HMI = ip->XFRRED_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_RED2_HMI = ip->XRRRED_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_SIDE1_HMI = ip->XFRSIDERED_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_SIDE2_HMI = ip->XRRSIDERED_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XGREEN_1 = ip->XWHITE_1;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XGREEN_2 = ip->XWHITE_2;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_GREEN_HMI = ip->XFRWHITE_HMI;
   (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_GREEN_HMI = ip->XRRWHITE_HMI;

   PRG_EXIT(44, ip);
}
