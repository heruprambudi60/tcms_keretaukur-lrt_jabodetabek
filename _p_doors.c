#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_DOORS_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_DOORS_00127(P_DOORS_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_DOORS_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_DOORS_00052(void)
{
   P_DOORS_TYP *ip = (P_DOORS_TYP*) __OD(128);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(52, ip);
   (*(DOORS_T_TYP*)__OD(16)).XD1_OPEN = LD_I_X(3,1,1,19);
   (*(DOORS_T_TYP*)__OD(16)).XD2_OPEN = LD_I_X(3,1,1,20);
   (*(DOORS_T_TYP*)__OD(16)).XDALLCLOSED =  (!(((1 & LD_I_X(3,1,1,19)) & (1 & LD_I_X(3,1,1,
   20)))));
   (*(DOORS_T_TYP*)__OD(16)).XD1_LOCKED = LD_I_X(3,1,1,21);
   (*(DOORS_T_TYP*)__OD(16)).XD2_LOCKED = LD_I_X(3,1,1,22);
   (*(DOORS_T_TYP*)__OD(16)).XDBAYPASS = LD_I_X(3,1,1,25);
   (*(DOORS_T_TYP*)__OD(16)).XDFAULT = ((((1 & LD_I_X(3,1,1,19)) & (1 & LD_I_X(3,1,1,21))) 
   | ((1 & LD_I_X(3,1,1,20)) & (1 & LD_I_X(3,1,1,22)))) |  (!((((1 & (*(DOORS_T_TYP*)__OD(16)).XDALLCLOSED) 
   | (1 & (*(DOORS_T_TYP*)__OD(16)).XDBAYPASS)) | (1 & (*(SPEED_T_TYP*)__OD(15)).XZEROSPEED)))));

   PRG_EXIT(52, ip);
}
