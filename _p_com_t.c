#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_INT                     XFILLINGTIME;
   SC_INT                     XLEAKAGECOMMAND;
}COMPRESOR_TEST_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
   SC__INSTC                  CTU_1;
   SC_INT                     IMRPRESSURE;
   SC_INT                     IMRSTART;
   SC_INT                     IMRSTOP;
   SC_INT                     ILEAKKAGE_RATE;
   SC_BOOL                    XENABLEFILL;
   SC_INT                     IFILLINGTIME;
   SC_BOOL                    XFILLING;
   SC_BOOL                    XRSTFILLINGCOUNTER;
   SC_BOOL                    XLEAKAGETESTRUNNING;
   SC_BOOL                    XENABLELEAK;
   SC_BOOL                    XSTOP;
   SC__INSTC                  R_TRIG_1;
   SC__INSTC                  F_TRIG_1;
}P_COM_TEST_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_COM_TEST_00105(P_COM_TEST_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_COM_TEST_TYP, F_TRIG_1) + sizeof(ip->F_TRIG_1);
   /* non retains: */
   ip->IMRPRESSURE = SC_C_INT(0);
   ip->ILEAKKAGE_RATE = SC_C_INT(0);
   ip->XENABLEFILL = SC_C_BOOL(0);
   ip->IFILLINGTIME = SC_C_INT(0);
   ip->XFILLING = SC_C_BOOL(0);
   ip->XRSTFILLINGCOUNTER = SC_C_BOOL(0);
   ip->XLEAKAGETESTRUNNING = SC_C_BOOL(0);
   ip->XENABLELEAK = SC_C_BOOL(0);
   ip->XSTOP = SC_C_BOOL(0);

   /* retains: */
   if(iRetain)
   {
      ip->IMRSTART = SC_C_INT(0);
      ip->IMRSTOP = SC_C_INT(0);

   }
}


/* PROGRAM BODY: */
void SC_BODY__P_COM_TEST_00041(void)
{
   P_COM_TEST_TYP *ip = (P_COM_TEST_TYP*) __OD(106);

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   PRG_ENTRY(41, ip);
/******************* Network *************************/
{
   RS_TYP* __lpt0 = (RS_TYP*)__OD(ip->RS_1);
   (*__lpt0).SET = ip->XENABLEFILL;
   (*__lpt0).RESET1 = (ip->IMRPRESSURE >= SC_C_INT(10000));
   RS((__lpt0));
   ip->XFILLING = (*__lpt0).Q1;

}
/******************* Network *************************/
{
   TON_TYP* __lpt0 = (TON_TYP*)__OD(ip->TON_1);
   CTU_TYP* __lpt1 = (CTU_TYP*)__OD(ip->CTU_1);
   RS_TYP* __lpt2 = (RS_TYP*)__OD(ip->RS_2);
   (*__lpt2).SET = ip->XFILLING;
   (*__lpt2).RESET1 = ip->XRSTFILLINGCOUNTER;
   RS((__lpt2));
   (*__lpt0).IN = (*__lpt2).Q1;
   (*__lpt0).PT = SC_C_TIME(1000);
   TON((__lpt0));
   ip->XRSTFILLINGCOUNTER = (*__lpt0).Q;
   (*__lpt1).CU = (*__lpt0).Q;
   (*__lpt1).RESET =  (!((1 & ip->XFILLING)));
   CTU((__lpt1));
   ip->IFILLINGTIME = (*__lpt1).CV;

}
/******************* Network *************************/
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_1);
   TON_TYP* __lpt1 = (TON_TYP*)__OD(ip->TON_2);
   (*__lpt0).SET1 = ip->XENABLELEAK;
   (*__lpt0).RESET = ip->XSTOP;
   SR((__lpt0));
   (*__lpt1).IN = (*__lpt0).Q1;
   (*__lpt1).PT = SC_C_TIME(1800000);
   TON((__lpt1));
   ip->XSTOP = (*__lpt1).Q;
   ip->XLEAKAGETESTRUNNING = (*__lpt0).Q1;

}
/******************* Network *************************/
{
   R_TRIG_TYP* __lpt0 = (R_TRIG_TYP*)__OD(ip->R_TRIG_1);
   (*__lpt0).CLK = ip->XLEAKAGETESTRUNNING;
   R_TRIG((__lpt0));
   ip->IMRSTART = SEL_INT((*__lpt0).Q,ip->IMRSTART,ip->IMRPRESSURE);

}
/******************* Network *************************/
{
   F_TRIG_TYP* __lpt0 = (F_TRIG_TYP*)__OD(ip->F_TRIG_1);
   (*__lpt0).CLK = ip->XLEAKAGETESTRUNNING;
   F_TRIG((__lpt0));
   ip->IMRSTOP = SEL_INT((*__lpt0).Q,ip->IMRSTART,ip->IMRPRESSURE);
   _SCT_B0 =  (!((1 & (*__lpt0).Q)));
   if((1 & _SCT_B0)){
      PRG_EXIT(41, ip);
      return;
   }

}
/******************* Network *************************/
{
   ip->ILEAKKAGE_RATE = ((ip->IMRSTART - ip->IMRSTOP) * SC_C_INT(2));

}
/******************* Network *************************/
{
   (*(COMPRESOR_TEST_T_TYP*)__OD(29)).XLEAKAGECOMMAND = ip->ILEAKKAGE_RATE;

}
/******************* Network *************************/
{
   (*(COMPRESOR_TEST_T_TYP*)__OD(29)).XFILLINGTIME = ip->IFILLINGTIME;

}

   PRG_EXIT(41, ip);
}
