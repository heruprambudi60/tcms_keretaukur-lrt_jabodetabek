#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLEHALF;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_DINT                    DIAC1TIME;
   SC_DINT                    DIAC2TIME;
   SC_BOOL                    XHALF1;
   SC_BOOL                    XHALF2;
   SC__INSTC                  R_TRIG_1;
   SC_BOOL                    XERROR;
   SC__INSTC                  SR_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    XRESET;
}FB_AC_HALFSEL_REVA_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_AC_HALFSEL_REVA_00076(FB_AC_HALFSEL_REVA_TYP* ip1, FB_AC_HALFSEL_REVA_TYP* 
ip2)
{
   MOVE_ANY(&(ip1->XENABLEHALF), &(ip2->XENABLEHALF), (long) (&(ip1->R_TRIG_1)) - (long) (
   &(ip1->XENABLEHALF)));
   MOVE_ANY(__OD(ip1->R_TRIG_1), __OD(ip2->R_TRIG_1), sizeof(R_TRIG_TYP));
   MOVE_ANY(&(ip1->XERROR), &(ip2->XERROR), (long) (&(ip1->SR_1)) - (long) (&(ip1->XERROR)));
   MOVE_ANY(__OD(ip1->SR_1), __OD(ip2->SR_1), sizeof(SR_TYP));
   MOVE_ANY(__OD(ip1->TON_1), __OD(ip2->TON_1), sizeof(TON_TYP));
   MOVE_ANY(&(ip1->XRESET), &(ip2->XRESET), (long) (ip1) + sizeof(FB_AC_HALFSEL_REVA_TYP) 
   - (long) (&(ip1->XRESET)));
}

void SC_INIT__FB_AC_HALFSEL_REVA_00077(FB_AC_HALFSEL_REVA_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_AC_HALFSEL_REVA_TYP, XRESET) + sizeof(ip->XRESET);
   /* non retains: */
   ip->XENABLEHALF = SC_C_BOOL(0);
   ip->XAC1ERROR = SC_C_BOOL(0);
   ip->XAC2ERROR = SC_C_BOOL(0);
   ip->DIAC1TIME = SC_C_DINT(0);
   ip->DIAC2TIME = SC_C_DINT(0);
   ip->XHALF1 = SC_C_BOOL(0);
   ip->XHALF2 = SC_C_BOOL(0);
   ip->XERROR = SC_C_BOOL(0);
   ip->XRESET = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_AC_HALFSEL_REVA_00060(FB_AC_HALFSEL_REVA_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;
   SC_BOOL                    _SCT_B1;
   SC_BOOL                    _SCT_B2;
   SC_BOOL                    _SCT_B3;

   FB_ENTRY(60, ip);
/******************* Network *************************/
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_1);
   TON_TYP* __lpt1 = (TON_TYP*)__OD(ip->TON_1);
   R_TRIG_TYP* __lpt2 = (R_TRIG_TYP*)__OD(ip->R_TRIG_1);
   _SCT_B0 =  (!((1 & ip->XENABLEHALF)));
   (*__lpt2).CLK = ip->XENABLEHALF;
   R_TRIG((__lpt2));
   (*__lpt0).SET1 = ((1 & (*__lpt2).Q) &  (!(((1 & ip->XAC1ERROR) | (1 & ip->XAC2ERROR)))));
   (*__lpt0).RESET = ip->XRESET;
   SR((__lpt0));
   _SCT_B1 = (*__lpt0).Q1;
   (*__lpt1).IN = (*__lpt0).Q1;
   (*__lpt1).PT = SC_C_TIME(3000);
   TON((__lpt1));
   ip->XRESET = (*__lpt1).Q;
   _SCT_B2 = ((1 & ip->XAC1ERROR) | (1 & ip->XAC2ERROR));
   _SCT_B3 = _SCT_B2;
   ip->XERROR = _SCT_B2;
   if((1 & _SCT_B0))goto SCL__RESET;
   if((1 & _SCT_B1))goto SCL__STARTNORMAL;
   if((1 & _SCT_B3))goto SCL__ERROR;

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(60, ip);
      return;
   }

}
/******************* Network *************************/
SCL__STARTNORMAL:;
{
   _SCT_B0 = (ip->DIAC1TIME >= ip->DIAC2TIME);
   ip->XHALF2 = _SCT_B0;
   ip->XHALF1 = NOT_BOOL(_SCT_B0);

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(60, ip);
      return;
   }

}
/******************* Network *************************/
SCL__ERROR:;
{
   ip->XHALF2 = ((1 & ip->XAC1ERROR) &  (!((1 & ip->XAC2ERROR))));
   ip->XHALF1 = ( (!((1 & ip->XAC1ERROR))) & (1 & ip->XAC2ERROR));

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(60, ip);
      return;
   }

}
/******************* Network *************************/
SCL__RESET:;
{
   ip->XHALF1 = SC_C_BOOL(0);
   ip->XHALF2 = SC_C_BOOL(0);

}

   FB_EXIT(60, ip);
}
