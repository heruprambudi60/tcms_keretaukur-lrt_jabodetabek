/**********************************************************************
 *****                        CSTRUCT.H                           *****
 *****                  Additional C-Header-File                  *****
 *****                 for direct use in User-Area                *****
 *****            (see options "Additional C-Headers")            *****
 **********************************************************************/


#ifndef _CSTRUCTS_H_
#define _CSTRUCTS_H_


/***** LIST OF DUMPED TYPES *****/
/*
 ***** DUT TYPES:
   CABIN_T_XTYP;
   MASTERCTRL_T_XTYP;
   MANUALMODE_T_XTYP;
   DEADMAN_T_XTYP;
   SPEED_T_XTYP;
   DOORS_T_XTYP;
   BATT_T_XTYP;
   LAMP_T_XTYP;
   BRAKE_T_XTYP;
   MODBUSBOOL_R_T_XTYP;
   MODBUSINT_R_T_XTYP;
   MODBUSBOOL_RW_T_XTYP;
   MODBUSINT_RW_T_XTYP;
   TRACTION_MNGMNT_XTYP;
   COMPRESSOR_T_XTYP;
   SIGNAL_LAMP_T_XTYP;
   HEAD_LAMP_T_XTYP;
   CALENDAR_T_XTYP;
   COMPRESOR_TEST_T_XTYP;
   ULTRASON_MODE_T_XTYP;
   TRAIN_WASH_MODE_T_XTYP;
   R1_T_XTYP;
   ENGINE_STATUS_T_XTYP;
   AC_T_XTYP;

 ***** FB TYPES:
   FB_AC_CD_XTYP;
   FB_AC_CP1_XTYP;
   FB_AC_CP2_XTYP;
   FB_AC_EVAP_XTYP;
   FB_AC_1_XTYP;
   FB_AC_2_XTYP;
   FB_TIMECOUNT_DINT_XTYP;
   FB_AC_HALFSEL_REVA_XTYP;
   FB_AC_GETTEMP_REVA_XTYP;
   FB_FILTER_XTYP;
   FB_GETTEMP_XTYP;
   FB_PRESSURE_CHECK_XTYP;
*/

/***** STRING TYPE DEFINITIONS: *****/
typedef SC_STRINGDEF(20)  XSTRING__TYPE20;


/***** DUT SECTION: *****/
typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_XTYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_XTYP;

typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_XTYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_XTYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_XTYP;

typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_XTYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_XTYP;

typedef struct
{
   SC_BOOL                    XROOMLAMPSTAT_1;
   SC_BOOL                    XROOMLAMPSTAT_2;
   SC_BOOL                    XROOMLAMPFAULT_1;
   SC_BOOL                    XROOMLAMPFAULT_2;
}LAMP_T_XTYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_XTYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XENGINE_RUNNING;
   SC_BOOL                    XENGINE_STOP;
   SC_BOOL                    XENGINE_EMG_STOP;
   SC_BOOL                    XMODEULTRASONIK;
   SC_BOOL                    XMODETRAINWASH;
   SC_BOOL                    XSMOKEDETECTORCAB1;
   SC_BOOL                    XSMOKEDETECTORCAB2;
   SC_BOOL                    XHEAD_LAMP1_ON;
   SC_BOOL                    XHEAD_LAMP1_FAULT;
   SC_BOOL                    XHEAD_LAMP2_ON;
   SC_BOOL                    XHEAD_LAMP2_FAULT;
   SC_BOOL                    XHEAD_LAMP_NOTIF_ON;
   SC_BOOL                    XFRWHITE;
   SC_BOOL                    XRRWHITE;
   SC_BOOL                    XFRRED;
   SC_BOOL                    XRRRED;
   SC_BOOL                    XFRGREEN;
   SC_BOOL                    XRRGREEN;
   SC_BOOL                    XFRSIDERED;
   SC_BOOL                    XRRSIDERED;
   SC_BOOL                    XPARKINGBRAKEAPPLIED;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XBRAKECYLINDER;
   SC_BOOL                    XBRAKEPIPEPRESURE;
   SC_BOOL                    XHOLDINGBRAKE;
   SC_BOOL                    XDYNAMICBRAKESW;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XCOMFAULT;
   SC_BOOL                    XCOMEMG;
   SC_BOOL                    XCOMBYPASS;
   SC_BOOL                    XCOM1_ON;
   SC_BOOL                    XCOM2_ON;
   SC_BOOL                    XCOMPRESSOR_ERROR;
   SC_BOOL                    XCOM_OFF;
   SC_BOOL                    XTRACTIONREADY;
   SC_BOOL                    XCAB1FRONT;
   SC_BOOL                    XCAB2FRONT;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XFORWARD;
   SC_BOOL                    XREVERSE;
   SC_BOOL                    XCAB1DIR;
   SC_BOOL                    XCAB2DIR;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XBATTSHUTDOWN;
   SC_BOOL                    XDEADMANACTIVE;
   SC_BOOL                    XDEADMANTRCUTOFF;
   SC_BOOL                    XDEADMANEB;
   SC_BOOL                    XROOMLPSTAT_1;
   SC_BOOL                    XROOMLPFAULT_1;
   SC_BOOL                    XROOMLPSTAT_2;
   SC_BOOL                    XROOMLPFAULT_2;
   SC_BOOL                    XD1OPEN;
   SC_BOOL                    XD2OPEN;
   SC_BOOL                    XD1LOCKED;
   SC_BOOL                    XD2LOCKED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBYPASS;
   SC_BOOL                    XRIOM1_DI1_0;
   SC_BOOL                    XRIOM1_DI1_1;
   SC_BOOL                    XRIOM1_DI1_2;
   SC_BOOL                    XRIOM1_DI1_3;
   SC_BOOL                    XRIOM1_DI1_4;
   SC_BOOL                    XRIOM1_DI1_5;
   SC_BOOL                    XRIOM1_DI1_6;
   SC_BOOL                    XRIOM1_DI1_7;
   SC_BOOL                    XRIOM1_DI1_8;
   SC_BOOL                    XRIOM1_DI1_9;
   SC_BOOL                    XRIOM1_DI1_10;
   SC_BOOL                    XRIOM1_DI1_11;
   SC_BOOL                    XRIOM1_DI1_12;
   SC_BOOL                    XRIOM1_DI1_13;
   SC_BOOL                    XRIOM1_DI1_14;
   SC_BOOL                    XRIOM1_DI1_15;
   SC_BOOL                    XRIOM1_DI1_16;
   SC_BOOL                    XRIOM1_DI1_17;
   SC_BOOL                    XRIOM1_DI1_18;
   SC_BOOL                    XRIOM1_DI1_19;
   SC_BOOL                    XRIOM1_DI1_20;
   SC_BOOL                    XRIOM1_DI1_21;
   SC_BOOL                    XRIOM1_DI1_22;
   SC_BOOL                    XRIOM1_DI1_23;
   SC_BOOL                    XRIOM1_DI1_24;
   SC_BOOL                    XRIOM1_DI1_25;
   SC_BOOL                    XRIOM1_DI1_26;
   SC_BOOL                    XRIOM1_DI1_27;
   SC_BOOL                    XRIOM1_DI1_28;
   SC_BOOL                    XRIOM1_DI1_29;
   SC_BOOL                    XRIOM1_DI1_30;
   SC_BOOL                    XRIOM1_DI1_31;
   SC_BOOL                    XRIOM1_DI2_0;
   SC_BOOL                    XRIOM1_DI2_1;
   SC_BOOL                    XRIOM1_DI2_2;
   SC_BOOL                    XRIOM1_DI2_3;
   SC_BOOL                    XRIOM1_DI2_4;
   SC_BOOL                    XRIOM1_DI2_5;
   SC_BOOL                    XRIOM1_DI2_6;
   SC_BOOL                    XRIOM1_DI2_7;
   SC_BOOL                    XRIOM1_DI2_8;
   SC_BOOL                    XRIOM1_DI2_9;
   SC_BOOL                    XRIOM1_DI2_10;
   SC_BOOL                    XRIOM1_DI2_11;
   SC_BOOL                    XRIOM1_DI2_12;
   SC_BOOL                    XRIOM1_DI2_13;
   SC_BOOL                    XRIOM1_DI2_14;
   SC_BOOL                    XRIOM1_DI2_15;
   SC_BOOL                    XRIOM1_DI2_16;
   SC_BOOL                    XRIOM1_DI2_17;
   SC_BOOL                    XRIOM1_DI2_18;
   SC_BOOL                    XRIOM1_DI2_19;
   SC_BOOL                    XRIOM1_DI2_20;
   SC_BOOL                    XRIOM1_DI2_21;
   SC_BOOL                    XRIOM1_DI2_22;
   SC_BOOL                    XRIOM1_DI2_23;
   SC_BOOL                    XRIOM1_DI2_24;
   SC_BOOL                    XRIOM1_DI2_25;
   SC_BOOL                    XRIOM1_DI2_26;
   SC_BOOL                    XRIOM1_DI2_27;
   SC_BOOL                    XRIOM1_DI2_28;
   SC_BOOL                    XRIOM1_DI2_29;
   SC_BOOL                    XRIOM1_DI2_30;
   SC_BOOL                    XRIOM1_DI2_31;
   SC_BOOL                    XRIOM1_DO1_0;
   SC_BOOL                    XRIOM1_DO1_1;
   SC_BOOL                    XRIOM1_DO1_2;
   SC_BOOL                    XRIOM1_DO1_3;
   SC_BOOL                    XRIOM1_DO1_4;
   SC_BOOL                    XRIOM1_DO1_5;
   SC_BOOL                    XRIOM1_DO1_6;
   SC_BOOL                    XRIOM1_DO1_7;
   SC_BOOL                    XRIOM1_DO1_8;
   SC_BOOL                    XRIOM1_DO1_9;
   SC_BOOL                    XRIOM1_DO1_10;
   SC_BOOL                    XRIOM1_DO1_11;
   SC_BOOL                    XRIOM1_DO1_12;
   SC_BOOL                    XRIOM1_DO1_13;
   SC_BOOL                    XRIOM1_DO1_14;
   SC_BOOL                    XRIOM1_DO1_15;
   SC_BOOL                    XRIOM1_DO1_16;
   SC_BOOL                    XRIOM1_DO1_17;
   SC_BOOL                    XRIOM1_DO1_18;
   SC_BOOL                    XRIOM1_DO1_19;
   SC_BOOL                    XRIOM1_DO1_20;
   SC_BOOL                    XRIOM1_DO1_21;
   SC_BOOL                    XRIOM1_DO1_22;
   SC_BOOL                    XRIOM1_DO1_23;
   SC_BOOL                    XRIOM1_DO1_24;
   SC_BOOL                    XRIOM1_DO1_25;
   SC_BOOL                    XRIOM1_DO1_26;
   SC_BOOL                    XRIOM1_DO1_27;
   SC_BOOL                    XRIOM1_DO1_28;
   SC_BOOL                    XRIOM1_DO1_29;
   SC_BOOL                    XRIOM1_DO1_30;
   SC_BOOL                    XRIOM1_DO1_31;
   SC_BOOL                    XRIOM2_DI1_0;
   SC_BOOL                    XRIOM2_DI1_1;
   SC_BOOL                    XRIOM2_DI1_2;
   SC_BOOL                    XRIOM2_DI1_3;
   SC_BOOL                    XRIOM2_DI1_4;
   SC_BOOL                    XRIOM2_DI1_5;
   SC_BOOL                    XRIOM2_DI1_6;
   SC_BOOL                    XRIOM2_DI1_7;
   SC_BOOL                    XRIOM2_DI1_8;
   SC_BOOL                    XRIOM2_DI1_9;
   SC_BOOL                    XRIOM2_DI1_10;
   SC_BOOL                    XRIOM2_DI1_11;
   SC_BOOL                    XRIOM2_DI1_12;
   SC_BOOL                    XRIOM2_DI1_13;
   SC_BOOL                    XRIOM2_DI1_14;
   SC_BOOL                    XRIOM2_DI1_15;
   SC_BOOL                    XRIOM2_DI1_16;
   SC_BOOL                    XRIOM2_DI1_17;
   SC_BOOL                    XRIOM2_DI1_18;
   SC_BOOL                    XRIOM2_DI1_19;
   SC_BOOL                    XRIOM2_DI1_20;
   SC_BOOL                    XRIOM2_DI1_21;
   SC_BOOL                    XRIOM2_DI1_22;
   SC_BOOL                    XRIOM2_DI1_23;
   SC_BOOL                    XRIOM2_DI1_24;
   SC_BOOL                    XRIOM2_DI1_25;
   SC_BOOL                    XRIOM2_DI1_26;
   SC_BOOL                    XRIOM2_DI1_27;
   SC_BOOL                    XRIOM2_DI1_28;
   SC_BOOL                    XRIOM2_DI1_29;
   SC_BOOL                    XRIOM2_DI1_30;
   SC_BOOL                    XRIOM2_DI1_31;
   SC_BOOL                    XRIOM2_DI2_0;
   SC_BOOL                    XRIOM2_DI2_1;
   SC_BOOL                    XRIOM2_DI2_2;
   SC_BOOL                    XRIOM2_DI2_3;
   SC_BOOL                    XRIOM2_DI2_4;
   SC_BOOL                    XRIOM2_DI2_5;
   SC_BOOL                    XRIOM2_DI2_6;
   SC_BOOL                    XRIOM2_DI2_7;
   SC_BOOL                    XRIOM2_DI2_8;
   SC_BOOL                    XRIOM2_DI2_9;
   SC_BOOL                    XRIOM2_DI2_10;
   SC_BOOL                    XRIOM2_DI2_11;
   SC_BOOL                    XRIOM2_DI2_12;
   SC_BOOL                    XRIOM2_DI2_13;
   SC_BOOL                    XRIOM2_DI2_14;
   SC_BOOL                    XRIOM2_DI2_15;
   SC_BOOL                    XRIOM2_DI2_16;
   SC_BOOL                    XRIOM2_DI2_17;
   SC_BOOL                    XRIOM2_DI2_18;
   SC_BOOL                    XRIOM2_DI2_19;
   SC_BOOL                    XRIOM2_DI2_20;
   SC_BOOL                    XRIOM2_DI2_21;
   SC_BOOL                    XRIOM2_DI2_22;
   SC_BOOL                    XRIOM2_DI2_23;
   SC_BOOL                    XRIOM2_DI2_24;
   SC_BOOL                    XRIOM2_DI2_25;
   SC_BOOL                    XRIOM2_DI2_26;
   SC_BOOL                    XRIOM2_DI2_27;
   SC_BOOL                    XRIOM2_DI2_28;
   SC_BOOL                    XRIOM2_DI2_29;
   SC_BOOL                    XRIOM2_DI2_30;
   SC_BOOL                    XRIOM2_DI2_31;
   SC_BOOL                    XRIOM2_DO1_0;
   SC_BOOL                    XRIOM2_DO1_1;
   SC_BOOL                    XRIOM2_DO1_2;
   SC_BOOL                    XRIOM2_DO1_3;
   SC_BOOL                    XRIOM2_DO1_4;
   SC_BOOL                    XRIOM2_DO1_5;
   SC_BOOL                    XRIOM2_DO1_6;
   SC_BOOL                    XRIOM2_DO1_7;
   SC_BOOL                    XRIOM2_DO1_8;
   SC_BOOL                    XRIOM2_DO1_9;
   SC_BOOL                    XRIOM2_DO1_10;
   SC_BOOL                    XRIOM2_DO1_11;
   SC_BOOL                    XRIOM2_DO1_12;
   SC_BOOL                    XRIOM2_DO1_13;
   SC_BOOL                    XRIOM2_DO1_14;
   SC_BOOL                    XRIOM2_DO1_15;
   SC_BOOL                    XRIOM2_DO1_16;
   SC_BOOL                    XRIOM2_DO1_17;
   SC_BOOL                    XRIOM2_DO1_18;
   SC_BOOL                    XRIOM2_DO1_19;
   SC_BOOL                    XRIOM2_DO1_20;
   SC_BOOL                    XRIOM2_DO1_21;
   SC_BOOL                    XRIOM2_DO1_22;
   SC_BOOL                    XRIOM2_DO1_23;
   SC_BOOL                    XRIOM2_DO1_24;
   SC_BOOL                    XRIOM2_DO1_25;
   SC_BOOL                    XRIOM2_DO1_26;
   SC_BOOL                    XRIOM2_DO1_27;
   SC_BOOL                    XRIOM2_DO1_28;
   SC_BOOL                    XRIOM2_DO1_29;
   SC_BOOL                    XRIOM2_DO1_30;
   SC_BOOL                    XRIOM2_DO1_31;
}MODBUSBOOL_R_T_XTYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_INT                     ILIMITULTRASONIK;
   SC_INT                     IFILLINGTIME;
   SC_INT                     ILEAKAGETIME;
   SC_INT                     ICALENDAR_YYYY;
   SC_INT                     ICALENDAR_MM;
   SC_INT                     ICALENDAR_DD;
   SC_INT                     ICALENDAR_HH;
   SC_INT                     ICALENDAR_MIN;
   SC_INT                     ICALENDAR_SS;
   SC_INT                     ITRAINSPEED;
   SC_INT                     ITRAINSPEEDLIMIT;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BYTE                    __align1[2];
   SC_REAL                    RBATTVOLTAGE;
   SC_INT                     ICMP1_HH;
   SC_INT                     ICMP2_HH;
}MODBUSINT_R_T_XTYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XCMPBAYPASS1;
   SC_BOOL                    XSETDT;
   SC_BOOL                    XTEMPUP;
   SC_BOOL                    XTEMPDOWN;
}MODBUSBOOL_RW_T_XTYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_UDINT                   UDALARMAUTOREFRESHTIMESEC;
   SC_UINT                    UIALARMINDEXONFIRSTOUTELEM;
   SC_INT                     IALARMFILTERSUBSYSTEM;
   SC_INT                     IALARMFILTERMINDDS;
   SC_INT                     IALARMFILTERMAXDDS;
   SC_UDINT                   UDALARMFILTERMINDDSTIME;
   SC_UDINT                   UDALARMFILTERMAXDDSTIME;
   SC_UINT                    UIALARMSORTMODE;
   SC_WORD                    WALARMCUSTOMATTR;
   SC_UINT                    UIALARMCUSTOMATTRFUNC;
   SC_UINT                    UIALARMDDSACKNOWLEDGED;
   SC_UINT                    UIALARMACKNOWLEDGEUSER;
   SC_INT                     IWHEELDIAMETER;
   SC_INT                     ISETDD;
   SC_INT                     ISETMM;
   SC_INT                     ISETYYYY;
   SC_INT                     ISETHH;
   SC_INT                     ISETMIN;
   SC_INT                     ISETSS;
   SC_INT                     IACK;
}MODBUSINT_RW_T_XTYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_XTYP;

typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_XTYP;

typedef struct
{
   SC_BOOL                    XFRONTR_WHT1_HMI;
   SC_BOOL                    XREARR_WHT2_HMI;
   SC_BOOL                    XFRONTR_RED1_HMI;
   SC_BOOL                    XREARR_RED2_HMI;
   SC_BOOL                    XFRONTR_SIDE1_HMI;
   SC_BOOL                    XREARR_SIDE2_HMI;
   SC_BOOL                    XFRONTR_GREEN_HMI;
   SC_BOOL                    XREARR_GREEN_HMI;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDE_RED_1;
   SC_BOOL                    XSIDE_RED_2;
   SC_BOOL                    XGREEN_1;
   SC_BOOL                    XGREEN_2;
}SIGNAL_LAMP_T_XTYP;

typedef struct
{
   SC_BOOL                    XHEADLP_STAT_1;
   SC_BOOL                    XHEADLP_STAT_2;
   SC_BOOL                    XFAULTLP_STAT_1;
   SC_BOOL                    XFAULTLP_STAT_2;
   SC_BOOL                    XHEADLP_NOTIF;
}HEAD_LAMP_T_XTYP;

typedef struct
{
   SC_UINT                    UIYYYY;
   SC_USINT                   USMM;
   SC_USINT                   USDD;
   SC_USINT                   USHH;
   SC_USINT                   USMIN;
   SC_USINT                   USSS;
   SC_INT                     IDAYOFWK;
   SC_DT                      DTDATETIME;
   XSTRING__TYPE20            SDATEANDTIME;
}CALENDAR_T_XTYP;

typedef struct
{
   SC_INT                     XFILLINGTIME;
   SC_INT                     XLEAKAGECOMMAND;
}COMPRESOR_TEST_T_XTYP;

typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_XTYP;

typedef struct
{
   SC_BOOL                    XTRAINWASH_ON;
}TRAIN_WASH_MODE_T_XTYP;

typedef struct
{
   SC_BOOL                    XAC_ONCMD;
   SC_INT                     IAC_SETTEMP;
   SC_INT                     IAC_TEMP_1;
   SC_INT                     IAC_TEMP_2;
   SC_INT                     IAC_TEMP_3;
   SC_INT                     IAC_TEMP_4;
   SC_BOOL                    XAC_HALF_1;
   SC_BOOL                    XAC_HALF_2;
   SC_BOOL                    XAC_HALF_3;
   SC_BOOL                    XAC_HALF_4;
   SC_BOOL                    XAC_FULL_1;
   SC_BOOL                    XAC_FULL_2;
   SC_BOOL                    XAC_FULL_3;
   SC_BOOL                    XAC_FULL_4;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP1FAULT_2;
   SC_BOOL                    XEVAP1FAULT_3;
   SC_BOOL                    XEVAP1FAULT_4;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XEVAP2FAULT_2;
   SC_BOOL                    XEVAP2FAULT_3;
   SC_BOOL                    XEVAP2FAULT_4;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD1FAULT_2;
   SC_BOOL                    XCD1FAULT_3;
   SC_BOOL                    XCD1FAULT_4;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD2FAULT_2;
   SC_BOOL                    XCD2FAULT_3;
   SC_BOOL                    XCD2FAULT_4;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD3FAULT_2;
   SC_BOOL                    XCD3FAULT_3;
   SC_BOOL                    XCD3FAULT_4;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCD4FAULT_2;
   SC_BOOL                    XCD4FAULT_3;
   SC_BOOL                    XCD4FAULT_4;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP1FAULT_2;
   SC_BOOL                    XCP1FAULT_3;
   SC_BOOL                    XCP1FAULT_4;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP2FAULT_2;
   SC_BOOL                    XCP2FAULT_3;
   SC_BOOL                    XCP2FAULT_4;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP3FAULT_2;
   SC_BOOL                    XCP3FAULT_3;
   SC_BOOL                    XCP3FAULT_4;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XCP4FAULT_2;
   SC_BOOL                    XCP4FAULT_3;
   SC_BOOL                    XCP4FAULT_4;
   SC_BOOL                    XEVAP1OVERTEMP_1;
   SC_BOOL                    XEVAP1OVERTEMP_2;
   SC_BOOL                    XEVAP1OVERTEMP_3;
   SC_BOOL                    XEVAP1OVERTEMP_4;
   SC_BOOL                    XEVAP2OVERTEMP_1;
   SC_BOOL                    XEVAP2OVERTEMP_2;
   SC_BOOL                    XEVAP2OVERTEMP_3;
   SC_BOOL                    XEVAP2OVERTEMP_4;
   SC_BOOL                    XCD1OVERTEMP_1;
   SC_BOOL                    XCD1OVERTEMP_2;
   SC_BOOL                    XCD1OVERTEMP_3;
   SC_BOOL                    XCD1OVERTEMP_4;
   SC_BOOL                    XCD2OVERTEMP_1;
   SC_BOOL                    XCD2OVERTEMP_2;
   SC_BOOL                    XCD2OVERTEMP_3;
   SC_BOOL                    XCD2OVERTEMP_4;
   SC_BOOL                    XCD3OVERTEMP_1;
   SC_BOOL                    XCD3OVERTEMP_2;
   SC_BOOL                    XCD3OVERTEMP_3;
   SC_BOOL                    XCD3OVERTEMP_4;
   SC_BOOL                    XCD4OVERTEMP_1;
   SC_BOOL                    XCD4OVERTEMP_2;
   SC_BOOL                    XCD4OVERTEMP_3;
   SC_BOOL                    XCD4OVERTEMP_4;
   SC_BOOL                    XCP1OVERTEMP_1;
   SC_BOOL                    XCP1OVERTEMP_2;
   SC_BOOL                    XCP1OVERTEMP_3;
   SC_BOOL                    XCP1OVERTEMP_4;
   SC_BOOL                    XCP2OVERTEMP_1;
   SC_BOOL                    XCP2OVERTEMP_2;
   SC_BOOL                    XCP2OVERTEMP_3;
   SC_BOOL                    XCP2OVERTEMP_4;
   SC_BOOL                    XCP3OVERTEMP_1;
   SC_BOOL                    XCP3OVERTEMP_2;
   SC_BOOL                    XCP3OVERTEMP_3;
   SC_BOOL                    XCP3OVERTEMP_4;
   SC_BOOL                    XCP4OVERTEMP_1;
   SC_BOOL                    XCP4OVERTEMP_2;
   SC_BOOL                    XCP4OVERTEMP_3;
   SC_BOOL                    XCP4OVERTEMP_4;
   SC_BOOL                    XENGIDLEMODECMD;
   SC_BOOL                    XTECU1FAULT;
   SC_BOOL                    XTECU2FAULT;
   SC_UINT                    UIDCLINKV;
   SC_UINT                    UIDCLINKA;
   SC_UINT                    UIENG1RPM;
   SC_UINT                    UIENG2RPM;
   SC_BOOL                    XENG1RUNNING;
   SC_BOOL                    XENG2RUNNING;
   SC_BOOL                    XENG1FAULT;
   SC_BOOL                    XENG2FAULT;
}R1_T_XTYP;

typedef struct
{
   SC_BOOL                    XENGINERUNNING;
   SC_BOOL                    XENGINESTOP;
   SC_BOOL                    XENGINEEMERGENCYSTOP;
}ENGINE_STATUS_T_XTYP;

typedef struct
{
   SC_BOOL                    XTEMPSENSORERROR_1;
   SC_BOOL                    XAC_COMMAND_ON;
   SC_BOOL                    XAC1;
   SC_BOOL                    XAC2;
   SC_INT                     ISETTEMP_HUNDREDS;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XEVAP1_ON;
   SC_BOOL                    XEVAP2_ON;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XACSTARTUP2;
   SC_BOOL                    XCD_AC1_ON;
   SC_BOOL                    XCD_AC2_ON;
   SC_BOOL                    XCP_AC1_ON;
   SC_BOOL                    XCP_AC2_ON;
   SC_INT                     ITEMP_1;
   SC_BOOL                    XHALFMODE_1;
   SC_BOOL                    XFULLMODE_1;
   SC_BOOL                    XACERROR_1;
   SC_DINT                    DICP1TIME_1;
   SC_DINT                    DICP2TIME_1;
}AC_T_XTYP;



/***** FB SECTION: *****/
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1K;
   SC_BOOL                    XEVAP2K;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XTEMPPROTECTIONCDA;
   SC_BOOL                    XTEMPPROTECTIONCDB;
   SC_TIME                    TCDDELAY;
   SC_BOOL                    XCDAB_ON;
   SC__INSTC                  TON_2;
}FB_AC_CD_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XHP_CMPB;
   SC_BOOL                    XLP_CMPB;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP1_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP2_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1NFB;
   SC_BOOL                    XEVAP2NFB;
   SC_BOOL                    X3PHCOUNTER;
   SC_BOOL                    XTEMPPROTECTIONEV1;
   SC_BOOL                    XTEMPPROTECTIONEV2;
   SC_BOOL                    XACON;
   SC_BOOL                    XEVAP1ON;
   SC_BOOL                    XEVAP2ON;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  TOF_1;
   SC__INSTC                  TOF_2;
}FB_AC_EVAP_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_1_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_2_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLE1;
   SC_BOOL                    XENABLE2;
   SC_DINT*                   DITIME1;
   SC_DINT*                   DITIME2;
   SC__INSTC                  RS_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    X1MIN1;
   SC_BOOL                    X1MIN2;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_TIME                    TET;
   SC_TIME                    TET2;
}FB_TIMECOUNT_DINT_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLEHALF;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_DINT                    DIAC1TIME;
   SC_DINT                    DIAC2TIME;
   SC_BOOL                    XHALF1;
   SC_BOOL                    XHALF2;
   SC__INSTC                  R_TRIG_1;
   SC_BOOL                    XERROR;
   SC__INSTC                  SR_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    XRESET;
}FB_AC_HALFSEL_REVA_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP1;
   SC_WORD                    WTEMP2;
   SC_INT                     ITEMP;
   SC_BOOL                    XTEMPSENSORERROR1;
   SC_BOOL                    XTEMPSENSORERROR2;
   SC_BOOL                    XTEMPSENSORERROR;
   SC_INT                     I;
   SC_INT                     ISUM1;
   SC_INT                     ITEMP1;
   SC_INT                     ISUM2;
   SC_INT                     ITEMP1SAVED;
   SC_INT                     ITEMP2SAVED;
   SC_INT                     ITEMP2;
   SC__INSTC                  FB_FILTER_1;
   SC_REAL                    RTEMP1;
   SC_REAL                    RTEMP2;
   SC__INSTC                  FB_FILTER_2;
   SC__INSTC                  TON_1;
   SC_INT                     ITEMPORARY1;
   SC_INT                     ITEMPORARY2;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_BOOL                    XTAKE2;
   SC__INSTC                  FB_GETTEMP_1;
   SC_INT                     ITEMP1_TRY;
   SC__INSTC                  FB_GETTEMP_2;
   SC_INT                     ITEMP2_TRY;
   SC_BOOL                    XTAKE1;
}FB_AC_GETTEMP_REVA_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP;
   SC_REAL                    RFILTERCONSTANT;
   SC_INT                     ITEMP;
   SC__INSTC                  FB_FILTER_1;
}FB_GETTEMP_XTYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WSENSOR;
   SC_REAL                    RA_FILTER;
   SC_INT                     IUPPER_HYSTERESYS;
   SC_INT                     ILOWER_HYSTERESYS1;
   SC_INT                     IPRESSURE;
   SC_BOOL                    XLOWPRESSURE;
   SC_REAL                    RANALOG;
   SC_REAL                    RFILTERED;
   SC_REAL                    RPRESSURE;
   SC_REAL                    RPRESSURE_REGRESSION;
   SC__INSTC                  FB_FILTER_1;
}FB_PRESSURE_CHECK_XTYP;

#endif /*_CSTRUCTS_H_*/