#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  CUTOFF_TIMER1;
   SC__INSTC                  CUTOFF_TIMER2;
   SC__INSTC                  CUTOFF_TIMER3;
   SC__INSTC                  CUTOFF_TIMER4;
   SC__INSTC                  EMG_TIMER1;
   SC__INSTC                  EMG_TIMER2;
   SC__INSTC                  EMG_TIMER3;
   SC__INSTC                  EMG_TIMER4;
   SC__INSTC                  PRESSED_TIMER;
   SC__INSTC                  RELEASED_TIMER;
   SC__INSTC                  SR_BUZZER1;
   SC__INSTC                  SR_BUZZER2;
   SC__INSTC                  SR_BUZZER3;
   SC__INSTC                  SR_BUZZER4;
   SC_TIME                    TPRESS;
   SC_TIME                    TRELEASE;
   SC_BOOL                    XDEADCUTOFF1;
   SC_BOOL                    XDEADCUTOFF2;
   SC_BOOL                    XDEADBUZZER1;
   SC_BOOL                    XDEADBUZZER2;
   SC_BOOL                    XEMGDEADMAN1;
   SC_BOOL                    XEMGDEADMAN2;
   SC_BOOL                    XPRESSED;
   SC_BOOL                    XRELEASED;
}P_DEADMAN_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_DEADMAN_00121(P_DEADMAN_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_DEADMAN_TYP, XRELEASED) + sizeof(ip->XRELEASED);
   /* non retains: */
   ip->TPRESS = SC_C_TIME(0);
   ip->TRELEASE = SC_C_TIME(0);
   ip->XDEADCUTOFF1 = SC_C_BOOL(0);
   ip->XDEADCUTOFF2 = SC_C_BOOL(0);
   ip->XDEADBUZZER1 = SC_C_BOOL(0);
   ip->XDEADBUZZER2 = SC_C_BOOL(0);
   ip->XEMGDEADMAN1 = SC_C_BOOL(0);
   ip->XEMGDEADMAN2 = SC_C_BOOL(0);
   ip->XPRESSED = SC_C_BOOL(0);
   ip->XRELEASED = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_DEADMAN_00049(void)
{
   P_DEADMAN_TYP *ip = (P_DEADMAN_TYP*) __OD(122);

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   PRG_ENTRY(49, ip);
/******************* Network *************************/
{
   _SCT_B0 = ((1 & (*(CABIN_T_TYP*)__OD(11)).XCABERROR) | (1 & (*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE));
   if((1 & _SCT_B0))goto SCL__RESET;

}
/******************* Network *************************/
{
   ip->TPRESS = REAL_TO_TIME(((SC_C_REAL(90.89) - (INT_TO_REAL((*(SPEED_T_TYP*)__OD(15)).ISPEED) 
   * SC_C_REAL(0.89))) * SC_C_REAL(1000.0)));

}
/******************* Network *************************/
{
   ip->TRELEASE = SEL_TIME(((*(SPEED_T_TYP*)__OD(15)).ISPEED > SC_C_INT(40)),SC_C_TIME(6000),
   SC_C_TIME(5000));

}
/******************* Network *************************/
{
   _SCT_B0 = (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE;
   if((1 & _SCT_B0))goto SCL__CABIN2;

}
/******************* Network *************************/
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_BUZZER1);
   MASTERCTRL_T_TYP* __lpt1 = (MASTERCTRL_T_TYP*)__OD(12);
   TON_TYP* __lpt2 = (TON_TYP*)__OD(ip->EMG_TIMER1);
   TON_TYP* __lpt3 = (TON_TYP*)__OD(ip->CUTOFF_TIMER1);
   TON_TYP* __lpt4 = (TON_TYP*)__OD(ip->PRESSED_TIMER);
   _SCT_B0 = (((1 & LD_I_X(3,1,1,17)) &  (!((1 & (*__lpt1).XFASTBRAKE)))) & ((1 & (*__lpt1).XFWD) 
   | (1 & (*__lpt1).XREV)));
   ip->XPRESSED = _SCT_B0;
   (*__lpt4).IN = _SCT_B0;
   (*__lpt4).PT = ip->TPRESS;
   TON((__lpt4));
   (*__lpt0).SET1 = (*__lpt4).Q;
   (*__lpt0).RESET = SEL_BOOL(ip->XDEADCUTOFF1,ip->XRELEASED,((1 & (*__lpt1).XFASTBRAKE) & 
   (1 & (*(SPEED_T_TYP*)__OD(15)).XZEROSPEED)));
   SR((__lpt0));
   ip->XDEADBUZZER1 = (*__lpt0).Q1;
   (*__lpt3).IN = (*__lpt0).Q1;
   (*__lpt3).PT = SC_C_TIME(3000);
   TON((__lpt3));
   ip->XDEADCUTOFF1 = (*__lpt3).Q;
   (*__lpt2).IN = (*__lpt0).Q1;
   (*__lpt2).PT = SC_C_TIME(5000);
   TON((__lpt2));
   ip->XEMGDEADMAN1 = (*__lpt2).Q;

}
/******************* Network *************************/
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_BUZZER2);
   MASTERCTRL_T_TYP* __lpt1 = (MASTERCTRL_T_TYP*)__OD(12);
   TON_TYP* __lpt2 = (TON_TYP*)__OD(ip->EMG_TIMER2);
   TON_TYP* __lpt3 = (TON_TYP*)__OD(ip->CUTOFF_TIMER2);
   TON_TYP* __lpt4 = (TON_TYP*)__OD(ip->RELEASED_TIMER);
   _SCT_B0 = (( (!((1 & LD_I_X(3,1,1,17)))) &  (!((1 & (*__lpt1).XFASTBRAKE)))) & ((1 & (*__lpt1).XFWD) 
   | (1 & (*__lpt1).XREV)));
   ip->XRELEASED = _SCT_B0;
   (*__lpt4).IN = _SCT_B0;
   (*__lpt4).PT = ip->TRELEASE;
   TON((__lpt4));
   (*__lpt0).SET1 = (*__lpt4).Q;
   (*__lpt0).RESET = SEL_BOOL(ip->XDEADCUTOFF2,ip->XPRESSED,((1 & (*__lpt1).XFASTBRAKE) & 
   (1 & (*(SPEED_T_TYP*)__OD(15)).XZEROSPEED)));
   SR((__lpt0));
   ip->XDEADBUZZER2 = (*__lpt0).Q1;
   (*__lpt3).IN = (*__lpt0).Q1;
   (*__lpt3).PT = SC_C_TIME(3000);
   TON((__lpt3));
   ip->XDEADCUTOFF2 = (*__lpt3).Q;
   (*__lpt2).IN = (*__lpt0).Q1;
   (*__lpt2).PT = SC_C_TIME(5000);
   TON((__lpt2));
   ip->XEMGDEADMAN2 = (*__lpt2).Q;

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0))goto SCL__UNITED;

}
/******************* Network *************************/
SCL__CABIN2:;
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_BUZZER3);
   MASTERCTRL_T_TYP* __lpt1 = (MASTERCTRL_T_TYP*)__OD(12);
   TON_TYP* __lpt2 = (TON_TYP*)__OD(ip->EMG_TIMER3);
   TON_TYP* __lpt3 = (TON_TYP*)__OD(ip->CUTOFF_TIMER3);
   TON_TYP* __lpt4 = (TON_TYP*)__OD(ip->PRESSED_TIMER);
   _SCT_B0 = (((1 & LD_I_X(3,2,2,17)) &  (!((1 & (*__lpt1).XFASTBRAKE)))) & ((1 & (*__lpt1).XFWD) 
   | (1 & (*__lpt1).XREV)));
   ip->XPRESSED = _SCT_B0;
   (*__lpt4).IN = _SCT_B0;
   (*__lpt4).PT = ip->TPRESS;
   TON((__lpt4));
   (*__lpt0).SET1 = (*__lpt4).Q;
   (*__lpt0).RESET = SEL_BOOL(ip->XDEADCUTOFF1,ip->XRELEASED,((1 & (*__lpt1).XFASTBRAKE) & 
   (1 & (*(SPEED_T_TYP*)__OD(15)).XZEROSPEED)));
   SR((__lpt0));
   ip->XDEADBUZZER1 = (*__lpt0).Q1;
   (*__lpt3).IN = (*__lpt0).Q1;
   (*__lpt3).PT = SC_C_TIME(3000);
   TON((__lpt3));
   ip->XDEADCUTOFF1 = (*__lpt3).Q;
   (*__lpt2).IN = (*__lpt0).Q1;
   (*__lpt2).PT = SC_C_TIME(5000);
   TON((__lpt2));
   ip->XEMGDEADMAN1 = (*__lpt2).Q;

}
/******************* Network *************************/
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_BUZZER4);
   MASTERCTRL_T_TYP* __lpt1 = (MASTERCTRL_T_TYP*)__OD(12);
   TON_TYP* __lpt2 = (TON_TYP*)__OD(ip->EMG_TIMER4);
   TON_TYP* __lpt3 = (TON_TYP*)__OD(ip->CUTOFF_TIMER4);
   TON_TYP* __lpt4 = (TON_TYP*)__OD(ip->RELEASED_TIMER);
   _SCT_B0 = (( (!((1 & LD_I_X(3,2,2,17)))) &  (!((1 & (*__lpt1).XFASTBRAKE)))) & ((1 & (*__lpt1).XFWD) 
   | (1 & (*__lpt1).XREV)));
   ip->XRELEASED = _SCT_B0;
   (*__lpt4).IN = _SCT_B0;
   (*__lpt4).PT = ip->TRELEASE;
   TON((__lpt4));
   (*__lpt0).SET1 = (*__lpt4).Q;
   (*__lpt0).RESET = SEL_BOOL(ip->XDEADCUTOFF2,ip->XPRESSED,((1 & (*__lpt1).XFASTBRAKE) & 
   (1 & (*(SPEED_T_TYP*)__OD(15)).XZEROSPEED)));
   SR((__lpt0));
   ip->XDEADBUZZER2 = (*__lpt0).Q1;
   (*__lpt3).IN = (*__lpt0).Q1;
   (*__lpt3).PT = SC_C_TIME(3000);
   TON((__lpt3));
   ip->XDEADCUTOFF2 = (*__lpt3).Q;
   (*__lpt2).IN = (*__lpt0).Q1;
   (*__lpt2).PT = SC_C_TIME(5000);
   TON((__lpt2));
   ip->XEMGDEADMAN2 = (*__lpt2).Q;

}
/******************* Network *************************/
SCL__UNITED:;
{
   (*(DEADMAN_T_TYP*)__OD(14)).XDEADBUZZER = ((1 & ip->XDEADBUZZER1) | (1 & ip->XDEADBUZZER2));

}
/******************* Network *************************/
{
   (*(DEADMAN_T_TYP*)__OD(14)).XDEADTRCUTOFF = ((1 & ip->XDEADCUTOFF1) | (1 & ip->XDEADCUTOFF2));

}
/******************* Network *************************/
{
   (*(DEADMAN_T_TYP*)__OD(14)).XDEADEB = ((1 & ip->XEMGDEADMAN1) | (1 & ip->XEMGDEADMAN2));

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      PRG_EXIT(49, ip);
      return;
   }

}
/******************* Network *************************/
SCL__RESET:;
{
   DEADMAN_T_TYP* __lpt0 = (DEADMAN_T_TYP*)__OD(14);
   (*__lpt0).XDEADBUZZER = SC_C_BOOL(0);
   (*__lpt0).XDEADTRCUTOFF = SC_C_BOOL(0);
   (*__lpt0).XDEADEB = SC_C_BOOL(0);

}

   PRG_EXIT(49, ip);
}
