#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"

void SC_INIT__TON_00230(TON_TYP* ip, int iRetain);
void SC_INIT__TOF_00231(TOF_TYP* ip, int iRetain);
void SC_INIT__RS_00232(RS_TYP* ip, int iRetain);
void SC_INIT__R_TRIG_00233(R_TRIG_TYP* ip, int iRetain);
void SC_INIT__SR_00234(SR_TYP* ip, int iRetain);
void SC_INIT__CTU_00235(CTU_TYP* ip, int iRetain);
void SC_INIT__F_TRIG_00236(F_TRIG_TYP* ip, int iRetain);
void SC_INIT__CNV_DT_TO_STRING_00237(CNV_DT_TO_STRING_TYP* ip, int iRetain);
void SC_INIT__CNV_STRING_TO_DINT_00238(CNV_STRING_TO_DINT_TYP* ip, int iRetain);
void SC_INIT__TIME_GET_RTC_00239(TIME_GET_RTC_TYP* ip, int iRetain);

void SC_INIT__TON_00230(TON_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->IN = SC_C_BOOL(0);
   ip->PT = SC_C_TIME(0);
   ip->Q = SC_C_BOOL(0);
   ip->ET = SC_C_TIME(0);
   ip->IN_M = SC_C_BOOL(0);
   ip->START = SC_C_TIME(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__TOF_00231(TOF_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->IN = SC_C_BOOL(0);
   ip->PT = SC_C_TIME(0);
   ip->Q = SC_C_BOOL(0);
   ip->ET = SC_C_TIME(0);
   ip->IN_M = SC_C_BOOL(0);
   ip->START = SC_C_TIME(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__RS_00232(RS_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->SET = SC_C_BOOL(0);
   ip->RESET1 = SC_C_BOOL(0);
   ip->Q1 = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__R_TRIG_00233(R_TRIG_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->CLK = SC_C_BOOL(0);
   ip->Q = SC_C_BOOL(0);
   ip->M = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__SR_00234(SR_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->SET1 = SC_C_BOOL(0);
   ip->RESET = SC_C_BOOL(0);
   ip->Q1 = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__CTU_00235(CTU_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->CU = SC_C_BOOL(0);
   ip->RESET = SC_C_BOOL(0);
   ip->PV = SC_C_INT(0);
   ip->Q = SC_C_BOOL(0);
   ip->CV = SC_C_INT(0);
   ip->CU_M = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__F_TRIG_00236(F_TRIG_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->CLK = SC_C_BOOL(0);
   ip->Q = SC_C_BOOL(0);
   ip->M = SC_C_BOOL(1);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__CNV_DT_TO_STRING_00237(CNV_DT_TO_STRING_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->DTACTUALDT = SC_C_DT(694224000);
   ip->USMODE_DT_D_T = SC_C_USINT(0);
   ip->XYEARIN4DIG = SC_C_BOOL(0);
   ip->XHRSMINSEC = SC_C_BOOL(0);
   ip->X12HRS = SC_C_BOOL(0);
   INIT_STRING((SC_STRING*)&ip->SDATESEPCHAR,1,1,(SC_STRING_CHAR*)".");
   INIT_STRING((SC_STRING*)&ip->STIMESEPCHAR,1,1,(SC_STRING_CHAR*)":");
   ip->USCOUNTRY = SC_C_USINT(0);
   INIT_STRING((SC_STRING*)&ip->SSTRING,20,0,(SC_STRING_CHAR*)"");
   ip->IERROR = SC_C_INT(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__CNV_STRING_TO_DINT_00238(CNV_STRING_TO_DINT_TYP* ip, int iRetain)
{
   /* non retains: */
   INIT_STRING((SC_STRING*)&ip->SSTRING,20,0,(SC_STRING_CHAR*)"");
   ip->DIDINT = SC_C_DINT(0);
   ip->IERROR = SC_C_INT(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}

void SC_INIT__TIME_GET_RTC_00239(TIME_GET_RTC_TYP* ip, int iRetain)
{
   /* non retains: */
   ip->DTACTUALDT = SC_C_DT(694224000);
   ip->UIMILLISEC = SC_C_UINT(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}



/* ******* GLOBAL INIT ARRAY FOR FUNCTION CALLS ******* */
const SC__INSTC __IFArray[] =
{
66,67,
91,137,
230,138,
91,139,
230,140,
89,141,
230,142,
230,143,
87,144,
230,145,
230,146,
85,147,
231,148,
231,149,
83,150,
81,151,
79,152,
232,153,
230,154,
232,155,
230,156,
77,157,
233,158,
234,159,
230,160,
77,161,
233,162,
234,163,
230,164,
73,165,
75,166,
75,167,
230,168,
232,169,
232,170,
230,171,
74,172,
75,173,
74,174,
75,175,
92,93,
232,176,
232,177,
230,178,
233,179,
233,180,
233,181,
95,96,
98,182,
75,183,
79,184,
232,185,
230,186,
232,187,
230,188,
232,189,
99,100,
101,102,
103,104,
234,190,
105,106,
234,191,
232,192,
232,193,
230,194,
230,195,
235,196,
233,197,
236,198,
107,108,
109,110,
237,199,
238,200,
238,201,
238,202,
238,203,
238,204,
238,205,
233,206,
239,207,
237,208,
237,209,
111,112,
113,114,
115,116,
117,118,
119,120,
121,122,
230,210,
230,211,
230,212,
230,213,
230,214,
230,215,
230,216,
230,217,
230,218,
230,219,
234,220,
234,221,
234,222,
234,223,
123,124,
125,126,
234,224,
127,128,
129,130,
230,225,
230,226,
234,227,
75,228,
131,132,
232,229,
133,134,
135,136,
5,4,
-1,-1
};
