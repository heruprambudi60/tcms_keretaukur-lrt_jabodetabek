#include "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include "hw_conf.h"

NODE_TYPE	Link3[3]	 = 
{
	{
	0,	/* Min_Update_Time */
	0,	/* Max_Update_Time */
	0,	/* Startup_Time */
	0,	/* Min_Status_Time */
	0,	/* Min_Interrupt_Time */
	3,	/* Link */
	0,	/* Node */
	(SC_BYTE *)&O_L3_K0,
	(SC_BYTE *)&I_L3_K0,
	{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	{
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	0,	/* Nom_Node_Flag */
	0,	/* Act_Node_Flag */
	0,	/* Node_Version */
	{0},	/* Act_Modul_Type */
	{0},	/* Status_Reg */
	{0},	/* Error Reg0 */
	{0},	/* Error Reg1 */
	{0}	/* Error Reg2 */

	},
	{
	0,	/* Min_Update_Time */
	0,	/* Max_Update_Time */
	0,	/* Startup_Time */
	0,	/* Min_Status_Time */
	0,	/* Min_Interrupt_Time */
	3,	/* Link */
	1,	/* Node */
	(SC_BYTE *)&O_L3_K1,
	(SC_BYTE *)&I_L3_K1,
	{
	(SC_BYTE *)&SC_RIOM1_DDE,
	(SC_BYTE *)&SC_RIOM1_DI1,
	(SC_BYTE *)&SC_RIOM1_DI2,
	(SC_BYTE *)&SC_RIOM1_DO1,
	(SC_BYTE *)&SC_RIOM1_AIT_PTC,
	(SC_BYTE *)&SC_RIOM1_AAT
	,0,0,0,0,0,0,0,0,0,0
	},
	{
	DDE731,
	DIT732,
	DIT732,
	DOT732,
	AIT732,
	AAT731
	,0,0,0,0,0,0,0,0,0,0
	},
	{
	0,
	0,
	0+DIT732_I_SIZE,
	0+DIT732_I_SIZE+DIT732_I_SIZE,
	0+DIT732_I_SIZE+DIT732_I_SIZE,
	0+DIT732_I_SIZE+DIT732_I_SIZE+AIT732_I_SIZE,
	0,0,0,0,0,0,0,0,0,0
	},
	{
	0,
	0,
	0,
	0,
	0+DOT732_O_SIZE,
	0+DOT732_O_SIZE,
	0,0,0,0,0,0,0,0,0,0
	},
	0,	/* Nom_Node_Flag */
	0,	/* Act_Node_Flag */
	0,	/* Node_Version */
	{0},	/* Act_Modul_Type */
	{0},	/* Status_Reg */
	{0},	/* Error Reg0 */
	{0},	/* Error Reg1 */
	{0}	/* Error Reg2 */

	},
	{
	0,	/* Min_Update_Time */
	0,	/* Max_Update_Time */
	0,	/* Startup_Time */
	0,	/* Min_Status_Time */
	0,	/* Min_Interrupt_Time */
	3,	/* Link */
	2,	/* Node */
	(SC_BYTE *)&O_L3_K2,
	(SC_BYTE *)&I_L3_K2,
	{
	(SC_BYTE *)&SC_RIOM2_DDE,
	(SC_BYTE *)&SC_RIOM2_DIT733,
	(SC_BYTE *)&SC_RIOM2_DI1,
	(SC_BYTE *)&SC_RIOM2_DI2,
	(SC_BYTE *)&SC_RIOM2_DO1,
	(SC_BYTE *)&SC_RIOM2_AIT_PTC,
	(SC_BYTE *)&SC_RIOM2_AAT
	,0,0,0,0,0,0,0,0,0
	},
	{
	DDE731,
	DIT733,
	DIT732,
	DIT732,
	DOT732,
	AIT732,
	AAT731
	,0,0,0,0,0,0,0,0,0
	},
	{
	0,
	0,
	0+DIT733_I_SIZE,
	0+DIT733_I_SIZE+DIT732_I_SIZE,
	0+DIT733_I_SIZE+DIT732_I_SIZE+DIT732_I_SIZE,
	0+DIT733_I_SIZE+DIT732_I_SIZE+DIT732_I_SIZE,
	0+DIT733_I_SIZE+DIT732_I_SIZE+DIT732_I_SIZE+AIT732_I_SIZE,
	0,0,0,0,0,0,0,0,0
	},
	{
	0,
	0,
	0+DIT733_O_SIZE,
	0+DIT733_O_SIZE,
	0+DIT733_O_SIZE,
	0+DIT733_O_SIZE+DOT732_O_SIZE,
	0+DIT733_O_SIZE+DOT732_O_SIZE,
	0,0,0,0,0,0,0,0,0
	},
	0,	/* Nom_Node_Flag */
	0,	/* Act_Node_Flag */
	0,	/* Node_Version */
	{0},	/* Act_Modul_Type */
	{0},	/* Status_Reg */
	{0},	/* Error Reg0 */
	{0},	/* Error Reg1 */
	{0}	/* Error Reg2 */

	}
};

