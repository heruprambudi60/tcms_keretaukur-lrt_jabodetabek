#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_TYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_TYP;

typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_TYP;

typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_TRACTION_M_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_TRACTION_M_00113(P_TRACTION_M_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_TRACTION_M_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_TRACTION_M_00045(void)
{
   P_TRACTION_M_TYP *ip = (P_TRACTION_M_TYP*) __OD(114);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(45, ip);
   (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTTOUT = ((1 & (*(DEADMAN_T_TYP*)__OD(14)).XDEADTRCUTOFF) 
   | (1 & (*(BATT_T_TYP*)__OD(17)).XBATTLOW));
   (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRREADY = (((((1 & (*(DOORS_T_TYP*)__OD(16)).XDALLCLOSED) 
   & (1 & (*(COMPRESSOR_T_TYP*)__OD(25)).XMRPSOK)) & (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XVTDCOK)) 
   & (1 & (*(ULTRASON_MODE_T_TYP*)__OD(30)).XTRULTRSN_RDY)) &  (!((((1 & (*(BRAKE_T_TYP*)__OD(19)).XPARKINGBRK) 
   & (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTTOUT)) & (1 & (*(BRAKE_T_TYP*)__OD(19)).XBRAKEPIPEPRES_SW)))));
   (*(TRACTION_MNGMNT_TYP*)__OD(24)).XVTDCOK =  (!((1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTTOUT)));
   (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTOFF_DO = ((((1 & (*(DOORS_T_TYP*)__OD(16)).XDALLCLOSED) 
   | (1 & (*(SPEED_T_TYP*)__OD(15)).XTRCUTTOFF)) | (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTTOUT)) 
   | (1 & (*(ULTRASON_MODE_T_TYP*)__OD(30)).XULTRSN_ON));

   PRG_EXIT(45, ip);
}
