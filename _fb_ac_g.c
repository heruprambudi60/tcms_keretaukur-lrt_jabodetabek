#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;
typedef void (*__BPFN_FB_FILTER)(FB_FILTER_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP;
   SC_REAL                    RFILTERCONSTANT;
   SC_INT                     ITEMP;
   SC__INSTC                  FB_FILTER_1;
}FB_GETTEMP_TYP;
typedef void (*__BPFN_FB_GETTEMP)(FB_GETTEMP_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP1;
   SC_WORD                    WTEMP2;
   SC_INT                     ITEMP;
   SC_BOOL                    XTEMPSENSORERROR1;
   SC_BOOL                    XTEMPSENSORERROR2;
   SC_BOOL                    XTEMPSENSORERROR;
   SC_INT                     I;
   SC_INT                     ISUM1;
   SC_INT                     ITEMP1;
   SC_INT                     ISUM2;
   SC_INT                     ITEMP1SAVED;
   SC_INT                     ITEMP2SAVED;
   SC_INT                     ITEMP2;
   SC__INSTC                  FB_FILTER_1;
   SC_REAL                    RTEMP1;
   SC_REAL                    RTEMP2;
   SC__INSTC                  FB_FILTER_2;
   SC__INSTC                  TON_1;
   SC_INT                     ITEMPORARY1;
   SC_INT                     ITEMPORARY2;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_BOOL                    XTAKE2;
   SC__INSTC                  FB_GETTEMP_1;
   SC_INT                     ITEMP1_TRY;
   SC__INSTC                  FB_GETTEMP_2;
   SC_INT                     ITEMP2_TRY;
   SC_BOOL                    XTAKE1;
}FB_AC_GETTEMP_REVA_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_AC_GETTEMP_REVA_00070(FB_AC_GETTEMP_REVA_TYP* ip1, FB_AC_GETTEMP_REVA_TYP* 
ip2)
{
   MOVE_ANY(&(ip1->WTEMP1), &(ip2->WTEMP1), (long) (&(ip1->FB_FILTER_1)) - (long) (&(ip1->WTEMP1)));
   ((__PFN_COPY)__OF(71))(__OD(ip1->FB_FILTER_1), __OD(ip2->FB_FILTER_1));
   MOVE_ANY(&(ip1->RTEMP1), &(ip2->RTEMP1), (long) (&(ip1->FB_FILTER_2)) - (long) (&(ip1->RTEMP1)));
   ((__PFN_COPY)__OF(71))(__OD(ip1->FB_FILTER_2), __OD(ip2->FB_FILTER_2));
   MOVE_ANY(__OD(ip1->TON_1), __OD(ip2->TON_1), sizeof(TON_TYP));
   MOVE_ANY(&(ip1->ITEMPORARY1), &(ip2->ITEMPORARY1), (long) (&(ip1->RS_1)) - (long) (&(ip1->ITEMPORARY1)));
   MOVE_ANY(__OD(ip1->RS_1), __OD(ip2->RS_1), sizeof(RS_TYP));
   MOVE_ANY(__OD(ip1->RS_2), __OD(ip2->RS_2), sizeof(RS_TYP));
   MOVE_ANY(__OD(ip1->TON_2), __OD(ip2->TON_2), sizeof(TON_TYP));
   MOVE_ANY(&(ip1->XTAKE2), &(ip2->XTAKE2), (long) (&(ip1->FB_GETTEMP_1)) - (long) (&(ip1->XTAKE2)));
   ((__PFN_COPY)__OF(72))(__OD(ip1->FB_GETTEMP_1), __OD(ip2->FB_GETTEMP_1));
   MOVE_ANY(&(ip1->ITEMP1_TRY), &(ip2->ITEMP1_TRY), (long) (&(ip1->FB_GETTEMP_2)) - (long) 
   (&(ip1->ITEMP1_TRY)));
   ((__PFN_COPY)__OF(72))(__OD(ip1->FB_GETTEMP_2), __OD(ip2->FB_GETTEMP_2));
   MOVE_ANY(&(ip1->ITEMP2_TRY), &(ip2->ITEMP2_TRY), (long) (ip1) + sizeof(FB_AC_GETTEMP_REVA_TYP) 
   - (long) (&(ip1->ITEMP2_TRY)));
}

void SC_INIT__FB_AC_GETTEMP_REVA_00073(FB_AC_GETTEMP_REVA_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_AC_GETTEMP_REVA_TYP, XTAKE1) + sizeof(ip->XTAKE1);
   /* non retains: */
   ip->WTEMP1 = SC_C_WORD(0);
   ip->WTEMP2 = SC_C_WORD(0);
   ip->ITEMP = SC_C_INT(0);
   ip->XTEMPSENSORERROR1 = SC_C_BOOL(0);
   ip->XTEMPSENSORERROR2 = SC_C_BOOL(0);
   ip->XTEMPSENSORERROR = SC_C_BOOL(0);
   ip->I = SC_C_INT(0);
   ip->ISUM1 = SC_C_INT(0);
   ip->ITEMP1 = SC_C_INT(0);
   ip->ISUM2 = SC_C_INT(0);
   ip->ITEMP1SAVED = SC_C_INT(0);
   ip->ITEMP2SAVED = SC_C_INT(0);
   ip->ITEMP2 = SC_C_INT(0);
   ip->RTEMP1 = SC_C_REAL(0.0);
   ip->RTEMP2 = SC_C_REAL(0.0);
   ip->ITEMPORARY1 = SC_C_INT(0);
   ip->ITEMPORARY2 = SC_C_INT(0);
   ip->XTAKE2 = SC_C_BOOL(1);
   ip->ITEMP1_TRY = SC_C_INT(0);
   ip->ITEMP2_TRY = SC_C_INT(0);
   ip->XTAKE1 = SC_C_BOOL(1);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_AC_GETTEMP_REVA_00057(FB_AC_GETTEMP_REVA_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   FB_ENTRY(57, ip);
/******************* Network *************************/
{
   FB_GETTEMP_TYP* __lpt0 = (FB_GETTEMP_TYP*)__OD(ip->FB_GETTEMP_1);
   (*__lpt0).WTEMP = ip->WTEMP1;
   (*__lpt0).RFILTERCONSTANT = SC_C_REAL(0.001);
   ((__BPFN_FB_GETTEMP)__OF(68))((__lpt0));
   ip->ITEMP1_TRY = (*__lpt0).ITEMP;

}
/******************* Network *************************/
{
   FB_GETTEMP_TYP* __lpt0 = (FB_GETTEMP_TYP*)__OD(ip->FB_GETTEMP_2);
   (*__lpt0).WTEMP = ip->WTEMP2;
   (*__lpt0).RFILTERCONSTANT = SC_C_REAL(0.001);
   ((__BPFN_FB_GETTEMP)__OF(68))((__lpt0));
   ip->ITEMP2_TRY = (*__lpt0).ITEMP;

}
/******************* Network *************************/
{
   FB_FILTER_TYP* __lpt0 = (FB_FILTER_TYP*)__OD(ip->FB_FILTER_1);
   (*__lpt0).RA = SC_C_REAL(0.001);
   (*__lpt0).RVAR = DINT_TO_REAL((((WORD_TO_DINT(ip->WTEMP1) * SC_C_DINT(6500)) / SC_C_DINT(
   65535)) - SC_C_DINT(680)));
   ((__BPFN_FB_FILTER)__OF(69))((__lpt0));
   ip->ITEMP1 = REAL_TO_INT((*__lpt0).RVAR_FILTERED);

}
/******************* Network *************************/
{
   FB_FILTER_TYP* __lpt0 = (FB_FILTER_TYP*)__OD(ip->FB_FILTER_2);
   (*__lpt0).RA = SC_C_REAL(0.001);
   (*__lpt0).RVAR = DINT_TO_REAL((((WORD_TO_DINT(ip->WTEMP2) * SC_C_DINT(6500)) / SC_C_DINT(
   65535)) - SC_C_DINT(680)));
   ((__BPFN_FB_FILTER)__OF(69))((__lpt0));
   ip->ITEMP2 = REAL_TO_INT((*__lpt0).RVAR_FILTERED);

}
/******************* Network *************************/
SCL__ERROR_CHECK:;
{
   ip->XTEMPSENSORERROR1 = ((((ip->ITEMP1 < SC_C_INT(150)) | (ip->ITEMP1 > SC_C_INT(500))) 
   | (ip->ITEMP2 < SC_C_INT(150))) | (ip->ITEMP2 > SC_C_INT(500)));
   _SCT_B0 = (ABS_INT((ip->ITEMP1 - ip->ITEMP2)) > SC_C_INT(100));
   ip->XTEMPSENSORERROR2 = _SCT_B0;
   ip->ITEMP = SEL_INT(_SCT_B0,((ip->ITEMP1 + ip->ITEMP2) / SC_C_INT(2)),SEL_INT((ip->ITEMP1 
   > ip->ITEMP2),ip->ITEMP2,ip->ITEMP1));

}
/******************* Network *************************/
{
   ip->XTEMPSENSORERROR = ((1 & ip->XTEMPSENSORERROR1) | (1 & ip->XTEMPSENSORERROR2));

}

   FB_EXIT(57, ip);
}
