#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_BRAKE_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_BRAKE_00119(P_BRAKE_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_BRAKE_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_BRAKE_00048(void)
{
   P_BRAKE_TYP *ip = (P_BRAKE_TYP*) __OD(120);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(48, ip);
   (*(BRAKE_T_TYP*)__OD(19)).XEMGBRAKE_DO = ((((1 & (*(DEADMAN_T_TYP*)__OD(14)).XDEADEB) | 
   (1 & (*(BRAKE_T_TYP*)__OD(19)).XEMGBRAKE)) | (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFASTBRAKE)) 
   | (1 & (*(ULTRASON_MODE_T_TYP*)__OD(30)).XEMGULTRSN));
   (*(BRAKE_T_TYP*)__OD(19)).XEBSW = (((1 & LD_I_X(3,1,1,30)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,3,17)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(BRAKE_T_TYP*)__OD(19)).XEBBYPASS = (((1 & LD_I_X(3,1,1,31)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,3,18)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(BRAKE_T_TYP*)__OD(19)).XPARKINGBRK = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & 
   (1 & LD_I_X(3,1,1,27))) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & LD_I_X(3,
   2,2,27))));
   (*(BRAKE_T_TYP*)__OD(19)).XALLBRAKERELEASE = ((1 & LD_I_X(3,1,2,16)) | (1 & LD_I_X(3,2,
   3,19)));
   (*(BRAKE_T_TYP*)__OD(19)).XBRAKEPRES_SW = ((1 & LD_I_X(3,1,2,17)) | (1 & LD_I_X(3,2,3,20)));
   (*(BRAKE_T_TYP*)__OD(19)).XHOLDBRAKE = (((1 & LD_I_X(3,1,2,21)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,3,24)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(BRAKE_T_TYP*)__OD(19)).XBRAKEPIPEPRES_SW = ((1 & LD_I_X(3,1,1,29)) & (1 & LD_I_X(3,2,
   3,21)));

   PRG_EXIT(48, ip);
}
