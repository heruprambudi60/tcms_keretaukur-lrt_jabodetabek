#include "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include "hw_conf.h"
#include "cg_types.h"

I_L0_K0_TYP	I_L0_K0 = {0};
O_L0_K0_TYP	O_L0_K0 = {0};

CPU83x_INIT_TYP	SC_MODL0K0M0 = 
{
	sizeof(CPU83x_INIT_TYP),
	PACK_ADR(0,0,0),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	34,	/* wOffsetTypeInfo */
	62,	/* wOffsetStartParam */
	64,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	20,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* wStatusInfoCAN */
	0,	/* wStatusInfoSIO */
	0,	/* wStatusInfoLAN */
	0,	/* wStatusInfoCBusConfigError */
	0,	/* wStatusInfoCBusReady */
	0,	/* wStatusInfoLBusConfigError */
	0,	/* wStatusInfoLBusReady */
	0	/* wStatusInfoSDcard */
	,
	28,	/* wSizeTypeInfo */
	0,	/* wModulStartMode */
	0,	/* wModulType */
	0,	/* wSWVersion */
	0,	/* wSWBuild */
	0,	/* wInputUsed */
	0,	/* wOutputUsed */
	18,	/* wStatusUsed */
	0,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	2	/* wSizeStartParam */
	,
	2	/* wSizeStopParam */
};

CAN_CH_INIT_TYP	SC_L1_CAN1 = 
{
	sizeof(CAN_CH_INIT_TYP),
	PACK_ADR(1,0,0),
	0,	/* uiFbState */
	0,	/* iChannel */
	CPU83x_CAN_INSTALL,
	0,	/* uiBitrate */
	0,	/* uiNode */
};

CAN_CH_INIT_TYP	SC_L2_CAN2 = 
{
	sizeof(CAN_CH_INIT_TYP),
	PACK_ADR(2,0,0),
	0,	/* uiFbState */
	0,	/* iChannel */
	CPU83x_CAN_INSTALL,
	0,	/* uiBitrate */
	0,	/* uiNode */
};

LAN_CH_INIT_TYP	SC_L3_LAN1 = 
{
	sizeof(LAN_CH_INIT_TYP),
	PACK_ADR(3,0,0),
	0,	/* uiFbState */
	0,	/* iChannel */
	CPU83x_LAN_INSTALL
};

SIO_CH_INIT_TYP	SC_L4_SIO1 = 
{
	sizeof(SIO_CH_INIT_TYP),
	PACK_ADR(4,0,0),
	0,	/* uiFbState */
	0,	/* iChannel */
	CPU83x_SIO_INSTALL,
	9600,	/* uiBaud */
	8,	/* iDataBits */
	0,	/* iParity */
	1,	/* iStopBits */
	0,	/* iProtocol */
};

I_L5_K0_TYP	I_L5_K0 = {0};
O_L5_K0_TYP	O_L5_K0 = {0};

void InitHW()
{
	Mem_Usr[0].Maxinlist_Reg = 0;
	Mem_Usr[0].Taskzuordnung = 0;
	Mem_Usr[0].Node = Link0;
	Mem_Usr[0].OperationMode = 0;
	Mem_Usr[0].xListener = 0;
	Mem_Usr[0].ModulType = CPU833;
	Mem_Usr[0].LinkType = LinkType_CBus;
	Mem_Usr[0].LinkExtension = 0;
	Mem_Usr[0].Interface = 0;
	Mem_Usr[1].Maxinlist_Reg = 0;
	Mem_Usr[1].Taskzuordnung = 0;
	Mem_Usr[1].Node = Link1;
	Mem_Usr[1].OperationMode = 0;
	Mem_Usr[1].xListener = 0;
	Mem_Usr[1].ModulType = CPU833;
	Mem_Usr[1].LinkType = LinkType_CAN;
	Mem_Usr[1].LinkExtension = 0;
	Mem_Usr[1].Interface = 0;
	CAN_CH_INIT(&SC_L1_CAN1);
	Mem_Usr[2].Maxinlist_Reg = 0;
	Mem_Usr[2].Taskzuordnung = 0;
	Mem_Usr[2].Node = Link2;
	Mem_Usr[2].OperationMode = 0;
	Mem_Usr[2].xListener = 0;
	Mem_Usr[2].ModulType = CPU833;
	Mem_Usr[2].LinkType = LinkType_CAN;
	Mem_Usr[2].LinkExtension = 0;
	Mem_Usr[2].Interface = 1;
	CAN_CH_INIT(&SC_L2_CAN2);
	Mem_Usr[3].Maxinlist_Reg = 2;
	Mem_Usr[3].Taskzuordnung = 0;
	Mem_Usr[3].Node = Link3;
	Mem_Usr[3].OperationMode = 0;
	Mem_Usr[3].xListener = 0;
	Mem_Usr[3].ModulType = CPU833;
	Mem_Usr[3].LinkType = LinkType_LAN;
	Mem_Usr[3].LinkExtension = 0;
	Mem_Usr[3].Interface = 0;
	LAN_CH_INIT(&SC_L3_LAN1);
	Mem_Usr[4].Maxinlist_Reg = 0;
	Mem_Usr[4].Taskzuordnung = 0;
	Mem_Usr[4].Node = 0;
	Mem_Usr[4].OperationMode = 0;
	Mem_Usr[4].xListener = 0;
	Mem_Usr[4].ModulType = CPU833;
	Mem_Usr[4].LinkType = LinkType_SIO;
	Mem_Usr[4].LinkExtension = 0;
	Mem_Usr[4].Interface = 0;
	SIO_CH_INIT(&SC_L4_SIO1);
	Mem_Usr[5].Maxinlist_Reg = 0;
	Mem_Usr[5].Taskzuordnung = 0;
	Mem_Usr[5].Node = 0;
	Mem_Usr[5].OperationMode = 0;
	Mem_Usr[5].xListener = 0;
	Mem_Usr[5].ModulType = CPU833;
	Mem_Usr[5].LinkType = LinkType_LBus;
	Mem_Usr[5].LinkExtension = 0;
	Mem_Usr[5].Interface = 0;
}
