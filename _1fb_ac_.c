#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP2_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_AC_CP2_00086(FB_AC_CP2_TYP* ip1, FB_AC_CP2_TYP* ip2)
{
   MOVE_ANY(&(ip1->TCMPSTARTUP), &(ip2->TCMPSTARTUP), (long) (&(ip1->TON_1)) - (long) (&(ip1->TCMPSTARTUP)));
   MOVE_ANY(__OD(ip1->TON_1), __OD(ip2->TON_1), sizeof(TON_TYP));
   MOVE_ANY(__OD(ip1->TON_2), __OD(ip2->TON_2), sizeof(TON_TYP));
}

void SC_INIT__FB_AC_CP2_00087(FB_AC_CP2_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_AC_CP2_TYP, TON_2) + sizeof(ip->TON_2);
   /* non retains: */
   ip->TCMPSTARTUP = SC_C_TIME(0);
   ip->XCMPA_NFB = SC_C_BOOL(0);
   ip->XCMPB_NFB = SC_C_BOOL(0);
   ip->XENABLE = SC_C_BOOL(0);
   ip->XHP_CMPA = SC_C_BOOL(0);
   ip->XLP_CMPA = SC_C_BOOL(0);
   ip->XTEMPPROTECTIONCPA = SC_C_BOOL(0);
   ip->XTEMPPROTECTIONCPB = SC_C_BOOL(0);
   ip->XACSTARTUP = SC_C_BOOL(0);
   ip->XCDANFB = SC_C_BOOL(0);
   ip->XCDBNFB = SC_C_BOOL(0);
   ip->XHALF = SC_C_BOOL(0);
   ip->XFULL = SC_C_BOOL(0);
   ip->TCMPNORMAL = SC_C_TIME(0);
   ip->XCMPAB_ON = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_AC_CP2_00064(FB_AC_CP2_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   FB_ENTRY(64, ip);
/******************* Network *************************/
{
   TON_TYP* __lpt0 = (TON_TYP*)__OD(ip->TON_1);
   TON_TYP* __lpt1 = (TON_TYP*)__OD(ip->TON_2);
   _SCT_B0 = ((((((1 & ip->XENABLE) & (1 & ip->XTEMPPROTECTIONCPA)) & (1 & ip->XTEMPPROTECTIONCPB)) 
   & (1 & ip->XCDANFB)) & (1 & ip->XCDBNFB)) & ((1 & ip->XHALF) | (1 & ip->XFULL)));
   (*__lpt1).IN = ((1 & _SCT_B0) &  (!((1 & ip->XACSTARTUP))));
   (*__lpt1).PT = ip->TCMPNORMAL;
   TON((__lpt1));
   (*__lpt0).IN = ((1 & _SCT_B0) & (1 & ip->XACSTARTUP));
   (*__lpt0).PT = ip->TCMPSTARTUP;
   TON((__lpt0));
   ip->XCMPAB_ON = ((((((1 & (*__lpt1).Q) | (1 & (*__lpt0).Q)) & (1 & ip->XCMPA_NFB)) &  (
   !((1 & ip->XHP_CMPA)))) &  (!((1 & ip->XLP_CMPA)))) & (1 & ip->XCMPB_NFB));

}

   FB_EXIT(64, ip);
}
