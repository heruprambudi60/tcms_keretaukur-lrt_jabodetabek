#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* TASK PROCEDURE: */
void SC_BODY__MY_TASK_00007(void )
{
   TASK_ENTRY(7);
   ((void (*)())__OF(35))();
   ((void (*)())__OF(36))();
   ((void (*)())__OF(37))();
   ((void (*)())__OF(38))();
   ((void (*)())__OF(39))();
   ((void (*)())__OF(40))();
   ((void (*)())__OF(41))();
   ((void (*)())__OF(42))();
   ((void (*)())__OF(43))();
   ((void (*)())__OF(44))();
   ((void (*)())__OF(45))();
   ((void (*)())__OF(46))();
   ((void (*)())__OF(47))();
   ((void (*)())__OF(48))();
   ((void (*)())__OF(49))();
   ((void (*)())__OF(50))();
   ((void (*)())__OF(51))();
   ((void (*)())__OF(52))();
   ((void (*)())__OF(53))();
   ((void (*)())__OF(54))();
   ((void (*)())__OF(55))();
   ((void (*)())__OF(56))();
   TASK_EXIT(7);
}
