#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;
typedef void (*__BPFN_FB_FILTER)(FB_FILTER_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XBATTOFFSW;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
   SC__INSTC                  SR_1;
   SC_TIME                    TSWTIME;
   SC_TIME                    TOFFTIME;
   SC_REAL                    RFILTERED;
   SC_REAL                    RRES;
   SC_REAL                    RANALOG;
   SC_INT                     IANALOG;
   SC_WORD                    WANALOG;
   SC__INSTC                  FB_FILTER_1;
   SC_BOOL                    XBATTLOW;
}P_BATT_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_BATT_00129(P_BATT_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_BATT_TYP, XBATTLOW) + sizeof(ip->XBATTLOW);
   /* non retains: */
   ip->XBATTOFFSW = SC_C_BOOL(0);
   ip->TSWTIME = SC_C_TIME(3000);
   ip->TOFFTIME = SC_C_TIME(30000);
   ip->RFILTERED = SC_C_REAL(0.0);
   ip->RRES = SC_C_REAL(0.0);
   ip->RANALOG = SC_C_REAL(0.0);
   ip->IANALOG = SC_C_INT(0);
   ip->WANALOG = SC_C_WORD(0);
   ip->XBATTLOW = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_BATT_00053(void)
{
   P_BATT_TYP *ip = (P_BATT_TYP*) __OD(130);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(53, ip);
   ip->RRES = SC_C_REAL(0.59);
   ip->WANALOG = SHR_WORD(LD_I_W(3,1,5,1),SC_C_USINT(4));
   ip->IANALOG = WORD_TO_INT(ip->WANALOG);
   ip->RANALOG = INT_TO_REAL(ip->IANALOG);
   (*(FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)).RA = SC_C_REAL(0.01);
   (*(FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)).RVAR = ip->RANALOG;
   ((__BPFN_FB_FILTER)__OF(69))(((FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)));
   ip->RFILTERED = (*(FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)).RVAR_FILTERED;
   (*(BATT_T_TYP*)__OD(17)).RBATTV = ((ip->RFILTERED * ip->RRES) / SC_C_REAL(10.0));

   if(((*(BATT_T_TYP*)__OD(17)).RBATTV >= SC_C_REAL(88.0)))
   {
      ip->XBATTLOW = SC_C_BOOL(0);
   }
   else if(((*(BATT_T_TYP*)__OD(17)).RBATTV < SC_C_REAL(79.00)))
   {
      ip->XBATTLOW = SC_C_BOOL(1);
   }

   (*(BATT_T_TYP*)__OD(17)).XBATTLOW = ip->XBATTLOW;
   (*(BATT_T_TYP*)__OD(17)).XEBBATTLOW = (*(BATT_T_TYP*)__OD(17)).XBATTLOW;

   if(((*(BATT_T_TYP*)__OD(17)).RBATTV >= SC_C_REAL(79.0)))
   {
      (*(BATT_T_TYP*)__OD(17)).XBATTLOW_BUZZER = SC_C_BOOL(0);
   }
   else if(((*(BATT_T_TYP*)__OD(17)).RBATTV < SC_C_REAL(79.0)))
   {
      (*(BATT_T_TYP*)__OD(17)).XBATTLOW_BUZZER = SC_C_BOOL(1);
   }

   ip->XBATTOFFSW = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (1 & LD_I_X(3,1,1,16))) 
   | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & LD_I_X(3,2,2,16))));
   (*(TON_TYP*)__OD(ip->TON_1)).IN = ip->XBATTOFFSW;
   (*(TON_TYP*)__OD(ip->TON_1)).PT = ip->TSWTIME;
   TON(((TON_TYP*)__OD(ip->TON_1)));
   (*(SR_TYP*)__OD(ip->SR_1)).SET1 = (*(TON_TYP*)__OD(ip->TON_1)).Q;
   (*(SR_TYP*)__OD(ip->SR_1)).RESET = (*(BATT_T_TYP*)__OD(17)).XBATTOFF_DO;
   SR(((SR_TYP*)__OD(ip->SR_1)));
   (*(BATT_T_TYP*)__OD(17)).XBATTOFF_CMD = (*(SR_TYP*)__OD(ip->SR_1)).Q1;
   (*(TON_TYP*)__OD(ip->TON_2)).IN = (*(BATT_T_TYP*)__OD(17)).XBATTOFF_CMD;
   (*(TON_TYP*)__OD(ip->TON_2)).PT = ip->TOFFTIME;
   TON(((TON_TYP*)__OD(ip->TON_2)));
   (*(BATT_T_TYP*)__OD(17)).XBATTOFF_DO = (*(TON_TYP*)__OD(ip->TON_2)).Q;
   (*(BATT_T_TYP*)__OD(17)).XBATTCHRFFAULT = ((1 & LD_I_X(3,1,2,9)) | (1 & LD_I_X(3,2,3,16)));

   PRG_EXIT(53, ip);
}
