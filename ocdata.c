#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
typedef SC_STRINGDEF(20)      STRING__TYPE20;


/* PROGRAM, FUNCTION BLOCK, DUT AND __GLOBALS_TYP STRUCTURES: */
typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_TYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_TYP;

typedef struct
{
   SC_BOOL                    XROOMLAMPSTAT_1;
   SC_BOOL                    XROOMLAMPSTAT_2;
   SC_BOOL                    XROOMLAMPFAULT_1;
   SC_BOOL                    XROOMLAMPFAULT_2;
}LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XENGINE_RUNNING;
   SC_BOOL                    XENGINE_STOP;
   SC_BOOL                    XENGINE_EMG_STOP;
   SC_BOOL                    XMODEULTRASONIK;
   SC_BOOL                    XMODETRAINWASH;
   SC_BOOL                    XSMOKEDETECTORCAB1;
   SC_BOOL                    XSMOKEDETECTORCAB2;
   SC_BOOL                    XHEAD_LAMP1_ON;
   SC_BOOL                    XHEAD_LAMP1_FAULT;
   SC_BOOL                    XHEAD_LAMP2_ON;
   SC_BOOL                    XHEAD_LAMP2_FAULT;
   SC_BOOL                    XHEAD_LAMP_NOTIF_ON;
   SC_BOOL                    XFRWHITE;
   SC_BOOL                    XRRWHITE;
   SC_BOOL                    XFRRED;
   SC_BOOL                    XRRRED;
   SC_BOOL                    XFRGREEN;
   SC_BOOL                    XRRGREEN;
   SC_BOOL                    XFRSIDERED;
   SC_BOOL                    XRRSIDERED;
   SC_BOOL                    XPARKINGBRAKEAPPLIED;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XBRAKECYLINDER;
   SC_BOOL                    XBRAKEPIPEPRESURE;
   SC_BOOL                    XHOLDINGBRAKE;
   SC_BOOL                    XDYNAMICBRAKESW;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XCOMFAULT;
   SC_BOOL                    XCOMEMG;
   SC_BOOL                    XCOMBYPASS;
   SC_BOOL                    XCOM1_ON;
   SC_BOOL                    XCOM2_ON;
   SC_BOOL                    XCOMPRESSOR_ERROR;
   SC_BOOL                    XCOM_OFF;
   SC_BOOL                    XTRACTIONREADY;
   SC_BOOL                    XCAB1FRONT;
   SC_BOOL                    XCAB2FRONT;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XFORWARD;
   SC_BOOL                    XREVERSE;
   SC_BOOL                    XCAB1DIR;
   SC_BOOL                    XCAB2DIR;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XBATTSHUTDOWN;
   SC_BOOL                    XDEADMANACTIVE;
   SC_BOOL                    XDEADMANTRCUTOFF;
   SC_BOOL                    XDEADMANEB;
   SC_BOOL                    XROOMLPSTAT_1;
   SC_BOOL                    XROOMLPFAULT_1;
   SC_BOOL                    XROOMLPSTAT_2;
   SC_BOOL                    XROOMLPFAULT_2;
   SC_BOOL                    XD1OPEN;
   SC_BOOL                    XD2OPEN;
   SC_BOOL                    XD1LOCKED;
   SC_BOOL                    XD2LOCKED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBYPASS;
   SC_BOOL                    XRIOM1_DI1_0;
   SC_BOOL                    XRIOM1_DI1_1;
   SC_BOOL                    XRIOM1_DI1_2;
   SC_BOOL                    XRIOM1_DI1_3;
   SC_BOOL                    XRIOM1_DI1_4;
   SC_BOOL                    XRIOM1_DI1_5;
   SC_BOOL                    XRIOM1_DI1_6;
   SC_BOOL                    XRIOM1_DI1_7;
   SC_BOOL                    XRIOM1_DI1_8;
   SC_BOOL                    XRIOM1_DI1_9;
   SC_BOOL                    XRIOM1_DI1_10;
   SC_BOOL                    XRIOM1_DI1_11;
   SC_BOOL                    XRIOM1_DI1_12;
   SC_BOOL                    XRIOM1_DI1_13;
   SC_BOOL                    XRIOM1_DI1_14;
   SC_BOOL                    XRIOM1_DI1_15;
   SC_BOOL                    XRIOM1_DI1_16;
   SC_BOOL                    XRIOM1_DI1_17;
   SC_BOOL                    XRIOM1_DI1_18;
   SC_BOOL                    XRIOM1_DI1_19;
   SC_BOOL                    XRIOM1_DI1_20;
   SC_BOOL                    XRIOM1_DI1_21;
   SC_BOOL                    XRIOM1_DI1_22;
   SC_BOOL                    XRIOM1_DI1_23;
   SC_BOOL                    XRIOM1_DI1_24;
   SC_BOOL                    XRIOM1_DI1_25;
   SC_BOOL                    XRIOM1_DI1_26;
   SC_BOOL                    XRIOM1_DI1_27;
   SC_BOOL                    XRIOM1_DI1_28;
   SC_BOOL                    XRIOM1_DI1_29;
   SC_BOOL                    XRIOM1_DI1_30;
   SC_BOOL                    XRIOM1_DI1_31;
   SC_BOOL                    XRIOM1_DI2_0;
   SC_BOOL                    XRIOM1_DI2_1;
   SC_BOOL                    XRIOM1_DI2_2;
   SC_BOOL                    XRIOM1_DI2_3;
   SC_BOOL                    XRIOM1_DI2_4;
   SC_BOOL                    XRIOM1_DI2_5;
   SC_BOOL                    XRIOM1_DI2_6;
   SC_BOOL                    XRIOM1_DI2_7;
   SC_BOOL                    XRIOM1_DI2_8;
   SC_BOOL                    XRIOM1_DI2_9;
   SC_BOOL                    XRIOM1_DI2_10;
   SC_BOOL                    XRIOM1_DI2_11;
   SC_BOOL                    XRIOM1_DI2_12;
   SC_BOOL                    XRIOM1_DI2_13;
   SC_BOOL                    XRIOM1_DI2_14;
   SC_BOOL                    XRIOM1_DI2_15;
   SC_BOOL                    XRIOM1_DI2_16;
   SC_BOOL                    XRIOM1_DI2_17;
   SC_BOOL                    XRIOM1_DI2_18;
   SC_BOOL                    XRIOM1_DI2_19;
   SC_BOOL                    XRIOM1_DI2_20;
   SC_BOOL                    XRIOM1_DI2_21;
   SC_BOOL                    XRIOM1_DI2_22;
   SC_BOOL                    XRIOM1_DI2_23;
   SC_BOOL                    XRIOM1_DI2_24;
   SC_BOOL                    XRIOM1_DI2_25;
   SC_BOOL                    XRIOM1_DI2_26;
   SC_BOOL                    XRIOM1_DI2_27;
   SC_BOOL                    XRIOM1_DI2_28;
   SC_BOOL                    XRIOM1_DI2_29;
   SC_BOOL                    XRIOM1_DI2_30;
   SC_BOOL                    XRIOM1_DI2_31;
   SC_BOOL                    XRIOM1_DO1_0;
   SC_BOOL                    XRIOM1_DO1_1;
   SC_BOOL                    XRIOM1_DO1_2;
   SC_BOOL                    XRIOM1_DO1_3;
   SC_BOOL                    XRIOM1_DO1_4;
   SC_BOOL                    XRIOM1_DO1_5;
   SC_BOOL                    XRIOM1_DO1_6;
   SC_BOOL                    XRIOM1_DO1_7;
   SC_BOOL                    XRIOM1_DO1_8;
   SC_BOOL                    XRIOM1_DO1_9;
   SC_BOOL                    XRIOM1_DO1_10;
   SC_BOOL                    XRIOM1_DO1_11;
   SC_BOOL                    XRIOM1_DO1_12;
   SC_BOOL                    XRIOM1_DO1_13;
   SC_BOOL                    XRIOM1_DO1_14;
   SC_BOOL                    XRIOM1_DO1_15;
   SC_BOOL                    XRIOM1_DO1_16;
   SC_BOOL                    XRIOM1_DO1_17;
   SC_BOOL                    XRIOM1_DO1_18;
   SC_BOOL                    XRIOM1_DO1_19;
   SC_BOOL                    XRIOM1_DO1_20;
   SC_BOOL                    XRIOM1_DO1_21;
   SC_BOOL                    XRIOM1_DO1_22;
   SC_BOOL                    XRIOM1_DO1_23;
   SC_BOOL                    XRIOM1_DO1_24;
   SC_BOOL                    XRIOM1_DO1_25;
   SC_BOOL                    XRIOM1_DO1_26;
   SC_BOOL                    XRIOM1_DO1_27;
   SC_BOOL                    XRIOM1_DO1_28;
   SC_BOOL                    XRIOM1_DO1_29;
   SC_BOOL                    XRIOM1_DO1_30;
   SC_BOOL                    XRIOM1_DO1_31;
   SC_BOOL                    XRIOM2_DI1_0;
   SC_BOOL                    XRIOM2_DI1_1;
   SC_BOOL                    XRIOM2_DI1_2;
   SC_BOOL                    XRIOM2_DI1_3;
   SC_BOOL                    XRIOM2_DI1_4;
   SC_BOOL                    XRIOM2_DI1_5;
   SC_BOOL                    XRIOM2_DI1_6;
   SC_BOOL                    XRIOM2_DI1_7;
   SC_BOOL                    XRIOM2_DI1_8;
   SC_BOOL                    XRIOM2_DI1_9;
   SC_BOOL                    XRIOM2_DI1_10;
   SC_BOOL                    XRIOM2_DI1_11;
   SC_BOOL                    XRIOM2_DI1_12;
   SC_BOOL                    XRIOM2_DI1_13;
   SC_BOOL                    XRIOM2_DI1_14;
   SC_BOOL                    XRIOM2_DI1_15;
   SC_BOOL                    XRIOM2_DI1_16;
   SC_BOOL                    XRIOM2_DI1_17;
   SC_BOOL                    XRIOM2_DI1_18;
   SC_BOOL                    XRIOM2_DI1_19;
   SC_BOOL                    XRIOM2_DI1_20;
   SC_BOOL                    XRIOM2_DI1_21;
   SC_BOOL                    XRIOM2_DI1_22;
   SC_BOOL                    XRIOM2_DI1_23;
   SC_BOOL                    XRIOM2_DI1_24;
   SC_BOOL                    XRIOM2_DI1_25;
   SC_BOOL                    XRIOM2_DI1_26;
   SC_BOOL                    XRIOM2_DI1_27;
   SC_BOOL                    XRIOM2_DI1_28;
   SC_BOOL                    XRIOM2_DI1_29;
   SC_BOOL                    XRIOM2_DI1_30;
   SC_BOOL                    XRIOM2_DI1_31;
   SC_BOOL                    XRIOM2_DI2_0;
   SC_BOOL                    XRIOM2_DI2_1;
   SC_BOOL                    XRIOM2_DI2_2;
   SC_BOOL                    XRIOM2_DI2_3;
   SC_BOOL                    XRIOM2_DI2_4;
   SC_BOOL                    XRIOM2_DI2_5;
   SC_BOOL                    XRIOM2_DI2_6;
   SC_BOOL                    XRIOM2_DI2_7;
   SC_BOOL                    XRIOM2_DI2_8;
   SC_BOOL                    XRIOM2_DI2_9;
   SC_BOOL                    XRIOM2_DI2_10;
   SC_BOOL                    XRIOM2_DI2_11;
   SC_BOOL                    XRIOM2_DI2_12;
   SC_BOOL                    XRIOM2_DI2_13;
   SC_BOOL                    XRIOM2_DI2_14;
   SC_BOOL                    XRIOM2_DI2_15;
   SC_BOOL                    XRIOM2_DI2_16;
   SC_BOOL                    XRIOM2_DI2_17;
   SC_BOOL                    XRIOM2_DI2_18;
   SC_BOOL                    XRIOM2_DI2_19;
   SC_BOOL                    XRIOM2_DI2_20;
   SC_BOOL                    XRIOM2_DI2_21;
   SC_BOOL                    XRIOM2_DI2_22;
   SC_BOOL                    XRIOM2_DI2_23;
   SC_BOOL                    XRIOM2_DI2_24;
   SC_BOOL                    XRIOM2_DI2_25;
   SC_BOOL                    XRIOM2_DI2_26;
   SC_BOOL                    XRIOM2_DI2_27;
   SC_BOOL                    XRIOM2_DI2_28;
   SC_BOOL                    XRIOM2_DI2_29;
   SC_BOOL                    XRIOM2_DI2_30;
   SC_BOOL                    XRIOM2_DI2_31;
   SC_BOOL                    XRIOM2_DO1_0;
   SC_BOOL                    XRIOM2_DO1_1;
   SC_BOOL                    XRIOM2_DO1_2;
   SC_BOOL                    XRIOM2_DO1_3;
   SC_BOOL                    XRIOM2_DO1_4;
   SC_BOOL                    XRIOM2_DO1_5;
   SC_BOOL                    XRIOM2_DO1_6;
   SC_BOOL                    XRIOM2_DO1_7;
   SC_BOOL                    XRIOM2_DO1_8;
   SC_BOOL                    XRIOM2_DO1_9;
   SC_BOOL                    XRIOM2_DO1_10;
   SC_BOOL                    XRIOM2_DO1_11;
   SC_BOOL                    XRIOM2_DO1_12;
   SC_BOOL                    XRIOM2_DO1_13;
   SC_BOOL                    XRIOM2_DO1_14;
   SC_BOOL                    XRIOM2_DO1_15;
   SC_BOOL                    XRIOM2_DO1_16;
   SC_BOOL                    XRIOM2_DO1_17;
   SC_BOOL                    XRIOM2_DO1_18;
   SC_BOOL                    XRIOM2_DO1_19;
   SC_BOOL                    XRIOM2_DO1_20;
   SC_BOOL                    XRIOM2_DO1_21;
   SC_BOOL                    XRIOM2_DO1_22;
   SC_BOOL                    XRIOM2_DO1_23;
   SC_BOOL                    XRIOM2_DO1_24;
   SC_BOOL                    XRIOM2_DO1_25;
   SC_BOOL                    XRIOM2_DO1_26;
   SC_BOOL                    XRIOM2_DO1_27;
   SC_BOOL                    XRIOM2_DO1_28;
   SC_BOOL                    XRIOM2_DO1_29;
   SC_BOOL                    XRIOM2_DO1_30;
   SC_BOOL                    XRIOM2_DO1_31;
}MODBUSBOOL_R_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_INT                     ILIMITULTRASONIK;
   SC_INT                     IFILLINGTIME;
   SC_INT                     ILEAKAGETIME;
   SC_INT                     ICALENDAR_YYYY;
   SC_INT                     ICALENDAR_MM;
   SC_INT                     ICALENDAR_DD;
   SC_INT                     ICALENDAR_HH;
   SC_INT                     ICALENDAR_MIN;
   SC_INT                     ICALENDAR_SS;
   SC_INT                     ITRAINSPEED;
   SC_INT                     ITRAINSPEEDLIMIT;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BYTE                    __align1[2];
   SC_REAL                    RBATTVOLTAGE;
   SC_INT                     ICMP1_HH;
   SC_INT                     ICMP2_HH;
}MODBUSINT_R_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XCMPBAYPASS1;
   SC_BOOL                    XSETDT;
   SC_BOOL                    XTEMPUP;
   SC_BOOL                    XTEMPDOWN;
}MODBUSBOOL_RW_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_UDINT                   UDALARMAUTOREFRESHTIMESEC;
   SC_UINT                    UIALARMINDEXONFIRSTOUTELEM;
   SC_INT                     IALARMFILTERSUBSYSTEM;
   SC_INT                     IALARMFILTERMINDDS;
   SC_INT                     IALARMFILTERMAXDDS;
   SC_UDINT                   UDALARMFILTERMINDDSTIME;
   SC_UDINT                   UDALARMFILTERMAXDDSTIME;
   SC_UINT                    UIALARMSORTMODE;
   SC_WORD                    WALARMCUSTOMATTR;
   SC_UINT                    UIALARMCUSTOMATTRFUNC;
   SC_UINT                    UIALARMDDSACKNOWLEDGED;
   SC_UINT                    UIALARMACKNOWLEDGEUSER;
   SC_INT                     IWHEELDIAMETER;
   SC_INT                     ISETDD;
   SC_INT                     ISETMM;
   SC_INT                     ISETYYYY;
   SC_INT                     ISETHH;
   SC_INT                     ISETMIN;
   SC_INT                     ISETSS;
   SC_INT                     IACK;
}MODBUSINT_RW_T_TYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_TYP;

typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_TYP;

typedef struct
{
   SC_BOOL                    XFRONTR_WHT1_HMI;
   SC_BOOL                    XREARR_WHT2_HMI;
   SC_BOOL                    XFRONTR_RED1_HMI;
   SC_BOOL                    XREARR_RED2_HMI;
   SC_BOOL                    XFRONTR_SIDE1_HMI;
   SC_BOOL                    XREARR_SIDE2_HMI;
   SC_BOOL                    XFRONTR_GREEN_HMI;
   SC_BOOL                    XREARR_GREEN_HMI;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDE_RED_1;
   SC_BOOL                    XSIDE_RED_2;
   SC_BOOL                    XGREEN_1;
   SC_BOOL                    XGREEN_2;
}SIGNAL_LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XHEADLP_STAT_1;
   SC_BOOL                    XHEADLP_STAT_2;
   SC_BOOL                    XFAULTLP_STAT_1;
   SC_BOOL                    XFAULTLP_STAT_2;
   SC_BOOL                    XHEADLP_NOTIF;
}HEAD_LAMP_T_TYP;

typedef struct
{
   SC_UINT                    UIYYYY;
   SC_USINT                   USMM;
   SC_USINT                   USDD;
   SC_USINT                   USHH;
   SC_USINT                   USMIN;
   SC_USINT                   USSS;
   SC_INT                     IDAYOFWK;
   SC_DT                      DTDATETIME;
   STRING__TYPE20             SDATEANDTIME;
}CALENDAR_T_TYP;

typedef struct
{
   SC_INT                     XFILLINGTIME;
   SC_INT                     XLEAKAGECOMMAND;
}COMPRESOR_TEST_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XTRAINWASH_ON;
}TRAIN_WASH_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XAC_ONCMD;
   SC_INT                     IAC_SETTEMP;
   SC_INT                     IAC_TEMP_1;
   SC_INT                     IAC_TEMP_2;
   SC_INT                     IAC_TEMP_3;
   SC_INT                     IAC_TEMP_4;
   SC_BOOL                    XAC_HALF_1;
   SC_BOOL                    XAC_HALF_2;
   SC_BOOL                    XAC_HALF_3;
   SC_BOOL                    XAC_HALF_4;
   SC_BOOL                    XAC_FULL_1;
   SC_BOOL                    XAC_FULL_2;
   SC_BOOL                    XAC_FULL_3;
   SC_BOOL                    XAC_FULL_4;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP1FAULT_2;
   SC_BOOL                    XEVAP1FAULT_3;
   SC_BOOL                    XEVAP1FAULT_4;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XEVAP2FAULT_2;
   SC_BOOL                    XEVAP2FAULT_3;
   SC_BOOL                    XEVAP2FAULT_4;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD1FAULT_2;
   SC_BOOL                    XCD1FAULT_3;
   SC_BOOL                    XCD1FAULT_4;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD2FAULT_2;
   SC_BOOL                    XCD2FAULT_3;
   SC_BOOL                    XCD2FAULT_4;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD3FAULT_2;
   SC_BOOL                    XCD3FAULT_3;
   SC_BOOL                    XCD3FAULT_4;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCD4FAULT_2;
   SC_BOOL                    XCD4FAULT_3;
   SC_BOOL                    XCD4FAULT_4;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP1FAULT_2;
   SC_BOOL                    XCP1FAULT_3;
   SC_BOOL                    XCP1FAULT_4;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP2FAULT_2;
   SC_BOOL                    XCP2FAULT_3;
   SC_BOOL                    XCP2FAULT_4;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP3FAULT_2;
   SC_BOOL                    XCP3FAULT_3;
   SC_BOOL                    XCP3FAULT_4;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XCP4FAULT_2;
   SC_BOOL                    XCP4FAULT_3;
   SC_BOOL                    XCP4FAULT_4;
   SC_BOOL                    XEVAP1OVERTEMP_1;
   SC_BOOL                    XEVAP1OVERTEMP_2;
   SC_BOOL                    XEVAP1OVERTEMP_3;
   SC_BOOL                    XEVAP1OVERTEMP_4;
   SC_BOOL                    XEVAP2OVERTEMP_1;
   SC_BOOL                    XEVAP2OVERTEMP_2;
   SC_BOOL                    XEVAP2OVERTEMP_3;
   SC_BOOL                    XEVAP2OVERTEMP_4;
   SC_BOOL                    XCD1OVERTEMP_1;
   SC_BOOL                    XCD1OVERTEMP_2;
   SC_BOOL                    XCD1OVERTEMP_3;
   SC_BOOL                    XCD1OVERTEMP_4;
   SC_BOOL                    XCD2OVERTEMP_1;
   SC_BOOL                    XCD2OVERTEMP_2;
   SC_BOOL                    XCD2OVERTEMP_3;
   SC_BOOL                    XCD2OVERTEMP_4;
   SC_BOOL                    XCD3OVERTEMP_1;
   SC_BOOL                    XCD3OVERTEMP_2;
   SC_BOOL                    XCD3OVERTEMP_3;
   SC_BOOL                    XCD3OVERTEMP_4;
   SC_BOOL                    XCD4OVERTEMP_1;
   SC_BOOL                    XCD4OVERTEMP_2;
   SC_BOOL                    XCD4OVERTEMP_3;
   SC_BOOL                    XCD4OVERTEMP_4;
   SC_BOOL                    XCP1OVERTEMP_1;
   SC_BOOL                    XCP1OVERTEMP_2;
   SC_BOOL                    XCP1OVERTEMP_3;
   SC_BOOL                    XCP1OVERTEMP_4;
   SC_BOOL                    XCP2OVERTEMP_1;
   SC_BOOL                    XCP2OVERTEMP_2;
   SC_BOOL                    XCP2OVERTEMP_3;
   SC_BOOL                    XCP2OVERTEMP_4;
   SC_BOOL                    XCP3OVERTEMP_1;
   SC_BOOL                    XCP3OVERTEMP_2;
   SC_BOOL                    XCP3OVERTEMP_3;
   SC_BOOL                    XCP3OVERTEMP_4;
   SC_BOOL                    XCP4OVERTEMP_1;
   SC_BOOL                    XCP4OVERTEMP_2;
   SC_BOOL                    XCP4OVERTEMP_3;
   SC_BOOL                    XCP4OVERTEMP_4;
   SC_BOOL                    XENGIDLEMODECMD;
   SC_BOOL                    XTECU1FAULT;
   SC_BOOL                    XTECU2FAULT;
   SC_UINT                    UIDCLINKV;
   SC_UINT                    UIDCLINKA;
   SC_UINT                    UIENG1RPM;
   SC_UINT                    UIENG2RPM;
   SC_BOOL                    XENG1RUNNING;
   SC_BOOL                    XENG2RUNNING;
   SC_BOOL                    XENG1FAULT;
   SC_BOOL                    XENG2FAULT;
}R1_T_TYP;

typedef struct
{
   SC_BOOL                    XENGINERUNNING;
   SC_BOOL                    XENGINESTOP;
   SC_BOOL                    XENGINEEMERGENCYSTOP;
}ENGINE_STATUS_T_TYP;

typedef struct
{
   SC_BOOL                    XTEMPSENSORERROR_1;
   SC_BOOL                    XAC_COMMAND_ON;
   SC_BOOL                    XAC1;
   SC_BOOL                    XAC2;
   SC_INT                     ISETTEMP_HUNDREDS;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XEVAP1_ON;
   SC_BOOL                    XEVAP2_ON;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XACSTARTUP2;
   SC_BOOL                    XCD_AC1_ON;
   SC_BOOL                    XCD_AC2_ON;
   SC_BOOL                    XCP_AC1_ON;
   SC_BOOL                    XCP_AC2_ON;
   SC_INT                     ITEMP_1;
   SC_BOOL                    XHALFMODE_1;
   SC_BOOL                    XFULLMODE_1;
   SC_BOOL                    XACERROR_1;
   SC_DINT                    DICP1TIME_1;
   SC_DINT                    DICP2TIME_1;
}AC_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENGINEOFF;
   SC_BOOL                    XENGINE_OFF;
}__GLOBALS_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1K;
   SC_BOOL                    XEVAP2K;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XTEMPPROTECTIONCDA;
   SC_BOOL                    XTEMPPROTECTIONCDB;
   SC_TIME                    TCDDELAY;
   SC_BOOL                    XCDAB_ON;
   SC__INSTC                  TON_2;
}FB_AC_CD_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XHP_CMPB;
   SC_BOOL                    XLP_CMPB;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP1_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP2_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1NFB;
   SC_BOOL                    XEVAP2NFB;
   SC_BOOL                    X3PHCOUNTER;
   SC_BOOL                    XTEMPPROTECTIONEV1;
   SC_BOOL                    XTEMPPROTECTIONEV2;
   SC_BOOL                    XACON;
   SC_BOOL                    XEVAP1ON;
   SC_BOOL                    XEVAP2ON;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  TOF_1;
   SC__INSTC                  TOF_2;
}FB_AC_EVAP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_1_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_2_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLE1;
   SC_BOOL                    XENABLE2;
   SC_DINT*                   DITIME1;
   SC_DINT*                   DITIME2;
   SC__INSTC                  RS_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    X1MIN1;
   SC_BOOL                    X1MIN2;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_TIME                    TET;
   SC_TIME                    TET2;
}FB_TIMECOUNT_DINT_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLEHALF;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_DINT                    DIAC1TIME;
   SC_DINT                    DIAC2TIME;
   SC_BOOL                    XHALF1;
   SC_BOOL                    XHALF2;
   SC__INSTC                  R_TRIG_1;
   SC_BOOL                    XERROR;
   SC__INSTC                  SR_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    XRESET;
}FB_AC_HALFSEL_REVA_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP;
   SC_REAL                    RFILTERCONSTANT;
   SC_INT                     ITEMP;
   SC__INSTC                  FB_FILTER_1;
}FB_GETTEMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP1;
   SC_WORD                    WTEMP2;
   SC_INT                     ITEMP;
   SC_BOOL                    XTEMPSENSORERROR1;
   SC_BOOL                    XTEMPSENSORERROR2;
   SC_BOOL                    XTEMPSENSORERROR;
   SC_INT                     I;
   SC_INT                     ISUM1;
   SC_INT                     ITEMP1;
   SC_INT                     ISUM2;
   SC_INT                     ITEMP1SAVED;
   SC_INT                     ITEMP2SAVED;
   SC_INT                     ITEMP2;
   SC__INSTC                  FB_FILTER_1;
   SC_REAL                    RTEMP1;
   SC_REAL                    RTEMP2;
   SC__INSTC                  FB_FILTER_2;
   SC__INSTC                  TON_1;
   SC_INT                     ITEMPORARY1;
   SC_INT                     ITEMPORARY2;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_BOOL                    XTAKE2;
   SC__INSTC                  FB_GETTEMP_1;
   SC_INT                     ITEMP1_TRY;
   SC__INSTC                  FB_GETTEMP_2;
   SC_INT                     ITEMP2_TRY;
   SC_BOOL                    XTAKE1;
}FB_AC_GETTEMP_REVA_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XPHCOUNTER_OK;
   SC__INSTC                  FB_AC_CD_1;
   SC__INSTC                  FB_AC_CD_2;
   SC__INSTC                  FB_AC_CP1;
   SC__INSTC                  FB_AC_CP2;
   SC__INSTC                  FB_AC_EVAP_1;
   SC__INSTC                  FB_AC_1_1;
   SC__INSTC                  FB_AC_2_1;
   SC__INSTC                  FB_TIMECOUNT_DINT_1;
   SC_INT                     ITEMP_AC1;
   SC_INT                     ITEMP_AC2;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XERRORTEMPSENSOR2;
   SC_BOOL                    XFULL_AC1;
   SC_BOOL                    XFULL_AC2;
   SC_BOOL                    XH1_AC1;
   SC_BOOL                    XH1_AC2;
   SC_BOOL                    XH2_AC1;
   SC_BOOL                    XH2_AC2;
   SC_BOOL                    XHALF_AC1;
   SC_BOOL                    XHALF_AC2;
   SC__INSTC                  FB_HALFSEL_AC1;
   SC__INSTC                  FB_HALFSEL_AC2;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  FB_AC_GETTEMP_REVA_1;
   SC_TIME                    TCMP1NORMAL;
   SC_TIME                    TCMP1STARTUP;
   SC_TIME                    TCMP2NORMAL;
   SC_TIME                    TCMP2STARTUP;
   SC_DINT                    DIAC1TIME_1;
   SC_DINT                    DIAC2TIME_1;
}P_AC_CABIN_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_INT                     IACSETTEMP;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_1;
   SC__INSTC                  R_TRIG_1;
   SC__INSTC                  R_TRIG_2;
   SC__INSTC                  R_TRIG_3;
}P_AC_MODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WSENSOR;
   SC_REAL                    RA_FILTER;
   SC_INT                     IUPPER_HYSTERESYS;
   SC_INT                     ILOWER_HYSTERESYS1;
   SC_INT                     IPRESSURE;
   SC_BOOL                    XLOWPRESSURE;
   SC_REAL                    RANALOG;
   SC_REAL                    RFILTERED;
   SC_REAL                    RPRESSURE;
   SC_REAL                    RPRESSURE_REGRESSION;
   SC__INSTC                  FB_FILTER_1;
}FB_PRESSURE_CHECK_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  FB_PRESSURE_CHECK_1;
   SC__INSTC                  FB_TIMECOUNT_DINT_1;
   SC__INSTC                  RS_1;
   SC_REAL                    RA;
   SC_DINT                    DICMP1TIME;
   SC_DINT                    DICMP2TIME;
}P_COMPRESOR_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_ENGINE_STATUS_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_TRAIN_WASH_MODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
}P_ULTRASON_MODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
   SC__INSTC                  CTU_1;
   SC_INT                     IMRPRESSURE;
   SC_INT                     IMRSTART;
   SC_INT                     IMRSTOP;
   SC_INT                     ILEAKKAGE_RATE;
   SC_BOOL                    XENABLEFILL;
   SC_INT                     IFILLINGTIME;
   SC_BOOL                    XFILLING;
   SC_BOOL                    XRSTFILLINGCOUNTER;
   SC_BOOL                    XLEAKAGETESTRUNNING;
   SC_BOOL                    XENABLELEAK;
   SC_BOOL                    XSTOP;
   SC__INSTC                  R_TRIG_1;
   SC__INSTC                  F_TRIG_1;
}P_COM_TEST_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XHDLP_FAULT1;
   SC_BOOL                    XHDLP_FAULT2;
   SC_BOOL                    XHDLP_STAT1;
   SC_BOOL                    XHDLP_STAT2;
}P_HEAD_LAMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  CNV_DT_TO_STRING_1;
   SC__INSTC                  CNV_STRING_TO_DINT_1;
   SC__INSTC                  CNV_STRING_TO_DINT_2;
   SC__INSTC                  CNV_STRING_TO_DINT_3;
   SC__INSTC                  CNV_STRING_TO_DINT_4;
   SC__INSTC                  CNV_STRING_TO_DINT_5;
   SC__INSTC                  CNV_STRING_TO_DINT_6;
   SC_BYTE                    IBDAY_VCU;
   SC_BYTE                    IBHOUR_VCU;
   SC_BYTE                    IBMIN_VCU;
   SC_BYTE                    IBMONTH_VCU;
   SC_BYTE                    IBSEC_VCU;
   SC_BYTE                    IBYEAR_VCU;
   SC_INT                     IERRORCNVDD;
   SC_INT                     IERRORCNVDTTOSTR;
   SC_INT                     IERRORCNVHH;
   SC_INT                     IERRORCNVMIN;
   SC_INT                     IERRORCNVMM;
   SC_INT                     IERRORCNVSS;
   SC_INT                     IERRORCNVYYYY;
   SC_INT                     IERRORSETDT;
   SC__INSTC                  R_TRIG_1;
   STRING__TYPE20             SDATETIME;
   SC_BOOL                    SXMVBSTRONG;
   SC_BOOL                    SXMVBWEAK;
   SC__INSTC                  TIME_GET_RTC_1;
   SC_UINT                    UIMS;
   SC__INSTC                  CNV_DT_TO_STRING_2;
   SC__INSTC                  CNV_DT_TO_STRING_3;
}P_CALENDER_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XFRONT_ACTV;
   SC_BOOL                    XSIDE_ACTV;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDERED_1;
   SC_BOOL                    XSIDERED_2;
   SC_BOOL                    XFRWHITE_HMI;
   SC_BOOL                    XRRWHITE_HMI;
   SC_BOOL                    XFRSIDERED_HMI;
   SC_BOOL                    XRRSIDERED_HMI;
   SC_BOOL                    XFRRED_HMI;
   SC_BOOL                    XRRRED_HMI;
}P_SIGNAL_LAMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_TRACTION_M_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_OO_HMI_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_OO_DO_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_BRAKE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  CUTOFF_TIMER1;
   SC__INSTC                  CUTOFF_TIMER2;
   SC__INSTC                  CUTOFF_TIMER3;
   SC__INSTC                  CUTOFF_TIMER4;
   SC__INSTC                  EMG_TIMER1;
   SC__INSTC                  EMG_TIMER2;
   SC__INSTC                  EMG_TIMER3;
   SC__INSTC                  EMG_TIMER4;
   SC__INSTC                  PRESSED_TIMER;
   SC__INSTC                  RELEASED_TIMER;
   SC__INSTC                  SR_BUZZER1;
   SC__INSTC                  SR_BUZZER2;
   SC__INSTC                  SR_BUZZER3;
   SC__INSTC                  SR_BUZZER4;
   SC_TIME                    TPRESS;
   SC_TIME                    TRELEASE;
   SC_BOOL                    XDEADCUTOFF1;
   SC_BOOL                    XDEADCUTOFF2;
   SC_BOOL                    XDEADBUZZER1;
   SC_BOOL                    XDEADBUZZER2;
   SC_BOOL                    XEMGDEADMAN1;
   SC_BOOL                    XEMGDEADMAN2;
   SC_BOOL                    XPRESSED;
   SC_BOOL                    XRELEASED;
}P_DEADMAN_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_LAMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
}P_SPEED_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_DOORS_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XBATTOFFSW;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
   SC__INSTC                  SR_1;
   SC_TIME                    TSWTIME;
   SC_TIME                    TOFFTIME;
   SC_REAL                    RFILTERED;
   SC_REAL                    RRES;
   SC_REAL                    RANALOG;
   SC_INT                     IANALOG;
   SC_WORD                    WANALOG;
   SC__INSTC                  FB_FILTER_1;
   SC_BOOL                    XBATTLOW;
}P_BATT_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSET_1;
   SC_BOOL                    XRESET_1;
   SC__INSTC                  RS_1;
}P_MANUALMODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XP2;
   SC_BOOL                    XP5;
   SC_BOOL                    XB1;
   SC_BOOL                    XB2;
   SC_BOOL                    XB3;
   SC_BOOL                    XB4;
   SC_BOOL                    XB5;
   SC_BOOL                    XB6;
   SC_BOOL                    XB7;
}P_MASTERCTRL_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_CABSES_TYP;



/* ******* GLOBAL DATA INSTANCES ******* */
P_AC_CABIN_TYP                SC_DATA__P_AC_CABIN_00067 __LOCATE_IN_RETAINMEM__ ;
FB_AC_CD_TYP                  SC_DATA__FB_AC_CD_00137;
TON_TYP                       SC_DATA__TON_00138;
FB_AC_CD_TYP                  SC_DATA__FB_AC_CD_00139;
TON_TYP                       SC_DATA__TON_00140;
FB_AC_CP1_TYP                 SC_DATA__FB_AC_CP1_00141;
TON_TYP                       SC_DATA__TON_00142;
TON_TYP                       SC_DATA__TON_00143;
FB_AC_CP2_TYP                 SC_DATA__FB_AC_CP2_00144;
TON_TYP                       SC_DATA__TON_00145;
TON_TYP                       SC_DATA__TON_00146;
FB_AC_EVAP_TYP                SC_DATA__FB_AC_EVAP_00147;
TOF_TYP                       SC_DATA__TOF_00148;
TOF_TYP                       SC_DATA__TOF_00149;
FB_AC_1_TYP                   SC_DATA__FB_AC_1_00150;
FB_AC_2_TYP                   SC_DATA__FB_AC_2_00151;
FB_TIMECOUNT_DINT_TYP         SC_DATA__FB_TIMECOUNT_DINT_00152;
RS_TYP                        SC_DATA__RS_00153;
TON_TYP                       SC_DATA__TON_00154;
RS_TYP                        SC_DATA__RS_00155;
TON_TYP                       SC_DATA__TON_00156;
FB_AC_HALFSEL_REVA_TYP        SC_DATA__FB_AC_HALFSEL_REVA_00157;
R_TRIG_TYP                    SC_DATA__R_TRIG_00158;
SR_TYP                        SC_DATA__SR_00159;
TON_TYP                       SC_DATA__TON_00160;
FB_AC_HALFSEL_REVA_TYP        SC_DATA__FB_AC_HALFSEL_REVA_00161;
R_TRIG_TYP                    SC_DATA__R_TRIG_00162;
SR_TYP                        SC_DATA__SR_00163;
TON_TYP                       SC_DATA__TON_00164;
FB_AC_GETTEMP_REVA_TYP        SC_DATA__FB_AC_GETTEMP_REVA_00165;
FB_FILTER_TYP                 SC_DATA__FB_FILTER_00166;
FB_FILTER_TYP                 SC_DATA__FB_FILTER_00167;
TON_TYP                       SC_DATA__TON_00168;
RS_TYP                        SC_DATA__RS_00169;
RS_TYP                        SC_DATA__RS_00170;
TON_TYP                       SC_DATA__TON_00171;
FB_GETTEMP_TYP                SC_DATA__FB_GETTEMP_00172;
FB_FILTER_TYP                 SC_DATA__FB_FILTER_00173;
FB_GETTEMP_TYP                SC_DATA__FB_GETTEMP_00174;
FB_FILTER_TYP                 SC_DATA__FB_FILTER_00175;
P_AC_MODE_TYP                 SC_DATA__P_AC_MODE_00093 __LOCATE_IN_RETAINMEM__ ;
RS_TYP                        SC_DATA__RS_00176;
RS_TYP                        SC_DATA__RS_00177;
TON_TYP                       SC_DATA__TON_00178;
R_TRIG_TYP                    SC_DATA__R_TRIG_00179;
R_TRIG_TYP                    SC_DATA__R_TRIG_00180;
R_TRIG_TYP                    SC_DATA__R_TRIG_00181;
P_COMPRESOR_TYP               SC_DATA__P_COMPRESOR_00096 __LOCATE_IN_RETAINMEM__ ;
FB_PRESSURE_CHECK_TYP         SC_DATA__FB_PRESSURE_CHECK_00182;
FB_FILTER_TYP                 SC_DATA__FB_FILTER_00183;
FB_TIMECOUNT_DINT_TYP         SC_DATA__FB_TIMECOUNT_DINT_00184;
RS_TYP                        SC_DATA__RS_00185;
TON_TYP                       SC_DATA__TON_00186;
RS_TYP                        SC_DATA__RS_00187;
TON_TYP                       SC_DATA__TON_00188;
RS_TYP                        SC_DATA__RS_00189;
P_ENGINE_STATUS_TYP           SC_DATA__P_ENGINE_STATUS_00100;
P_TRAIN_WASH_MODE_TYP         SC_DATA__P_TRAIN_WASH_MODE_00102;
P_ULTRASON_MODE_TYP           SC_DATA__P_ULTRASON_MODE_00104;
SR_TYP                        SC_DATA__SR_00190;
P_COM_TEST_TYP                SC_DATA__P_COM_TEST_00106 __LOCATE_IN_RETAINMEM__ ;
SR_TYP                        SC_DATA__SR_00191;
RS_TYP                        SC_DATA__RS_00192;
RS_TYP                        SC_DATA__RS_00193;
TON_TYP                       SC_DATA__TON_00194;
TON_TYP                       SC_DATA__TON_00195;
CTU_TYP                       SC_DATA__CTU_00196;
R_TRIG_TYP                    SC_DATA__R_TRIG_00197;
F_TRIG_TYP                    SC_DATA__F_TRIG_00198;
P_HEAD_LAMP_TYP               SC_DATA__P_HEAD_LAMP_00108;
P_CALENDER_TYP                SC_DATA__P_CALENDER_00110;
CNV_DT_TO_STRING_TYP          SC_DATA__CNV_DT_TO_STRING_00199;
CNV_STRING_TO_DINT_TYP        SC_DATA__CNV_STRING_TO_DINT_00200;
CNV_STRING_TO_DINT_TYP        SC_DATA__CNV_STRING_TO_DINT_00201;
CNV_STRING_TO_DINT_TYP        SC_DATA__CNV_STRING_TO_DINT_00202;
CNV_STRING_TO_DINT_TYP        SC_DATA__CNV_STRING_TO_DINT_00203;
CNV_STRING_TO_DINT_TYP        SC_DATA__CNV_STRING_TO_DINT_00204;
CNV_STRING_TO_DINT_TYP        SC_DATA__CNV_STRING_TO_DINT_00205;
R_TRIG_TYP                    SC_DATA__R_TRIG_00206;
TIME_GET_RTC_TYP              SC_DATA__TIME_GET_RTC_00207;
CNV_DT_TO_STRING_TYP          SC_DATA__CNV_DT_TO_STRING_00208;
CNV_DT_TO_STRING_TYP          SC_DATA__CNV_DT_TO_STRING_00209;
P_SIGNAL_LAMP_TYP             SC_DATA__P_SIGNAL_LAMP_00112;
P_TRACTION_M_TYP              SC_DATA__P_TRACTION_M_00114;
P_OO_HMI_TYP                  SC_DATA__P_OO_HMI_00116;
P_OO_DO_TYP                   SC_DATA__P_OO_DO_00118;
P_BRAKE_TYP                   SC_DATA__P_BRAKE_00120;
P_DEADMAN_TYP                 SC_DATA__P_DEADMAN_00122;
TON_TYP                       SC_DATA__TON_00210;
TON_TYP                       SC_DATA__TON_00211;
TON_TYP                       SC_DATA__TON_00212;
TON_TYP                       SC_DATA__TON_00213;
TON_TYP                       SC_DATA__TON_00214;
TON_TYP                       SC_DATA__TON_00215;
TON_TYP                       SC_DATA__TON_00216;
TON_TYP                       SC_DATA__TON_00217;
TON_TYP                       SC_DATA__TON_00218;
TON_TYP                       SC_DATA__TON_00219;
SR_TYP                        SC_DATA__SR_00220;
SR_TYP                        SC_DATA__SR_00221;
SR_TYP                        SC_DATA__SR_00222;
SR_TYP                        SC_DATA__SR_00223;
P_LAMP_TYP                    SC_DATA__P_LAMP_00124;
P_SPEED_TYP                   SC_DATA__P_SPEED_00126;
SR_TYP                        SC_DATA__SR_00224;
P_DOORS_TYP                   SC_DATA__P_DOORS_00128;
P_BATT_TYP                    SC_DATA__P_BATT_00130;
TON_TYP                       SC_DATA__TON_00225;
TON_TYP                       SC_DATA__TON_00226;
SR_TYP                        SC_DATA__SR_00227;
FB_FILTER_TYP                 SC_DATA__FB_FILTER_00228;
P_MANUALMODE_TYP              SC_DATA__P_MANUALMODE_00132;
RS_TYP                        SC_DATA__RS_00229;
P_MASTERCTRL_TYP              SC_DATA__P_MASTERCTRL_00134;
P_CABSES_TYP                  SC_DATA__P_CABSES_00136;
__GLOBALS_TYP                 SC_DATA____GLOBALS_00004;
CABIN_T_TYP                   SC_DATA__CABIN_T_00011;
MASTERCTRL_T_TYP              SC_DATA__MASTERCTRL_T_00012;
MANUALMODE_T_TYP              SC_DATA__MANUALMODE_T_00013;
DEADMAN_T_TYP                 SC_DATA__DEADMAN_T_00014;
SPEED_T_TYP                   SC_DATA__SPEED_T_00015;
DOORS_T_TYP                   SC_DATA__DOORS_T_00016;
BATT_T_TYP                    SC_DATA__BATT_T_00017;
LAMP_T_TYP                    SC_DATA__LAMP_T_00018;
BRAKE_T_TYP                   SC_DATA__BRAKE_T_00019;
MODBUSBOOL_R_T_TYP            SC_DATA__MODBUSBOOL_R_T_00020;
MODBUSINT_R_T_TYP             SC_DATA__MODBUSINT_R_T_00021;
MODBUSBOOL_RW_T_TYP           SC_DATA__MODBUSBOOL_RW_T_00022;
MODBUSINT_RW_T_TYP            SC_DATA__MODBUSINT_RW_T_00023;
TRACTION_MNGMNT_TYP           SC_DATA__TRACTION_MNGMNT_00024;
COMPRESSOR_T_TYP              SC_DATA__COMPRESSOR_T_00025;
SIGNAL_LAMP_T_TYP             SC_DATA__SIGNAL_LAMP_T_00026;
HEAD_LAMP_T_TYP               SC_DATA__HEAD_LAMP_T_00027;
CALENDAR_T_TYP                SC_DATA__CALENDAR_T_00028;
COMPRESOR_TEST_T_TYP          SC_DATA__COMPRESOR_TEST_T_00029;
ULTRASON_MODE_T_TYP           SC_DATA__ULTRASON_MODE_T_00030;
TRAIN_WASH_MODE_T_TYP         SC_DATA__TRAIN_WASH_MODE_T_00031;
R1_T_TYP                      SC_DATA__R1_T_00032;
ENGINE_STATUS_T_TYP           SC_DATA__ENGINE_STATUS_T_00033;
AC_T_TYP                      SC_DATA__AC_T_00034;


/* ******* HELPER ARRAY FOR ID AND __ARRSIZE SETTINGS ******* */
const long __IDArray[] = {
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_CD_1) / sizeof(SC__INSTC),137,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_CD_2) / sizeof(SC__INSTC),139,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_CP1) / sizeof(SC__INSTC),141,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_CP2) / sizeof(SC__INSTC),144,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_EVAP_1) / sizeof(SC__INSTC),147,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_1_1) / sizeof(SC__INSTC),150,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_2_1) / sizeof(SC__INSTC),151,
 67, OFFSET(P_AC_CABIN_TYP, FB_TIMECOUNT_DINT_1) / sizeof(SC__INSTC),152,
 67, OFFSET(P_AC_CABIN_TYP, FB_HALFSEL_AC1) / sizeof(SC__INSTC),157,
 67, OFFSET(P_AC_CABIN_TYP, FB_HALFSEL_AC2) / sizeof(SC__INSTC),161,
 67, OFFSET(P_AC_CABIN_TYP, FB_AC_GETTEMP_REVA_1) / sizeof(SC__INSTC),165,
 137, OFFSET(FB_AC_CD_TYP, TON_2) / sizeof(SC__INSTC),138,
 139, OFFSET(FB_AC_CD_TYP, TON_2) / sizeof(SC__INSTC),140,
 141, OFFSET(FB_AC_CP1_TYP, TON_1) / sizeof(SC__INSTC),142,
 141, OFFSET(FB_AC_CP1_TYP, TON_2) / sizeof(SC__INSTC),143,
 144, OFFSET(FB_AC_CP2_TYP, TON_1) / sizeof(SC__INSTC),145,
 144, OFFSET(FB_AC_CP2_TYP, TON_2) / sizeof(SC__INSTC),146,
 147, OFFSET(FB_AC_EVAP_TYP, TOF_1) / sizeof(SC__INSTC),148,
 147, OFFSET(FB_AC_EVAP_TYP, TOF_2) / sizeof(SC__INSTC),149,
 152, OFFSET(FB_TIMECOUNT_DINT_TYP, RS_1) / sizeof(SC__INSTC),153,
 152, OFFSET(FB_TIMECOUNT_DINT_TYP, TON_1) / sizeof(SC__INSTC),154,
 152, OFFSET(FB_TIMECOUNT_DINT_TYP, RS_2) / sizeof(SC__INSTC),155,
 152, OFFSET(FB_TIMECOUNT_DINT_TYP, TON_2) / sizeof(SC__INSTC),156,
 157, OFFSET(FB_AC_HALFSEL_REVA_TYP, R_TRIG_1) / sizeof(SC__INSTC),158,
 157, OFFSET(FB_AC_HALFSEL_REVA_TYP, SR_1) / sizeof(SC__INSTC),159,
 157, OFFSET(FB_AC_HALFSEL_REVA_TYP, TON_1) / sizeof(SC__INSTC),160,
 161, OFFSET(FB_AC_HALFSEL_REVA_TYP, R_TRIG_1) / sizeof(SC__INSTC),162,
 161, OFFSET(FB_AC_HALFSEL_REVA_TYP, SR_1) / sizeof(SC__INSTC),163,
 161, OFFSET(FB_AC_HALFSEL_REVA_TYP, TON_1) / sizeof(SC__INSTC),164,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, FB_FILTER_1) / sizeof(SC__INSTC),166,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, FB_FILTER_2) / sizeof(SC__INSTC),167,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, TON_1) / sizeof(SC__INSTC),168,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, RS_1) / sizeof(SC__INSTC),169,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, RS_2) / sizeof(SC__INSTC),170,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, TON_2) / sizeof(SC__INSTC),171,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, FB_GETTEMP_1) / sizeof(SC__INSTC),172,
 165, OFFSET(FB_AC_GETTEMP_REVA_TYP, FB_GETTEMP_2) / sizeof(SC__INSTC),174,
 172, OFFSET(FB_GETTEMP_TYP, FB_FILTER_1) / sizeof(SC__INSTC),173,
 174, OFFSET(FB_GETTEMP_TYP, FB_FILTER_1) / sizeof(SC__INSTC),175,
 93, OFFSET(P_AC_MODE_TYP, RS_1) / sizeof(SC__INSTC),176,
 93, OFFSET(P_AC_MODE_TYP, RS_2) / sizeof(SC__INSTC),177,
 93, OFFSET(P_AC_MODE_TYP, TON_1) / sizeof(SC__INSTC),178,
 93, OFFSET(P_AC_MODE_TYP, R_TRIG_1) / sizeof(SC__INSTC),179,
 93, OFFSET(P_AC_MODE_TYP, R_TRIG_2) / sizeof(SC__INSTC),180,
 93, OFFSET(P_AC_MODE_TYP, R_TRIG_3) / sizeof(SC__INSTC),181,
 96, OFFSET(P_COMPRESOR_TYP, FB_PRESSURE_CHECK_1) / sizeof(SC__INSTC),182,
 96, OFFSET(P_COMPRESOR_TYP, FB_TIMECOUNT_DINT_1) / sizeof(SC__INSTC),184,
 96, OFFSET(P_COMPRESOR_TYP, RS_1) / sizeof(SC__INSTC),189,
 182, OFFSET(FB_PRESSURE_CHECK_TYP, FB_FILTER_1) / sizeof(SC__INSTC),183,
 184, OFFSET(FB_TIMECOUNT_DINT_TYP, RS_1) / sizeof(SC__INSTC),185,
 184, OFFSET(FB_TIMECOUNT_DINT_TYP, TON_1) / sizeof(SC__INSTC),186,
 184, OFFSET(FB_TIMECOUNT_DINT_TYP, RS_2) / sizeof(SC__INSTC),187,
 184, OFFSET(FB_TIMECOUNT_DINT_TYP, TON_2) / sizeof(SC__INSTC),188,
 104, OFFSET(P_ULTRASON_MODE_TYP, SR_1) / sizeof(SC__INSTC),190,
 106, OFFSET(P_COM_TEST_TYP, SR_1) / sizeof(SC__INSTC),191,
 106, OFFSET(P_COM_TEST_TYP, RS_1) / sizeof(SC__INSTC),192,
 106, OFFSET(P_COM_TEST_TYP, RS_2) / sizeof(SC__INSTC),193,
 106, OFFSET(P_COM_TEST_TYP, TON_1) / sizeof(SC__INSTC),194,
 106, OFFSET(P_COM_TEST_TYP, TON_2) / sizeof(SC__INSTC),195,
 106, OFFSET(P_COM_TEST_TYP, CTU_1) / sizeof(SC__INSTC),196,
 106, OFFSET(P_COM_TEST_TYP, R_TRIG_1) / sizeof(SC__INSTC),197,
 106, OFFSET(P_COM_TEST_TYP, F_TRIG_1) / sizeof(SC__INSTC),198,
 110, OFFSET(P_CALENDER_TYP, CNV_DT_TO_STRING_1) / sizeof(SC__INSTC),199,
 110, OFFSET(P_CALENDER_TYP, CNV_STRING_TO_DINT_1) / sizeof(SC__INSTC),200,
 110, OFFSET(P_CALENDER_TYP, CNV_STRING_TO_DINT_2) / sizeof(SC__INSTC),201,
 110, OFFSET(P_CALENDER_TYP, CNV_STRING_TO_DINT_3) / sizeof(SC__INSTC),202,
 110, OFFSET(P_CALENDER_TYP, CNV_STRING_TO_DINT_4) / sizeof(SC__INSTC),203,
 110, OFFSET(P_CALENDER_TYP, CNV_STRING_TO_DINT_5) / sizeof(SC__INSTC),204,
 110, OFFSET(P_CALENDER_TYP, CNV_STRING_TO_DINT_6) / sizeof(SC__INSTC),205,
 110, OFFSET(P_CALENDER_TYP, R_TRIG_1) / sizeof(SC__INSTC),206,
 110, OFFSET(P_CALENDER_TYP, TIME_GET_RTC_1) / sizeof(SC__INSTC),207,
 110, OFFSET(P_CALENDER_TYP, CNV_DT_TO_STRING_2) / sizeof(SC__INSTC),208,
 110, OFFSET(P_CALENDER_TYP, CNV_DT_TO_STRING_3) / sizeof(SC__INSTC),209,
 122, OFFSET(P_DEADMAN_TYP, CUTOFF_TIMER1) / sizeof(SC__INSTC),210,
 122, OFFSET(P_DEADMAN_TYP, CUTOFF_TIMER2) / sizeof(SC__INSTC),211,
 122, OFFSET(P_DEADMAN_TYP, CUTOFF_TIMER3) / sizeof(SC__INSTC),212,
 122, OFFSET(P_DEADMAN_TYP, CUTOFF_TIMER4) / sizeof(SC__INSTC),213,
 122, OFFSET(P_DEADMAN_TYP, EMG_TIMER1) / sizeof(SC__INSTC),214,
 122, OFFSET(P_DEADMAN_TYP, EMG_TIMER2) / sizeof(SC__INSTC),215,
 122, OFFSET(P_DEADMAN_TYP, EMG_TIMER3) / sizeof(SC__INSTC),216,
 122, OFFSET(P_DEADMAN_TYP, EMG_TIMER4) / sizeof(SC__INSTC),217,
 122, OFFSET(P_DEADMAN_TYP, PRESSED_TIMER) / sizeof(SC__INSTC),218,
 122, OFFSET(P_DEADMAN_TYP, RELEASED_TIMER) / sizeof(SC__INSTC),219,
 122, OFFSET(P_DEADMAN_TYP, SR_BUZZER1) / sizeof(SC__INSTC),220,
 122, OFFSET(P_DEADMAN_TYP, SR_BUZZER2) / sizeof(SC__INSTC),221,
 122, OFFSET(P_DEADMAN_TYP, SR_BUZZER3) / sizeof(SC__INSTC),222,
 122, OFFSET(P_DEADMAN_TYP, SR_BUZZER4) / sizeof(SC__INSTC),223,
 126, OFFSET(P_SPEED_TYP, SR_1) / sizeof(SC__INSTC),224,
 130, OFFSET(P_BATT_TYP, TON_1) / sizeof(SC__INSTC),225,
 130, OFFSET(P_BATT_TYP, TON_2) / sizeof(SC__INSTC),226,
 130, OFFSET(P_BATT_TYP, SR_1) / sizeof(SC__INSTC),227,
 130, OFFSET(P_BATT_TYP, FB_FILTER_1) / sizeof(SC__INSTC),228,
 132, OFFSET(P_MANUALMODE_TYP, RS_1) / sizeof(SC__INSTC),229,
 -1,-1,-1};


/* ******* GLOBAL INIT FUNCTIONS ******* */
extern const SC__INSTC __IFArray[];
void GlobalInit0(int retain)
{
   /* Init of object-ids and global array's __ARRSIZE: */
   {
      int i;
      for(i=0;__IDArray[i]>=0;i+=3)
      {
          ((SC__INSTC*) __OD(__IDArray[i]))[__IDArray[i+1]] = (SC__INSTC)__IDArray[i+2];
      }
   }

   /* Call PRG/FB init functions, retain instances have bitmask 0x80000000): */
   {
      int i;
      for(i=0;__IFArray[i]>=0;i+=2)
      {
          if(retain || !(__IFArray[i+1] & 0x80000000))
          {
              ((__PFN_INIT)__OF(__IFArray[i]))(__OD(__IFArray[i+1] & 0x7FFFFFFF), retain);
          }
      }
   }

}
/* ******* DIRECT INITIALISATION OF RETAINS ******* */
/* ******* DIRECT INITIALISATION OF NON RETAINS ******* */
void GlobalInit1(int retain)
{
   {
      CABIN_T_TYP* ip = (CABIN_T_TYP*)__OD(11);
      memset(ip, 0, sizeof(CABIN_T_TYP));
      ip->XCAB1ACTIVE = SC_C_BOOL(0);
      ip->XCAB2ACTIVE = SC_C_BOOL(0);
      ip->XCABERROR = SC_C_BOOL(0);
      ip->XNEUTRAL = SC_C_BOOL(0);

   }
   {
      MASTERCTRL_T_TYP* ip = (MASTERCTRL_T_TYP*)__OD(12);
      memset(ip, 0, sizeof(MASTERCTRL_T_TYP));
      ip->XFWD = SC_C_BOOL(0);
      ip->XREV = SC_C_BOOL(0);
      ip->IPOWERING = SC_C_INT(0);
      ip->IBRAKING = SC_C_INT(0);
      ip->XFASTBRAKE = SC_C_BOOL(0);
      ip->XEB = SC_C_BOOL(0);
      ip->RPOWERING = SC_C_REAL(0.0);
      ip->RPOWER_MAX = SC_C_REAL(0.0);

   }
   {
      MANUALMODE_T_TYP* ip = (MANUALMODE_T_TYP*)__OD(13);
      memset(ip, 0, sizeof(MANUALMODE_T_TYP));
      ip->XMANUALMODE = SC_C_BOOL(0);
      ip->XTCMSFAULT = SC_C_BOOL(0);
      ip->XTCMSOK = SC_C_BOOL(0);

   }
   {
      DEADMAN_T_TYP* ip = (DEADMAN_T_TYP*)__OD(14);
      memset(ip, 0, sizeof(DEADMAN_T_TYP));
      ip->XDEADBUZZER = SC_C_BOOL(0);
      ip->XDEADTRCUTOFF = SC_C_BOOL(0);
      ip->XDEADEB = SC_C_BOOL(0);

   }
   {
      SPEED_T_TYP* ip = (SPEED_T_TYP*)__OD(15);
      memset(ip, 0, sizeof(SPEED_T_TYP));
      ip->ISPEED = SC_C_INT(0);
      ip->ILIMITSPEED = SC_C_INT(0);
      ip->XOVERSPEED = SC_C_BOOL(0);
      ip->XOVERSPEED_DO = SC_C_BOOL(0);
      ip->XZEROSPEED = SC_C_BOOL(0);
      ip->XALARMBUZZER = SC_C_BOOL(0);
      ip->XTRCUTTOFF = SC_C_BOOL(0);

   }
   {
      DOORS_T_TYP* ip = (DOORS_T_TYP*)__OD(16);
      memset(ip, 0, sizeof(DOORS_T_TYP));
      ip->XD1_OPEN = SC_C_BOOL(0);
      ip->XD2_OPEN = SC_C_BOOL(0);
      ip->XD1_LOCKED = SC_C_BOOL(0);
      ip->XD2_LOCKED = SC_C_BOOL(0);
      ip->XDALLCLOSED = SC_C_BOOL(0);
      ip->XDFAULT = SC_C_BOOL(0);
      ip->XDBAYPASS = SC_C_BOOL(0);

   }
   {
      BATT_T_TYP* ip = (BATT_T_TYP*)__OD(17);
      memset(ip, 0, sizeof(BATT_T_TYP));
      ip->XBATTOFF_CMD = SC_C_BOOL(0);
      ip->XBATTOFF_DO = SC_C_BOOL(0);
      ip->IBATTV = SC_C_INT(0);
      ip->RBATTV = SC_C_REAL(0.0);
      ip->IBATTFRAC = SC_C_INT(0);
      ip->XBATTLOW = SC_C_BOOL(0);
      ip->XEBBATTLOW = SC_C_BOOL(0);
      ip->XBATTLOW_BUZZER = SC_C_BOOL(0);
      ip->XBATTCHRFFAULT = SC_C_BOOL(0);

   }
   {
      LAMP_T_TYP* ip = (LAMP_T_TYP*)__OD(18);
      memset(ip, 0, sizeof(LAMP_T_TYP));
      ip->XROOMLAMPSTAT_1 = SC_C_BOOL(0);
      ip->XROOMLAMPSTAT_2 = SC_C_BOOL(0);
      ip->XROOMLAMPFAULT_1 = SC_C_BOOL(0);
      ip->XROOMLAMPFAULT_2 = SC_C_BOOL(0);

   }
   {
      BRAKE_T_TYP* ip = (BRAKE_T_TYP*)__OD(19);
      memset(ip, 0, sizeof(BRAKE_T_TYP));
      ip->XEMGBRAKE = SC_C_BOOL(0);
      ip->XEMGBRAKE_DO = SC_C_BOOL(0);
      ip->XEBSW = SC_C_BOOL(0);
      ip->XEBBYPASS = SC_C_BOOL(0);
      ip->XPARKINGBRK = SC_C_BOOL(0);
      ip->XALLBRAKERELEASE = SC_C_BOOL(0);
      ip->XHOLDBRAKE = SC_C_BOOL(0);
      ip->XBRAKERELEASE = SC_C_BOOL(0);
      ip->XBRAKEPRES_SW = SC_C_BOOL(0);
      ip->XBCRELEASE = SC_C_BOOL(0);
      ip->XBRAKEPIPEPRES_SW = SC_C_BOOL(0);
      ip->XDB_SW = SC_C_BOOL(0);
      ip->XDB_CUTTOUT = SC_C_BOOL(0);

   }
   {
      MODBUSBOOL_R_T_TYP* ip = (MODBUSBOOL_R_T_TYP*)__OD(20);
      memset(ip, 0, sizeof(MODBUSBOOL_R_T_TYP));
      ip->DWSTART = SC_C_DWORD(0);
      ip->XENGINE_RUNNING = SC_C_BOOL(0);
      ip->XENGINE_STOP = SC_C_BOOL(0);
      ip->XENGINE_EMG_STOP = SC_C_BOOL(0);
      ip->XMODEULTRASONIK = SC_C_BOOL(0);
      ip->XMODETRAINWASH = SC_C_BOOL(0);
      ip->XSMOKEDETECTORCAB1 = SC_C_BOOL(0);
      ip->XSMOKEDETECTORCAB2 = SC_C_BOOL(0);
      ip->XHEAD_LAMP1_ON = SC_C_BOOL(0);
      ip->XHEAD_LAMP1_FAULT = SC_C_BOOL(0);
      ip->XHEAD_LAMP2_ON = SC_C_BOOL(0);
      ip->XHEAD_LAMP2_FAULT = SC_C_BOOL(0);
      ip->XHEAD_LAMP_NOTIF_ON = SC_C_BOOL(0);
      ip->XFRWHITE = SC_C_BOOL(0);
      ip->XRRWHITE = SC_C_BOOL(0);
      ip->XFRRED = SC_C_BOOL(0);
      ip->XRRRED = SC_C_BOOL(0);
      ip->XFRGREEN = SC_C_BOOL(0);
      ip->XRRGREEN = SC_C_BOOL(0);
      ip->XFRSIDERED = SC_C_BOOL(0);
      ip->XRRSIDERED = SC_C_BOOL(0);
      ip->XPARKINGBRAKEAPPLIED = SC_C_BOOL(0);
      ip->XALLBRAKERELEASE = SC_C_BOOL(0);
      ip->XBRAKECYLINDER = SC_C_BOOL(0);
      ip->XBRAKEPIPEPRESURE = SC_C_BOOL(0);
      ip->XHOLDINGBRAKE = SC_C_BOOL(0);
      ip->XDYNAMICBRAKESW = SC_C_BOOL(0);
      ip->XEBSW = SC_C_BOOL(0);
      ip->XEBBYPASS = SC_C_BOOL(0);
      ip->XBATTLOW = SC_C_BOOL(0);
      ip->XCOMFAULT = SC_C_BOOL(0);
      ip->XCOMEMG = SC_C_BOOL(0);
      ip->XCOMBYPASS = SC_C_BOOL(0);
      ip->XCOM1_ON = SC_C_BOOL(0);
      ip->XCOM2_ON = SC_C_BOOL(0);
      ip->XCOMPRESSOR_ERROR = SC_C_BOOL(0);
      ip->XCOM_OFF = SC_C_BOOL(0);
      ip->XTRACTIONREADY = SC_C_BOOL(0);
      ip->XCAB1FRONT = SC_C_BOOL(0);
      ip->XCAB2FRONT = SC_C_BOOL(0);
      ip->XCABERROR = SC_C_BOOL(0);
      ip->XMANUALMODE = SC_C_BOOL(0);
      ip->XFORWARD = SC_C_BOOL(0);
      ip->XREVERSE = SC_C_BOOL(0);
      ip->XCAB1DIR = SC_C_BOOL(0);
      ip->XCAB2DIR = SC_C_BOOL(0);
      ip->XFASTBRAKE = SC_C_BOOL(0);
      ip->XBATTSHUTDOWN = SC_C_BOOL(0);
      ip->XDEADMANACTIVE = SC_C_BOOL(0);
      ip->XDEADMANTRCUTOFF = SC_C_BOOL(0);
      ip->XDEADMANEB = SC_C_BOOL(0);
      ip->XROOMLPSTAT_1 = SC_C_BOOL(0);
      ip->XROOMLPFAULT_1 = SC_C_BOOL(0);
      ip->XROOMLPSTAT_2 = SC_C_BOOL(0);
      ip->XROOMLPFAULT_2 = SC_C_BOOL(0);
      ip->XD1OPEN = SC_C_BOOL(0);
      ip->XD2OPEN = SC_C_BOOL(0);
      ip->XD1LOCKED = SC_C_BOOL(0);
      ip->XD2LOCKED = SC_C_BOOL(0);
      ip->XDFAULT = SC_C_BOOL(0);
      ip->XDBYPASS = SC_C_BOOL(0);
      ip->XRIOM1_DI1_0 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_1 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_2 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_3 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_4 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_5 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_6 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_7 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_8 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_9 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_10 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_11 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_12 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_13 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_14 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_15 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_16 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_17 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_18 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_19 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_20 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_21 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_22 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_23 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_24 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_25 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_26 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_27 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_28 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_29 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_30 = SC_C_BOOL(0);
      ip->XRIOM1_DI1_31 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_0 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_1 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_2 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_3 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_4 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_5 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_6 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_7 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_8 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_9 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_10 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_11 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_12 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_13 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_14 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_15 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_16 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_17 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_18 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_19 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_20 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_21 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_22 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_23 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_24 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_25 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_26 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_27 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_28 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_29 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_30 = SC_C_BOOL(0);
      ip->XRIOM1_DI2_31 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_0 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_1 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_2 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_3 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_4 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_5 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_6 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_7 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_8 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_9 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_10 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_11 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_12 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_13 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_14 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_15 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_16 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_17 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_18 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_19 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_20 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_21 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_22 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_23 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_24 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_25 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_26 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_27 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_28 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_29 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_30 = SC_C_BOOL(0);
      ip->XRIOM1_DO1_31 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_0 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_1 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_2 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_3 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_4 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_5 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_6 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_7 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_8 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_9 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_10 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_11 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_12 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_13 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_14 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_15 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_16 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_17 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_18 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_19 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_20 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_21 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_22 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_23 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_24 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_25 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_26 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_27 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_28 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_29 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_30 = SC_C_BOOL(0);
      ip->XRIOM2_DI1_31 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_0 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_1 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_2 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_3 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_4 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_5 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_6 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_7 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_8 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_9 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_10 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_11 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_12 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_13 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_14 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_15 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_16 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_17 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_18 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_19 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_20 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_21 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_22 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_23 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_24 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_25 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_26 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_27 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_28 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_29 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_30 = SC_C_BOOL(0);
      ip->XRIOM2_DI2_31 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_0 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_1 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_2 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_3 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_4 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_5 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_6 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_7 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_8 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_9 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_10 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_11 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_12 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_13 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_14 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_15 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_16 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_17 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_18 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_19 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_20 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_21 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_22 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_23 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_24 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_25 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_26 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_27 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_28 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_29 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_30 = SC_C_BOOL(0);
      ip->XRIOM2_DO1_31 = SC_C_BOOL(0);

   }
   {
      MODBUSINT_R_T_TYP* ip = (MODBUSINT_R_T_TYP*)__OD(21);
      memset(ip, 0, sizeof(MODBUSINT_R_T_TYP));
      ip->DWSTART = SC_C_DWORD(0);
      ip->ILIMITULTRASONIK = SC_C_INT(0);
      ip->IFILLINGTIME = SC_C_INT(0);
      ip->ILEAKAGETIME = SC_C_INT(0);
      ip->ICALENDAR_YYYY = SC_C_INT(0);
      ip->ICALENDAR_MM = SC_C_INT(0);
      ip->ICALENDAR_DD = SC_C_INT(0);
      ip->ICALENDAR_HH = SC_C_INT(0);
      ip->ICALENDAR_MIN = SC_C_INT(0);
      ip->ICALENDAR_SS = SC_C_INT(0);
      ip->ITRAINSPEED = SC_C_INT(0);
      ip->ITRAINSPEEDLIMIT = SC_C_INT(0);
      ip->IPOWERING = SC_C_INT(0);
      ip->IBRAKING = SC_C_INT(0);
      ip->RBATTVOLTAGE = SC_C_REAL(0.0);
      ip->ICMP1_HH = SC_C_INT(0);
      ip->ICMP2_HH = SC_C_INT(0);

   }
   {
      MODBUSBOOL_RW_T_TYP* ip = (MODBUSBOOL_RW_T_TYP*)__OD(22);
      memset(ip, 0, sizeof(MODBUSBOOL_RW_T_TYP));
      ip->DWSTART = SC_C_DWORD(0);
      ip->XCMPBAYPASS1 = SC_C_BOOL(0);
      ip->XSETDT = SC_C_BOOL(0);
      ip->XTEMPUP = SC_C_BOOL(0);
      ip->XTEMPDOWN = SC_C_BOOL(0);

   }
   {
      MODBUSINT_RW_T_TYP* ip = (MODBUSINT_RW_T_TYP*)__OD(23);
      memset(ip, 0, sizeof(MODBUSINT_RW_T_TYP));
      ip->DWSTART = SC_C_DWORD(0);
      ip->UDALARMAUTOREFRESHTIMESEC = SC_C_UDINT(0);
      ip->UIALARMINDEXONFIRSTOUTELEM = SC_C_UINT(0);
      ip->IALARMFILTERSUBSYSTEM = SC_C_INT(-1);
      ip->IALARMFILTERMINDDS = SC_C_INT(1);
      ip->IALARMFILTERMAXDDS = SC_C_INT(5000);
      ip->UDALARMFILTERMINDDSTIME = SC_C_UDINT(0);
      ip->UDALARMFILTERMAXDDSTIME = SC_C_UDINT(0);
      ip->UIALARMSORTMODE = SC_C_UINT(0);
      ip->WALARMCUSTOMATTR = SC_C_WORD(0);
      ip->UIALARMCUSTOMATTRFUNC = SC_C_UINT(0);
      ip->UIALARMDDSACKNOWLEDGED = SC_C_UINT(0);
      ip->UIALARMACKNOWLEDGEUSER = SC_C_UINT(0);
      ip->IWHEELDIAMETER = SC_C_INT(860);
      ip->ISETDD = SC_C_INT(0);
      ip->ISETMM = SC_C_INT(0);
      ip->ISETYYYY = SC_C_INT(0);
      ip->ISETHH = SC_C_INT(0);
      ip->ISETMIN = SC_C_INT(0);
      ip->ISETSS = SC_C_INT(0);
      ip->IACK = SC_C_INT(0);

   }
   {
      TRACTION_MNGMNT_TYP* ip = (TRACTION_MNGMNT_TYP*)__OD(24);
      memset(ip, 0, sizeof(TRACTION_MNGMNT_TYP));
      ip->XTRREADY = SC_C_BOOL(0);
      ip->XTRCUTTOUT = SC_C_BOOL(0);
      ip->XTRCUTOFF_DO = SC_C_BOOL(0);
      ip->XVTDCOK = SC_C_BOOL(0);

   }
   {
      COMPRESSOR_T_TYP* ip = (COMPRESSOR_T_TYP*)__OD(25);
      memset(ip, 0, sizeof(COMPRESSOR_T_TYP));
      ip->XMRPSOK = SC_C_BOOL(0);
      ip->XMRSENSOR_ERROR = SC_C_BOOL(0);
      ip->XCOMP1_ACTV = SC_C_BOOL(0);
      ip->XCOMP2_ACTV = SC_C_BOOL(0);
      ip->XCOMPFAULT = SC_C_BOOL(0);
      ip->XCOMPBYPASS = SC_C_BOOL(0);
      ip->XCOMPERR = SC_C_BOOL(0);
      ip->XCOMPOFF = SC_C_BOOL(0);
      ip->XCOMPPRESS_SW = SC_C_BOOL(0);
      ip->XEMGCOMP = SC_C_BOOL(0);
      ip->XCOMMANDON = SC_C_BOOL(0);
      ip->XPRESSURE = SC_C_INT(0);
      ip->IMRPRESSURE_1 = SC_C_INT(0);
      ip->DICMPTIME_1 = SC_C_DINT(0);
      ip->DICMPTIME_2 = SC_C_DINT(0);

   }
   {
      SIGNAL_LAMP_T_TYP* ip = (SIGNAL_LAMP_T_TYP*)__OD(26);
      memset(ip, 0, sizeof(SIGNAL_LAMP_T_TYP));
      ip->XFRONTR_WHT1_HMI = SC_C_BOOL(0);
      ip->XREARR_WHT2_HMI = SC_C_BOOL(0);
      ip->XFRONTR_RED1_HMI = SC_C_BOOL(0);
      ip->XREARR_RED2_HMI = SC_C_BOOL(0);
      ip->XFRONTR_SIDE1_HMI = SC_C_BOOL(0);
      ip->XREARR_SIDE2_HMI = SC_C_BOOL(0);
      ip->XFRONTR_GREEN_HMI = SC_C_BOOL(0);
      ip->XREARR_GREEN_HMI = SC_C_BOOL(0);
      ip->XWHITE_1 = SC_C_BOOL(0);
      ip->XWHITE_2 = SC_C_BOOL(0);
      ip->XSIDE_RED_1 = SC_C_BOOL(0);
      ip->XSIDE_RED_2 = SC_C_BOOL(0);
      ip->XGREEN_1 = SC_C_BOOL(0);
      ip->XGREEN_2 = SC_C_BOOL(0);

   }
   {
      HEAD_LAMP_T_TYP* ip = (HEAD_LAMP_T_TYP*)__OD(27);
      memset(ip, 0, sizeof(HEAD_LAMP_T_TYP));
      ip->XHEADLP_STAT_1 = SC_C_BOOL(0);
      ip->XHEADLP_STAT_2 = SC_C_BOOL(0);
      ip->XFAULTLP_STAT_1 = SC_C_BOOL(0);
      ip->XFAULTLP_STAT_2 = SC_C_BOOL(0);
      ip->XHEADLP_NOTIF = SC_C_BOOL(0);

   }
   {
      CALENDAR_T_TYP* ip = (CALENDAR_T_TYP*)__OD(28);
      memset(ip, 0, sizeof(CALENDAR_T_TYP));
      ip->UIYYYY = SC_C_UINT(0);
      ip->USMM = SC_C_USINT(0);
      ip->USDD = SC_C_USINT(0);
      ip->USHH = SC_C_USINT(0);
      ip->USMIN = SC_C_USINT(0);
      ip->USSS = SC_C_USINT(0);
      ip->IDAYOFWK = SC_C_INT(0);
      ip->DTDATETIME = SC_C_DT(694224000);
      INIT_STRING((SC_STRING*)&ip->SDATEANDTIME,20,0,(SC_STRING_CHAR*)"");

   }
   {
      COMPRESOR_TEST_T_TYP* ip = (COMPRESOR_TEST_T_TYP*)__OD(29);
      memset(ip, 0, sizeof(COMPRESOR_TEST_T_TYP));
      ip->XFILLINGTIME = SC_C_INT(0);
      ip->XLEAKAGECOMMAND = SC_C_INT(0);

   }
   {
      ULTRASON_MODE_T_TYP* ip = (ULTRASON_MODE_T_TYP*)__OD(30);
      memset(ip, 0, sizeof(ULTRASON_MODE_T_TYP));
      ip->XEMGULTRSN = SC_C_BOOL(0);
      ip->XULTRSN_ON = SC_C_BOOL(0);
      ip->XLIMITULTRSN = SC_C_INT(0);
      ip->XTRULTRSN_RDY = SC_C_BOOL(0);

   }
}

void GlobalInit2(int retain)
{
   {
      TRAIN_WASH_MODE_T_TYP* ip = (TRAIN_WASH_MODE_T_TYP*)__OD(31);
      memset(ip, 0, sizeof(TRAIN_WASH_MODE_T_TYP));
      ip->XTRAINWASH_ON = SC_C_BOOL(0);

   }
   {
      R1_T_TYP* ip = (R1_T_TYP*)__OD(32);
      memset(ip, 0, sizeof(R1_T_TYP));
      ip->XAC_ONCMD = SC_C_BOOL(0);
      ip->IAC_SETTEMP = SC_C_INT(0);
      ip->IAC_TEMP_1 = SC_C_INT(0);
      ip->IAC_TEMP_2 = SC_C_INT(0);
      ip->IAC_TEMP_3 = SC_C_INT(0);
      ip->IAC_TEMP_4 = SC_C_INT(0);
      ip->XAC_HALF_1 = SC_C_BOOL(0);
      ip->XAC_HALF_2 = SC_C_BOOL(0);
      ip->XAC_HALF_3 = SC_C_BOOL(0);
      ip->XAC_HALF_4 = SC_C_BOOL(0);
      ip->XAC_FULL_1 = SC_C_BOOL(0);
      ip->XAC_FULL_2 = SC_C_BOOL(0);
      ip->XAC_FULL_3 = SC_C_BOOL(0);
      ip->XAC_FULL_4 = SC_C_BOOL(0);
      ip->XEVAP1FAULT_1 = SC_C_BOOL(0);
      ip->XEVAP1FAULT_2 = SC_C_BOOL(0);
      ip->XEVAP1FAULT_3 = SC_C_BOOL(0);
      ip->XEVAP1FAULT_4 = SC_C_BOOL(0);
      ip->XEVAP2FAULT_1 = SC_C_BOOL(0);
      ip->XEVAP2FAULT_2 = SC_C_BOOL(0);
      ip->XEVAP2FAULT_3 = SC_C_BOOL(0);
      ip->XEVAP2FAULT_4 = SC_C_BOOL(0);
      ip->XCD1FAULT_1 = SC_C_BOOL(0);
      ip->XCD1FAULT_2 = SC_C_BOOL(0);
      ip->XCD1FAULT_3 = SC_C_BOOL(0);
      ip->XCD1FAULT_4 = SC_C_BOOL(0);
      ip->XCD2FAULT_1 = SC_C_BOOL(0);
      ip->XCD2FAULT_2 = SC_C_BOOL(0);
      ip->XCD2FAULT_3 = SC_C_BOOL(0);
      ip->XCD2FAULT_4 = SC_C_BOOL(0);
      ip->XCD3FAULT_1 = SC_C_BOOL(0);
      ip->XCD3FAULT_2 = SC_C_BOOL(0);
      ip->XCD3FAULT_3 = SC_C_BOOL(0);
      ip->XCD3FAULT_4 = SC_C_BOOL(0);
      ip->XCD4FAULT_1 = SC_C_BOOL(0);
      ip->XCD4FAULT_2 = SC_C_BOOL(0);
      ip->XCD4FAULT_3 = SC_C_BOOL(0);
      ip->XCD4FAULT_4 = SC_C_BOOL(0);
      ip->XCP1FAULT_1 = SC_C_BOOL(0);
      ip->XCP1FAULT_2 = SC_C_BOOL(0);
      ip->XCP1FAULT_3 = SC_C_BOOL(0);
      ip->XCP1FAULT_4 = SC_C_BOOL(0);
      ip->XCP2FAULT_1 = SC_C_BOOL(0);
      ip->XCP2FAULT_2 = SC_C_BOOL(0);
      ip->XCP2FAULT_3 = SC_C_BOOL(0);
      ip->XCP2FAULT_4 = SC_C_BOOL(0);
      ip->XCP3FAULT_1 = SC_C_BOOL(0);
      ip->XCP3FAULT_2 = SC_C_BOOL(0);
      ip->XCP3FAULT_3 = SC_C_BOOL(0);
      ip->XCP3FAULT_4 = SC_C_BOOL(0);
      ip->XCP4FAULT_1 = SC_C_BOOL(0);
      ip->XCP4FAULT_2 = SC_C_BOOL(0);
      ip->XCP4FAULT_3 = SC_C_BOOL(0);
      ip->XCP4FAULT_4 = SC_C_BOOL(0);
      ip->XEVAP1OVERTEMP_1 = SC_C_BOOL(0);
      ip->XEVAP1OVERTEMP_2 = SC_C_BOOL(0);
      ip->XEVAP1OVERTEMP_3 = SC_C_BOOL(0);
      ip->XEVAP1OVERTEMP_4 = SC_C_BOOL(0);
      ip->XEVAP2OVERTEMP_1 = SC_C_BOOL(0);
      ip->XEVAP2OVERTEMP_2 = SC_C_BOOL(0);
      ip->XEVAP2OVERTEMP_3 = SC_C_BOOL(0);
      ip->XEVAP2OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCD1OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCD1OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCD1OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCD1OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCD2OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCD2OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCD2OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCD2OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCD3OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCD3OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCD3OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCD3OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCD4OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCD4OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCD4OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCD4OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCP1OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCP1OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCP1OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCP1OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCP2OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCP2OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCP2OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCP2OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCP3OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCP3OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCP3OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCP3OVERTEMP_4 = SC_C_BOOL(0);
      ip->XCP4OVERTEMP_1 = SC_C_BOOL(0);
      ip->XCP4OVERTEMP_2 = SC_C_BOOL(0);
      ip->XCP4OVERTEMP_3 = SC_C_BOOL(0);
      ip->XCP4OVERTEMP_4 = SC_C_BOOL(0);
      ip->XENGIDLEMODECMD = SC_C_BOOL(0);
      ip->XTECU1FAULT = SC_C_BOOL(0);
      ip->XTECU2FAULT = SC_C_BOOL(0);
      ip->UIDCLINKV = SC_C_UINT(0);
      ip->UIDCLINKA = SC_C_UINT(0);
      ip->UIENG1RPM = SC_C_UINT(0);
      ip->UIENG2RPM = SC_C_UINT(0);
      ip->XENG1RUNNING = SC_C_BOOL(0);
      ip->XENG2RUNNING = SC_C_BOOL(0);
      ip->XENG1FAULT = SC_C_BOOL(0);
      ip->XENG2FAULT = SC_C_BOOL(0);

   }
   {
      ENGINE_STATUS_T_TYP* ip = (ENGINE_STATUS_T_TYP*)__OD(33);
      memset(ip, 0, sizeof(ENGINE_STATUS_T_TYP));
      ip->XENGINERUNNING = SC_C_BOOL(0);
      ip->XENGINESTOP = SC_C_BOOL(0);
      ip->XENGINEEMERGENCYSTOP = SC_C_BOOL(0);

   }
   {
      AC_T_TYP* ip = (AC_T_TYP*)__OD(34);
      memset(ip, 0, sizeof(AC_T_TYP));
      ip->XTEMPSENSORERROR_1 = SC_C_BOOL(0);
      ip->XAC_COMMAND_ON = SC_C_BOOL(0);
      ip->XAC1 = SC_C_BOOL(0);
      ip->XAC2 = SC_C_BOOL(0);
      ip->ISETTEMP_HUNDREDS = SC_C_INT(0);
      ip->XACSTARTUP = SC_C_BOOL(0);
      ip->XEVAP1_ON = SC_C_BOOL(0);
      ip->XEVAP2_ON = SC_C_BOOL(0);
      ip->XEVAP1FAULT_1 = SC_C_BOOL(0);
      ip->XEVAP2FAULT_1 = SC_C_BOOL(0);
      ip->XCD1FAULT_1 = SC_C_BOOL(0);
      ip->XCD2FAULT_1 = SC_C_BOOL(0);
      ip->XCD3FAULT_1 = SC_C_BOOL(0);
      ip->XCD4FAULT_1 = SC_C_BOOL(0);
      ip->XCP1FAULT_1 = SC_C_BOOL(0);
      ip->XCP2FAULT_1 = SC_C_BOOL(0);
      ip->XCP3FAULT_1 = SC_C_BOOL(0);
      ip->XCP4FAULT_1 = SC_C_BOOL(0);
      ip->XACSTARTUP2 = SC_C_BOOL(0);
      ip->XCD_AC1_ON = SC_C_BOOL(0);
      ip->XCD_AC2_ON = SC_C_BOOL(0);
      ip->XCP_AC1_ON = SC_C_BOOL(0);
      ip->XCP_AC2_ON = SC_C_BOOL(0);
      ip->ITEMP_1 = SC_C_INT(0);
      ip->XHALFMODE_1 = SC_C_BOOL(0);
      ip->XFULLMODE_1 = SC_C_BOOL(0);
      ip->XACERROR_1 = SC_C_BOOL(0);
      ip->DICP1TIME_1 = SC_C_DINT(0);
      ip->DICP2TIME_1 = SC_C_DINT(0);

   }
}



/* ******* MAIN GLOBAL INIT FUNCTION ******* */
void GlobalInit(int retain)
{
   GlobalInit0(retain);
   GlobalInit1(retain);
   GlobalInit2(retain);
}
