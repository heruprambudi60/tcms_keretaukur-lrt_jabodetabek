#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */

/* GLOBAL SIMPLE VARIABLES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENGINEOFF;
   SC_BOOL                    XENGINE_OFF;
}__GLOBALS_TYP;


/* GVL INITIALISATION ROUTINE: */
void SC_INIT____GLOBALS_00005(__GLOBALS_TYP *ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(__GLOBALS_TYP, XENGINE_OFF) + sizeof(ip->XENGINE_OFF);
   /* non retains: */
   ip->XENGINEOFF = SC_C_BOOL(0);
   ip->XENGINE_OFF = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
}
