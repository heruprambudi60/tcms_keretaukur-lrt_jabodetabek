#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLE1;
   SC_BOOL                    XENABLE2;
   SC_DINT*                   DITIME1;
   SC_DINT*                   DITIME2;
   SC__INSTC                  RS_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    X1MIN1;
   SC_BOOL                    X1MIN2;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_TIME                    TET;
   SC_TIME                    TET2;
}FB_TIMECOUNT_DINT_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_TIMECOUNT_DINT_00078(FB_TIMECOUNT_DINT_TYP* ip1, FB_TIMECOUNT_DINT_TYP* ip2)
{
   MOVE_ANY(&(ip1->XENABLE1), &(ip2->XENABLE1), (long) (&(ip1->DITIME1)) - (long) (&(ip1->XENABLE1)));
   MOVE_ANY(__OD(ip1->RS_1), __OD(ip2->RS_1), sizeof(RS_TYP));
   MOVE_ANY(__OD(ip1->TON_1), __OD(ip2->TON_1), sizeof(TON_TYP));
   MOVE_ANY(&(ip1->X1MIN1), &(ip2->X1MIN1), (long) (&(ip1->RS_2)) - (long) (&(ip1->X1MIN1)));
   MOVE_ANY(__OD(ip1->RS_2), __OD(ip2->RS_2), sizeof(RS_TYP));
   MOVE_ANY(__OD(ip1->TON_2), __OD(ip2->TON_2), sizeof(TON_TYP));
   MOVE_ANY(&(ip1->TET), &(ip2->TET), (long) (ip1) + sizeof(FB_TIMECOUNT_DINT_TYP) - (long) 
   (&(ip1->TET)));
}

void SC_INIT__FB_TIMECOUNT_DINT_00079(FB_TIMECOUNT_DINT_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_TIMECOUNT_DINT_TYP, TET2) + sizeof(ip->TET2);
   /* non retains: */
   ip->XENABLE1 = SC_C_BOOL(0);
   ip->XENABLE2 = SC_C_BOOL(0);
   ip->DITIME1 = NULL;
   ip->DITIME2 = NULL;
   ip->X1MIN1 = SC_C_BOOL(0);
   ip->X1MIN2 = SC_C_BOOL(0);
   ip->TET = SC_C_TIME(0);
   ip->TET2 = SC_C_TIME(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_TIMECOUNT_DINT_00065(FB_TIMECOUNT_DINT_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   /* <non> */

   FB_ENTRY(65, ip);
/******************* Network *************************/
{
   TON_TYP* __lpt0 = (TON_TYP*)__OD(ip->TON_1);
   RS_TYP* __lpt1 = (RS_TYP*)__OD(ip->RS_1);
   (*__lpt1).SET = ip->XENABLE1;
   (*__lpt1).RESET1 = ip->X1MIN1;
   RS((__lpt1));
   (*__lpt0).IN = (*__lpt1).Q1;
   (*__lpt0).PT = SC_C_TIME(60000);
   TON((__lpt0));
   ip->X1MIN1 = (*__lpt0).Q;
   ip->TET = (*__lpt0).ET;
   (*ip->DITIME1) = SEL_DINT((*__lpt0).Q,(*ip->DITIME1),((*ip->DITIME1) + SC_C_DINT(1)));

}
/******************* Network *************************/
{
   TON_TYP* __lpt0 = (TON_TYP*)__OD(ip->TON_2);
   RS_TYP* __lpt1 = (RS_TYP*)__OD(ip->RS_2);
   (*__lpt1).SET = ip->XENABLE2;
   (*__lpt1).RESET1 = ip->X1MIN2;
   RS((__lpt1));
   (*__lpt0).IN = (*__lpt1).Q1;
   (*__lpt0).PT = SC_C_TIME(60000);
   TON((__lpt0));
   ip->X1MIN2 = (*__lpt0).Q;
   ip->TET2 = (*__lpt0).ET;
   (*ip->DITIME2) = SEL_DINT((*__lpt0).Q,(*ip->DITIME2),((*ip->DITIME2) + SC_C_DINT(1)));

}
/******************* Network *************************/
{
   (*ip->DITIME1) = SEL_DINT(((*ip->DITIME1) <= SC_C_DINT(0)),(*ip->DITIME1),SC_C_DINT(0));

}
/******************* Network *************************/
{
   (*ip->DITIME2) = SEL_DINT(((*ip->DITIME2) <= SC_C_DINT(0)),(*ip->DITIME2),SC_C_DINT(0));

}

   FB_EXIT(65, ip);
}
