#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XTEMPSENSORERROR_1;
   SC_BOOL                    XAC_COMMAND_ON;
   SC_BOOL                    XAC1;
   SC_BOOL                    XAC2;
   SC_INT                     ISETTEMP_HUNDREDS;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XEVAP1_ON;
   SC_BOOL                    XEVAP2_ON;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XACSTARTUP2;
   SC_BOOL                    XCD_AC1_ON;
   SC_BOOL                    XCD_AC2_ON;
   SC_BOOL                    XCP_AC1_ON;
   SC_BOOL                    XCP_AC2_ON;
   SC_INT                     ITEMP_1;
   SC_BOOL                    XHALFMODE_1;
   SC_BOOL                    XFULLMODE_1;
   SC_BOOL                    XACERROR_1;
   SC_DINT                    DICP1TIME_1;
   SC_DINT                    DICP2TIME_1;
}AC_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XCMPBAYPASS1;
   SC_BOOL                    XSETDT;
   SC_BOOL                    XTEMPUP;
   SC_BOOL                    XTEMPDOWN;
}MODBUSBOOL_RW_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_INT                     IACSETTEMP;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_1;
   SC__INSTC                  R_TRIG_1;
   SC__INSTC                  R_TRIG_2;
   SC__INSTC                  R_TRIG_3;
}P_AC_MODE_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_AC_MODE_00092(P_AC_MODE_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_AC_MODE_TYP, R_TRIG_3) + sizeof(ip->R_TRIG_3);
   /* non retains: */
   /* <non> */

   /* retains: */
   if(iRetain)
   {
      ip->IACSETTEMP = SC_C_INT(220);

   }
}


/* PROGRAM BODY: */
void SC_BODY__P_AC_MODE_00036(void)
{
   P_AC_MODE_TYP *ip = (P_AC_MODE_TYP*) __OD(93);

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   PRG_ENTRY(36, ip);
/******************* Network *************************/
{
   R_TRIG_TYP* __lpt0 = (R_TRIG_TYP*)__OD(ip->R_TRIG_2);
   (*__lpt0).CLK = (*(MODBUSBOOL_RW_T_TYP*)__OD(22)).XTEMPUP;
   R_TRIG((__lpt0));
   ip->IACSETTEMP = SEL_INT((*__lpt0).Q,ip->IACSETTEMP,(ip->IACSETTEMP + SC_C_INT(10)));

}
/******************* Network *************************/
{
   R_TRIG_TYP* __lpt0 = (R_TRIG_TYP*)__OD(ip->R_TRIG_3);
   (*__lpt0).CLK = (*(MODBUSBOOL_RW_T_TYP*)__OD(22)).XTEMPDOWN;
   R_TRIG((__lpt0));
   ip->IACSETTEMP = SEL_INT((*__lpt0).Q,ip->IACSETTEMP,(ip->IACSETTEMP - SC_C_INT(10)));

}
/******************* Network *************************/
{
   ip->IACSETTEMP = SEL_INT((ip->IACSETTEMP < SC_C_INT(160)),ip->IACSETTEMP,SC_C_INT(160));

}
/******************* Network *************************/
{
   ip->IACSETTEMP = SEL_INT((ip->IACSETTEMP > SC_C_INT(280)),ip->IACSETTEMP,SC_C_INT(280));

}
/******************* Network *************************/
{
   (*(AC_T_TYP*)__OD(34)).ISETTEMP_HUNDREDS = ip->IACSETTEMP;

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      PRG_EXIT(36, ip);
      return;
   }

}
/******************* Network *************************/
{
   CABIN_T_TYP* __lpt0 = (CABIN_T_TYP*)__OD(11);
   RS_TYP* __lpt1 = (RS_TYP*)__OD(ip->RS_1);
   AC_T_TYP* __lpt2 = (AC_T_TYP*)__OD(34);
   TON_TYP* __lpt3 = (TON_TYP*)__OD(ip->TON_1);
   RS_TYP* __lpt4 = (RS_TYP*)__OD(ip->RS_2);
   R_TRIG_TYP* __lpt5 = (R_TRIG_TYP*)__OD(ip->R_TRIG_1);
   (*__lpt1).SET = (((1 & LD_I_X(3,1,2,29)) & (1 & (*__lpt0).XCAB1ACTIVE)) | ((1 & LD_I_X(
   3,2,2,22)) & (1 & (*__lpt0).XCAB2ACTIVE)));
   (*__lpt1).RESET1 = (((( (!((1 & LD_I_X(3,1,2,29)))) & (1 & (*__lpt0).XCAB1ACTIVE)) &  (
   !((1 & (*__lpt0).XNEUTRAL)))) | (( (!((1 & LD_I_X(3,2,2,22)))) & (1 & (*__lpt0).XCAB2ACTIVE)) 
   &  (!((1 & (*__lpt0).XNEUTRAL))))) | (1 & (*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE));
   RS((__lpt1));
   (*__lpt2).XAC_COMMAND_ON = (*__lpt1).Q1;
   (*__lpt5).CLK = (*__lpt1).Q1;
   R_TRIG((__lpt5));
   (*__lpt3).IN = (*__lpt2).XACSTARTUP;
   (*__lpt3).PT = SC_C_TIME(90000);
   TON((__lpt3));
   (*__lpt4).SET = (*__lpt5).Q;
   (*__lpt4).RESET1 = ( (!((1 & (*__lpt1).Q1))) | (1 & (*__lpt3).Q));
   RS((__lpt4));
   (*__lpt2).XACSTARTUP = (*__lpt4).Q1;

}

   PRG_EXIT(36, ip);
}
