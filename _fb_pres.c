#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;
typedef void (*__BPFN_FB_FILTER)(FB_FILTER_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WSENSOR;
   SC_REAL                    RA_FILTER;
   SC_INT                     IUPPER_HYSTERESYS;
   SC_INT                     ILOWER_HYSTERESYS1;
   SC_INT                     IPRESSURE;
   SC_BOOL                    XLOWPRESSURE;
   SC_REAL                    RANALOG;
   SC_REAL                    RFILTERED;
   SC_REAL                    RPRESSURE;
   SC_REAL                    RPRESSURE_REGRESSION;
   SC__INSTC                  FB_FILTER_1;
}FB_PRESSURE_CHECK_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_PRESSURE_CHECK_00097(FB_PRESSURE_CHECK_TYP* ip1, FB_PRESSURE_CHECK_TYP* ip2)
{
   MOVE_ANY(&(ip1->WSENSOR), &(ip2->WSENSOR), (long) (&(ip1->FB_FILTER_1)) - (long) (&(ip1->WSENSOR)));
   ((__PFN_COPY)__OF(71))(__OD(ip1->FB_FILTER_1), __OD(ip2->FB_FILTER_1));
}

void SC_INIT__FB_PRESSURE_CHECK_00098(FB_PRESSURE_CHECK_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_PRESSURE_CHECK_TYP, FB_FILTER_1) + sizeof(ip->FB_FILTER_1);
   /* non retains: */
   ip->WSENSOR = SC_C_WORD(0);
   ip->RA_FILTER = SC_C_REAL(0.9);
   ip->IUPPER_HYSTERESYS = SC_C_INT(0);
   ip->ILOWER_HYSTERESYS1 = SC_C_INT(0);
   ip->IPRESSURE = SC_C_INT(0);
   ip->XLOWPRESSURE = SC_C_BOOL(0);
   ip->RANALOG = SC_C_REAL(0.0);
   ip->RFILTERED = SC_C_REAL(0.0);
   ip->RPRESSURE = SC_C_REAL(0.0);
   ip->RPRESSURE_REGRESSION = SC_C_REAL(0.0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_PRESSURE_CHECK_00094(FB_PRESSURE_CHECK_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   /* <non> */

   FB_ENTRY(94, ip);
   ip->RANALOG = WORD_TO_REAL(ip->WSENSOR);
   (*(FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)).RA = ip->RA_FILTER;
   (*(FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)).RVAR = ip->RANALOG;
   ((__BPFN_FB_FILTER)__OF(69))(((FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)));
   ip->RFILTERED = (*(FB_FILTER_TYP*)__OD(ip->FB_FILTER_1)).RVAR_FILTERED;
   ip->RPRESSURE = (((ip->RFILTERED - SC_C_REAL(6400.0)) * SC_C_REAL(1000.0)) / SC_C_REAL(
   32000.0));
   ip->RPRESSURE_REGRESSION = ((SC_C_REAL(1.2513) * ip->RPRESSURE) + SC_C_REAL(0.0771));
   ip->IPRESSURE = REAL_TO_INT(ip->RPRESSURE_REGRESSION);

   if((ip->IPRESSURE <= SC_C_INT(600)))
   {
      ip->XLOWPRESSURE = SC_C_BOOL(1);
   }
   else if((ip->IPRESSURE > SC_C_INT(700)))
   {
      ip->XLOWPRESSURE = SC_C_BOOL(0);
   }


   FB_EXIT(94, ip);
}
