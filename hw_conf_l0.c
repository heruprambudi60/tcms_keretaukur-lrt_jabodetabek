#include "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include "hw_conf.h"

NODE_TYPE	Link0[1] = 
{
	{
	5,	/* Min_Update_Time */
	1000,	/* Max_Update_Time */
	1000,	/* Startup_Time */
	1000,	/* Min_Status_Time */
	0,	/* Min_Interrupt_Time */
	0,	/* Link */
	0,	/* Node */
	(SC_BYTE *)&O_L0_K0,
	(SC_BYTE *)&I_L0_K0,
	{
	(SC_BYTE *)&SC_MODL0K0M0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	{
	CPU83x,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	{
	0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	{
	0,
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	},
	0,	/* Nom_Node_Flag */
	0,	/* Act_Node_Flag */
	0,	/* Node_Version */
	{0},	/* Act_Modul_Type */
	{0},	/* Status_Reg */
	{0},	/* Error Reg0 */
	{0},	/* Error Reg1 */
	{0}	/* Error Reg2 */
	}
};

