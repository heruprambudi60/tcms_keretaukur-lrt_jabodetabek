#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1NFB;
   SC_BOOL                    XEVAP2NFB;
   SC_BOOL                    X3PHCOUNTER;
   SC_BOOL                    XTEMPPROTECTIONEV1;
   SC_BOOL                    XTEMPPROTECTIONEV2;
   SC_BOOL                    XACON;
   SC_BOOL                    XEVAP1ON;
   SC_BOOL                    XEVAP2ON;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  TOF_1;
   SC__INSTC                  TOF_2;
}FB_AC_EVAP_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_AC_EVAP_00084(FB_AC_EVAP_TYP* ip1, FB_AC_EVAP_TYP* ip2)
{
   MOVE_ANY(&(ip1->XEVAP1NFB), &(ip2->XEVAP1NFB), (long) (&(ip1->TOF_1)) - (long) (&(ip1->XEVAP1NFB)));
   MOVE_ANY(__OD(ip1->TOF_1), __OD(ip2->TOF_1), sizeof(TOF_TYP));
   MOVE_ANY(__OD(ip1->TOF_2), __OD(ip2->TOF_2), sizeof(TOF_TYP));
}

void SC_INIT__FB_AC_EVAP_00085(FB_AC_EVAP_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_AC_EVAP_TYP, TOF_2) + sizeof(ip->TOF_2);
   /* non retains: */
   ip->XEVAP1NFB = SC_C_BOOL(0);
   ip->XEVAP2NFB = SC_C_BOOL(0);
   ip->X3PHCOUNTER = SC_C_BOOL(0);
   ip->XTEMPPROTECTIONEV1 = SC_C_BOOL(0);
   ip->XTEMPPROTECTIONEV2 = SC_C_BOOL(0);
   ip->XACON = SC_C_BOOL(0);
   ip->XEVAP1ON = SC_C_BOOL(0);
   ip->XEVAP2ON = SC_C_BOOL(0);
   ip->XENABLECDCP = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_AC_EVAP_00061(FB_AC_EVAP_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;
   SC_BOOL                    _SCT_B1;
   SC_BOOL                    _SCT_B2;

   FB_ENTRY(61, ip);
/******************* Network *************************/
{
   TOF_TYP* __lpt0 = (TOF_TYP*)__OD(ip->TOF_2);
   TOF_TYP* __lpt1 = (TOF_TYP*)__OD(ip->TOF_1);
   _SCT_B0 = (((1 & ip->X3PHCOUNTER) & (1 & ip->XACON)) &  (!(((1 & ip->XTEMPPROTECTIONEV1) 
   & (1 & ip->XTEMPPROTECTIONEV2)))));
   _SCT_B1 = ((1 & _SCT_B0) & (1 & ip->XEVAP1NFB));
   (*__lpt1).IN = _SCT_B1;
   (*__lpt1).PT = SC_C_TIME(3000);
   TOF((__lpt1));
   ip->XEVAP1ON = (*__lpt1).Q;
   _SCT_B2 = ((1 & _SCT_B0) & (1 & ip->XEVAP2NFB));
   (*__lpt0).IN = _SCT_B2;
   (*__lpt0).PT = SC_C_TIME(3000);
   TOF((__lpt0));
   ip->XEVAP2ON = (*__lpt0).Q;
   ip->XENABLECDCP = ((1 & _SCT_B1) & (1 & _SCT_B2));

}

   FB_EXIT(61, ip);
}
