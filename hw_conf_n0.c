#include "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include "hw_conf.h"
#include "cg_types.h"

I_L1_K0_TYP	I_L1_K0 = {0};
O_L1_K0_TYP	O_L1_K0 = {0};

I_L2_K0_TYP	I_L2_K0 = {0};
O_L2_K0_TYP	O_L2_K0 = {0};

I_L3_K0_TYP	I_L3_K0 = {0};
O_L3_K0_TYP	O_L3_K0 = {0};

I_L3_K1_TYP	I_L3_K1 = {OFFSET(I_L3_K1_TYP, TP1) - OFFSET(I_L3_K1_TYP, M1)};
O_L3_K1_TYP	O_L3_K1 = {OFFSET(O_L3_K1_TYP, M_INTR3) - OFFSET(O_L3_K1_TYP, M3)};

I_L3_K2_TYP	I_L3_K2 = {OFFSET(I_L3_K2_TYP, TP1) - OFFSET(I_L3_K2_TYP, M1)};
O_L3_K2_TYP	O_L3_K2 = {OFFSET(O_L3_K2_TYP, M_INTR1) - OFFSET(O_L3_K2_TYP, M1)};

DDE731_INIT_TYP	SC_RIOM1_DDE = 
{
	sizeof(DDE731_INIT_TYP),
	PACK_ADR(3,1,0),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	26,	/* wOffsetTypeInfo */
	58,	/* wOffsetStartParam */
	338,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	12,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* wStatusInfoLAN */
	0,	/* wStatusInfoCBus1 */
	0,	/* wStatusInfoCBus2 */
	0	/* wStatusInfoCBus3 */
	,
	32,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wSWVersion */
	0,	/* wTypeInfoReserved2 */
	0,	/* wInputUsed */
	0,	/* wOutputUsed */
	10,	/* wStatusUsed */
	278,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0,	/* wHW_Revision */
	0,	/* wStopParamUsed */
	0	/* wEAN */
	,
	296	/* wSizeStartParam */
	,
	""	/* NodeAddr */
	,
	0,	/* ListenerMode */
	0	/* UploadTypeInfos */
	,
	0,	/* ComIdInput */
	20,	/* CycleTimeInput */
	50,	/* TimeoutFactorInput */
	0,	/* ErrBhvInput */
	0,	/* ComIdOutput */
	20,	/* CycleTimeOutput */
	50,	/* TimeoutFactorOutput */
	1	/* ErrBhvOutput */
	,
	2	/* wSizeStopParam */
	,
	8528,	/* wTRDP_BufferLen */
	512,	/* wTRDP_BufferLen */
	0	/* wSlaveWarning */
};

DIT732_INIT_TYP	SC_RIOM1_DI1 = 
{
	sizeof(DIT732_INIT_TYP),
	PACK_ADR(3,1,1),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	24,	/* wOffsetTypeInfo */
	52,	/* wOffsetStartParam */
	162,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	10,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wGlitch_DI_00_15 */
	0,	/* wGlitch_DI_16_31 */
	0	/* wProtection */
	,
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	4,	/* wInputUsed */
	0,	/* wOutputUsed */
	8,	/* wStatusUsed */
	108,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	110	/* wSizeStartParam */
	,
	5,	/* InhibitTimeInput */
	20,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* RisingDelay_DI_00_15 */
	0,	/* FallingDelay_DI_00_15 */
	0,	/* RisingDelay_DI_16_31 */
	0	/* FallingDelay_DI_16_31 */
	,
	0,	/* Delay_DI_00 */
	0,	/* Delay_DI_01 */
	0,	/* Delay_DI_02 */
	0,	/* Delay_DI_03 */
	0,	/* Delay_DI_04 */
	0,	/* Delay_DI_05 */
	0,	/* Delay_DI_06 */
	0,	/* Delay_DI_07 */
	0,	/* Delay_DI_08 */
	0,	/* Delay_DI_09 */
	0,	/* Delay_DI_10 */
	0,	/* Delay_DI_11 */
	0,	/* Delay_DI_12 */
	0,	/* Delay_DI_13 */
	0,	/* Delay_DI_14 */
	0	/* Delay_DI_15 */
	,
	0,	/* Delay_DI_16 */
	0,	/* Delay_DI_17 */
	0,	/* Delay_DI_18 */
	0,	/* Delay_DI_19 */
	0,	/* Delay_DI_20 */
	0,	/* Delay_DI_21 */
	0,	/* Delay_DI_22 */
	0,	/* Delay_DI_23 */
	0,	/* Delay_DI_24 */
	0,	/* Delay_DI_25 */
	0,	/* Delay_DI_26 */
	0,	/* Delay_DI_27 */
	0,	/* Delay_DI_28 */
	0,	/* Delay_DI_29 */
	0,	/* Delay_DI_30 */
	0	/* Delay_DI_31 */
	,
	0,	/* PulseEnable_DI_00_15 */
	0,	/* PulseEnable_DI_16_31 */
	65535,	/* SinglePulse_DI_00_15 */
	65535	/* SinglePulse_DI_16_31 */
	,
	10,	/* PulseFreqGrp01 */
	10,	/* PulseFreqGrp02 */
	10,	/* PulseFreqGrp03 */
	10	/* PulseFreqGrp04 */
	,
	2	/* wSizeStopParam */
};

DIT732_INIT_TYP	SC_RIOM1_DI2 = 
{
	sizeof(DIT732_INIT_TYP),
	PACK_ADR(3,1,2),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	24,	/* wOffsetTypeInfo */
	52,	/* wOffsetStartParam */
	162,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	10,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wGlitch_DI_00_15 */
	0,	/* wGlitch_DI_16_31 */
	0	/* wProtection */
	,
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	4,	/* wInputUsed */
	0,	/* wOutputUsed */
	8,	/* wStatusUsed */
	108,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	110	/* wSizeStartParam */
	,
	5,	/* InhibitTimeInput */
	20,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* RisingDelay_DI_00_15 */
	0,	/* FallingDelay_DI_00_15 */
	0,	/* RisingDelay_DI_16_31 */
	0	/* FallingDelay_DI_16_31 */
	,
	0,	/* Delay_DI_00 */
	0,	/* Delay_DI_01 */
	0,	/* Delay_DI_02 */
	0,	/* Delay_DI_03 */
	0,	/* Delay_DI_04 */
	0,	/* Delay_DI_05 */
	0,	/* Delay_DI_06 */
	0,	/* Delay_DI_07 */
	0,	/* Delay_DI_08 */
	0,	/* Delay_DI_09 */
	0,	/* Delay_DI_10 */
	0,	/* Delay_DI_11 */
	0,	/* Delay_DI_12 */
	0,	/* Delay_DI_13 */
	0,	/* Delay_DI_14 */
	0	/* Delay_DI_15 */
	,
	0,	/* Delay_DI_16 */
	0,	/* Delay_DI_17 */
	0,	/* Delay_DI_18 */
	0,	/* Delay_DI_19 */
	0,	/* Delay_DI_20 */
	0,	/* Delay_DI_21 */
	0,	/* Delay_DI_22 */
	0,	/* Delay_DI_23 */
	0,	/* Delay_DI_24 */
	0,	/* Delay_DI_25 */
	0,	/* Delay_DI_26 */
	0,	/* Delay_DI_27 */
	0,	/* Delay_DI_28 */
	0,	/* Delay_DI_29 */
	0,	/* Delay_DI_30 */
	0	/* Delay_DI_31 */
	,
	0,	/* PulseEnable_DI_00_15 */
	0,	/* PulseEnable_DI_16_31 */
	65535,	/* SinglePulse_DI_00_15 */
	65535	/* SinglePulse_DI_16_31 */
	,
	10,	/* PulseFreqGrp01 */
	10,	/* PulseFreqGrp02 */
	10,	/* PulseFreqGrp03 */
	10	/* PulseFreqGrp04 */
	,
	2	/* wSizeStopParam */
};

DOT732_INIT_TYP	SC_RIOM1_DO1 = 
{
	sizeof(DOT732_INIT_TYP),
	PACK_ADR(3,1,3),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	22,	/* wOffsetTypeInfo */
	54,	/* wOffsetStartParam */
	76,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	8,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* wStatusInfoPowerSupply */
	0,	/* bStatusInfoOverload */
	0	/* bStatusInfoFault */
	,
	32,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModuleType */
	0,	/* wModuleSWVersion */
	0,	/* wTypeInfoReserved2 */
	0,	/* wInputUsed */
	4,	/* wOutputUsed */
	6,	/* wStatusUsed */
	20,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0,	/* wHW_Revision */
	0,	/* wStopParamUsed */
	0	/* wTypeInfoReserved3 */
	,
	22	/* wSizeStartParam */
	,
	0,	/* InhibitTimeInput */
	20,	/* InhibitTimeStatus */
	0,	/* EventTimeInput */
	2000,	/* EventTimeStatus */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	5,	/* InhibitTimeOutput */
	1000,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	2	/* wSizeStopParam */
};

AIT732_INIT_TYP	SC_RIOM1_AIT_PTC = 
{
	sizeof(AIT732_INIT_TYP),
	PACK_ADR(3,1,4),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	22,	/* wOffsetTypeInfo */
	54,	/* wOffsetStartParam */
	92,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	8,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* bStatusInfoOpencircuit */
	0,	/* bStatusInfoOverload */
	0,	/* bStatusInfoFault */
	0	/* bStuffing */
	,
	32,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModuleType */
	0,	/* wModuleSWVersion */
	0,	/* wTypeInfoReserved2 */
	12,	/* wInputUsed */
	0,	/* wOutputUsed */
	6,	/* wStatusUsed */
	36,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0,	/* wHW_Revision */
	0,	/* wStopParamUsed */
	0	/* wTypeInfoReserved3 */
	,
	38	/* wSizeStartParam */
	,
	100,	/* InhibitTimeInput */
	200,	/* InhibitTimeStatus */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	0,	/* EventTimeOutput */
	0,	/* Reserved1 */
	0	/* Reserved2 */
	,
	63,	/* EnableOpencircuitDetection */
	63,	/* EnableOverloadDetection */
	1,	/* SensorTypeR0 */
	1,	/* SensorTypeR1 */
	1,	/* SensorTypeR2 */
	1,	/* SensorTypeR3 */
	1,	/* SensorTypeR4 */
	1	/* SensorTypeR5 */
	,
	2	/* wSizeStopParam */
};

AAT731_INIT_TYP	SC_RIOM1_AAT = 
{
	sizeof(AAT731_INIT_TYP),
	PACK_ADR(3,1,5),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	22,	/* wOffsetTypeInfo */
	50,	/* wOffsetStartParam */
	82,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	8,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wProtection */
	0,	/* wOverUnderFlow */
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	8,	/* wInputUsed */
	4,	/* wOutputUsed */
	6,	/* wStatusUsed */
	30,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	32	/* wSizeStartParam */
	,
	100,	/* InhibitTimeInput */
	200,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	1,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* wDeltaValue_AI_00 */
	0,	/* wDeltaValue_AI_01 */
	0,	/* wDeltaValue_AI_02 */
	0	/* wDeltaValue_AI_03 */
	,
	0	/* wOutputType */
	,
	10	/* wSizeStopParam */
	,
	0,	/* wErrorMode_AO_00 */
	0,	/* iErrorValue_AO_00 */
	0,	/* wErrorMode_AO_01 */
	0	/* iErrorValue_AO_01 */
};

DDE731_INIT_TYP	SC_RIOM2_DDE = 
{
	sizeof(DDE731_INIT_TYP),
	PACK_ADR(3,2,0),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	26,	/* wOffsetTypeInfo */
	58,	/* wOffsetStartParam */
	338,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	12,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* wStatusInfoLAN */
	0,	/* wStatusInfoCBus1 */
	0,	/* wStatusInfoCBus2 */
	0	/* wStatusInfoCBus3 */
	,
	32,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wSWVersion */
	0,	/* wTypeInfoReserved2 */
	0,	/* wInputUsed */
	0,	/* wOutputUsed */
	10,	/* wStatusUsed */
	278,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0,	/* wHW_Revision */
	0,	/* wStopParamUsed */
	0	/* wEAN */
	,
	296	/* wSizeStartParam */
	,
	""	/* NodeAddr */
	,
	0,	/* ListenerMode */
	0	/* UploadTypeInfos */
	,
	0,	/* ComIdInput */
	20,	/* CycleTimeInput */
	50,	/* TimeoutFactorInput */
	0,	/* ErrBhvInput */
	0,	/* ComIdOutput */
	20,	/* CycleTimeOutput */
	50,	/* TimeoutFactorOutput */
	1	/* ErrBhvOutput */
	,
	2	/* wSizeStopParam */
	,
	8528,	/* wTRDP_BufferLen */
	512,	/* wTRDP_BufferLen */
	0	/* wSlaveWarning */
};

DIT733_INIT_TYP	SC_RIOM2_DIT733 = 
{
	sizeof(DIT733_INIT_TYP),
	PACK_ADR(3,2,1),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	20,	/* wOffsetTypeInfo */
	48,	/* wOffsetStartParam */
	114,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	6,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wGlitch */
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	6,	/* wInputUsed */
	2,	/* wOutputUsed */
	4,	/* wStatusUsed */
	64,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	66	/* wSizeStartParam */
	,
	10,	/* InhibitTimeInput */
	20,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* RisingDelay */
	0	/* FallingDelay */
	,
	0,	/* Delay_DI_00 */
	0,	/* Delay_DI_01 */
	0,	/* Delay_DI_02 */
	0,	/* Delay_DI_03 */
	0,	/* Delay_DI_04 */
	0,	/* Delay_DI_05 */
	0,	/* Delay_DI_06 */
	0,	/* Delay_DI_07 */
	0,	/* Delay_DI_08 */
	0,	/* Delay_DI_09 */
	0,	/* Delay_DI_10 */
	0,	/* Delay_DI_11 */
	0,	/* Delay_DI_12 */
	0,	/* Delay_DI_13 */
	0,	/* Delay_DI_14 */
	0	/* Delay_DI_15 */
	,
	0,	/* ModeEncoder_0 */
	0,	/* DeltaValueEncoder_0 */
	0,	/* ModeEncoder_1 */
	0	/* DeltaValueEncoder_1 */
	,
	6	/* wSizeStopParam */
	,
	0,	/* ErrorMode */
	0	/* ErrorValue */
};

DIT732_INIT_TYP	SC_RIOM2_DI1 = 
{
	sizeof(DIT732_INIT_TYP),
	PACK_ADR(3,2,2),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	24,	/* wOffsetTypeInfo */
	52,	/* wOffsetStartParam */
	162,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	10,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wGlitch_DI_00_15 */
	0,	/* wGlitch_DI_16_31 */
	0	/* wProtection */
	,
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	4,	/* wInputUsed */
	0,	/* wOutputUsed */
	8,	/* wStatusUsed */
	108,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	110	/* wSizeStartParam */
	,
	5,	/* InhibitTimeInput */
	20,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* RisingDelay_DI_00_15 */
	0,	/* FallingDelay_DI_00_15 */
	0,	/* RisingDelay_DI_16_31 */
	0	/* FallingDelay_DI_16_31 */
	,
	0,	/* Delay_DI_00 */
	0,	/* Delay_DI_01 */
	0,	/* Delay_DI_02 */
	0,	/* Delay_DI_03 */
	0,	/* Delay_DI_04 */
	0,	/* Delay_DI_05 */
	0,	/* Delay_DI_06 */
	0,	/* Delay_DI_07 */
	0,	/* Delay_DI_08 */
	0,	/* Delay_DI_09 */
	0,	/* Delay_DI_10 */
	0,	/* Delay_DI_11 */
	0,	/* Delay_DI_12 */
	0,	/* Delay_DI_13 */
	0,	/* Delay_DI_14 */
	0	/* Delay_DI_15 */
	,
	0,	/* Delay_DI_16 */
	0,	/* Delay_DI_17 */
	0,	/* Delay_DI_18 */
	0,	/* Delay_DI_19 */
	0,	/* Delay_DI_20 */
	0,	/* Delay_DI_21 */
	0,	/* Delay_DI_22 */
	0,	/* Delay_DI_23 */
	0,	/* Delay_DI_24 */
	0,	/* Delay_DI_25 */
	0,	/* Delay_DI_26 */
	0,	/* Delay_DI_27 */
	0,	/* Delay_DI_28 */
	0,	/* Delay_DI_29 */
	0,	/* Delay_DI_30 */
	0	/* Delay_DI_31 */
	,
	0,	/* PulseEnable_DI_00_15 */
	0,	/* PulseEnable_DI_16_31 */
	65535,	/* SinglePulse_DI_00_15 */
	65535	/* SinglePulse_DI_16_31 */
	,
	10,	/* PulseFreqGrp01 */
	10,	/* PulseFreqGrp02 */
	10,	/* PulseFreqGrp03 */
	10	/* PulseFreqGrp04 */
	,
	2	/* wSizeStopParam */
};

DIT732_INIT_TYP	SC_RIOM2_DI2 = 
{
	sizeof(DIT732_INIT_TYP),
	PACK_ADR(3,2,3),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	24,	/* wOffsetTypeInfo */
	52,	/* wOffsetStartParam */
	162,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	10,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wGlitch_DI_00_15 */
	0,	/* wGlitch_DI_16_31 */
	0	/* wProtection */
	,
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	4,	/* wInputUsed */
	0,	/* wOutputUsed */
	8,	/* wStatusUsed */
	108,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	110	/* wSizeStartParam */
	,
	5,	/* InhibitTimeInput */
	20,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* RisingDelay_DI_00_15 */
	0,	/* FallingDelay_DI_00_15 */
	0,	/* RisingDelay_DI_16_31 */
	0	/* FallingDelay_DI_16_31 */
	,
	0,	/* Delay_DI_00 */
	0,	/* Delay_DI_01 */
	0,	/* Delay_DI_02 */
	0,	/* Delay_DI_03 */
	0,	/* Delay_DI_04 */
	0,	/* Delay_DI_05 */
	0,	/* Delay_DI_06 */
	0,	/* Delay_DI_07 */
	0,	/* Delay_DI_08 */
	0,	/* Delay_DI_09 */
	0,	/* Delay_DI_10 */
	0,	/* Delay_DI_11 */
	0,	/* Delay_DI_12 */
	0,	/* Delay_DI_13 */
	0,	/* Delay_DI_14 */
	0	/* Delay_DI_15 */
	,
	0,	/* Delay_DI_16 */
	0,	/* Delay_DI_17 */
	0,	/* Delay_DI_18 */
	0,	/* Delay_DI_19 */
	0,	/* Delay_DI_20 */
	0,	/* Delay_DI_21 */
	0,	/* Delay_DI_22 */
	0,	/* Delay_DI_23 */
	0,	/* Delay_DI_24 */
	0,	/* Delay_DI_25 */
	0,	/* Delay_DI_26 */
	0,	/* Delay_DI_27 */
	0,	/* Delay_DI_28 */
	0,	/* Delay_DI_29 */
	0,	/* Delay_DI_30 */
	0	/* Delay_DI_31 */
	,
	0,	/* PulseEnable_DI_00_15 */
	0,	/* PulseEnable_DI_16_31 */
	65535,	/* SinglePulse_DI_00_15 */
	65535	/* SinglePulse_DI_16_31 */
	,
	10,	/* PulseFreqGrp01 */
	10,	/* PulseFreqGrp02 */
	10,	/* PulseFreqGrp03 */
	10	/* PulseFreqGrp04 */
	,
	2	/* wSizeStopParam */
};

DOT732_INIT_TYP	SC_RIOM2_DO1 = 
{
	sizeof(DOT732_INIT_TYP),
	PACK_ADR(3,2,4),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	22,	/* wOffsetTypeInfo */
	54,	/* wOffsetStartParam */
	76,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	8,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* wStatusInfoPowerSupply */
	0,	/* bStatusInfoOverload */
	0	/* bStatusInfoFault */
	,
	32,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModuleType */
	0,	/* wModuleSWVersion */
	0,	/* wTypeInfoReserved2 */
	0,	/* wInputUsed */
	4,	/* wOutputUsed */
	6,	/* wStatusUsed */
	20,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0,	/* wHW_Revision */
	0,	/* wStopParamUsed */
	0	/* wTypeInfoReserved3 */
	,
	22	/* wSizeStartParam */
	,
	0,	/* InhibitTimeInput */
	20,	/* InhibitTimeStatus */
	0,	/* EventTimeInput */
	2000,	/* EventTimeStatus */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	5,	/* InhibitTimeOutput */
	1000,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	2	/* wSizeStopParam */
};

AIT732_INIT_TYP	SC_RIOM2_AIT_PTC = 
{
	sizeof(AIT732_INIT_TYP),
	PACK_ADR(3,2,5),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	22,	/* wOffsetTypeInfo */
	54,	/* wOffsetStartParam */
	92,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	8,	/* wSizeStatusInfo */
	0,	/* wStatusInfoGeneral */
	0,	/* bStatusInfoOpencircuit */
	0,	/* bStatusInfoOverload */
	0,	/* bStatusInfoFault */
	0	/* bStuffing */
	,
	32,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModuleType */
	0,	/* wModuleSWVersion */
	0,	/* wTypeInfoReserved2 */
	12,	/* wInputUsed */
	0,	/* wOutputUsed */
	6,	/* wStatusUsed */
	36,	/* wStartParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0,	/* wHW_Revision */
	0,	/* wStopParamUsed */
	0	/* wTypeInfoReserved3 */
	,
	38	/* wSizeStartParam */
	,
	100,	/* InhibitTimeInput */
	200,	/* InhibitTimeStatus */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	0,	/* InhibitTimeOutput */
	0,	/* EventTimeOutput */
	0,	/* Reserved1 */
	0	/* Reserved2 */
	,
	63,	/* EnableOpencircuitDetection */
	63,	/* EnableOverloadDetection */
	1,	/* SensorTypeR0 */
	1,	/* SensorTypeR1 */
	1,	/* SensorTypeR2 */
	1,	/* SensorTypeR3 */
	1,	/* SensorTypeR4 */
	1	/* SensorTypeR5 */
	,
	2	/* wSizeStopParam */
};

AAT731_INIT_TYP	SC_RIOM2_AAT = 
{
	sizeof(AAT731_INIT_TYP),
	PACK_ADR(3,2,6),
	0,	/* uiFbState */
	14,	/* wSizeSectionInfo */
	14,	/* wOffsetStatusInfo */
	22,	/* wOffsetTypeInfo */
	50,	/* wOffsetStartParam */
	82,	/* wOffsetStopParam */
	0,	/* wOffsetRes1 */
	0	/* wOffsetRes2 */
	,
	8,	/* wSizeStatusInfo */
	0,	/* wStateInfoGen */
	0,	/* wProtection */
	0,	/* wOverUnderFlow */
	28,	/* wSizeTypeInfo */
	0,	/* wTypeInfoReserved1 */
	0,	/* wModulType */
	0,	/* wModulSWVersion */
	0,	/* wTypeInfoReserved2 */
	8,	/* wInputUsed */
	4,	/* wOutputUsed */
	6,	/* wStatusUsed */
	30,	/* wParamUsed */
	0,	/* wSerialnumberLow */
	0,	/* wSerialnumberHigh */
	0,	/* wBrandCode */
	0,	/* wBootSWVersion */
	0	/* wHW_Revision */
	,
	32	/* wSizeStartParam */
	,
	100,	/* InhibitTimeInput */
	200,	/* InhibitTimeState */
	1000,	/* EventTimeInput */
	2000,	/* EventTimeState */
	500,	/* HBProducerTime */
	1250,	/* HBConsumerTime */
	1,	/* InhibitTimeOutput */
	500,	/* EventTimeOutput */
	0,	/* Param09 */
	0	/* Param10 */
	,
	0,	/* wDeltaValue_AI_00 */
	0,	/* wDeltaValue_AI_01 */
	0,	/* wDeltaValue_AI_02 */
	0	/* wDeltaValue_AI_03 */
	,
	0	/* wOutputType */
	,
	10	/* wSizeStopParam */
	,
	0,	/* wErrorMode_AO_00 */
	0,	/* iErrorValue_AO_00 */
	0,	/* wErrorMode_AO_01 */
	0	/* iErrorValue_AO_01 */
};

