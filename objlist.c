#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"

/* *************** Type definitions *************** */
typedef SC_STRINGDEF(20)      STRING__TYPE20;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_TYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_TYP;

typedef struct
{
   SC_BOOL                    XROOMLAMPSTAT_1;
   SC_BOOL                    XROOMLAMPSTAT_2;
   SC_BOOL                    XROOMLAMPFAULT_1;
   SC_BOOL                    XROOMLAMPFAULT_2;
}LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XENGINE_RUNNING;
   SC_BOOL                    XENGINE_STOP;
   SC_BOOL                    XENGINE_EMG_STOP;
   SC_BOOL                    XMODEULTRASONIK;
   SC_BOOL                    XMODETRAINWASH;
   SC_BOOL                    XSMOKEDETECTORCAB1;
   SC_BOOL                    XSMOKEDETECTORCAB2;
   SC_BOOL                    XHEAD_LAMP1_ON;
   SC_BOOL                    XHEAD_LAMP1_FAULT;
   SC_BOOL                    XHEAD_LAMP2_ON;
   SC_BOOL                    XHEAD_LAMP2_FAULT;
   SC_BOOL                    XHEAD_LAMP_NOTIF_ON;
   SC_BOOL                    XFRWHITE;
   SC_BOOL                    XRRWHITE;
   SC_BOOL                    XFRRED;
   SC_BOOL                    XRRRED;
   SC_BOOL                    XFRGREEN;
   SC_BOOL                    XRRGREEN;
   SC_BOOL                    XFRSIDERED;
   SC_BOOL                    XRRSIDERED;
   SC_BOOL                    XPARKINGBRAKEAPPLIED;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XBRAKECYLINDER;
   SC_BOOL                    XBRAKEPIPEPRESURE;
   SC_BOOL                    XHOLDINGBRAKE;
   SC_BOOL                    XDYNAMICBRAKESW;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XCOMFAULT;
   SC_BOOL                    XCOMEMG;
   SC_BOOL                    XCOMBYPASS;
   SC_BOOL                    XCOM1_ON;
   SC_BOOL                    XCOM2_ON;
   SC_BOOL                    XCOMPRESSOR_ERROR;
   SC_BOOL                    XCOM_OFF;
   SC_BOOL                    XTRACTIONREADY;
   SC_BOOL                    XCAB1FRONT;
   SC_BOOL                    XCAB2FRONT;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XFORWARD;
   SC_BOOL                    XREVERSE;
   SC_BOOL                    XCAB1DIR;
   SC_BOOL                    XCAB2DIR;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XBATTSHUTDOWN;
   SC_BOOL                    XDEADMANACTIVE;
   SC_BOOL                    XDEADMANTRCUTOFF;
   SC_BOOL                    XDEADMANEB;
   SC_BOOL                    XROOMLPSTAT_1;
   SC_BOOL                    XROOMLPFAULT_1;
   SC_BOOL                    XROOMLPSTAT_2;
   SC_BOOL                    XROOMLPFAULT_2;
   SC_BOOL                    XD1OPEN;
   SC_BOOL                    XD2OPEN;
   SC_BOOL                    XD1LOCKED;
   SC_BOOL                    XD2LOCKED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBYPASS;
   SC_BOOL                    XRIOM1_DI1_0;
   SC_BOOL                    XRIOM1_DI1_1;
   SC_BOOL                    XRIOM1_DI1_2;
   SC_BOOL                    XRIOM1_DI1_3;
   SC_BOOL                    XRIOM1_DI1_4;
   SC_BOOL                    XRIOM1_DI1_5;
   SC_BOOL                    XRIOM1_DI1_6;
   SC_BOOL                    XRIOM1_DI1_7;
   SC_BOOL                    XRIOM1_DI1_8;
   SC_BOOL                    XRIOM1_DI1_9;
   SC_BOOL                    XRIOM1_DI1_10;
   SC_BOOL                    XRIOM1_DI1_11;
   SC_BOOL                    XRIOM1_DI1_12;
   SC_BOOL                    XRIOM1_DI1_13;
   SC_BOOL                    XRIOM1_DI1_14;
   SC_BOOL                    XRIOM1_DI1_15;
   SC_BOOL                    XRIOM1_DI1_16;
   SC_BOOL                    XRIOM1_DI1_17;
   SC_BOOL                    XRIOM1_DI1_18;
   SC_BOOL                    XRIOM1_DI1_19;
   SC_BOOL                    XRIOM1_DI1_20;
   SC_BOOL                    XRIOM1_DI1_21;
   SC_BOOL                    XRIOM1_DI1_22;
   SC_BOOL                    XRIOM1_DI1_23;
   SC_BOOL                    XRIOM1_DI1_24;
   SC_BOOL                    XRIOM1_DI1_25;
   SC_BOOL                    XRIOM1_DI1_26;
   SC_BOOL                    XRIOM1_DI1_27;
   SC_BOOL                    XRIOM1_DI1_28;
   SC_BOOL                    XRIOM1_DI1_29;
   SC_BOOL                    XRIOM1_DI1_30;
   SC_BOOL                    XRIOM1_DI1_31;
   SC_BOOL                    XRIOM1_DI2_0;
   SC_BOOL                    XRIOM1_DI2_1;
   SC_BOOL                    XRIOM1_DI2_2;
   SC_BOOL                    XRIOM1_DI2_3;
   SC_BOOL                    XRIOM1_DI2_4;
   SC_BOOL                    XRIOM1_DI2_5;
   SC_BOOL                    XRIOM1_DI2_6;
   SC_BOOL                    XRIOM1_DI2_7;
   SC_BOOL                    XRIOM1_DI2_8;
   SC_BOOL                    XRIOM1_DI2_9;
   SC_BOOL                    XRIOM1_DI2_10;
   SC_BOOL                    XRIOM1_DI2_11;
   SC_BOOL                    XRIOM1_DI2_12;
   SC_BOOL                    XRIOM1_DI2_13;
   SC_BOOL                    XRIOM1_DI2_14;
   SC_BOOL                    XRIOM1_DI2_15;
   SC_BOOL                    XRIOM1_DI2_16;
   SC_BOOL                    XRIOM1_DI2_17;
   SC_BOOL                    XRIOM1_DI2_18;
   SC_BOOL                    XRIOM1_DI2_19;
   SC_BOOL                    XRIOM1_DI2_20;
   SC_BOOL                    XRIOM1_DI2_21;
   SC_BOOL                    XRIOM1_DI2_22;
   SC_BOOL                    XRIOM1_DI2_23;
   SC_BOOL                    XRIOM1_DI2_24;
   SC_BOOL                    XRIOM1_DI2_25;
   SC_BOOL                    XRIOM1_DI2_26;
   SC_BOOL                    XRIOM1_DI2_27;
   SC_BOOL                    XRIOM1_DI2_28;
   SC_BOOL                    XRIOM1_DI2_29;
   SC_BOOL                    XRIOM1_DI2_30;
   SC_BOOL                    XRIOM1_DI2_31;
   SC_BOOL                    XRIOM1_DO1_0;
   SC_BOOL                    XRIOM1_DO1_1;
   SC_BOOL                    XRIOM1_DO1_2;
   SC_BOOL                    XRIOM1_DO1_3;
   SC_BOOL                    XRIOM1_DO1_4;
   SC_BOOL                    XRIOM1_DO1_5;
   SC_BOOL                    XRIOM1_DO1_6;
   SC_BOOL                    XRIOM1_DO1_7;
   SC_BOOL                    XRIOM1_DO1_8;
   SC_BOOL                    XRIOM1_DO1_9;
   SC_BOOL                    XRIOM1_DO1_10;
   SC_BOOL                    XRIOM1_DO1_11;
   SC_BOOL                    XRIOM1_DO1_12;
   SC_BOOL                    XRIOM1_DO1_13;
   SC_BOOL                    XRIOM1_DO1_14;
   SC_BOOL                    XRIOM1_DO1_15;
   SC_BOOL                    XRIOM1_DO1_16;
   SC_BOOL                    XRIOM1_DO1_17;
   SC_BOOL                    XRIOM1_DO1_18;
   SC_BOOL                    XRIOM1_DO1_19;
   SC_BOOL                    XRIOM1_DO1_20;
   SC_BOOL                    XRIOM1_DO1_21;
   SC_BOOL                    XRIOM1_DO1_22;
   SC_BOOL                    XRIOM1_DO1_23;
   SC_BOOL                    XRIOM1_DO1_24;
   SC_BOOL                    XRIOM1_DO1_25;
   SC_BOOL                    XRIOM1_DO1_26;
   SC_BOOL                    XRIOM1_DO1_27;
   SC_BOOL                    XRIOM1_DO1_28;
   SC_BOOL                    XRIOM1_DO1_29;
   SC_BOOL                    XRIOM1_DO1_30;
   SC_BOOL                    XRIOM1_DO1_31;
   SC_BOOL                    XRIOM2_DI1_0;
   SC_BOOL                    XRIOM2_DI1_1;
   SC_BOOL                    XRIOM2_DI1_2;
   SC_BOOL                    XRIOM2_DI1_3;
   SC_BOOL                    XRIOM2_DI1_4;
   SC_BOOL                    XRIOM2_DI1_5;
   SC_BOOL                    XRIOM2_DI1_6;
   SC_BOOL                    XRIOM2_DI1_7;
   SC_BOOL                    XRIOM2_DI1_8;
   SC_BOOL                    XRIOM2_DI1_9;
   SC_BOOL                    XRIOM2_DI1_10;
   SC_BOOL                    XRIOM2_DI1_11;
   SC_BOOL                    XRIOM2_DI1_12;
   SC_BOOL                    XRIOM2_DI1_13;
   SC_BOOL                    XRIOM2_DI1_14;
   SC_BOOL                    XRIOM2_DI1_15;
   SC_BOOL                    XRIOM2_DI1_16;
   SC_BOOL                    XRIOM2_DI1_17;
   SC_BOOL                    XRIOM2_DI1_18;
   SC_BOOL                    XRIOM2_DI1_19;
   SC_BOOL                    XRIOM2_DI1_20;
   SC_BOOL                    XRIOM2_DI1_21;
   SC_BOOL                    XRIOM2_DI1_22;
   SC_BOOL                    XRIOM2_DI1_23;
   SC_BOOL                    XRIOM2_DI1_24;
   SC_BOOL                    XRIOM2_DI1_25;
   SC_BOOL                    XRIOM2_DI1_26;
   SC_BOOL                    XRIOM2_DI1_27;
   SC_BOOL                    XRIOM2_DI1_28;
   SC_BOOL                    XRIOM2_DI1_29;
   SC_BOOL                    XRIOM2_DI1_30;
   SC_BOOL                    XRIOM2_DI1_31;
   SC_BOOL                    XRIOM2_DI2_0;
   SC_BOOL                    XRIOM2_DI2_1;
   SC_BOOL                    XRIOM2_DI2_2;
   SC_BOOL                    XRIOM2_DI2_3;
   SC_BOOL                    XRIOM2_DI2_4;
   SC_BOOL                    XRIOM2_DI2_5;
   SC_BOOL                    XRIOM2_DI2_6;
   SC_BOOL                    XRIOM2_DI2_7;
   SC_BOOL                    XRIOM2_DI2_8;
   SC_BOOL                    XRIOM2_DI2_9;
   SC_BOOL                    XRIOM2_DI2_10;
   SC_BOOL                    XRIOM2_DI2_11;
   SC_BOOL                    XRIOM2_DI2_12;
   SC_BOOL                    XRIOM2_DI2_13;
   SC_BOOL                    XRIOM2_DI2_14;
   SC_BOOL                    XRIOM2_DI2_15;
   SC_BOOL                    XRIOM2_DI2_16;
   SC_BOOL                    XRIOM2_DI2_17;
   SC_BOOL                    XRIOM2_DI2_18;
   SC_BOOL                    XRIOM2_DI2_19;
   SC_BOOL                    XRIOM2_DI2_20;
   SC_BOOL                    XRIOM2_DI2_21;
   SC_BOOL                    XRIOM2_DI2_22;
   SC_BOOL                    XRIOM2_DI2_23;
   SC_BOOL                    XRIOM2_DI2_24;
   SC_BOOL                    XRIOM2_DI2_25;
   SC_BOOL                    XRIOM2_DI2_26;
   SC_BOOL                    XRIOM2_DI2_27;
   SC_BOOL                    XRIOM2_DI2_28;
   SC_BOOL                    XRIOM2_DI2_29;
   SC_BOOL                    XRIOM2_DI2_30;
   SC_BOOL                    XRIOM2_DI2_31;
   SC_BOOL                    XRIOM2_DO1_0;
   SC_BOOL                    XRIOM2_DO1_1;
   SC_BOOL                    XRIOM2_DO1_2;
   SC_BOOL                    XRIOM2_DO1_3;
   SC_BOOL                    XRIOM2_DO1_4;
   SC_BOOL                    XRIOM2_DO1_5;
   SC_BOOL                    XRIOM2_DO1_6;
   SC_BOOL                    XRIOM2_DO1_7;
   SC_BOOL                    XRIOM2_DO1_8;
   SC_BOOL                    XRIOM2_DO1_9;
   SC_BOOL                    XRIOM2_DO1_10;
   SC_BOOL                    XRIOM2_DO1_11;
   SC_BOOL                    XRIOM2_DO1_12;
   SC_BOOL                    XRIOM2_DO1_13;
   SC_BOOL                    XRIOM2_DO1_14;
   SC_BOOL                    XRIOM2_DO1_15;
   SC_BOOL                    XRIOM2_DO1_16;
   SC_BOOL                    XRIOM2_DO1_17;
   SC_BOOL                    XRIOM2_DO1_18;
   SC_BOOL                    XRIOM2_DO1_19;
   SC_BOOL                    XRIOM2_DO1_20;
   SC_BOOL                    XRIOM2_DO1_21;
   SC_BOOL                    XRIOM2_DO1_22;
   SC_BOOL                    XRIOM2_DO1_23;
   SC_BOOL                    XRIOM2_DO1_24;
   SC_BOOL                    XRIOM2_DO1_25;
   SC_BOOL                    XRIOM2_DO1_26;
   SC_BOOL                    XRIOM2_DO1_27;
   SC_BOOL                    XRIOM2_DO1_28;
   SC_BOOL                    XRIOM2_DO1_29;
   SC_BOOL                    XRIOM2_DO1_30;
   SC_BOOL                    XRIOM2_DO1_31;
}MODBUSBOOL_R_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_INT                     ILIMITULTRASONIK;
   SC_INT                     IFILLINGTIME;
   SC_INT                     ILEAKAGETIME;
   SC_INT                     ICALENDAR_YYYY;
   SC_INT                     ICALENDAR_MM;
   SC_INT                     ICALENDAR_DD;
   SC_INT                     ICALENDAR_HH;
   SC_INT                     ICALENDAR_MIN;
   SC_INT                     ICALENDAR_SS;
   SC_INT                     ITRAINSPEED;
   SC_INT                     ITRAINSPEEDLIMIT;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BYTE                    __align1[2];
   SC_REAL                    RBATTVOLTAGE;
   SC_INT                     ICMP1_HH;
   SC_INT                     ICMP2_HH;
}MODBUSINT_R_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XCMPBAYPASS1;
   SC_BOOL                    XSETDT;
   SC_BOOL                    XTEMPUP;
   SC_BOOL                    XTEMPDOWN;
}MODBUSBOOL_RW_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_UDINT                   UDALARMAUTOREFRESHTIMESEC;
   SC_UINT                    UIALARMINDEXONFIRSTOUTELEM;
   SC_INT                     IALARMFILTERSUBSYSTEM;
   SC_INT                     IALARMFILTERMINDDS;
   SC_INT                     IALARMFILTERMAXDDS;
   SC_UDINT                   UDALARMFILTERMINDDSTIME;
   SC_UDINT                   UDALARMFILTERMAXDDSTIME;
   SC_UINT                    UIALARMSORTMODE;
   SC_WORD                    WALARMCUSTOMATTR;
   SC_UINT                    UIALARMCUSTOMATTRFUNC;
   SC_UINT                    UIALARMDDSACKNOWLEDGED;
   SC_UINT                    UIALARMACKNOWLEDGEUSER;
   SC_INT                     IWHEELDIAMETER;
   SC_INT                     ISETDD;
   SC_INT                     ISETMM;
   SC_INT                     ISETYYYY;
   SC_INT                     ISETHH;
   SC_INT                     ISETMIN;
   SC_INT                     ISETSS;
   SC_INT                     IACK;
}MODBUSINT_RW_T_TYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_TYP;

typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_TYP;

typedef struct
{
   SC_BOOL                    XFRONTR_WHT1_HMI;
   SC_BOOL                    XREARR_WHT2_HMI;
   SC_BOOL                    XFRONTR_RED1_HMI;
   SC_BOOL                    XREARR_RED2_HMI;
   SC_BOOL                    XFRONTR_SIDE1_HMI;
   SC_BOOL                    XREARR_SIDE2_HMI;
   SC_BOOL                    XFRONTR_GREEN_HMI;
   SC_BOOL                    XREARR_GREEN_HMI;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDE_RED_1;
   SC_BOOL                    XSIDE_RED_2;
   SC_BOOL                    XGREEN_1;
   SC_BOOL                    XGREEN_2;
}SIGNAL_LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XHEADLP_STAT_1;
   SC_BOOL                    XHEADLP_STAT_2;
   SC_BOOL                    XFAULTLP_STAT_1;
   SC_BOOL                    XFAULTLP_STAT_2;
   SC_BOOL                    XHEADLP_NOTIF;
}HEAD_LAMP_T_TYP;

typedef struct
{
   SC_UINT                    UIYYYY;
   SC_USINT                   USMM;
   SC_USINT                   USDD;
   SC_USINT                   USHH;
   SC_USINT                   USMIN;
   SC_USINT                   USSS;
   SC_INT                     IDAYOFWK;
   SC_DT                      DTDATETIME;
   STRING__TYPE20             SDATEANDTIME;
}CALENDAR_T_TYP;

typedef struct
{
   SC_INT                     XFILLINGTIME;
   SC_INT                     XLEAKAGECOMMAND;
}COMPRESOR_TEST_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XTRAINWASH_ON;
}TRAIN_WASH_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XAC_ONCMD;
   SC_INT                     IAC_SETTEMP;
   SC_INT                     IAC_TEMP_1;
   SC_INT                     IAC_TEMP_2;
   SC_INT                     IAC_TEMP_3;
   SC_INT                     IAC_TEMP_4;
   SC_BOOL                    XAC_HALF_1;
   SC_BOOL                    XAC_HALF_2;
   SC_BOOL                    XAC_HALF_3;
   SC_BOOL                    XAC_HALF_4;
   SC_BOOL                    XAC_FULL_1;
   SC_BOOL                    XAC_FULL_2;
   SC_BOOL                    XAC_FULL_3;
   SC_BOOL                    XAC_FULL_4;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP1FAULT_2;
   SC_BOOL                    XEVAP1FAULT_3;
   SC_BOOL                    XEVAP1FAULT_4;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XEVAP2FAULT_2;
   SC_BOOL                    XEVAP2FAULT_3;
   SC_BOOL                    XEVAP2FAULT_4;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD1FAULT_2;
   SC_BOOL                    XCD1FAULT_3;
   SC_BOOL                    XCD1FAULT_4;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD2FAULT_2;
   SC_BOOL                    XCD2FAULT_3;
   SC_BOOL                    XCD2FAULT_4;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD3FAULT_2;
   SC_BOOL                    XCD3FAULT_3;
   SC_BOOL                    XCD3FAULT_4;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCD4FAULT_2;
   SC_BOOL                    XCD4FAULT_3;
   SC_BOOL                    XCD4FAULT_4;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP1FAULT_2;
   SC_BOOL                    XCP1FAULT_3;
   SC_BOOL                    XCP1FAULT_4;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP2FAULT_2;
   SC_BOOL                    XCP2FAULT_3;
   SC_BOOL                    XCP2FAULT_4;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP3FAULT_2;
   SC_BOOL                    XCP3FAULT_3;
   SC_BOOL                    XCP3FAULT_4;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XCP4FAULT_2;
   SC_BOOL                    XCP4FAULT_3;
   SC_BOOL                    XCP4FAULT_4;
   SC_BOOL                    XEVAP1OVERTEMP_1;
   SC_BOOL                    XEVAP1OVERTEMP_2;
   SC_BOOL                    XEVAP1OVERTEMP_3;
   SC_BOOL                    XEVAP1OVERTEMP_4;
   SC_BOOL                    XEVAP2OVERTEMP_1;
   SC_BOOL                    XEVAP2OVERTEMP_2;
   SC_BOOL                    XEVAP2OVERTEMP_3;
   SC_BOOL                    XEVAP2OVERTEMP_4;
   SC_BOOL                    XCD1OVERTEMP_1;
   SC_BOOL                    XCD1OVERTEMP_2;
   SC_BOOL                    XCD1OVERTEMP_3;
   SC_BOOL                    XCD1OVERTEMP_4;
   SC_BOOL                    XCD2OVERTEMP_1;
   SC_BOOL                    XCD2OVERTEMP_2;
   SC_BOOL                    XCD2OVERTEMP_3;
   SC_BOOL                    XCD2OVERTEMP_4;
   SC_BOOL                    XCD3OVERTEMP_1;
   SC_BOOL                    XCD3OVERTEMP_2;
   SC_BOOL                    XCD3OVERTEMP_3;
   SC_BOOL                    XCD3OVERTEMP_4;
   SC_BOOL                    XCD4OVERTEMP_1;
   SC_BOOL                    XCD4OVERTEMP_2;
   SC_BOOL                    XCD4OVERTEMP_3;
   SC_BOOL                    XCD4OVERTEMP_4;
   SC_BOOL                    XCP1OVERTEMP_1;
   SC_BOOL                    XCP1OVERTEMP_2;
   SC_BOOL                    XCP1OVERTEMP_3;
   SC_BOOL                    XCP1OVERTEMP_4;
   SC_BOOL                    XCP2OVERTEMP_1;
   SC_BOOL                    XCP2OVERTEMP_2;
   SC_BOOL                    XCP2OVERTEMP_3;
   SC_BOOL                    XCP2OVERTEMP_4;
   SC_BOOL                    XCP3OVERTEMP_1;
   SC_BOOL                    XCP3OVERTEMP_2;
   SC_BOOL                    XCP3OVERTEMP_3;
   SC_BOOL                    XCP3OVERTEMP_4;
   SC_BOOL                    XCP4OVERTEMP_1;
   SC_BOOL                    XCP4OVERTEMP_2;
   SC_BOOL                    XCP4OVERTEMP_3;
   SC_BOOL                    XCP4OVERTEMP_4;
   SC_BOOL                    XENGIDLEMODECMD;
   SC_BOOL                    XTECU1FAULT;
   SC_BOOL                    XTECU2FAULT;
   SC_UINT                    UIDCLINKV;
   SC_UINT                    UIDCLINKA;
   SC_UINT                    UIENG1RPM;
   SC_UINT                    UIENG2RPM;
   SC_BOOL                    XENG1RUNNING;
   SC_BOOL                    XENG2RUNNING;
   SC_BOOL                    XENG1FAULT;
   SC_BOOL                    XENG2FAULT;
}R1_T_TYP;

typedef struct
{
   SC_BOOL                    XENGINERUNNING;
   SC_BOOL                    XENGINESTOP;
   SC_BOOL                    XENGINEEMERGENCYSTOP;
}ENGINE_STATUS_T_TYP;

typedef struct
{
   SC_BOOL                    XTEMPSENSORERROR_1;
   SC_BOOL                    XAC_COMMAND_ON;
   SC_BOOL                    XAC1;
   SC_BOOL                    XAC2;
   SC_INT                     ISETTEMP_HUNDREDS;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XEVAP1_ON;
   SC_BOOL                    XEVAP2_ON;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XACSTARTUP2;
   SC_BOOL                    XCD_AC1_ON;
   SC_BOOL                    XCD_AC2_ON;
   SC_BOOL                    XCP_AC1_ON;
   SC_BOOL                    XCP_AC2_ON;
   SC_INT                     ITEMP_1;
   SC_BOOL                    XHALFMODE_1;
   SC_BOOL                    XFULLMODE_1;
   SC_BOOL                    XACERROR_1;
   SC_DINT                    DICP1TIME_1;
   SC_DINT                    DICP2TIME_1;
}AC_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENGINEOFF;
   SC_BOOL                    XENGINE_OFF;
}__GLOBALS_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1K;
   SC_BOOL                    XEVAP2K;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XTEMPPROTECTIONCDA;
   SC_BOOL                    XTEMPPROTECTIONCDB;
   SC_TIME                    TCDDELAY;
   SC_BOOL                    XCDAB_ON;
   SC__INSTC                  TON_2;
}FB_AC_CD_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XHP_CMPB;
   SC_BOOL                    XLP_CMPB;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP1_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP2_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1NFB;
   SC_BOOL                    XEVAP2NFB;
   SC_BOOL                    X3PHCOUNTER;
   SC_BOOL                    XTEMPPROTECTIONEV1;
   SC_BOOL                    XTEMPPROTECTIONEV2;
   SC_BOOL                    XACON;
   SC_BOOL                    XEVAP1ON;
   SC_BOOL                    XEVAP2ON;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  TOF_1;
   SC__INSTC                  TOF_2;
}FB_AC_EVAP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_1_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_2_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLE1;
   SC_BOOL                    XENABLE2;
   SC_DINT*                   DITIME1;
   SC_DINT*                   DITIME2;
   SC__INSTC                  RS_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    X1MIN1;
   SC_BOOL                    X1MIN2;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_TIME                    TET;
   SC_TIME                    TET2;
}FB_TIMECOUNT_DINT_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLEHALF;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_DINT                    DIAC1TIME;
   SC_DINT                    DIAC2TIME;
   SC_BOOL                    XHALF1;
   SC_BOOL                    XHALF2;
   SC__INSTC                  R_TRIG_1;
   SC_BOOL                    XERROR;
   SC__INSTC                  SR_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    XRESET;
}FB_AC_HALFSEL_REVA_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP;
   SC_REAL                    RFILTERCONSTANT;
   SC_INT                     ITEMP;
   SC__INSTC                  FB_FILTER_1;
}FB_GETTEMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP1;
   SC_WORD                    WTEMP2;
   SC_INT                     ITEMP;
   SC_BOOL                    XTEMPSENSORERROR1;
   SC_BOOL                    XTEMPSENSORERROR2;
   SC_BOOL                    XTEMPSENSORERROR;
   SC_INT                     I;
   SC_INT                     ISUM1;
   SC_INT                     ITEMP1;
   SC_INT                     ISUM2;
   SC_INT                     ITEMP1SAVED;
   SC_INT                     ITEMP2SAVED;
   SC_INT                     ITEMP2;
   SC__INSTC                  FB_FILTER_1;
   SC_REAL                    RTEMP1;
   SC_REAL                    RTEMP2;
   SC__INSTC                  FB_FILTER_2;
   SC__INSTC                  TON_1;
   SC_INT                     ITEMPORARY1;
   SC_INT                     ITEMPORARY2;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_BOOL                    XTAKE2;
   SC__INSTC                  FB_GETTEMP_1;
   SC_INT                     ITEMP1_TRY;
   SC__INSTC                  FB_GETTEMP_2;
   SC_INT                     ITEMP2_TRY;
   SC_BOOL                    XTAKE1;
}FB_AC_GETTEMP_REVA_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XPHCOUNTER_OK;
   SC__INSTC                  FB_AC_CD_1;
   SC__INSTC                  FB_AC_CD_2;
   SC__INSTC                  FB_AC_CP1;
   SC__INSTC                  FB_AC_CP2;
   SC__INSTC                  FB_AC_EVAP_1;
   SC__INSTC                  FB_AC_1_1;
   SC__INSTC                  FB_AC_2_1;
   SC__INSTC                  FB_TIMECOUNT_DINT_1;
   SC_INT                     ITEMP_AC1;
   SC_INT                     ITEMP_AC2;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XERRORTEMPSENSOR2;
   SC_BOOL                    XFULL_AC1;
   SC_BOOL                    XFULL_AC2;
   SC_BOOL                    XH1_AC1;
   SC_BOOL                    XH1_AC2;
   SC_BOOL                    XH2_AC1;
   SC_BOOL                    XH2_AC2;
   SC_BOOL                    XHALF_AC1;
   SC_BOOL                    XHALF_AC2;
   SC__INSTC                  FB_HALFSEL_AC1;
   SC__INSTC                  FB_HALFSEL_AC2;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  FB_AC_GETTEMP_REVA_1;
   SC_TIME                    TCMP1NORMAL;
   SC_TIME                    TCMP1STARTUP;
   SC_TIME                    TCMP2NORMAL;
   SC_TIME                    TCMP2STARTUP;
   SC_DINT                    DIAC1TIME_1;
   SC_DINT                    DIAC2TIME_1;
}P_AC_CABIN_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_INT                     IACSETTEMP;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_1;
   SC__INSTC                  R_TRIG_1;
   SC__INSTC                  R_TRIG_2;
   SC__INSTC                  R_TRIG_3;
}P_AC_MODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WSENSOR;
   SC_REAL                    RA_FILTER;
   SC_INT                     IUPPER_HYSTERESYS;
   SC_INT                     ILOWER_HYSTERESYS1;
   SC_INT                     IPRESSURE;
   SC_BOOL                    XLOWPRESSURE;
   SC_REAL                    RANALOG;
   SC_REAL                    RFILTERED;
   SC_REAL                    RPRESSURE;
   SC_REAL                    RPRESSURE_REGRESSION;
   SC__INSTC                  FB_FILTER_1;
}FB_PRESSURE_CHECK_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  FB_PRESSURE_CHECK_1;
   SC__INSTC                  FB_TIMECOUNT_DINT_1;
   SC__INSTC                  RS_1;
   SC_REAL                    RA;
   SC_DINT                    DICMP1TIME;
   SC_DINT                    DICMP2TIME;
}P_COMPRESOR_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_ENGINE_STATUS_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_TRAIN_WASH_MODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
}P_ULTRASON_MODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
   SC__INSTC                  CTU_1;
   SC_INT                     IMRPRESSURE;
   SC_INT                     IMRSTART;
   SC_INT                     IMRSTOP;
   SC_INT                     ILEAKKAGE_RATE;
   SC_BOOL                    XENABLEFILL;
   SC_INT                     IFILLINGTIME;
   SC_BOOL                    XFILLING;
   SC_BOOL                    XRSTFILLINGCOUNTER;
   SC_BOOL                    XLEAKAGETESTRUNNING;
   SC_BOOL                    XENABLELEAK;
   SC_BOOL                    XSTOP;
   SC__INSTC                  R_TRIG_1;
   SC__INSTC                  F_TRIG_1;
}P_COM_TEST_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XHDLP_FAULT1;
   SC_BOOL                    XHDLP_FAULT2;
   SC_BOOL                    XHDLP_STAT1;
   SC_BOOL                    XHDLP_STAT2;
}P_HEAD_LAMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  CNV_DT_TO_STRING_1;
   SC__INSTC                  CNV_STRING_TO_DINT_1;
   SC__INSTC                  CNV_STRING_TO_DINT_2;
   SC__INSTC                  CNV_STRING_TO_DINT_3;
   SC__INSTC                  CNV_STRING_TO_DINT_4;
   SC__INSTC                  CNV_STRING_TO_DINT_5;
   SC__INSTC                  CNV_STRING_TO_DINT_6;
   SC_BYTE                    IBDAY_VCU;
   SC_BYTE                    IBHOUR_VCU;
   SC_BYTE                    IBMIN_VCU;
   SC_BYTE                    IBMONTH_VCU;
   SC_BYTE                    IBSEC_VCU;
   SC_BYTE                    IBYEAR_VCU;
   SC_INT                     IERRORCNVDD;
   SC_INT                     IERRORCNVDTTOSTR;
   SC_INT                     IERRORCNVHH;
   SC_INT                     IERRORCNVMIN;
   SC_INT                     IERRORCNVMM;
   SC_INT                     IERRORCNVSS;
   SC_INT                     IERRORCNVYYYY;
   SC_INT                     IERRORSETDT;
   SC__INSTC                  R_TRIG_1;
   STRING__TYPE20             SDATETIME;
   SC_BOOL                    SXMVBSTRONG;
   SC_BOOL                    SXMVBWEAK;
   SC__INSTC                  TIME_GET_RTC_1;
   SC_UINT                    UIMS;
   SC__INSTC                  CNV_DT_TO_STRING_2;
   SC__INSTC                  CNV_DT_TO_STRING_3;
}P_CALENDER_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XFRONT_ACTV;
   SC_BOOL                    XSIDE_ACTV;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDERED_1;
   SC_BOOL                    XSIDERED_2;
   SC_BOOL                    XFRWHITE_HMI;
   SC_BOOL                    XRRWHITE_HMI;
   SC_BOOL                    XFRSIDERED_HMI;
   SC_BOOL                    XRRSIDERED_HMI;
   SC_BOOL                    XFRRED_HMI;
   SC_BOOL                    XRRRED_HMI;
}P_SIGNAL_LAMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_TRACTION_M_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_OO_HMI_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_OO_DO_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_BRAKE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  CUTOFF_TIMER1;
   SC__INSTC                  CUTOFF_TIMER2;
   SC__INSTC                  CUTOFF_TIMER3;
   SC__INSTC                  CUTOFF_TIMER4;
   SC__INSTC                  EMG_TIMER1;
   SC__INSTC                  EMG_TIMER2;
   SC__INSTC                  EMG_TIMER3;
   SC__INSTC                  EMG_TIMER4;
   SC__INSTC                  PRESSED_TIMER;
   SC__INSTC                  RELEASED_TIMER;
   SC__INSTC                  SR_BUZZER1;
   SC__INSTC                  SR_BUZZER2;
   SC__INSTC                  SR_BUZZER3;
   SC__INSTC                  SR_BUZZER4;
   SC_TIME                    TPRESS;
   SC_TIME                    TRELEASE;
   SC_BOOL                    XDEADCUTOFF1;
   SC_BOOL                    XDEADCUTOFF2;
   SC_BOOL                    XDEADBUZZER1;
   SC_BOOL                    XDEADBUZZER2;
   SC_BOOL                    XEMGDEADMAN1;
   SC_BOOL                    XEMGDEADMAN2;
   SC_BOOL                    XPRESSED;
   SC_BOOL                    XRELEASED;
}P_DEADMAN_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_LAMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
}P_SPEED_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_DOORS_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XBATTOFFSW;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
   SC__INSTC                  SR_1;
   SC_TIME                    TSWTIME;
   SC_TIME                    TOFFTIME;
   SC_REAL                    RFILTERED;
   SC_REAL                    RRES;
   SC_REAL                    RANALOG;
   SC_INT                     IANALOG;
   SC_WORD                    WANALOG;
   SC__INSTC                  FB_FILTER_1;
   SC_BOOL                    XBATTLOW;
}P_BATT_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSET_1;
   SC_BOOL                    XRESET_1;
   SC__INSTC                  RS_1;
}P_MANUALMODE_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XP2;
   SC_BOOL                    XP5;
   SC_BOOL                    XB1;
   SC_BOOL                    XB2;
   SC_BOOL                    XB3;
   SC_BOOL                    XB4;
   SC_BOOL                    XB5;
   SC_BOOL                    XB6;
   SC_BOOL                    XB7;
}P_MASTERCTRL_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_CABSES_TYP;

/* *************** Externals ********************** */
extern char ProjInfoCodeV23[];
#if defined(__BACK_TRACE__) 
   extern const char* szObjectStringTable[];
#endif

/* Body/Init/Copy Functions: */
extern void SC_INIT____GLOBALS_00005(void);
extern void SC_BODY__MY_TASK_00007(void );
extern void SC_INIT__P_AC_CABIN_00066(void *Data, int retain);
extern void SC_BODY__P_AC_CABIN_00035(void);
extern void SC_INIT__P_AC_MODE_00092(void *Data, int retain);
extern void SC_BODY__P_AC_MODE_00036(void);
extern void SC_INIT__P_COMPRESOR_00095(void *Data, int retain);
extern void SC_BODY__P_COMPRESOR_00037(void);
extern void SC_INIT__P_ENGINE_STATUS_00099(void *Data, int retain);
extern void SC_BODY__P_ENGINE_STATUS_00038(void);
extern void SC_INIT__P_TRAIN_WASH_MODE_00101(void *Data, int retain);
extern void SC_BODY__P_TRAIN_WASH_MODE_00039(void);
extern void SC_INIT__P_ULTRASON_MODE_00103(void *Data, int retain);
extern void SC_BODY__P_ULTRASON_MODE_00040(void);
extern void SC_INIT__P_COM_TEST_00105(void *Data, int retain);
extern void SC_BODY__P_COM_TEST_00041(void);
extern void SC_INIT__P_HEAD_LAMP_00107(void *Data, int retain);
extern void SC_BODY__P_HEAD_LAMP_00042(void);
extern void SC_INIT__P_CALENDER_00109(void *Data, int retain);
extern void SC_BODY__P_CALENDER_00043(void);
extern void SC_INIT__P_SIGNAL_LAMP_00111(void *Data, int retain);
extern void SC_BODY__P_SIGNAL_LAMP_00044(void);
extern void SC_INIT__P_TRACTION_M_00113(void *Data, int retain);
extern void SC_BODY__P_TRACTION_M_00045(void);
extern void SC_INIT__P_OO_HMI_00115(void *Data, int retain);
extern void SC_BODY__P_OO_HMI_00046(void);
extern void SC_INIT__P_OO_DO_00117(void *Data, int retain);
extern void SC_BODY__P_OO_DO_00047(void);
extern void SC_INIT__P_BRAKE_00119(void *Data, int retain);
extern void SC_BODY__P_BRAKE_00048(void);
extern void SC_INIT__P_DEADMAN_00121(void *Data, int retain);
extern void SC_BODY__P_DEADMAN_00049(void);
extern void SC_INIT__P_LAMP_00123(void *Data, int retain);
extern void SC_BODY__P_LAMP_00050(void);
extern void SC_INIT__P_SPEED_00125(void *Data, int retain);
extern void SC_BODY__P_SPEED_00051(void);
extern void SC_INIT__P_DOORS_00127(void *Data, int retain);
extern void SC_BODY__P_DOORS_00052(void);
extern void SC_INIT__P_BATT_00129(void *Data, int retain);
extern void SC_BODY__P_BATT_00053(void);
extern void SC_INIT__P_MANUALMODE_00131(void *Data, int retain);
extern void SC_BODY__P_MANUALMODE_00054(void);
extern void SC_INIT__P_MASTERCTRL_00133(void *Data, int retain);
extern void SC_BODY__P_MASTERCTRL_00055(void);
extern void SC_INIT__P_CABSES_00135(void *Data, int retain);
extern void SC_BODY__P_CABSES_00056(void);
extern void SC_COPY__FB_AC_CD_00090(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_CD_00091(void *Data, int retain);
extern void SC_BODY__FB_AC_CD_00062(void);
extern void SC_INIT__TON_00230(void *Data, int retain);
extern void SC_COPY__FB_AC_CP1_00088(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_CP1_00089(void *Data, int retain);
extern void SC_BODY__FB_AC_CP1_00063(void);
extern void SC_COPY__FB_AC_CP2_00086(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_CP2_00087(void *Data, int retain);
extern void SC_BODY__FB_AC_CP2_00064(void);
extern void SC_COPY__FB_AC_EVAP_00084(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_EVAP_00085(void *Data, int retain);
extern void SC_BODY__FB_AC_EVAP_00061(void);
extern void SC_INIT__TOF_00231(void *Data, int retain);
extern void SC_COPY__FB_AC_1_00082(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_1_00083(void *Data, int retain);
extern void SC_BODY__FB_AC_1_00058(void);
extern void SC_COPY__FB_AC_2_00080(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_2_00081(void *Data, int retain);
extern void SC_BODY__FB_AC_2_00059(void);
extern void SC_COPY__FB_TIMECOUNT_DINT_00078(void *fb1, void *fb2);
extern void SC_INIT__FB_TIMECOUNT_DINT_00079(void *Data, int retain);
extern void SC_BODY__FB_TIMECOUNT_DINT_00065(void);
extern void SC_INIT__RS_00232(void *Data, int retain);
extern void SC_COPY__FB_AC_HALFSEL_REVA_00076(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_HALFSEL_REVA_00077(void *Data, int retain);
extern void SC_BODY__FB_AC_HALFSEL_REVA_00060(void);
extern void SC_INIT__R_TRIG_00233(void *Data, int retain);
extern void SC_INIT__SR_00234(void *Data, int retain);
extern void SC_COPY__FB_AC_GETTEMP_REVA_00070(void *fb1, void *fb2);
extern void SC_INIT__FB_AC_GETTEMP_REVA_00073(void *Data, int retain);
extern void SC_BODY__FB_AC_GETTEMP_REVA_00057(void);
extern void SC_COPY__FB_FILTER_00071(void *fb1, void *fb2);
extern void SC_INIT__FB_FILTER_00075(void *Data, int retain);
extern void SC_BODY__FB_FILTER_00069(void);
extern void SC_COPY__FB_GETTEMP_00072(void *fb1, void *fb2);
extern void SC_INIT__FB_GETTEMP_00074(void *Data, int retain);
extern void SC_BODY__FB_GETTEMP_00068(void);
extern void SC_COPY__FB_PRESSURE_CHECK_00097(void *fb1, void *fb2);
extern void SC_INIT__FB_PRESSURE_CHECK_00098(void *Data, int retain);
extern void SC_BODY__FB_PRESSURE_CHECK_00094(void);
extern void SC_INIT__CTU_00235(void *Data, int retain);
extern void SC_INIT__F_TRIG_00236(void *Data, int retain);
extern void SC_INIT__CNV_DT_TO_STRING_00237(void *Data, int retain);
extern void SC_INIT__CNV_STRING_TO_DINT_00238(void *Data, int retain);
extern void SC_INIT__TIME_GET_RTC_00239(void *Data, int retain);

/* Instances (Data): */
extern __GLOBALS_TYP          SC_DATA____GLOBALS_00004;
extern P_AC_CABIN_TYP         SC_DATA__P_AC_CABIN_00067;
extern FB_AC_CD_TYP           SC_DATA__FB_AC_CD_00137;
extern TON_TYP                SC_DATA__TON_00138;
extern FB_AC_CD_TYP           SC_DATA__FB_AC_CD_00139;
extern TON_TYP                SC_DATA__TON_00140;
extern FB_AC_CP1_TYP          SC_DATA__FB_AC_CP1_00141;
extern TON_TYP                SC_DATA__TON_00142;
extern TON_TYP                SC_DATA__TON_00143;
extern FB_AC_CP2_TYP          SC_DATA__FB_AC_CP2_00144;
extern TON_TYP                SC_DATA__TON_00145;
extern TON_TYP                SC_DATA__TON_00146;
extern FB_AC_EVAP_TYP         SC_DATA__FB_AC_EVAP_00147;
extern TOF_TYP                SC_DATA__TOF_00148;
extern TOF_TYP                SC_DATA__TOF_00149;
extern FB_AC_1_TYP            SC_DATA__FB_AC_1_00150;
extern FB_AC_2_TYP            SC_DATA__FB_AC_2_00151;
extern FB_TIMECOUNT_DINT_TYP  SC_DATA__FB_TIMECOUNT_DINT_00152;
extern RS_TYP                 SC_DATA__RS_00153;
extern TON_TYP                SC_DATA__TON_00154;
extern RS_TYP                 SC_DATA__RS_00155;
extern TON_TYP                SC_DATA__TON_00156;
extern FB_AC_HALFSEL_REVA_TYP SC_DATA__FB_AC_HALFSEL_REVA_00157;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00158;
extern SR_TYP                 SC_DATA__SR_00159;
extern TON_TYP                SC_DATA__TON_00160;
extern FB_AC_HALFSEL_REVA_TYP SC_DATA__FB_AC_HALFSEL_REVA_00161;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00162;
extern SR_TYP                 SC_DATA__SR_00163;
extern TON_TYP                SC_DATA__TON_00164;
extern FB_AC_GETTEMP_REVA_TYP SC_DATA__FB_AC_GETTEMP_REVA_00165;
extern FB_FILTER_TYP          SC_DATA__FB_FILTER_00166;
extern FB_FILTER_TYP          SC_DATA__FB_FILTER_00167;
extern TON_TYP                SC_DATA__TON_00168;
extern RS_TYP                 SC_DATA__RS_00169;
extern RS_TYP                 SC_DATA__RS_00170;
extern TON_TYP                SC_DATA__TON_00171;
extern FB_GETTEMP_TYP         SC_DATA__FB_GETTEMP_00172;
extern FB_FILTER_TYP          SC_DATA__FB_FILTER_00173;
extern FB_GETTEMP_TYP         SC_DATA__FB_GETTEMP_00174;
extern FB_FILTER_TYP          SC_DATA__FB_FILTER_00175;
extern P_AC_MODE_TYP          SC_DATA__P_AC_MODE_00093;
extern RS_TYP                 SC_DATA__RS_00176;
extern RS_TYP                 SC_DATA__RS_00177;
extern TON_TYP                SC_DATA__TON_00178;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00179;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00180;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00181;
extern P_COMPRESOR_TYP        SC_DATA__P_COMPRESOR_00096;
extern FB_PRESSURE_CHECK_TYP  SC_DATA__FB_PRESSURE_CHECK_00182;
extern FB_FILTER_TYP          SC_DATA__FB_FILTER_00183;
extern FB_TIMECOUNT_DINT_TYP  SC_DATA__FB_TIMECOUNT_DINT_00184;
extern RS_TYP                 SC_DATA__RS_00185;
extern TON_TYP                SC_DATA__TON_00186;
extern RS_TYP                 SC_DATA__RS_00187;
extern TON_TYP                SC_DATA__TON_00188;
extern RS_TYP                 SC_DATA__RS_00189;
extern P_ENGINE_STATUS_TYP    SC_DATA__P_ENGINE_STATUS_00100;
extern P_TRAIN_WASH_MODE_TYP  SC_DATA__P_TRAIN_WASH_MODE_00102;
extern P_ULTRASON_MODE_TYP    SC_DATA__P_ULTRASON_MODE_00104;
extern SR_TYP                 SC_DATA__SR_00190;
extern P_COM_TEST_TYP         SC_DATA__P_COM_TEST_00106;
extern SR_TYP                 SC_DATA__SR_00191;
extern RS_TYP                 SC_DATA__RS_00192;
extern RS_TYP                 SC_DATA__RS_00193;
extern TON_TYP                SC_DATA__TON_00194;
extern TON_TYP                SC_DATA__TON_00195;
extern CTU_TYP                SC_DATA__CTU_00196;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00197;
extern F_TRIG_TYP             SC_DATA__F_TRIG_00198;
extern P_HEAD_LAMP_TYP        SC_DATA__P_HEAD_LAMP_00108;
extern P_CALENDER_TYP         SC_DATA__P_CALENDER_00110;
extern CNV_DT_TO_STRING_TYP   SC_DATA__CNV_DT_TO_STRING_00199;
extern CNV_STRING_TO_DINT_TYP SC_DATA__CNV_STRING_TO_DINT_00200;
extern CNV_STRING_TO_DINT_TYP SC_DATA__CNV_STRING_TO_DINT_00201;
extern CNV_STRING_TO_DINT_TYP SC_DATA__CNV_STRING_TO_DINT_00202;
extern CNV_STRING_TO_DINT_TYP SC_DATA__CNV_STRING_TO_DINT_00203;
extern CNV_STRING_TO_DINT_TYP SC_DATA__CNV_STRING_TO_DINT_00204;
extern CNV_STRING_TO_DINT_TYP SC_DATA__CNV_STRING_TO_DINT_00205;
extern R_TRIG_TYP             SC_DATA__R_TRIG_00206;
extern TIME_GET_RTC_TYP       SC_DATA__TIME_GET_RTC_00207;
extern CNV_DT_TO_STRING_TYP   SC_DATA__CNV_DT_TO_STRING_00208;
extern CNV_DT_TO_STRING_TYP   SC_DATA__CNV_DT_TO_STRING_00209;
extern P_SIGNAL_LAMP_TYP      SC_DATA__P_SIGNAL_LAMP_00112;
extern P_TRACTION_M_TYP       SC_DATA__P_TRACTION_M_00114;
extern P_OO_HMI_TYP           SC_DATA__P_OO_HMI_00116;
extern P_OO_DO_TYP            SC_DATA__P_OO_DO_00118;
extern P_BRAKE_TYP            SC_DATA__P_BRAKE_00120;
extern P_DEADMAN_TYP          SC_DATA__P_DEADMAN_00122;
extern TON_TYP                SC_DATA__TON_00210;
extern TON_TYP                SC_DATA__TON_00211;
extern TON_TYP                SC_DATA__TON_00212;
extern TON_TYP                SC_DATA__TON_00213;
extern TON_TYP                SC_DATA__TON_00214;
extern TON_TYP                SC_DATA__TON_00215;
extern TON_TYP                SC_DATA__TON_00216;
extern TON_TYP                SC_DATA__TON_00217;
extern TON_TYP                SC_DATA__TON_00218;
extern TON_TYP                SC_DATA__TON_00219;
extern SR_TYP                 SC_DATA__SR_00220;
extern SR_TYP                 SC_DATA__SR_00221;
extern SR_TYP                 SC_DATA__SR_00222;
extern SR_TYP                 SC_DATA__SR_00223;
extern P_LAMP_TYP             SC_DATA__P_LAMP_00124;
extern P_SPEED_TYP            SC_DATA__P_SPEED_00126;
extern SR_TYP                 SC_DATA__SR_00224;
extern P_DOORS_TYP            SC_DATA__P_DOORS_00128;
extern P_BATT_TYP             SC_DATA__P_BATT_00130;
extern TON_TYP                SC_DATA__TON_00225;
extern TON_TYP                SC_DATA__TON_00226;
extern SR_TYP                 SC_DATA__SR_00227;
extern FB_FILTER_TYP          SC_DATA__FB_FILTER_00228;
extern P_MANUALMODE_TYP       SC_DATA__P_MANUALMODE_00132;
extern RS_TYP                 SC_DATA__RS_00229;
extern P_MASTERCTRL_TYP       SC_DATA__P_MASTERCTRL_00134;
extern P_CABSES_TYP           SC_DATA__P_CABSES_00136;
extern CABIN_T_TYP            SC_DATA__CABIN_T_00011;
extern MASTERCTRL_T_TYP       SC_DATA__MASTERCTRL_T_00012;
extern MANUALMODE_T_TYP       SC_DATA__MANUALMODE_T_00013;
extern DEADMAN_T_TYP          SC_DATA__DEADMAN_T_00014;
extern SPEED_T_TYP            SC_DATA__SPEED_T_00015;
extern DOORS_T_TYP            SC_DATA__DOORS_T_00016;
extern BATT_T_TYP             SC_DATA__BATT_T_00017;
extern LAMP_T_TYP             SC_DATA__LAMP_T_00018;
extern BRAKE_T_TYP            SC_DATA__BRAKE_T_00019;
extern MODBUSBOOL_R_T_TYP     SC_DATA__MODBUSBOOL_R_T_00020;
extern MODBUSINT_R_T_TYP      SC_DATA__MODBUSINT_R_T_00021;
extern MODBUSBOOL_RW_T_TYP    SC_DATA__MODBUSBOOL_RW_T_00022;
extern MODBUSINT_RW_T_TYP     SC_DATA__MODBUSINT_RW_T_00023;
extern TRACTION_MNGMNT_TYP    SC_DATA__TRACTION_MNGMNT_00024;
extern COMPRESSOR_T_TYP       SC_DATA__COMPRESSOR_T_00025;
extern SIGNAL_LAMP_T_TYP      SC_DATA__SIGNAL_LAMP_T_00026;
extern HEAD_LAMP_T_TYP        SC_DATA__HEAD_LAMP_T_00027;
extern CALENDAR_T_TYP         SC_DATA__CALENDAR_T_00028;
extern COMPRESOR_TEST_T_TYP   SC_DATA__COMPRESOR_TEST_T_00029;
extern ULTRASON_MODE_T_TYP    SC_DATA__ULTRASON_MODE_T_00030;
extern TRAIN_WASH_MODE_T_TYP  SC_DATA__TRAIN_WASH_MODE_T_00031;
extern R1_T_TYP               SC_DATA__R1_T_00032;
extern ENGINE_STATUS_T_TYP    SC_DATA__ENGINE_STATUS_T_00033;
extern AC_T_TYP               SC_DATA__AC_T_00034;

/* *************** Object Directory *************** */
void* OCO__PT[32768] =
{
   /*[  0]*/ &ProjInfoCodeV23,                  //internal use
   /*[  1]*/ 0,                                 //not used
   /*[  2]*/ 0,                                 //not used
   /*[  3]*/ 0,                                 //not used
   /*[  4]*/ &SC_DATA____GLOBALS_00004,         //<GVL>.<all simple types>/__OG(<glob var name>)
   /*[  5]*/ SC_INIT____GLOBALS_00005,          //GVL init/__OF(5)
#if defined(__BACK_TRACE__)
   /*[  6]*/ szObjectStringTable,               //internal use
#else
   /*[  6]*/ 0,                                 //not used
#endif
   /*[  7]*/ SC_BODY__MY_TASK_00007,            //<tasks>.MY_Task/__OF(7)
   /*[  8]*/ 0,                                 //not used
   /*[  9]*/ 0,                                 //not used
   /*[ 10]*/ 0,                                 //not used
   /*[ 11]*/ &SC_DATA__CABIN_T_00011,           //<GVL>.gCabses_t/__OD(11)
   /*[ 12]*/ &SC_DATA__MASTERCTRL_T_00012,      //<GVL>.gMasterCtrl_t/__OD(12)
   /*[ 13]*/ &SC_DATA__MANUALMODE_T_00013,      //<GVL>.gManualMode_t/__OD(13)
   /*[ 14]*/ &SC_DATA__DEADMAN_T_00014,         //<GVL>.gDeadman_t/__OD(14)
   /*[ 15]*/ &SC_DATA__SPEED_T_00015,           //<GVL>.gSpeed_t/__OD(15)
   /*[ 16]*/ &SC_DATA__DOORS_T_00016,           //<GVL>.gDoors_t/__OD(16)
   /*[ 17]*/ &SC_DATA__BATT_T_00017,            //<GVL>.gBatt_t/__OD(17)
   /*[ 18]*/ &SC_DATA__LAMP_T_00018,            //<GVL>.gLamp_t/__OD(18)
   /*[ 19]*/ &SC_DATA__BRAKE_T_00019,           //<GVL>.gbrake_t/__OD(19)
   /*[ 20]*/ &SC_DATA__MODBUSBOOL_R_T_00020,    //<GVL>.gDiscreteInputs_t/__OD(20)
   /*[ 21]*/ &SC_DATA__MODBUSINT_R_T_00021,     //<GVL>.gInputRegisters_t/__OD(21)
   /*[ 22]*/ &SC_DATA__MODBUSBOOL_RW_T_00022,   //<GVL>.gcoils_t/__OD(22)
   /*[ 23]*/ &SC_DATA__MODBUSINT_RW_T_00023,    //<GVL>.gHoldingRegisters_t/__OD(23)
   /*[ 24]*/ &SC_DATA__TRACTION_MNGMNT_00024,   //<GVL>.gTraction_M_t/__OD(24)
   /*[ 25]*/ &SC_DATA__COMPRESSOR_T_00025,      //<GVL>.gCompressor_t/__OD(25)
   /*[ 26]*/ &SC_DATA__SIGNAL_LAMP_T_00026,     //<GVL>.gSignalLamp_t/__OD(26)
   /*[ 27]*/ &SC_DATA__HEAD_LAMP_T_00027,       //<GVL>.gHeadLamp_t/__OD(27)
   /*[ 28]*/ &SC_DATA__CALENDAR_T_00028,        //<GVL>.gCalendar_t/__OD(28)
   /*[ 29]*/ &SC_DATA__COMPRESOR_TEST_T_00029,  //<GVL>.gComTest_t/__OD(29)
   /*[ 30]*/ &SC_DATA__ULTRASON_MODE_T_00030,   //<GVL>.gUltrason_t/__OD(30)
   /*[ 31]*/ &SC_DATA__TRAIN_WASH_MODE_T_00031, //<GVL>.gTrainwsh_t/__OD(31)
   /*[ 32]*/ &SC_DATA__R1_T_00032,              //<GVL>.gll_R1_t/__OD(32)
   /*[ 33]*/ &SC_DATA__ENGINE_STATUS_T_00033,   //<GVL>.gEnginestatus_t/__OD(33)
   /*[ 34]*/ &SC_DATA__AC_T_00034,              //<GVL>.gAC_t/__OD(34)
   /*[ 35]*/ SC_BODY__P_AC_CABIN_00035,         //PRG body/__OF(35)
   /*[ 36]*/ SC_BODY__P_AC_MODE_00036,          //PRG body/__OF(36)
   /*[ 37]*/ SC_BODY__P_COMPRESOR_00037,        //PRG body/__OF(37)
   /*[ 38]*/ SC_BODY__P_ENGINE_STATUS_00038,    //PRG body/__OF(38)
   /*[ 39]*/ SC_BODY__P_TRAIN_WASH_MODE_00039,  //PRG body/__OF(39)
   /*[ 40]*/ SC_BODY__P_ULTRASON_MODE_00040,    //PRG body/__OF(40)
   /*[ 41]*/ SC_BODY__P_COM_TEST_00041,         //PRG body/__OF(41)
   /*[ 42]*/ SC_BODY__P_HEAD_LAMP_00042,        //PRG body/__OF(42)
   /*[ 43]*/ SC_BODY__P_CALENDER_00043,         //PRG body/__OF(43)
   /*[ 44]*/ SC_BODY__P_SIGNAL_LAMP_00044,      //PRG body/__OF(44)
   /*[ 45]*/ SC_BODY__P_TRACTION_M_00045,       //PRG body/__OF(45)
   /*[ 46]*/ SC_BODY__P_OO_HMI_00046,           //PRG body/__OF(46)
   /*[ 47]*/ SC_BODY__P_OO_DO_00047,            //PRG body/__OF(47)
   /*[ 48]*/ SC_BODY__P_BRAKE_00048,            //PRG body/__OF(48)
   /*[ 49]*/ SC_BODY__P_DEADMAN_00049,          //PRG body/__OF(49)
   /*[ 50]*/ SC_BODY__P_LAMP_00050,             //PRG body/__OF(50)
   /*[ 51]*/ SC_BODY__P_SPEED_00051,            //PRG body/__OF(51)
   /*[ 52]*/ SC_BODY__P_DOORS_00052,            //PRG body/__OF(52)
   /*[ 53]*/ SC_BODY__P_BATT_00053,             //PRG body/__OF(53)
   /*[ 54]*/ SC_BODY__P_MANUALMODE_00054,       //PRG body/__OF(54)
   /*[ 55]*/ SC_BODY__P_MASTERCTRL_00055,       //PRG body/__OF(55)
   /*[ 56]*/ SC_BODY__P_CABSES_00056,           //PRG body/__OF(56)
   /*[ 57]*/ SC_BODY__FB_AC_GETTEMP_REVA_00057, //FB  body/__OF(57)
   /*[ 58]*/ SC_BODY__FB_AC_1_00058,            //FB  body/__OF(58)
   /*[ 59]*/ SC_BODY__FB_AC_2_00059,            //FB  body/__OF(59)
   /*[ 60]*/ SC_BODY__FB_AC_HALFSEL_REVA_00060, //FB  body/__OF(60)
   /*[ 61]*/ SC_BODY__FB_AC_EVAP_00061,         //FB  body/__OF(61)
   /*[ 62]*/ SC_BODY__FB_AC_CD_00062,           //FB  body/__OF(62)
   /*[ 63]*/ SC_BODY__FB_AC_CP1_00063,          //FB  body/__OF(63)
   /*[ 64]*/ SC_BODY__FB_AC_CP2_00064,          //FB  body/__OF(64)
   /*[ 65]*/ SC_BODY__FB_TIMECOUNT_DINT_00065,  //FB  body/__OF(65)
   /*[ 66]*/ SC_INIT__P_AC_CABIN_00066,         //PRG init/__OF(66)
   /*[ 67]*/ &SC_DATA__P_AC_CABIN_00067,        //P_AC_CABIN/__OD(67)
   /*[ 68]*/ SC_BODY__FB_GETTEMP_00068,         //FB  body/__OF(68)
   /*[ 69]*/ SC_BODY__FB_FILTER_00069,          //FB  body/__OF(69)
   /*[ 70]*/ SC_COPY__FB_AC_GETTEMP_REVA_00070, //FB  copy/__OF(70)
   /*[ 71]*/ SC_COPY__FB_FILTER_00071,          //FB  copy/__OF(71)
   /*[ 72]*/ SC_COPY__FB_GETTEMP_00072,         //FB  copy/__OF(72)
   /*[ 73]*/ SC_INIT__FB_AC_GETTEMP_REVA_00073, //FB  init/__OF(73)
   /*[ 74]*/ SC_INIT__FB_GETTEMP_00074,         //FB  init/__OF(74)
   /*[ 75]*/ SC_INIT__FB_FILTER_00075,          //FB  init/__OF(75)
   /*[ 76]*/ SC_COPY__FB_AC_HALFSEL_REVA_00076, //FB  copy/__OF(76)
   /*[ 77]*/ SC_INIT__FB_AC_HALFSEL_REVA_00077, //FB  init/__OF(77)
   /*[ 78]*/ SC_COPY__FB_TIMECOUNT_DINT_00078,  //FB  copy/__OF(78)
   /*[ 79]*/ SC_INIT__FB_TIMECOUNT_DINT_00079,  //FB  init/__OF(79)
   /*[ 80]*/ SC_COPY__FB_AC_2_00080,            //FB  copy/__OF(80)
   /*[ 81]*/ SC_INIT__FB_AC_2_00081,            //FB  init/__OF(81)
   /*[ 82]*/ SC_COPY__FB_AC_1_00082,            //FB  copy/__OF(82)
   /*[ 83]*/ SC_INIT__FB_AC_1_00083,            //FB  init/__OF(83)
   /*[ 84]*/ SC_COPY__FB_AC_EVAP_00084,         //FB  copy/__OF(84)
   /*[ 85]*/ SC_INIT__FB_AC_EVAP_00085,         //FB  init/__OF(85)
   /*[ 86]*/ SC_COPY__FB_AC_CP2_00086,          //FB  copy/__OF(86)
   /*[ 87]*/ SC_INIT__FB_AC_CP2_00087,          //FB  init/__OF(87)
   /*[ 88]*/ SC_COPY__FB_AC_CP1_00088,          //FB  copy/__OF(88)
   /*[ 89]*/ SC_INIT__FB_AC_CP1_00089,          //FB  init/__OF(89)
   /*[ 90]*/ SC_COPY__FB_AC_CD_00090,           //FB  copy/__OF(90)
   /*[ 91]*/ SC_INIT__FB_AC_CD_00091,           //FB  init/__OF(91)
   /*[ 92]*/ SC_INIT__P_AC_MODE_00092,          //PRG init/__OF(92)
   /*[ 93]*/ &SC_DATA__P_AC_MODE_00093,         //P_AC_MODE/__OD(93)
   /*[ 94]*/ SC_BODY__FB_PRESSURE_CHECK_00094,  //FB  body/__OF(94)
   /*[ 95]*/ SC_INIT__P_COMPRESOR_00095,        //PRG init/__OF(95)
   /*[ 96]*/ &SC_DATA__P_COMPRESOR_00096,       //P_COMPRESOR/__OD(96)
   /*[ 97]*/ SC_COPY__FB_PRESSURE_CHECK_00097,  //FB  copy/__OF(97)
   /*[ 98]*/ SC_INIT__FB_PRESSURE_CHECK_00098,  //FB  init/__OF(98)
   /*[ 99]*/ SC_INIT__P_ENGINE_STATUS_00099,    //PRG init/__OF(99)
   /*[100]*/ &SC_DATA__P_ENGINE_STATUS_00100,   //P_ENGINE_STATUS/__OD(100)
   /*[101]*/ SC_INIT__P_TRAIN_WASH_MODE_00101,  //PRG init/__OF(101)
   /*[102]*/ &SC_DATA__P_TRAIN_WASH_MODE_00102, //P_TRAIN_WASH_MODE/__OD(102)
   /*[103]*/ SC_INIT__P_ULTRASON_MODE_00103,    //PRG init/__OF(103)
   /*[104]*/ &SC_DATA__P_ULTRASON_MODE_00104,   //P_ULTRASON_MODE/__OD(104)
   /*[105]*/ SC_INIT__P_COM_TEST_00105,         //PRG init/__OF(105)
   /*[106]*/ &SC_DATA__P_COM_TEST_00106,        //P_COM_TEST/__OD(106)
   /*[107]*/ SC_INIT__P_HEAD_LAMP_00107,        //PRG init/__OF(107)
   /*[108]*/ &SC_DATA__P_HEAD_LAMP_00108,       //P_HEAD_LAMP/__OD(108)
   /*[109]*/ SC_INIT__P_CALENDER_00109,         //PRG init/__OF(109)
   /*[110]*/ &SC_DATA__P_CALENDER_00110,        //P_CALENDER/__OD(110)
   /*[111]*/ SC_INIT__P_SIGNAL_LAMP_00111,      //PRG init/__OF(111)
   /*[112]*/ &SC_DATA__P_SIGNAL_LAMP_00112,     //P_SIGNAL_LAMP/__OD(112)
   /*[113]*/ SC_INIT__P_TRACTION_M_00113,       //PRG init/__OF(113)
   /*[114]*/ &SC_DATA__P_TRACTION_M_00114,      //P_TRACTION_M/__OD(114)
   /*[115]*/ SC_INIT__P_OO_HMI_00115,           //PRG init/__OF(115)
   /*[116]*/ &SC_DATA__P_OO_HMI_00116,          //P_OO_HMI/__OD(116)
   /*[117]*/ SC_INIT__P_OO_DO_00117,            //PRG init/__OF(117)
   /*[118]*/ &SC_DATA__P_OO_DO_00118,           //P_OO_DO/__OD(118)
   /*[119]*/ SC_INIT__P_BRAKE_00119,            //PRG init/__OF(119)
   /*[120]*/ &SC_DATA__P_BRAKE_00120,           //P_BRAKE/__OD(120)
   /*[121]*/ SC_INIT__P_DEADMAN_00121,          //PRG init/__OF(121)
   /*[122]*/ &SC_DATA__P_DEADMAN_00122,         //P_DEADMAN/__OD(122)
   /*[123]*/ SC_INIT__P_LAMP_00123,             //PRG init/__OF(123)
   /*[124]*/ &SC_DATA__P_LAMP_00124,            //P_LAMP/__OD(124)
   /*[125]*/ SC_INIT__P_SPEED_00125,            //PRG init/__OF(125)
   /*[126]*/ &SC_DATA__P_SPEED_00126,           //P_SPEED/__OD(126)
   /*[127]*/ SC_INIT__P_DOORS_00127,            //PRG init/__OF(127)
   /*[128]*/ &SC_DATA__P_DOORS_00128,           //P_DOORS/__OD(128)
   /*[129]*/ SC_INIT__P_BATT_00129,             //PRG init/__OF(129)
   /*[130]*/ &SC_DATA__P_BATT_00130,            //P_BATT/__OD(130)
   /*[131]*/ SC_INIT__P_MANUALMODE_00131,       //PRG init/__OF(131)
   /*[132]*/ &SC_DATA__P_MANUALMODE_00132,      //P_MANUALMODE/__OD(132)
   /*[133]*/ SC_INIT__P_MASTERCTRL_00133,       //PRG init/__OF(133)
   /*[134]*/ &SC_DATA__P_MASTERCTRL_00134,      //P_MASTERCTRL/__OD(134)
   /*[135]*/ SC_INIT__P_CABSES_00135,           //PRG init/__OF(135)
   /*[136]*/ &SC_DATA__P_CABSES_00136,          //P_CABSES/__OD(136)
   /*[137]*/ &SC_DATA__FB_AC_CD_00137,          //P_AC_CABIN.FB_AC_Cd_1/__OD(137)
   /*[138]*/ &SC_DATA__TON_00138,               //P_AC_CABIN.FB_AC_Cd_1.TON_2/__OD(138)
   /*[139]*/ &SC_DATA__FB_AC_CD_00139,          //P_AC_CABIN.FB_AC_Cd_2/__OD(139)
   /*[140]*/ &SC_DATA__TON_00140,               //P_AC_CABIN.FB_AC_Cd_2.TON_2/__OD(140)
   /*[141]*/ &SC_DATA__FB_AC_CP1_00141,         //P_AC_CABIN.FB_AC_Cp1/__OD(141)
   /*[142]*/ &SC_DATA__TON_00142,               //P_AC_CABIN.FB_AC_Cp1.TON_1/__OD(142)
   /*[143]*/ &SC_DATA__TON_00143,               //P_AC_CABIN.FB_AC_Cp1.TON_2/__OD(143)
   /*[144]*/ &SC_DATA__FB_AC_CP2_00144,         //P_AC_CABIN.FB_AC_Cp2/__OD(144)
   /*[145]*/ &SC_DATA__TON_00145,               //P_AC_CABIN.FB_AC_Cp2.TON_1/__OD(145)
   /*[146]*/ &SC_DATA__TON_00146,               //P_AC_CABIN.FB_AC_Cp2.TON_2/__OD(146)
   /*[147]*/ &SC_DATA__FB_AC_EVAP_00147,        //P_AC_CABIN.FB_AC_Evap_1/__OD(147)
   /*[148]*/ &SC_DATA__TOF_00148,               //P_AC_CABIN.FB_AC_Evap_1.TOF_1/__OD(148)
   /*[149]*/ &SC_DATA__TOF_00149,               //P_AC_CABIN.FB_AC_Evap_1.TOF_2/__OD(149)
   /*[150]*/ &SC_DATA__FB_AC_1_00150,           //P_AC_CABIN.FB_AC_1_1/__OD(150)
   /*[151]*/ &SC_DATA__FB_AC_2_00151,           //P_AC_CABIN.FB_AC_2_1/__OD(151)
   /*[152]*/ &SC_DATA__FB_TIMECOUNT_DINT_00152, //P_AC_CABIN.FB_TimeCount_DINT_1/__OD(152)
   /*[153]*/ &SC_DATA__RS_00153,                //P_AC_CABIN.FB_TimeCount_DINT_1.RS_1/__OD(153)
   /*[154]*/ &SC_DATA__TON_00154,               //P_AC_CABIN.FB_TimeCount_DINT_1.TON_1/__OD(154)
   /*[155]*/ &SC_DATA__RS_00155,                //P_AC_CABIN.FB_TimeCount_DINT_1.RS_2/__OD(155)
   /*[156]*/ &SC_DATA__TON_00156,               //P_AC_CABIN.FB_TimeCount_DINT_1.TON_2/__OD(156)
   /*[157]*/ &SC_DATA__FB_AC_HALFSEL_REVA_00157,//P_AC_CABIN.FB_HalfSel_AC1/__OD(157)
   /*[158]*/ &SC_DATA__R_TRIG_00158,            //P_AC_CABIN.FB_HalfSel_AC1.R_TRIG_1/__OD(158)
   /*[159]*/ &SC_DATA__SR_00159,                //P_AC_CABIN.FB_HalfSel_AC1.SR_1/__OD(159)
   /*[160]*/ &SC_DATA__TON_00160,               //P_AC_CABIN.FB_HalfSel_AC1.TON_1/__OD(160)
   /*[161]*/ &SC_DATA__FB_AC_HALFSEL_REVA_00161,//P_AC_CABIN.FB_HalfSel_AC2/__OD(161)
   /*[162]*/ &SC_DATA__R_TRIG_00162,            //P_AC_CABIN.FB_HalfSel_AC2.R_TRIG_1/__OD(162)
   /*[163]*/ &SC_DATA__SR_00163,                //P_AC_CABIN.FB_HalfSel_AC2.SR_1/__OD(163)
   /*[164]*/ &SC_DATA__TON_00164,               //P_AC_CABIN.FB_HalfSel_AC2.TON_1/__OD(164)
   /*[165]*/ &SC_DATA__FB_AC_GETTEMP_REVA_00165,//P_AC_CABIN.FB_AC_GetTemp_RevA_1/__OD(165)
   /*[166]*/ &SC_DATA__FB_FILTER_00166,         //P_AC_CABIN.FB_AC_GetTemp_RevA_1.FB_FILTER_1/__OD(166)
   /*[167]*/ &SC_DATA__FB_FILTER_00167,         //P_AC_CABIN.FB_AC_GetTemp_RevA_1.FB_FILTER_2/__OD(167)
   /*[168]*/ &SC_DATA__TON_00168,               //P_AC_CABIN.FB_AC_GetTemp_RevA_1.TON_1/__OD(168)
   /*[169]*/ &SC_DATA__RS_00169,                //P_AC_CABIN.FB_AC_GetTemp_RevA_1.RS_1/__OD(169)
   /*[170]*/ &SC_DATA__RS_00170,                //P_AC_CABIN.FB_AC_GetTemp_RevA_1.RS_2/__OD(170)
   /*[171]*/ &SC_DATA__TON_00171,               //P_AC_CABIN.FB_AC_GetTemp_RevA_1.TON_2/__OD(171)
   /*[172]*/ &SC_DATA__FB_GETTEMP_00172,        //P_AC_CABIN.FB_AC_GetTemp_RevA_1.FB_GetTemp_1/__OD(172)
   /*[173]*/ &SC_DATA__FB_FILTER_00173,         //P_AC_CABIN.FB_AC_GetTemp_RevA_1.FB_GetTemp_1.FB_FILTER_1/__OD(173)
   /*[174]*/ &SC_DATA__FB_GETTEMP_00174,        //P_AC_CABIN.FB_AC_GetTemp_RevA_1.FB_GetTemp_2/__OD(174)
   /*[175]*/ &SC_DATA__FB_FILTER_00175,         //P_AC_CABIN.FB_AC_GetTemp_RevA_1.FB_GetTemp_2.FB_FILTER_1/__OD(175)
   /*[176]*/ &SC_DATA__RS_00176,                //P_AC_MODE.RS_1/__OD(176)
   /*[177]*/ &SC_DATA__RS_00177,                //P_AC_MODE.RS_2/__OD(177)
   /*[178]*/ &SC_DATA__TON_00178,               //P_AC_MODE.TON_1/__OD(178)
   /*[179]*/ &SC_DATA__R_TRIG_00179,            //P_AC_MODE.R_TRIG_1/__OD(179)
   /*[180]*/ &SC_DATA__R_TRIG_00180,            //P_AC_MODE.R_TRIG_2/__OD(180)
   /*[181]*/ &SC_DATA__R_TRIG_00181,            //P_AC_MODE.R_TRIG_3/__OD(181)
   /*[182]*/ &SC_DATA__FB_PRESSURE_CHECK_00182, //P_COMPRESOR.FB_PRESSURE_CHECK_1/__OD(182)
   /*[183]*/ &SC_DATA__FB_FILTER_00183,         //P_COMPRESOR.FB_PRESSURE_CHECK_1.FB_FILTER_1/__OD(183)
   /*[184]*/ &SC_DATA__FB_TIMECOUNT_DINT_00184, //P_COMPRESOR.FB_TimeCount_DINT_1/__OD(184)
   /*[185]*/ &SC_DATA__RS_00185,                //P_COMPRESOR.FB_TimeCount_DINT_1.RS_1/__OD(185)
   /*[186]*/ &SC_DATA__TON_00186,               //P_COMPRESOR.FB_TimeCount_DINT_1.TON_1/__OD(186)
   /*[187]*/ &SC_DATA__RS_00187,                //P_COMPRESOR.FB_TimeCount_DINT_1.RS_2/__OD(187)
   /*[188]*/ &SC_DATA__TON_00188,               //P_COMPRESOR.FB_TimeCount_DINT_1.TON_2/__OD(188)
   /*[189]*/ &SC_DATA__RS_00189,                //P_COMPRESOR.RS_1/__OD(189)
   /*[190]*/ &SC_DATA__SR_00190,                //P_ULTRASON_MODE.SR_1/__OD(190)
   /*[191]*/ &SC_DATA__SR_00191,                //P_COM_TEST.SR_1/__OD(191)
   /*[192]*/ &SC_DATA__RS_00192,                //P_COM_TEST.RS_1/__OD(192)
   /*[193]*/ &SC_DATA__RS_00193,                //P_COM_TEST.RS_2/__OD(193)
   /*[194]*/ &SC_DATA__TON_00194,               //P_COM_TEST.TON_1/__OD(194)
   /*[195]*/ &SC_DATA__TON_00195,               //P_COM_TEST.TON_2/__OD(195)
   /*[196]*/ &SC_DATA__CTU_00196,               //P_COM_TEST.CTU_1/__OD(196)
   /*[197]*/ &SC_DATA__R_TRIG_00197,            //P_COM_TEST.R_TRIG_1/__OD(197)
   /*[198]*/ &SC_DATA__F_TRIG_00198,            //P_COM_TEST.F_TRIG_1/__OD(198)
   /*[199]*/ &SC_DATA__CNV_DT_TO_STRING_00199,  //P_CALENDER.CNV_DT_TO_STRING_1/__OD(199)
   /*[200]*/ &SC_DATA__CNV_STRING_TO_DINT_00200,//P_CALENDER.CNV_STRING_TO_DINT_1/__OD(200)
   /*[201]*/ &SC_DATA__CNV_STRING_TO_DINT_00201,//P_CALENDER.CNV_STRING_TO_DINT_2/__OD(201)
   /*[202]*/ &SC_DATA__CNV_STRING_TO_DINT_00202,//P_CALENDER.CNV_STRING_TO_DINT_3/__OD(202)
   /*[203]*/ &SC_DATA__CNV_STRING_TO_DINT_00203,//P_CALENDER.CNV_STRING_TO_DINT_4/__OD(203)
   /*[204]*/ &SC_DATA__CNV_STRING_TO_DINT_00204,//P_CALENDER.CNV_STRING_TO_DINT_5/__OD(204)
   /*[205]*/ &SC_DATA__CNV_STRING_TO_DINT_00205,//P_CALENDER.CNV_STRING_TO_DINT_6/__OD(205)
   /*[206]*/ &SC_DATA__R_TRIG_00206,            //P_CALENDER.R_TRIG_1/__OD(206)
   /*[207]*/ &SC_DATA__TIME_GET_RTC_00207,      //P_CALENDER.TIME_GET_RTC_1/__OD(207)
   /*[208]*/ &SC_DATA__CNV_DT_TO_STRING_00208,  //P_CALENDER.CNV_DT_TO_STRING_2/__OD(208)
   /*[209]*/ &SC_DATA__CNV_DT_TO_STRING_00209,  //P_CALENDER.CNV_DT_TO_STRING_3/__OD(209)
   /*[210]*/ &SC_DATA__TON_00210,               //P_DEADMAN.CutOff_Timer1/__OD(210)
   /*[211]*/ &SC_DATA__TON_00211,               //P_DEADMAN.CutOff_Timer2/__OD(211)
   /*[212]*/ &SC_DATA__TON_00212,               //P_DEADMAN.CutOff_Timer3/__OD(212)
   /*[213]*/ &SC_DATA__TON_00213,               //P_DEADMAN.CutOff_Timer4/__OD(213)
   /*[214]*/ &SC_DATA__TON_00214,               //P_DEADMAN.Emg_Timer1/__OD(214)
   /*[215]*/ &SC_DATA__TON_00215,               //P_DEADMAN.Emg_Timer2/__OD(215)
   /*[216]*/ &SC_DATA__TON_00216,               //P_DEADMAN.Emg_Timer3/__OD(216)
   /*[217]*/ &SC_DATA__TON_00217,               //P_DEADMAN.Emg_Timer4/__OD(217)
   /*[218]*/ &SC_DATA__TON_00218,               //P_DEADMAN.Pressed_Timer/__OD(218)
   /*[219]*/ &SC_DATA__TON_00219,               //P_DEADMAN.Released_Timer/__OD(219)
   /*[220]*/ &SC_DATA__SR_00220,                //P_DEADMAN.SR_Buzzer1/__OD(220)
   /*[221]*/ &SC_DATA__SR_00221,                //P_DEADMAN.SR_Buzzer2/__OD(221)
   /*[222]*/ &SC_DATA__SR_00222,                //P_DEADMAN.SR_Buzzer3/__OD(222)
   /*[223]*/ &SC_DATA__SR_00223,                //P_DEADMAN.SR_Buzzer4/__OD(223)
   /*[224]*/ &SC_DATA__SR_00224,                //P_SPEED.SR_1/__OD(224)
   /*[225]*/ &SC_DATA__TON_00225,               //P_BATT.TON_1/__OD(225)
   /*[226]*/ &SC_DATA__TON_00226,               //P_BATT.TON_2/__OD(226)
   /*[227]*/ &SC_DATA__SR_00227,                //P_BATT.SR_1/__OD(227)
   /*[228]*/ &SC_DATA__FB_FILTER_00228,         //P_BATT.FB_FILTER_1/__OD(228)
   /*[229]*/ &SC_DATA__RS_00229,                //P_MANUALMODE.RS_1/__OD(229)
   /*[230]*/ SC_INIT__TON_00230,                //FB  init/__OF(230)
   /*[231]*/ SC_INIT__TOF_00231,                //FB  init/__OF(231)
   /*[232]*/ SC_INIT__RS_00232,                 //FB  init/__OF(232)
   /*[233]*/ SC_INIT__R_TRIG_00233,             //FB  init/__OF(233)
   /*[234]*/ SC_INIT__SR_00234,                 //FB  init/__OF(234)
   /*[235]*/ SC_INIT__CTU_00235,                //FB  init/__OF(235)
   /*[236]*/ SC_INIT__F_TRIG_00236,             //FB  init/__OF(236)
   /*[237]*/ SC_INIT__CNV_DT_TO_STRING_00237,   //FB  init/__OF(237)
   /*[238]*/ SC_INIT__CNV_STRING_TO_DINT_00238, //FB  init/__OF(238)
   /*[239]*/ SC_INIT__TIME_GET_RTC_00239,       //FB  init/__OF(239)
};
