#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XENGINE_RUNNING;
   SC_BOOL                    XENGINE_STOP;
   SC_BOOL                    XENGINE_EMG_STOP;
   SC_BOOL                    XMODEULTRASONIK;
   SC_BOOL                    XMODETRAINWASH;
   SC_BOOL                    XSMOKEDETECTORCAB1;
   SC_BOOL                    XSMOKEDETECTORCAB2;
   SC_BOOL                    XHEAD_LAMP1_ON;
   SC_BOOL                    XHEAD_LAMP1_FAULT;
   SC_BOOL                    XHEAD_LAMP2_ON;
   SC_BOOL                    XHEAD_LAMP2_FAULT;
   SC_BOOL                    XHEAD_LAMP_NOTIF_ON;
   SC_BOOL                    XFRWHITE;
   SC_BOOL                    XRRWHITE;
   SC_BOOL                    XFRRED;
   SC_BOOL                    XRRRED;
   SC_BOOL                    XFRGREEN;
   SC_BOOL                    XRRGREEN;
   SC_BOOL                    XFRSIDERED;
   SC_BOOL                    XRRSIDERED;
   SC_BOOL                    XPARKINGBRAKEAPPLIED;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XBRAKECYLINDER;
   SC_BOOL                    XBRAKEPIPEPRESURE;
   SC_BOOL                    XHOLDINGBRAKE;
   SC_BOOL                    XDYNAMICBRAKESW;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XCOMFAULT;
   SC_BOOL                    XCOMEMG;
   SC_BOOL                    XCOMBYPASS;
   SC_BOOL                    XCOM1_ON;
   SC_BOOL                    XCOM2_ON;
   SC_BOOL                    XCOMPRESSOR_ERROR;
   SC_BOOL                    XCOM_OFF;
   SC_BOOL                    XTRACTIONREADY;
   SC_BOOL                    XCAB1FRONT;
   SC_BOOL                    XCAB2FRONT;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XFORWARD;
   SC_BOOL                    XREVERSE;
   SC_BOOL                    XCAB1DIR;
   SC_BOOL                    XCAB2DIR;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XBATTSHUTDOWN;
   SC_BOOL                    XDEADMANACTIVE;
   SC_BOOL                    XDEADMANTRCUTOFF;
   SC_BOOL                    XDEADMANEB;
   SC_BOOL                    XROOMLPSTAT_1;
   SC_BOOL                    XROOMLPFAULT_1;
   SC_BOOL                    XROOMLPSTAT_2;
   SC_BOOL                    XROOMLPFAULT_2;
   SC_BOOL                    XD1OPEN;
   SC_BOOL                    XD2OPEN;
   SC_BOOL                    XD1LOCKED;
   SC_BOOL                    XD2LOCKED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBYPASS;
   SC_BOOL                    XRIOM1_DI1_0;
   SC_BOOL                    XRIOM1_DI1_1;
   SC_BOOL                    XRIOM1_DI1_2;
   SC_BOOL                    XRIOM1_DI1_3;
   SC_BOOL                    XRIOM1_DI1_4;
   SC_BOOL                    XRIOM1_DI1_5;
   SC_BOOL                    XRIOM1_DI1_6;
   SC_BOOL                    XRIOM1_DI1_7;
   SC_BOOL                    XRIOM1_DI1_8;
   SC_BOOL                    XRIOM1_DI1_9;
   SC_BOOL                    XRIOM1_DI1_10;
   SC_BOOL                    XRIOM1_DI1_11;
   SC_BOOL                    XRIOM1_DI1_12;
   SC_BOOL                    XRIOM1_DI1_13;
   SC_BOOL                    XRIOM1_DI1_14;
   SC_BOOL                    XRIOM1_DI1_15;
   SC_BOOL                    XRIOM1_DI1_16;
   SC_BOOL                    XRIOM1_DI1_17;
   SC_BOOL                    XRIOM1_DI1_18;
   SC_BOOL                    XRIOM1_DI1_19;
   SC_BOOL                    XRIOM1_DI1_20;
   SC_BOOL                    XRIOM1_DI1_21;
   SC_BOOL                    XRIOM1_DI1_22;
   SC_BOOL                    XRIOM1_DI1_23;
   SC_BOOL                    XRIOM1_DI1_24;
   SC_BOOL                    XRIOM1_DI1_25;
   SC_BOOL                    XRIOM1_DI1_26;
   SC_BOOL                    XRIOM1_DI1_27;
   SC_BOOL                    XRIOM1_DI1_28;
   SC_BOOL                    XRIOM1_DI1_29;
   SC_BOOL                    XRIOM1_DI1_30;
   SC_BOOL                    XRIOM1_DI1_31;
   SC_BOOL                    XRIOM1_DI2_0;
   SC_BOOL                    XRIOM1_DI2_1;
   SC_BOOL                    XRIOM1_DI2_2;
   SC_BOOL                    XRIOM1_DI2_3;
   SC_BOOL                    XRIOM1_DI2_4;
   SC_BOOL                    XRIOM1_DI2_5;
   SC_BOOL                    XRIOM1_DI2_6;
   SC_BOOL                    XRIOM1_DI2_7;
   SC_BOOL                    XRIOM1_DI2_8;
   SC_BOOL                    XRIOM1_DI2_9;
   SC_BOOL                    XRIOM1_DI2_10;
   SC_BOOL                    XRIOM1_DI2_11;
   SC_BOOL                    XRIOM1_DI2_12;
   SC_BOOL                    XRIOM1_DI2_13;
   SC_BOOL                    XRIOM1_DI2_14;
   SC_BOOL                    XRIOM1_DI2_15;
   SC_BOOL                    XRIOM1_DI2_16;
   SC_BOOL                    XRIOM1_DI2_17;
   SC_BOOL                    XRIOM1_DI2_18;
   SC_BOOL                    XRIOM1_DI2_19;
   SC_BOOL                    XRIOM1_DI2_20;
   SC_BOOL                    XRIOM1_DI2_21;
   SC_BOOL                    XRIOM1_DI2_22;
   SC_BOOL                    XRIOM1_DI2_23;
   SC_BOOL                    XRIOM1_DI2_24;
   SC_BOOL                    XRIOM1_DI2_25;
   SC_BOOL                    XRIOM1_DI2_26;
   SC_BOOL                    XRIOM1_DI2_27;
   SC_BOOL                    XRIOM1_DI2_28;
   SC_BOOL                    XRIOM1_DI2_29;
   SC_BOOL                    XRIOM1_DI2_30;
   SC_BOOL                    XRIOM1_DI2_31;
   SC_BOOL                    XRIOM1_DO1_0;
   SC_BOOL                    XRIOM1_DO1_1;
   SC_BOOL                    XRIOM1_DO1_2;
   SC_BOOL                    XRIOM1_DO1_3;
   SC_BOOL                    XRIOM1_DO1_4;
   SC_BOOL                    XRIOM1_DO1_5;
   SC_BOOL                    XRIOM1_DO1_6;
   SC_BOOL                    XRIOM1_DO1_7;
   SC_BOOL                    XRIOM1_DO1_8;
   SC_BOOL                    XRIOM1_DO1_9;
   SC_BOOL                    XRIOM1_DO1_10;
   SC_BOOL                    XRIOM1_DO1_11;
   SC_BOOL                    XRIOM1_DO1_12;
   SC_BOOL                    XRIOM1_DO1_13;
   SC_BOOL                    XRIOM1_DO1_14;
   SC_BOOL                    XRIOM1_DO1_15;
   SC_BOOL                    XRIOM1_DO1_16;
   SC_BOOL                    XRIOM1_DO1_17;
   SC_BOOL                    XRIOM1_DO1_18;
   SC_BOOL                    XRIOM1_DO1_19;
   SC_BOOL                    XRIOM1_DO1_20;
   SC_BOOL                    XRIOM1_DO1_21;
   SC_BOOL                    XRIOM1_DO1_22;
   SC_BOOL                    XRIOM1_DO1_23;
   SC_BOOL                    XRIOM1_DO1_24;
   SC_BOOL                    XRIOM1_DO1_25;
   SC_BOOL                    XRIOM1_DO1_26;
   SC_BOOL                    XRIOM1_DO1_27;
   SC_BOOL                    XRIOM1_DO1_28;
   SC_BOOL                    XRIOM1_DO1_29;
   SC_BOOL                    XRIOM1_DO1_30;
   SC_BOOL                    XRIOM1_DO1_31;
   SC_BOOL                    XRIOM2_DI1_0;
   SC_BOOL                    XRIOM2_DI1_1;
   SC_BOOL                    XRIOM2_DI1_2;
   SC_BOOL                    XRIOM2_DI1_3;
   SC_BOOL                    XRIOM2_DI1_4;
   SC_BOOL                    XRIOM2_DI1_5;
   SC_BOOL                    XRIOM2_DI1_6;
   SC_BOOL                    XRIOM2_DI1_7;
   SC_BOOL                    XRIOM2_DI1_8;
   SC_BOOL                    XRIOM2_DI1_9;
   SC_BOOL                    XRIOM2_DI1_10;
   SC_BOOL                    XRIOM2_DI1_11;
   SC_BOOL                    XRIOM2_DI1_12;
   SC_BOOL                    XRIOM2_DI1_13;
   SC_BOOL                    XRIOM2_DI1_14;
   SC_BOOL                    XRIOM2_DI1_15;
   SC_BOOL                    XRIOM2_DI1_16;
   SC_BOOL                    XRIOM2_DI1_17;
   SC_BOOL                    XRIOM2_DI1_18;
   SC_BOOL                    XRIOM2_DI1_19;
   SC_BOOL                    XRIOM2_DI1_20;
   SC_BOOL                    XRIOM2_DI1_21;
   SC_BOOL                    XRIOM2_DI1_22;
   SC_BOOL                    XRIOM2_DI1_23;
   SC_BOOL                    XRIOM2_DI1_24;
   SC_BOOL                    XRIOM2_DI1_25;
   SC_BOOL                    XRIOM2_DI1_26;
   SC_BOOL                    XRIOM2_DI1_27;
   SC_BOOL                    XRIOM2_DI1_28;
   SC_BOOL                    XRIOM2_DI1_29;
   SC_BOOL                    XRIOM2_DI1_30;
   SC_BOOL                    XRIOM2_DI1_31;
   SC_BOOL                    XRIOM2_DI2_0;
   SC_BOOL                    XRIOM2_DI2_1;
   SC_BOOL                    XRIOM2_DI2_2;
   SC_BOOL                    XRIOM2_DI2_3;
   SC_BOOL                    XRIOM2_DI2_4;
   SC_BOOL                    XRIOM2_DI2_5;
   SC_BOOL                    XRIOM2_DI2_6;
   SC_BOOL                    XRIOM2_DI2_7;
   SC_BOOL                    XRIOM2_DI2_8;
   SC_BOOL                    XRIOM2_DI2_9;
   SC_BOOL                    XRIOM2_DI2_10;
   SC_BOOL                    XRIOM2_DI2_11;
   SC_BOOL                    XRIOM2_DI2_12;
   SC_BOOL                    XRIOM2_DI2_13;
   SC_BOOL                    XRIOM2_DI2_14;
   SC_BOOL                    XRIOM2_DI2_15;
   SC_BOOL                    XRIOM2_DI2_16;
   SC_BOOL                    XRIOM2_DI2_17;
   SC_BOOL                    XRIOM2_DI2_18;
   SC_BOOL                    XRIOM2_DI2_19;
   SC_BOOL                    XRIOM2_DI2_20;
   SC_BOOL                    XRIOM2_DI2_21;
   SC_BOOL                    XRIOM2_DI2_22;
   SC_BOOL                    XRIOM2_DI2_23;
   SC_BOOL                    XRIOM2_DI2_24;
   SC_BOOL                    XRIOM2_DI2_25;
   SC_BOOL                    XRIOM2_DI2_26;
   SC_BOOL                    XRIOM2_DI2_27;
   SC_BOOL                    XRIOM2_DI2_28;
   SC_BOOL                    XRIOM2_DI2_29;
   SC_BOOL                    XRIOM2_DI2_30;
   SC_BOOL                    XRIOM2_DI2_31;
   SC_BOOL                    XRIOM2_DO1_0;
   SC_BOOL                    XRIOM2_DO1_1;
   SC_BOOL                    XRIOM2_DO1_2;
   SC_BOOL                    XRIOM2_DO1_3;
   SC_BOOL                    XRIOM2_DO1_4;
   SC_BOOL                    XRIOM2_DO1_5;
   SC_BOOL                    XRIOM2_DO1_6;
   SC_BOOL                    XRIOM2_DO1_7;
   SC_BOOL                    XRIOM2_DO1_8;
   SC_BOOL                    XRIOM2_DO1_9;
   SC_BOOL                    XRIOM2_DO1_10;
   SC_BOOL                    XRIOM2_DO1_11;
   SC_BOOL                    XRIOM2_DO1_12;
   SC_BOOL                    XRIOM2_DO1_13;
   SC_BOOL                    XRIOM2_DO1_14;
   SC_BOOL                    XRIOM2_DO1_15;
   SC_BOOL                    XRIOM2_DO1_16;
   SC_BOOL                    XRIOM2_DO1_17;
   SC_BOOL                    XRIOM2_DO1_18;
   SC_BOOL                    XRIOM2_DO1_19;
   SC_BOOL                    XRIOM2_DO1_20;
   SC_BOOL                    XRIOM2_DO1_21;
   SC_BOOL                    XRIOM2_DO1_22;
   SC_BOOL                    XRIOM2_DO1_23;
   SC_BOOL                    XRIOM2_DO1_24;
   SC_BOOL                    XRIOM2_DO1_25;
   SC_BOOL                    XRIOM2_DO1_26;
   SC_BOOL                    XRIOM2_DO1_27;
   SC_BOOL                    XRIOM2_DO1_28;
   SC_BOOL                    XRIOM2_DO1_29;
   SC_BOOL                    XRIOM2_DO1_30;
   SC_BOOL                    XRIOM2_DO1_31;
}MODBUSBOOL_R_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_INT                     ILIMITULTRASONIK;
   SC_INT                     IFILLINGTIME;
   SC_INT                     ILEAKAGETIME;
   SC_INT                     ICALENDAR_YYYY;
   SC_INT                     ICALENDAR_MM;
   SC_INT                     ICALENDAR_DD;
   SC_INT                     ICALENDAR_HH;
   SC_INT                     ICALENDAR_MIN;
   SC_INT                     ICALENDAR_SS;
   SC_INT                     ITRAINSPEED;
   SC_INT                     ITRAINSPEEDLIMIT;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BYTE                    __align1[2];
   SC_REAL                    RBATTVOLTAGE;
   SC_INT                     ICMP1_HH;
   SC_INT                     ICMP2_HH;
}MODBUSINT_R_T_TYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_TYP;

typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_TYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_TYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_TYP;

typedef struct
{
   SC_BOOL                    XFRONTR_WHT1_HMI;
   SC_BOOL                    XREARR_WHT2_HMI;
   SC_BOOL                    XFRONTR_RED1_HMI;
   SC_BOOL                    XREARR_RED2_HMI;
   SC_BOOL                    XFRONTR_SIDE1_HMI;
   SC_BOOL                    XREARR_SIDE2_HMI;
   SC_BOOL                    XFRONTR_GREEN_HMI;
   SC_BOOL                    XREARR_GREEN_HMI;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDE_RED_1;
   SC_BOOL                    XSIDE_RED_2;
   SC_BOOL                    XGREEN_1;
   SC_BOOL                    XGREEN_2;
}SIGNAL_LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XHEADLP_STAT_1;
   SC_BOOL                    XHEADLP_STAT_2;
   SC_BOOL                    XFAULTLP_STAT_1;
   SC_BOOL                    XFAULTLP_STAT_2;
   SC_BOOL                    XHEADLP_NOTIF;
}HEAD_LAMP_T_TYP;

typedef struct
{
   SC_INT                     XFILLINGTIME;
   SC_INT                     XLEAKAGECOMMAND;
}COMPRESOR_TEST_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XTRAINWASH_ON;
}TRAIN_WASH_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XENGINERUNNING;
   SC_BOOL                    XENGINESTOP;
   SC_BOOL                    XENGINEEMERGENCYSTOP;
}ENGINE_STATUS_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_OO_HMI_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_OO_HMI_00115(P_OO_HMI_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_OO_HMI_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_OO_HMI_00046(void)
{
   P_OO_HMI_TYP *ip = (P_OO_HMI_TYP*) __OD(116);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(46, ip);
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCAB1FRONT = (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCAB2FRONT = (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCABERROR = (*(CABIN_T_TYP*)__OD(11)).XCABERROR;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XMANUALMODE = (*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCAB1DIR = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
   & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD)) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) 
   & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV)));
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCAB2DIR = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
   & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XREV)) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) 
   & (1 & (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD)));
   (*(MODBUSINT_R_T_TYP*)__OD(21)).IPOWERING = (*(MASTERCTRL_T_TYP*)__OD(12)).IPOWERING;
   (*(MODBUSINT_R_T_TYP*)__OD(21)).IBRAKING = (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XFASTBRAKE = (*(MASTERCTRL_T_TYP*)__OD(12)).XFASTBRAKE;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XBATTSHUTDOWN = (*(BATT_T_TYP*)__OD(17)).XBATTOFF_CMD;
   (*(MODBUSINT_R_T_TYP*)__OD(21)).RBATTVOLTAGE = (*(BATT_T_TYP*)__OD(17)).RBATTV;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XBATTLOW = (*(BATT_T_TYP*)__OD(17)).XBATTLOW;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XDEADMANACTIVE = (*(DEADMAN_T_TYP*)__OD(14)).XDEADBUZZER;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XDEADMANTRCUTOFF = (*(DEADMAN_T_TYP*)__OD(14)).XDEADTRCUTOFF;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XDEADMANEB = (*(DEADMAN_T_TYP*)__OD(14)).XDEADEB;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XD1OPEN = (*(DOORS_T_TYP*)__OD(16)).XD1_OPEN;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XD2OPEN = (*(DOORS_T_TYP*)__OD(16)).XD2_OPEN;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XD1LOCKED = (*(DOORS_T_TYP*)__OD(16)).XD1_LOCKED;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XD2LOCKED = (*(DOORS_T_TYP*)__OD(16)).XD2_LOCKED;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XEBSW = (*(BRAKE_T_TYP*)__OD(19)).XEBSW;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XEBBYPASS = (*(BRAKE_T_TYP*)__OD(19)).XEBBYPASS;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XPARKINGBRAKEAPPLIED = (*(BRAKE_T_TYP*)__OD(19)).XPARKINGBRK;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XALLBRAKERELEASE = (*(BRAKE_T_TYP*)__OD(19)).XALLBRAKERELEASE;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XBRAKECYLINDER = (*(BRAKE_T_TYP*)__OD(19)).XBRAKEPRES_SW;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XBRAKEPIPEPRESURE = (*(BRAKE_T_TYP*)__OD(19)).XBRAKEPIPEPRES_SW;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XHOLDINGBRAKE = (*(BRAKE_T_TYP*)__OD(19)).XHOLDBRAKE;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XDYNAMICBRAKESW = ((1 & LD_I_X(3,1,2,19)) | (1 & LD_I_X(
   3,2,3,22)));
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XTRACTIONREADY = (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRREADY;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOM1_ON = (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMP1_ACTV;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOM2_ON = (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMP2_ACTV;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOMFAULT = (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPFAULT;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOMPRESSOR_ERROR = (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPERR;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOM_OFF = (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPOFF;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOMEMG = (*(COMPRESSOR_T_TYP*)__OD(25)).XEMGCOMP;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XCOMBYPASS = (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPBYPASS;
   (*(MODBUSINT_R_T_TYP*)__OD(21)).ICMP1_HH = DINT_TO_INT(((*(COMPRESSOR_T_TYP*)__OD(25)).DICMPTIME_1 
   / SC_C_DINT(60)));
   (*(MODBUSINT_R_T_TYP*)__OD(21)).ICMP2_HH = DINT_TO_INT(((*(COMPRESSOR_T_TYP*)__OD(25)).DICMPTIME_2 
   / SC_C_DINT(60)));
   (*(MODBUSINT_R_T_TYP*)__OD(21)).IFILLINGTIME = (*(COMPRESOR_TEST_T_TYP*)__OD(29)).XFILLINGTIME;
   (*(MODBUSINT_R_T_TYP*)__OD(21)).ILEAKAGETIME = (*(COMPRESOR_TEST_T_TYP*)__OD(29)).XLEAKAGECOMMAND;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XFRWHITE = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_WHT1_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XRRWHITE = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_WHT2_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XFRRED = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_RED1_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XRRRED = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_RED2_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XFRSIDERED = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_SIDE1_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XRRSIDERED = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_SIDE2_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XFRGREEN = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XFRONTR_GREEN_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XRRGREEN = (*(SIGNAL_LAMP_T_TYP*)__OD(26)).XREARR_GREEN_HMI;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XHEAD_LAMP1_ON = (((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_STAT_1) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) | ((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_STAT_2) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XHEAD_LAMP1_FAULT = (((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XFAULTLP_STAT_1) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) | ((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XFAULTLP_STAT_2) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XHEAD_LAMP2_ON = (((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_STAT_2) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) | ((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_STAT_1) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XHEAD_LAMP2_FAULT = (((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XFAULTLP_STAT_2) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) | ((1 & (*(HEAD_LAMP_T_TYP*)__OD(27)).XFAULTLP_STAT_1) 
   & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XHEAD_LAMP_NOTIF_ON = (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_NOTIF;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XSMOKEDETECTORCAB1 = LD_I_X(3,1,2,26);
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XSMOKEDETECTORCAB2 = LD_I_X(3,2,3,31);
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XMODEULTRASONIK = (*(ULTRASON_MODE_T_TYP*)__OD(30)).XULTRSN_ON;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XMODETRAINWASH = (*(TRAIN_WASH_MODE_T_TYP*)__OD(31)).XTRAINWASH_ON;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XENGINE_RUNNING = (*(ENGINE_STATUS_T_TYP*)__OD(33)).XENGINERUNNING;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XENGINE_STOP = (*(ENGINE_STATUS_T_TYP*)__OD(33)).XENGINESTOP;
   (*(MODBUSBOOL_R_T_TYP*)__OD(20)).XENGINE_EMG_STOP = (*(ENGINE_STATUS_T_TYP*)__OD(33)).XENGINEEMERGENCYSTOP;

   PRG_EXIT(46, ip);
}
