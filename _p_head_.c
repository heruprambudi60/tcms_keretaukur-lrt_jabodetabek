#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
typedef SC_STRINGDEF(20)  STRING__TYPE20;


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XHEADLP_STAT_1;
   SC_BOOL                    XHEADLP_STAT_2;
   SC_BOOL                    XFAULTLP_STAT_1;
   SC_BOOL                    XFAULTLP_STAT_2;
   SC_BOOL                    XHEADLP_NOTIF;
}HEAD_LAMP_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_UINT                    UIYYYY;
   SC_USINT                   USMM;
   SC_USINT                   USDD;
   SC_USINT                   USHH;
   SC_USINT                   USMIN;
   SC_USINT                   USSS;
   SC_INT                     IDAYOFWK;
   SC_DT                      DTDATETIME;
   STRING__TYPE20             SDATEANDTIME;
}CALENDAR_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XHDLP_FAULT1;
   SC_BOOL                    XHDLP_FAULT2;
   SC_BOOL                    XHDLP_STAT1;
   SC_BOOL                    XHDLP_STAT2;
}P_HEAD_LAMP_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_HEAD_LAMP_00107(P_HEAD_LAMP_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_HEAD_LAMP_TYP, XHDLP_STAT2) + sizeof(ip->XHDLP_STAT2);
   /* non retains: */
   ip->XHDLP_FAULT1 = SC_C_BOOL(0);
   ip->XHDLP_FAULT2 = SC_C_BOOL(0);
   ip->XHDLP_STAT1 = SC_C_BOOL(0);
   ip->XHDLP_STAT2 = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_HEAD_LAMP_00042(void)
{
   P_HEAD_LAMP_TYP *ip = (P_HEAD_LAMP_TYP*) __OD(108);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(42, ip);
   (*(HEAD_LAMP_T_TYP*)__OD(27)).XFAULTLP_STAT_1 = ((1 & LD_I_X(3,1,2,25)) &  (!((1 & LD_I_X(
   3,1,2,24)))));
   (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_STAT_1 = LD_I_X(3,1,2,24);
   (*(HEAD_LAMP_T_TYP*)__OD(27)).XFAULTLP_STAT_2 = ((1 & LD_I_X(3,2,3,27)) &  (!((1 & LD_I_X(
   3,2,3,28)))));
   (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_STAT_2 = LD_I_X(3,2,3,28);

   if( (!(((((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (1 & ip->XHDLP_STAT1)) &  (!((1 
   & ip->XHDLP_FAULT1)))) | (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & ip->XHDLP_STAT2)) 
   &  (!((1 & ip->XHDLP_FAULT2))))))))
   {

      if(((((*(CALENDAR_T_TYP*)__OD(28)).USHH < SC_C_USINT(6)) | (((*(CALENDAR_T_TYP*)__OD(28)).USHH 
      == SC_C_USINT(17)) & ((*(CALENDAR_T_TYP*)__OD(28)).USMIN >= SC_C_USINT(30)))) | ((*(
      CALENDAR_T_TYP*)__OD(28)).USHH > SC_C_USINT(17))))
      {
         (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_NOTIF = SC_C_BOOL(1);
      }
      else
      {
         (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_NOTIF = SC_C_BOOL(0);
      }

   }
   else
   {
      (*(HEAD_LAMP_T_TYP*)__OD(27)).XHEADLP_NOTIF = SC_C_BOOL(0);
   }


   PRG_EXIT(42, ip);
}
