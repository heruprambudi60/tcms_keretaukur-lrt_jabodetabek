#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_FILTER_00071(FB_FILTER_TYP* ip1, FB_FILTER_TYP* ip2)
{
   MOVE_ANY(&(ip1->RA), &(ip2->RA), (long) (ip1) + sizeof(FB_FILTER_TYP) - (long) (&(ip1->RA)));
}

void SC_INIT__FB_FILTER_00075(FB_FILTER_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_FILTER_TYP, RVAR_PAST) + sizeof(ip->RVAR_PAST);
   /* non retains: */
   ip->RA = SC_C_REAL(0.0);
   ip->RVAR = SC_C_REAL(0.0);
   ip->RVAR_FILTERED = SC_C_REAL(0.0);
   ip->RVAR_PAST = SC_C_REAL(0.0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_FILTER_00069(FB_FILTER_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   /* <non> */

   FB_ENTRY(69, ip);
/******************* Network *************************/
{
   ip->RVAR_FILTERED = (((SC_C_REAL(1.0) - ip->RA) * ip->RVAR_PAST) + (ip->RA * ip->RVAR));

}
/******************* Network *************************/
{
   ip->RVAR_PAST = ip->RVAR_FILTERED;

}

   FB_EXIT(69, ip);
}
