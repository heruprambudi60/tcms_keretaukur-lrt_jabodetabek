#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_BOOL                    XDEADBUZZER;
   SC_BOOL                    XDEADTRCUTOFF;
   SC_BOOL                    XDEADEB;
}DEADMAN_T_TYP;

typedef struct
{
   SC_BOOL                    XD1_OPEN;
   SC_BOOL                    XD2_OPEN;
   SC_BOOL                    XD1_LOCKED;
   SC_BOOL                    XD2_LOCKED;
   SC_BOOL                    XDALLCLOSED;
   SC_BOOL                    XDFAULT;
   SC_BOOL                    XDBAYPASS;
}DOORS_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
}P_SPEED_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_SPEED_00125(P_SPEED_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_SPEED_TYP, SR_1) + sizeof(ip->SR_1);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_SPEED_00051(void)
{
   P_SPEED_TYP *ip = (P_SPEED_TYP*) __OD(126);

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   PRG_ENTRY(51, ip);
/******************* Network *************************/
{
   MASTERCTRL_T_TYP* __lpt0 = (MASTERCTRL_T_TYP*)__OD(12);
   (*(SPEED_T_TYP*)__OD(15)).ILIMITSPEED = SEL_INT((*__lpt0).XREV,SEL_INT((*__lpt0).XFWD,SC_C_INT(
   0),SC_C_INT(80)),SC_C_INT(15));

}
/******************* Network *************************/
{
   SPEED_T_TYP* __lpt0 = (SPEED_T_TYP*)__OD(15);
   SR_TYP* __lpt1 = (SR_TYP*)__OD(ip->SR_1);
   _SCT_B0 = ((*__lpt0).ISPEED > (*__lpt0).ILIMITSPEED);
   (*__lpt0).XOVERSPEED = _SCT_B0;
   (*__lpt0).XALARMBUZZER = _SCT_B0;
   (*__lpt1).SET1 = ((*__lpt0).ISPEED >= ((*__lpt0).ILIMITSPEED + SC_C_INT(5)));
   (*__lpt1).RESET = ((*__lpt0).ISPEED <= (*__lpt0).ILIMITSPEED);
   SR((__lpt1));
   (*__lpt0).XOVERSPEED_DO = (*__lpt1).Q1;

}
/******************* Network *************************/
{
   DOORS_T_TYP* __lpt0 = (DOORS_T_TYP*)__OD(16);
   SPEED_T_TYP* __lpt1 = (SPEED_T_TYP*)__OD(15);
   (*__lpt1).XTRCUTTOFF = (((1 & (*__lpt1).XOVERSPEED_DO) | (1 & (*(DEADMAN_T_TYP*)__OD(14)).XDEADTRCUTOFF)) 
   | (1 & NOT_BOOL(((1 & (*__lpt0).XDALLCLOSED) | (1 & (*__lpt0).XDBAYPASS)))));

}
/******************* Network *************************/
{
   (*(BRAKE_T_TYP*)__OD(19)).XEMGBRAKE = ((*(SPEED_T_TYP*)__OD(15)).ISPEED >= SC_C_INT(90));

}
/******************* Network *************************/
{
   SPEED_T_TYP* __lpt0 = (SPEED_T_TYP*)__OD(15);
   (*__lpt0).XZEROSPEED = ((((*__lpt0).ISPEED < SC_C_INT(4)) | (1 & LD_I_X(3,1,1,18))) | (
   1 & LD_I_X(3,2,2,18)));

}
/******************* Network *************************/
{
   (*(SPEED_T_TYP*)__OD(15)).ISPEED = SEL_INT((*(MASTERCTRL_T_TYP*)__OD(12)).XFWD,SC_C_INT(
   0),SC_C_INT(41));

}

   PRG_EXIT(51, ip);
}
