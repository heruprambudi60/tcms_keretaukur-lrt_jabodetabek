#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XROOMLAMPSTAT_1;
   SC_BOOL                    XROOMLAMPSTAT_2;
   SC_BOOL                    XROOMLAMPFAULT_1;
   SC_BOOL                    XROOMLAMPFAULT_2;
}LAMP_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_LAMP_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_LAMP_00123(P_LAMP_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_LAMP_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_LAMP_00050(void)
{
   P_LAMP_TYP *ip = (P_LAMP_TYP*) __OD(124);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(50, ip);
   (*(LAMP_T_TYP*)__OD(18)).XROOMLAMPSTAT_1 = LD_I_X(3,1,1,24);
   (*(LAMP_T_TYP*)__OD(18)).XROOMLAMPFAULT_1 = ((1 & LD_I_X(3,1,1,26)) != (1 & LD_I_X(3,1,
   1,24)));
   (*(LAMP_T_TYP*)__OD(18)).XROOMLAMPSTAT_2 = LD_I_X(3,2,2,24);
   (*(LAMP_T_TYP*)__OD(18)).XROOMLAMPFAULT_2 = ((1 & LD_I_X(3,2,2,26)) != (1 & LD_I_X(3,2,
   2,24)));

   PRG_EXIT(50, ip);
}
