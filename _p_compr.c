#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WSENSOR;
   SC_REAL                    RA_FILTER;
   SC_INT                     IUPPER_HYSTERESYS;
   SC_INT                     ILOWER_HYSTERESYS1;
   SC_INT                     IPRESSURE;
   SC_BOOL                    XLOWPRESSURE;
   SC_REAL                    RANALOG;
   SC_REAL                    RFILTERED;
   SC_REAL                    RPRESSURE;
   SC_REAL                    RPRESSURE_REGRESSION;
   SC__INSTC                  FB_FILTER_1;
}FB_PRESSURE_CHECK_TYP;
typedef void (*__BPFN_FB_PRESSURE_CHECK)(FB_PRESSURE_CHECK_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLE1;
   SC_BOOL                    XENABLE2;
   SC_DINT*                   DITIME1;
   SC_DINT*                   DITIME2;
   SC__INSTC                  RS_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    X1MIN1;
   SC_BOOL                    X1MIN2;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_TIME                    TET;
   SC_TIME                    TET2;
}FB_TIMECOUNT_DINT_TYP;
typedef void (*__BPFN_FB_TIMECOUNT_DINT)(FB_TIMECOUNT_DINT_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  FB_PRESSURE_CHECK_1;
   SC__INSTC                  FB_TIMECOUNT_DINT_1;
   SC__INSTC                  RS_1;
   SC_REAL                    RA;
   SC_DINT                    DICMP1TIME;
   SC_DINT                    DICMP2TIME;
}P_COMPRESOR_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_COMPRESOR_00095(P_COMPRESOR_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_COMPRESOR_TYP, DICMP2TIME) + sizeof(ip->DICMP2TIME);
   /* non retains: */
   ip->RA = SC_C_REAL(0.0);

   /* retains: */
   if(iRetain)
   {
      ip->DICMP1TIME = SC_C_DINT(0);
      ip->DICMP2TIME = SC_C_DINT(0);

   }
}


/* PROGRAM BODY: */
void SC_BODY__P_COMPRESOR_00037(void)
{
   P_COMPRESOR_TYP *ip = (P_COMPRESOR_TYP*) __OD(96);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(37, ip);
/******************* Network *************************/
{
   FB_PRESSURE_CHECK_TYP* __lpt0 = (FB_PRESSURE_CHECK_TYP*)__OD(ip->FB_PRESSURE_CHECK_1);
   (*__lpt0).WSENSOR = LD_I_W(3,1,5,3);
   (*__lpt0).RA_FILTER = ip->RA;
   (*__lpt0).IUPPER_HYSTERESYS = SC_C_INT(700);
   (*__lpt0).ILOWER_HYSTERESYS1 = SC_C_INT(600);
   ((__BPFN_FB_PRESSURE_CHECK)__OF(94))((__lpt0));
   (*(COMPRESSOR_T_TYP*)__OD(25)).IMRPRESSURE_1 = (*__lpt0).IPRESSURE;

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XMRSENSOR_ERROR = (ABS_INT((*__lpt0).IMRPRESSURE_1) >= SC_C_INT(200));

}
/******************* Network *************************/
{
   (*(COMPRESSOR_T_TYP*)__OD(25)).XMRPSOK = LD_I_X(3,1,1,28);

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   RS_TYP* __lpt1 = (RS_TYP*)__OD(ip->RS_1);
   (*__lpt1).SET = ((*__lpt0).IMRPRESSURE_1 < SC_C_INT(800));
   (*__lpt1).RESET1 = ((*__lpt0).IMRPRESSURE_1 > SC_C_INT(980));
   RS((__lpt1));
   (*__lpt0).XCOMMANDON = (*__lpt1).Q1;

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XCOMP1_ACTV = (((1 & LD_I_X(3,2,3,10)) & (1 & LD_I_X(3,1,2,6))) & (1 & (*__lpt0).XCOMMANDON));
   (*__lpt0).XCOMP2_ACTV = (((1 & LD_I_X(3,2,3,11)) & (1 & LD_I_X(3,1,2,7))) & (1 & (*__lpt0).XCOMMANDON));

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XCOMPFAULT = (((((1 & (*__lpt0).XCOMMANDON) & (1 & LD_I_X(3,1,2,6))) & (1 & NOT_BOOL(
   LD_I_X(3,2,3,10)))) | (((1 & LD_I_X(3,2,3,14)) & (1 & LD_I_X(3,1,2,6))) & (1 & NOT_BOOL(
   LD_I_X(3,2,3,10))))) | ((((1 & (*__lpt0).XCOMMANDON) & (1 & LD_I_X(3,1,2,7))) & (1 & NOT_BOOL(
   LD_I_X(3,2,3,11)))) | (((1 & LD_I_X(3,2,3,14)) & (1 & LD_I_X(3,1,2,7))) & (1 & NOT_BOOL(
   LD_I_X(3,2,3,11))))));

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XCOMPERR = (((((1 & NOT_BOOL((*__lpt0).XCOMMANDON)) & (1 & NOT_BOOL(LD_I_X(3,
   1,2,6)))) & (1 & NOT_BOOL(LD_I_X(3,2,3,14)))) & (1 & LD_I_X(3,2,3,10))) | ((((1 & NOT_BOOL(
   (*__lpt0).XCOMMANDON)) & (1 & NOT_BOOL(LD_I_X(3,1,2,7)))) & (1 & NOT_BOOL(LD_I_X(3,2,3,
   14)))) & (1 & LD_I_X(3,2,3,11))));

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XCOMPOFF = ((1 & NOT_BOOL(((((1 & (*__lpt0).XCOMMANDON) & (1 & LD_I_X(3,1,2,6))) 
   & (1 & LD_I_X(3,2,3,10))) & (1 & LD_I_X(3,2,3,14))))) | (1 & NOT_BOOL(((((1 & (*__lpt0).XCOMMANDON) 
   & (1 & LD_I_X(3,1,2,7))) & (1 & LD_I_X(3,2,3,11))) & (1 & LD_I_X(3,2,3,14))))));

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XCOMPBYPASS = ((((1 & (*__lpt0).XCOMMANDON) & (1 & LD_I_X(3,1,2,6))) & (1 & LD_I_X(
   3,2,3,10))) | (((1 & (*__lpt0).XCOMMANDON) & (1 & LD_I_X(3,1,2,7))) & (1 & LD_I_X(3,2,3,
   11))));

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XEMGCOMP = ((1 & LD_I_X(3,2,3,14)) & (1 & NOT_BOOL((*__lpt0).XCOMPPRESS_SW)));

}
/******************* Network *************************/
{
   (*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPPRESS_SW = LD_I_X(3,2,3,15);

}
/******************* Network *************************/
{
   FB_TIMECOUNT_DINT_TYP* __lpt0 = (FB_TIMECOUNT_DINT_TYP*)__OD(ip->FB_TIMECOUNT_DINT_1);
   COMPRESSOR_T_TYP* __lpt1 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).XENABLE1 = (*__lpt1).XCOMP1_ACTV;
   (*__lpt0).XENABLE2 = (*__lpt1).XCOMP2_ACTV;
   (*__lpt0).DITIME1 = &(ip->DICMP1TIME);
   (*__lpt0).DITIME2 = &(ip->DICMP2TIME);
   ((__BPFN_FB_TIMECOUNT_DINT)__OF(65))((__lpt0));

}
/******************* Network *************************/
{
   COMPRESSOR_T_TYP* __lpt0 = (COMPRESSOR_T_TYP*)__OD(25);
   (*__lpt0).DICMPTIME_1 = ip->DICMP1TIME;
   (*__lpt0).DICMPTIME_2 = ip->DICMP2TIME;

}

   PRG_EXIT(37, ip);
}
