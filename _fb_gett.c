#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;
typedef void (*__BPFN_FB_FILTER)(FB_FILTER_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP;
   SC_REAL                    RFILTERCONSTANT;
   SC_INT                     ITEMP;
   SC__INSTC                  FB_FILTER_1;
}FB_GETTEMP_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_GETTEMP_00072(FB_GETTEMP_TYP* ip1, FB_GETTEMP_TYP* ip2)
{
   MOVE_ANY(&(ip1->WTEMP), &(ip2->WTEMP), (long) (&(ip1->FB_FILTER_1)) - (long) (&(ip1->WTEMP)));
   ((__PFN_COPY)__OF(71))(__OD(ip1->FB_FILTER_1), __OD(ip2->FB_FILTER_1));
}

void SC_INIT__FB_GETTEMP_00074(FB_GETTEMP_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_GETTEMP_TYP, FB_FILTER_1) + sizeof(ip->FB_FILTER_1);
   /* non retains: */
   ip->WTEMP = SC_C_WORD(0);
   ip->RFILTERCONSTANT = SC_C_REAL(0.0);
   ip->ITEMP = SC_C_INT(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_GETTEMP_00068(FB_GETTEMP_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   /* <non> */

   FB_ENTRY(68, ip);
/******************* Network *************************/
{
   FB_FILTER_TYP* __lpt0 = (FB_FILTER_TYP*)__OD(ip->FB_FILTER_1);
   (*__lpt0).RA = ip->RFILTERCONSTANT;
   (*__lpt0).RVAR = DINT_TO_REAL((((WORD_TO_DINT(ip->WTEMP) * SC_C_DINT(6500)) / SC_C_DINT(
   65535)) - SC_C_DINT(680)));
   ((__BPFN_FB_FILTER)__OF(69))((__lpt0));
   ip->ITEMP = REAL_TO_INT((*__lpt0).RVAR_FILTERED);

}

   FB_EXIT(68, ip);
}
