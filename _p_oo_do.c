#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_BOOL                    XBATTOFF_CMD;
   SC_BOOL                    XBATTOFF_DO;
   SC_INT                     IBATTV;
   SC_REAL                    RBATTV;
   SC_INT                     IBATTFRAC;
   SC_BOOL                    XBATTLOW;
   SC_BOOL                    XEBBATTLOW;
   SC_BOOL                    XBATTLOW_BUZZER;
   SC_BOOL                    XBATTCHRFFAULT;
}BATT_T_TYP;

typedef struct
{
   SC_BOOL                    XEMGBRAKE;
   SC_BOOL                    XEMGBRAKE_DO;
   SC_BOOL                    XEBSW;
   SC_BOOL                    XEBBYPASS;
   SC_BOOL                    XPARKINGBRK;
   SC_BOOL                    XALLBRAKERELEASE;
   SC_BOOL                    XHOLDBRAKE;
   SC_BOOL                    XBRAKERELEASE;
   SC_BOOL                    XBRAKEPRES_SW;
   SC_BOOL                    XBCRELEASE;
   SC_BOOL                    XBRAKEPIPEPRES_SW;
   SC_BOOL                    XDB_SW;
   SC_BOOL                    XDB_CUTTOUT;
}BRAKE_T_TYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_TYP;

typedef struct
{
   SC_BOOL                    XMRPSOK;
   SC_BOOL                    XMRSENSOR_ERROR;
   SC_BOOL                    XCOMP1_ACTV;
   SC_BOOL                    XCOMP2_ACTV;
   SC_BOOL                    XCOMPFAULT;
   SC_BOOL                    XCOMPBYPASS;
   SC_BOOL                    XCOMPERR;
   SC_BOOL                    XCOMPOFF;
   SC_BOOL                    XCOMPPRESS_SW;
   SC_BOOL                    XEMGCOMP;
   SC_BOOL                    XCOMMANDON;
   SC_INT                     XPRESSURE;
   SC_INT                     IMRPRESSURE_1;
   SC_DINT                    DICMPTIME_1;
   SC_DINT                    DICMPTIME_2;
}COMPRESSOR_T_TYP;

typedef struct
{
   SC_BOOL                    XFRONTR_WHT1_HMI;
   SC_BOOL                    XREARR_WHT2_HMI;
   SC_BOOL                    XFRONTR_RED1_HMI;
   SC_BOOL                    XREARR_RED2_HMI;
   SC_BOOL                    XFRONTR_SIDE1_HMI;
   SC_BOOL                    XREARR_SIDE2_HMI;
   SC_BOOL                    XFRONTR_GREEN_HMI;
   SC_BOOL                    XREARR_GREEN_HMI;
   SC_BOOL                    XWHITE_1;
   SC_BOOL                    XWHITE_2;
   SC_BOOL                    XSIDE_RED_1;
   SC_BOOL                    XSIDE_RED_2;
   SC_BOOL                    XGREEN_1;
   SC_BOOL                    XGREEN_2;
}SIGNAL_LAMP_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_OO_DO_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_OO_DO_00117(P_OO_DO_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_OO_DO_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_OO_DO_00047(void)
{
   P_OO_DO_TYP *ip = (P_OO_DO_TYP*) __OD(118);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(47, ip);
   ST_Q_X(3,1,3,0,(*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE);
   ST_Q_X(3,1,3,1,((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (1 & (*(BATT_T_TYP*)__OD(17)).XBATTOFF_DO)));
   ST_Q_X(3,1,3,2,((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTOFF_DO)));
   ST_Q_X(3,1,3,4,((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (1 & (*(BRAKE_T_TYP*)__OD(19)).XEMGBRAKE_DO)));
   ST_Q_X(3,1,3,7,(*(COMPRESSOR_T_TYP*)__OD(25)).XCOMP1_ACTV);
   ST_Q_X(3,1,3,13,(*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPBYPASS);
   ST_Q_X(3,1,3,3,(*(COMPRESSOR_T_TYP*)__OD(25)).XCOMMANDON);
   ST_Q_X(3,1,3,14,(*(SIGNAL_LAMP_T_TYP*)__OD(26)).XWHITE_1);
   ST_Q_X(3,1,3,15,(*(SIGNAL_LAMP_T_TYP*)__OD(26)).XSIDE_RED_1);
   ST_Q_X(3,1,3,16,(*(SIGNAL_LAMP_T_TYP*)__OD(26)).XGREEN_1);
   ST_Q_X(3,2,4,0,(*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE);
   ST_Q_X(3,2,4,1,((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & (*(BATT_T_TYP*)__OD(17)).XBATTOFF_DO)));
   ST_Q_X(3,2,4,2,((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTOFF_DO)));
   ST_Q_X(3,2,4,4,((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & (*(BRAKE_T_TYP*)__OD(19)).XEMGBRAKE_DO)));
   ST_Q_X(3,2,4,6,(*(COMPRESSOR_T_TYP*)__OD(25)).XCOMP2_ACTV);
   ST_Q_X(3,2,4,7,(*(COMPRESSOR_T_TYP*)__OD(25)).XCOMPBYPASS);
   ST_Q_X(3,2,4,8,(*(COMPRESSOR_T_TYP*)__OD(25)).XCOMMANDON);
   ST_Q_X(3,2,4,12,(*(SIGNAL_LAMP_T_TYP*)__OD(26)).XWHITE_2);
   ST_Q_X(3,2,4,13,(*(SIGNAL_LAMP_T_TYP*)__OD(26)).XSIDE_RED_2);
   ST_Q_X(3,2,4,14,(*(SIGNAL_LAMP_T_TYP*)__OD(26)).XGREEN_2);

   PRG_EXIT(47, ip);
}
