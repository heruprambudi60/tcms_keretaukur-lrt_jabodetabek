#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XTEMPSENSORERROR_1;
   SC_BOOL                    XAC_COMMAND_ON;
   SC_BOOL                    XAC1;
   SC_BOOL                    XAC2;
   SC_INT                     ISETTEMP_HUNDREDS;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XEVAP1_ON;
   SC_BOOL                    XEVAP2_ON;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XACSTARTUP2;
   SC_BOOL                    XCD_AC1_ON;
   SC_BOOL                    XCD_AC2_ON;
   SC_BOOL                    XCP_AC1_ON;
   SC_BOOL                    XCP_AC2_ON;
   SC_INT                     ITEMP_1;
   SC_BOOL                    XHALFMODE_1;
   SC_BOOL                    XFULLMODE_1;
   SC_BOOL                    XACERROR_1;
   SC_DINT                    DICP1TIME_1;
   SC_DINT                    DICP2TIME_1;
}AC_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1K;
   SC_BOOL                    XEVAP2K;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XTEMPPROTECTIONCDA;
   SC_BOOL                    XTEMPPROTECTIONCDB;
   SC_TIME                    TCDDELAY;
   SC_BOOL                    XCDAB_ON;
   SC__INSTC                  TON_2;
}FB_AC_CD_TYP;
typedef void (*__BPFN_FB_AC_CD)(FB_AC_CD_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XHP_CMPB;
   SC_BOOL                    XLP_CMPB;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP1_TYP;
typedef void (*__BPFN_FB_AC_CP1)(FB_AC_CP1_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_TIME                    TCMPSTARTUP;
   SC_BOOL                    XCMPA_NFB;
   SC_BOOL                    XCMPB_NFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XHP_CMPA;
   SC_BOOL                    XLP_CMPA;
   SC_BOOL                    XTEMPPROTECTIONCPA;
   SC_BOOL                    XTEMPPROTECTIONCPB;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_TIME                    TCMPNORMAL;
   SC_BOOL                    XCMPAB_ON;
   SC__INSTC                  TON_1;
   SC__INSTC                  TON_2;
}FB_AC_CP2_TYP;
typedef void (*__BPFN_FB_AC_CP2)(FB_AC_CP2_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1NFB;
   SC_BOOL                    XEVAP2NFB;
   SC_BOOL                    X3PHCOUNTER;
   SC_BOOL                    XTEMPPROTECTIONEV1;
   SC_BOOL                    XTEMPPROTECTIONEV2;
   SC_BOOL                    XACON;
   SC_BOOL                    XEVAP1ON;
   SC_BOOL                    XEVAP2ON;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  TOF_1;
   SC__INSTC                  TOF_2;
}FB_AC_EVAP_TYP;
typedef void (*__BPFN_FB_AC_EVAP)(FB_AC_EVAP_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_1_TYP;
typedef void (*__BPFN_FB_AC_1)(FB_AC_1_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_2_TYP;
typedef void (*__BPFN_FB_AC_2)(FB_AC_2_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLE1;
   SC_BOOL                    XENABLE2;
   SC_DINT*                   DITIME1;
   SC_DINT*                   DITIME2;
   SC__INSTC                  RS_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    X1MIN1;
   SC_BOOL                    X1MIN2;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_TIME                    TET;
   SC_TIME                    TET2;
}FB_TIMECOUNT_DINT_TYP;
typedef void (*__BPFN_FB_TIMECOUNT_DINT)(FB_TIMECOUNT_DINT_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENABLEHALF;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_DINT                    DIAC1TIME;
   SC_DINT                    DIAC2TIME;
   SC_BOOL                    XHALF1;
   SC_BOOL                    XHALF2;
   SC__INSTC                  R_TRIG_1;
   SC_BOOL                    XERROR;
   SC__INSTC                  SR_1;
   SC__INSTC                  TON_1;
   SC_BOOL                    XRESET;
}FB_AC_HALFSEL_REVA_TYP;
typedef void (*__BPFN_FB_AC_HALFSEL_REVA)(FB_AC_HALFSEL_REVA_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_REAL                    RA;
   SC_REAL                    RVAR;
   SC_REAL                    RVAR_FILTERED;
   SC_REAL                    RVAR_PAST;
}FB_FILTER_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP;
   SC_REAL                    RFILTERCONSTANT;
   SC_INT                     ITEMP;
   SC__INSTC                  FB_FILTER_1;
}FB_GETTEMP_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_WORD                    WTEMP1;
   SC_WORD                    WTEMP2;
   SC_INT                     ITEMP;
   SC_BOOL                    XTEMPSENSORERROR1;
   SC_BOOL                    XTEMPSENSORERROR2;
   SC_BOOL                    XTEMPSENSORERROR;
   SC_INT                     I;
   SC_INT                     ISUM1;
   SC_INT                     ITEMP1;
   SC_INT                     ISUM2;
   SC_INT                     ITEMP1SAVED;
   SC_INT                     ITEMP2SAVED;
   SC_INT                     ITEMP2;
   SC__INSTC                  FB_FILTER_1;
   SC_REAL                    RTEMP1;
   SC_REAL                    RTEMP2;
   SC__INSTC                  FB_FILTER_2;
   SC__INSTC                  TON_1;
   SC_INT                     ITEMPORARY1;
   SC_INT                     ITEMPORARY2;
   SC__INSTC                  RS_1;
   SC__INSTC                  RS_2;
   SC__INSTC                  TON_2;
   SC_BOOL                    XTAKE2;
   SC__INSTC                  FB_GETTEMP_1;
   SC_INT                     ITEMP1_TRY;
   SC__INSTC                  FB_GETTEMP_2;
   SC_INT                     ITEMP2_TRY;
   SC_BOOL                    XTAKE1;
}FB_AC_GETTEMP_REVA_TYP;
typedef void (*__BPFN_FB_AC_GETTEMP_REVA)(FB_AC_GETTEMP_REVA_TYP* ip);

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XPHCOUNTER_OK;
   SC__INSTC                  FB_AC_CD_1;
   SC__INSTC                  FB_AC_CD_2;
   SC__INSTC                  FB_AC_CP1;
   SC__INSTC                  FB_AC_CP2;
   SC__INSTC                  FB_AC_EVAP_1;
   SC__INSTC                  FB_AC_1_1;
   SC__INSTC                  FB_AC_2_1;
   SC__INSTC                  FB_TIMECOUNT_DINT_1;
   SC_INT                     ITEMP_AC1;
   SC_INT                     ITEMP_AC2;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XERRORTEMPSENSOR2;
   SC_BOOL                    XFULL_AC1;
   SC_BOOL                    XFULL_AC2;
   SC_BOOL                    XH1_AC1;
   SC_BOOL                    XH1_AC2;
   SC_BOOL                    XH2_AC1;
   SC_BOOL                    XH2_AC2;
   SC_BOOL                    XHALF_AC1;
   SC_BOOL                    XHALF_AC2;
   SC__INSTC                  FB_HALFSEL_AC1;
   SC__INSTC                  FB_HALFSEL_AC2;
   SC_BOOL                    XENABLECDCP;
   SC__INSTC                  FB_AC_GETTEMP_REVA_1;
   SC_TIME                    TCMP1NORMAL;
   SC_TIME                    TCMP1STARTUP;
   SC_TIME                    TCMP2NORMAL;
   SC_TIME                    TCMP2STARTUP;
   SC_DINT                    DIAC1TIME_1;
   SC_DINT                    DIAC2TIME_1;
}P_AC_CABIN_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_AC_CABIN_00066(P_AC_CABIN_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_AC_CABIN_TYP, DIAC2TIME_1) + sizeof(ip->DIAC2TIME_1);
   /* non retains: */
   ip->XPHCOUNTER_OK = SC_C_BOOL(0);
   ip->ITEMP_AC1 = SC_C_INT(0);
   ip->ITEMP_AC2 = SC_C_INT(0);
   ip->XAC1ERROR = SC_C_BOOL(0);
   ip->XAC2ERROR = SC_C_BOOL(0);
   ip->XERRORTEMPSENSOR1 = SC_C_BOOL(0);
   ip->XERRORTEMPSENSOR2 = SC_C_BOOL(0);
   ip->XFULL_AC1 = SC_C_BOOL(0);
   ip->XFULL_AC2 = SC_C_BOOL(0);
   ip->XH1_AC1 = SC_C_BOOL(0);
   ip->XH1_AC2 = SC_C_BOOL(0);
   ip->XH2_AC1 = SC_C_BOOL(0);
   ip->XH2_AC2 = SC_C_BOOL(0);
   ip->XHALF_AC1 = SC_C_BOOL(0);
   ip->XHALF_AC2 = SC_C_BOOL(0);
   ip->XENABLECDCP = SC_C_BOOL(0);
   ip->TCMP1NORMAL = SC_C_TIME(1000);
   ip->TCMP1STARTUP = SC_C_TIME(5000);
   ip->TCMP2NORMAL = SC_C_TIME(3000);
   ip->TCMP2STARTUP = SC_C_TIME(25000);

   /* retains: */
   if(iRetain)
   {
      ip->DIAC1TIME_1 = SC_C_DINT(10266);
      ip->DIAC2TIME_1 = SC_C_DINT(10247);

   }
}


/* PROGRAM BODY: */
void SC_BODY__P_AC_CABIN_00035(void)
{
   P_AC_CABIN_TYP *ip = (P_AC_CABIN_TYP*) __OD(67);

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;

   PRG_ENTRY(35, ip);
/******************* Network *************************/
{
   FB_AC_GETTEMP_REVA_TYP* __lpt0 = (FB_AC_GETTEMP_REVA_TYP*)__OD(ip->FB_AC_GETTEMP_REVA_1);
   (*__lpt0).WTEMP1 = LD_I_W(3,1,4,2);
   (*__lpt0).WTEMP2 = LD_I_W(3,1,4,3);
   ((__BPFN_FB_AC_GETTEMP_REVA)__OF(57))((__lpt0));
   ip->ITEMP_AC1 = (*__lpt0).ITEMP;
   ip->XERRORTEMPSENSOR1 = (*__lpt0).XTEMPSENSORERROR1;
   ip->XERRORTEMPSENSOR2 = (*__lpt0).XTEMPSENSORERROR2;
   (*(AC_T_TYP*)__OD(34)).XTEMPSENSORERROR_1 = (*__lpt0).XTEMPSENSORERROR;

}
/******************* Network *************************/
{
   _SCT_B0 =  (!((1 & (*(AC_T_TYP*)__OD(34)).XAC_COMMAND_ON)));
   if((1 & _SCT_B0))goto SCL__RESET;

}
/******************* Network *************************/
{
   ip->XPHCOUNTER_OK = ((1 & LD_I_X(3,1,2,10)) & (1 & LD_I_X(3,2,3,6)));

}
/******************* Network *************************/
{
   FB_AC_1_TYP* __lpt0 = (FB_AC_1_TYP*)__OD(ip->FB_AC_1_1);
   (*__lpt0).XSTARTUP = (*(AC_T_TYP*)__OD(34)).XACSTARTUP;
   (*__lpt0).ITEMP = ip->ITEMP_AC1;
   (*__lpt0).XERRORTEMPSENSOR1 = ip->XERRORTEMPSENSOR1;
   (*__lpt0).XAC1ERROR = ip->XAC1ERROR;
   (*__lpt0).XAC2ERROR = ip->XAC2ERROR;
   (*__lpt0).XFULL = &(ip->XFULL_AC1);
   (*__lpt0).XHALF = &(ip->XHALF_AC1);
   ((__BPFN_FB_AC_1)__OF(58))((__lpt0));

}
/******************* Network *************************/
{
   FB_AC_2_TYP* __lpt0 = (FB_AC_2_TYP*)__OD(ip->FB_AC_2_1);
   (*__lpt0).XSTARTUP = (*(AC_T_TYP*)__OD(34)).XACSTARTUP;
   (*__lpt0).ITEMP = ip->ITEMP_AC2;
   (*__lpt0).XERRORTEMPSENSOR1 = ip->XERRORTEMPSENSOR2;
   (*__lpt0).XAC1ERROR = ip->XAC1ERROR;
   (*__lpt0).XAC2ERROR = ip->XAC2ERROR;
   (*__lpt0).XFULL = &(ip->XFULL_AC2);
   (*__lpt0).XHALF = &(ip->XHALF_AC2);
   ((__BPFN_FB_AC_2)__OF(59))((__lpt0));

}
/******************* Network *************************/
{
   FB_AC_HALFSEL_REVA_TYP* __lpt0 = (FB_AC_HALFSEL_REVA_TYP*)__OD(ip->FB_HALFSEL_AC1);
   (*__lpt0).XENABLEHALF = ip->XHALF_AC1;
   (*__lpt0).XAC1ERROR = ip->XAC1ERROR;
   (*__lpt0).XAC2ERROR = ip->XAC2ERROR;
   (*__lpt0).DIAC1TIME = ip->DIAC1TIME_1;
   (*__lpt0).DIAC2TIME = ip->DIAC2TIME_1;
   ((__BPFN_FB_AC_HALFSEL_REVA)__OF(60))((__lpt0));
   ip->XH1_AC1 = (*__lpt0).XHALF1;
   ip->XH2_AC1 = (*__lpt0).XHALF2;

}
/******************* Network *************************/
{
   FB_AC_HALFSEL_REVA_TYP* __lpt0 = (FB_AC_HALFSEL_REVA_TYP*)__OD(ip->FB_HALFSEL_AC2);
   (*__lpt0).XENABLEHALF = ip->XHALF_AC2;
   (*__lpt0).XAC1ERROR = ip->XAC1ERROR;
   (*__lpt0).XAC2ERROR = ip->XAC2ERROR;
   (*__lpt0).DIAC1TIME = ip->DIAC1TIME_1;
   (*__lpt0).DIAC2TIME = ip->DIAC2TIME_1;
   ((__BPFN_FB_AC_HALFSEL_REVA)__OF(60))((__lpt0));
   ip->XH1_AC2 = (*__lpt0).XHALF1;
   ip->XH2_AC2 = (*__lpt0).XHALF2;

}
/******************* Network *************************/
{
   FB_AC_EVAP_TYP* __lpt0 = (FB_AC_EVAP_TYP*)__OD(ip->FB_AC_EVAP_1);
   AC_T_TYP* __lpt1 = (AC_T_TYP*)__OD(34);
   (*__lpt0).XEVAP1NFB = LD_I_X(3,1,2,0);
   (*__lpt0).XEVAP2NFB = LD_I_X(3,1,2,3);
   (*__lpt0).X3PHCOUNTER = ip->XPHCOUNTER_OK;
   (*__lpt0).XTEMPPROTECTIONEV1 = LD_I_X(3,2,2,25);
   (*__lpt0).XTEMPPROTECTIONEV2 = LD_I_X(3,2,2,28);
   (*__lpt0).XACON = (*__lpt1).XAC_COMMAND_ON;
   ((__BPFN_FB_AC_EVAP)__OF(61))((__lpt0));
   (*__lpt1).XEVAP1_ON = (*__lpt0).XEVAP1ON;
   (*__lpt1).XEVAP2_ON = (*__lpt0).XEVAP2ON;
   ip->XENABLECDCP = (*__lpt0).XENABLECDCP;

}
/******************* Network *************************/
{
   FB_AC_CD_TYP* __lpt0 = (FB_AC_CD_TYP*)__OD(ip->FB_AC_CD_1);
   AC_T_TYP* __lpt1 = (AC_T_TYP*)__OD(34);
   (*__lpt0).XEVAP1K = LD_I_X(3,1,2,2);
   (*__lpt0).XEVAP2K = LD_I_X(3,2,3,1);
   (*__lpt0).XHALF = ip->XH1_AC1;
   (*__lpt0).XFULL = ip->XFULL_AC1;
   (*__lpt0).XCDANFB = LD_I_X(3,1,2,4);
   (*__lpt0).XCDBNFB = LD_I_X(3,1,2,5);
   (*__lpt0).XENABLE = ip->XENABLECDCP;
   (*__lpt0).XTEMPPROTECTIONCDA = LD_I_X(3,1,1,7);
   (*__lpt0).XTEMPPROTECTIONCDB = LD_I_X(3,1,1,23);
   (*__lpt0).TCDDELAY = SEL_TIME((*__lpt1).XACSTARTUP,ip->TCMP1NORMAL,ip->TCMP1STARTUP);
   ((__BPFN_FB_AC_CD)__OF(62))((__lpt0));
   (*__lpt1).XCD_AC1_ON = (*__lpt0).XCDAB_ON;

}
/******************* Network *************************/
{
   FB_AC_CD_TYP* __lpt0 = (FB_AC_CD_TYP*)__OD(ip->FB_AC_CD_2);
   AC_T_TYP* __lpt1 = (AC_T_TYP*)__OD(34);
   (*__lpt0).XEVAP1K = LD_I_X(3,1,2,2);
   (*__lpt0).XEVAP2K = LD_I_X(3,2,3,1);
   (*__lpt0).XHALF = ip->XH2_AC1;
   (*__lpt0).XFULL = ip->XFULL_AC1;
   (*__lpt0).XCDANFB = LD_I_X(3,2,3,2);
   (*__lpt0).XCDBNFB = LD_I_X(3,2,3,3);
   (*__lpt0).XENABLE = ip->XENABLECDCP;
   (*__lpt0).XTEMPPROTECTIONCDA = LD_I_X(3,2,2,7);
   (*__lpt0).XTEMPPROTECTIONCDB = LD_I_X(3,2,2,23);
   (*__lpt0).TCDDELAY = SEL_TIME((*__lpt1).XACSTARTUP,ip->TCMP2NORMAL,ip->TCMP2STARTUP);
   ((__BPFN_FB_AC_CD)__OF(62))((__lpt0));
   (*__lpt1).XCD_AC2_ON = (*__lpt0).XCDAB_ON;

}
/******************* Network *************************/
{
   FB_AC_CP1_TYP* __lpt0 = (FB_AC_CP1_TYP*)__OD(ip->FB_AC_CP1);
   AC_T_TYP* __lpt1 = (AC_T_TYP*)__OD(34);
   (*__lpt0).TCMPSTARTUP = ip->TCMP1STARTUP;
   (*__lpt0).XCMPA_NFB = LD_I_X(3,1,2,6);
   (*__lpt0).XCMPB_NFB = LD_I_X(3,1,2,7);
   (*__lpt0).XENABLE = ip->XENABLECDCP;
   (*__lpt0).XHP_CMPA = LD_I_X(3,1,2,14);
   (*__lpt0).XLP_CMPA = LD_I_X(3,1,2,12);
   (*__lpt0).XHP_CMPB = LD_I_X(3,1,2,15);
   (*__lpt0).XLP_CMPB = LD_I_X(3,1,2,13);
   (*__lpt0).XTEMPPROTECTIONCPA = LD_I_X(3,2,2,29);
   (*__lpt0).XTEMPPROTECTIONCPB = LD_I_X(3,2,2,30);
   (*__lpt0).XACSTARTUP = (*__lpt1).XACSTARTUP;
   (*__lpt0).XCDANFB = LD_I_X(3,1,2,4);
   (*__lpt0).XCDBNFB = LD_I_X(3,1,2,5);
   (*__lpt0).XHALF = ip->XH1_AC1;
   (*__lpt0).XFULL = ip->XFULL_AC1;
   (*__lpt0).TCMPNORMAL = ip->TCMP1NORMAL;
   ((__BPFN_FB_AC_CP1)__OF(63))((__lpt0));
   (*__lpt1).XCP_AC1_ON = (*__lpt0).XCMPAB_ON;

}
/******************* Network *************************/
{
   FB_AC_CP2_TYP* __lpt0 = (FB_AC_CP2_TYP*)__OD(ip->FB_AC_CP2);
   AC_T_TYP* __lpt1 = (AC_T_TYP*)__OD(34);
   (*__lpt0).TCMPSTARTUP = ip->TCMP2STARTUP;
   (*__lpt0).XCMPA_NFB = LD_I_X(3,1,2,6);
   (*__lpt0).XCMPB_NFB = LD_I_X(3,1,2,7);
   (*__lpt0).XENABLE = ip->XENABLECDCP;
   (*__lpt0).XHP_CMPA = LD_I_X(3,2,3,9);
   (*__lpt0).XLP_CMPA = LD_I_X(3,2,3,8);
   (*__lpt0).XTEMPPROTECTIONCPA = LD_I_X(3,2,2,31);
   (*__lpt0).XTEMPPROTECTIONCPB = LD_I_X(3,1,2,31);
   (*__lpt0).XACSTARTUP = (*__lpt1).XACSTARTUP;
   (*__lpt0).XCDANFB = LD_I_X(3,2,3,2);
   (*__lpt0).XCDBNFB = LD_I_X(3,2,3,3);
   (*__lpt0).XHALF = ip->XH2_AC1;
   (*__lpt0).XFULL = ip->XFULL_AC1;
   (*__lpt0).TCMPNORMAL = ip->TCMP2NORMAL;
   ((__BPFN_FB_AC_CP2)__OF(64))((__lpt0));
   (*__lpt1).XCP_AC2_ON = (*__lpt0).XCMPAB_ON;

}
/******************* Network *************************/
{
   FB_TIMECOUNT_DINT_TYP* __lpt0 = (FB_TIMECOUNT_DINT_TYP*)__OD(ip->FB_TIMECOUNT_DINT_1);
   (*__lpt0).XENABLE1 = (((1 & LD_I_X(3,1,2,6)) & (1 & LD_I_X(3,1,2,7))) & (1 & LD_I_X(3,2,
   3,10)));
   (*__lpt0).XENABLE2 = (((1 & LD_I_X(3,1,2,6)) & (1 & LD_I_X(3,1,2,7))) & (1 & LD_I_X(3,2,
   3,11)));
   (*__lpt0).DITIME1 = &(ip->DIAC1TIME_1);
   (*__lpt0).DITIME2 = &(ip->DIAC2TIME_1);
   ((__BPFN_FB_TIMECOUNT_DINT)__OF(65))((__lpt0));

}
/******************* Network *************************/
{
   AC_T_TYP* __lpt0 = (AC_T_TYP*)__OD(34);
   (*__lpt0).ITEMP_1 = ip->ITEMP_AC1;
   (*__lpt0).XHALFMODE_1 = ip->XHALF_AC1;
   (*__lpt0).XFULLMODE_1 = ip->XFULL_AC1;
   (*__lpt0).XACERROR_1 = ((1 & ip->XAC1ERROR) | (1 & ip->XAC2ERROR));
   (*__lpt0).DICP1TIME_1 = ip->DIAC1TIME_1;
   (*__lpt0).DICP2TIME_1 = ip->DIAC2TIME_1;

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      PRG_EXIT(35, ip);
      return;
   }

}
/******************* Network *************************/
SCL__RESET:;
{
   AC_T_TYP* __lpt0 = (AC_T_TYP*)__OD(34);
   (*__lpt0).XHALFMODE_1 = SC_C_BOOL(0);
   (*__lpt0).XFULLMODE_1 = SC_C_BOOL(0);
   (*__lpt0).XEVAP1_ON = SC_C_BOOL(0);
   (*__lpt0).XEVAP1FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XEVAP2_ON = SC_C_BOOL(0);
   (*__lpt0).XEVAP2FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCD_AC1_ON = SC_C_BOOL(0);
   (*__lpt0).XCD1FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCD2FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCD_AC2_ON = SC_C_BOOL(0);
   (*__lpt0).XCD3FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCD4FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCP_AC1_ON = SC_C_BOOL(0);
   (*__lpt0).XCP1FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCP2FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCP_AC2_ON = SC_C_BOOL(0);
   (*__lpt0).XCP3FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XCP4FAULT_1 = SC_C_BOOL(0);
   (*__lpt0).XACERROR_1 = SC_C_BOOL(0);

}
/******************* Network *************************/
{
   AC_T_TYP* __lpt0 = (AC_T_TYP*)__OD(34);
   (*__lpt0).ITEMP_1 = ip->ITEMP_AC1;
   (*__lpt0).DICP1TIME_1 = ip->DIAC1TIME_1;
   (*__lpt0).DICP2TIME_1 = ip->DIAC2TIME_1;

}

   PRG_EXIT(35, ip);
}
