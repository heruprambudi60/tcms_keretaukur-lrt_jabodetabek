#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


static const RunTimeCheckInit_T SC__RunTimeCheckInit =
{
   sizeof(RunTimeCheckInit_T),
   0,   /* Backtrace flag */
   0,   /* Array range check flag */
   0,   /* Array range error reset flag */
   0,   /* Stack check flag */
   1,   /* Watch dog flag */
   5,   /* Watch dog timer interval */
   32768,   /* Max. Object Table entries */
};

/* PROGRAM ENTRY: */
extern void GlobalInit(int iRetain);
void ProgramEntry(void)
{
   GlobalXROMInit();
   RunTimeCheckInit( &SC__RunTimeCheckInit);
   GlobalInit(ColdStart());
   InitHW();
   USR_SetTaskData(0,(void (*)())__OF(7),0,0,0);
}

