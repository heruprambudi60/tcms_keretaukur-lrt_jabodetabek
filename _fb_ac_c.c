#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XEVAP1K;
   SC_BOOL                    XEVAP2K;
   SC_BOOL                    XHALF;
   SC_BOOL                    XFULL;
   SC_BOOL                    XCDANFB;
   SC_BOOL                    XCDBNFB;
   SC_BOOL                    XENABLE;
   SC_BOOL                    XTEMPPROTECTIONCDA;
   SC_BOOL                    XTEMPPROTECTIONCDB;
   SC_TIME                    TCDDELAY;
   SC_BOOL                    XCDAB_ON;
   SC__INSTC                  TON_2;
}FB_AC_CD_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_AC_CD_00090(FB_AC_CD_TYP* ip1, FB_AC_CD_TYP* ip2)
{
   MOVE_ANY(&(ip1->XEVAP1K), &(ip2->XEVAP1K), (long) (&(ip1->TON_2)) - (long) (&(ip1->XEVAP1K)));
   MOVE_ANY(__OD(ip1->TON_2), __OD(ip2->TON_2), sizeof(TON_TYP));
}

void SC_INIT__FB_AC_CD_00091(FB_AC_CD_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_AC_CD_TYP, TON_2) + sizeof(ip->TON_2);
   /* non retains: */
   ip->XEVAP1K = SC_C_BOOL(0);
   ip->XEVAP2K = SC_C_BOOL(0);
   ip->XHALF = SC_C_BOOL(0);
   ip->XFULL = SC_C_BOOL(0);
   ip->XCDANFB = SC_C_BOOL(0);
   ip->XCDBNFB = SC_C_BOOL(0);
   ip->XENABLE = SC_C_BOOL(0);
   ip->XTEMPPROTECTIONCDA = SC_C_BOOL(0);
   ip->XTEMPPROTECTIONCDB = SC_C_BOOL(0);
   ip->TCDDELAY = SC_C_TIME(0);
   ip->XCDAB_ON = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_AC_CD_00062(FB_AC_CD_TYP* ip)
{

   /* TEMPORARY VARIABLES */
   /* <non> */

   FB_ENTRY(62, ip);
/******************* Network *************************/
{
   TON_TYP* __lpt0 = (TON_TYP*)__OD(ip->TON_2);
   (*__lpt0).IN = ((((((((1 & ip->XEVAP1K) & (1 & ip->XEVAP2K)) & (1 & ip->XENABLE)) & (1 
   & ip->XTEMPPROTECTIONCDA)) & (1 & ip->XTEMPPROTECTIONCDB)) & ((1 & ip->XHALF) | (1 & ip->XFULL))) 
   & (1 & ip->XCDANFB)) & (1 & ip->XCDBNFB));
   (*__lpt0).PT = ip->TCDDELAY;
   TON((__lpt0));
   ip->XCDAB_ON = (*__lpt0).Q;

}

   FB_EXIT(62, ip);
}
