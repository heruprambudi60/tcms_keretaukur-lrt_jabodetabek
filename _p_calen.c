#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
typedef SC_STRINGDEF(20)  STRING__TYPE20;


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_UINT                    UIYYYY;
   SC_USINT                   USMM;
   SC_USINT                   USDD;
   SC_USINT                   USHH;
   SC_USINT                   USMIN;
   SC_USINT                   USSS;
   SC_INT                     IDAYOFWK;
   SC_DT                      DTDATETIME;
   STRING__TYPE20             SDATEANDTIME;
}CALENDAR_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_BOOL                    XCMPBAYPASS1;
   SC_BOOL                    XSETDT;
   SC_BOOL                    XTEMPUP;
   SC_BOOL                    XTEMPDOWN;
}MODBUSBOOL_RW_T_TYP;

typedef struct
{
   SC_DWORD                   DWSTART;
   SC_UDINT                   UDALARMAUTOREFRESHTIMESEC;
   SC_UINT                    UIALARMINDEXONFIRSTOUTELEM;
   SC_INT                     IALARMFILTERSUBSYSTEM;
   SC_INT                     IALARMFILTERMINDDS;
   SC_INT                     IALARMFILTERMAXDDS;
   SC_UDINT                   UDALARMFILTERMINDDSTIME;
   SC_UDINT                   UDALARMFILTERMAXDDSTIME;
   SC_UINT                    UIALARMSORTMODE;
   SC_WORD                    WALARMCUSTOMATTR;
   SC_UINT                    UIALARMCUSTOMATTRFUNC;
   SC_UINT                    UIALARMDDSACKNOWLEDGED;
   SC_UINT                    UIALARMACKNOWLEDGEUSER;
   SC_INT                     IWHEELDIAMETER;
   SC_INT                     ISETDD;
   SC_INT                     ISETMM;
   SC_INT                     ISETYYYY;
   SC_INT                     ISETHH;
   SC_INT                     ISETMIN;
   SC_INT                     ISETSS;
   SC_INT                     IACK;
}MODBUSINT_RW_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  CNV_DT_TO_STRING_1;
   SC__INSTC                  CNV_STRING_TO_DINT_1;
   SC__INSTC                  CNV_STRING_TO_DINT_2;
   SC__INSTC                  CNV_STRING_TO_DINT_3;
   SC__INSTC                  CNV_STRING_TO_DINT_4;
   SC__INSTC                  CNV_STRING_TO_DINT_5;
   SC__INSTC                  CNV_STRING_TO_DINT_6;
   SC_BYTE                    IBDAY_VCU;
   SC_BYTE                    IBHOUR_VCU;
   SC_BYTE                    IBMIN_VCU;
   SC_BYTE                    IBMONTH_VCU;
   SC_BYTE                    IBSEC_VCU;
   SC_BYTE                    IBYEAR_VCU;
   SC_INT                     IERRORCNVDD;
   SC_INT                     IERRORCNVDTTOSTR;
   SC_INT                     IERRORCNVHH;
   SC_INT                     IERRORCNVMIN;
   SC_INT                     IERRORCNVMM;
   SC_INT                     IERRORCNVSS;
   SC_INT                     IERRORCNVYYYY;
   SC_INT                     IERRORSETDT;
   SC__INSTC                  R_TRIG_1;
   STRING__TYPE20             SDATETIME;
   SC_BOOL                    SXMVBSTRONG;
   SC_BOOL                    SXMVBWEAK;
   SC__INSTC                  TIME_GET_RTC_1;
   SC_UINT                    UIMS;
   SC__INSTC                  CNV_DT_TO_STRING_2;
   SC__INSTC                  CNV_DT_TO_STRING_3;
}P_CALENDER_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_CALENDER_00109(P_CALENDER_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_CALENDER_TYP, CNV_DT_TO_STRING_3) + sizeof(ip->CNV_DT_TO_STRING_3);
   /* non retains: */
   ip->IBDAY_VCU = SC_C_BYTE(0);
   ip->IBHOUR_VCU = SC_C_BYTE(0);
   ip->IBMIN_VCU = SC_C_BYTE(0);
   ip->IBMONTH_VCU = SC_C_BYTE(0);
   ip->IBSEC_VCU = SC_C_BYTE(0);
   ip->IBYEAR_VCU = SC_C_BYTE(0);
   ip->IERRORCNVDD = SC_C_INT(0);
   ip->IERRORCNVDTTOSTR = SC_C_INT(0);
   ip->IERRORCNVHH = SC_C_INT(0);
   ip->IERRORCNVMIN = SC_C_INT(0);
   ip->IERRORCNVMM = SC_C_INT(0);
   ip->IERRORCNVSS = SC_C_INT(0);
   ip->IERRORCNVYYYY = SC_C_INT(0);
   ip->IERRORSETDT = SC_C_INT(0);
   INIT_STRING((SC_STRING*)&ip->SDATETIME,20,0,(SC_STRING_CHAR*)"");
   ip->SXMVBSTRONG = SC_C_BOOL(0);
   ip->SXMVBWEAK = SC_C_BOOL(0);
   ip->UIMS = SC_C_UINT(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_CALENDER_00043(void)
{
   P_CALENDER_TYP *ip = (P_CALENDER_TYP*) __OD(110);

   /* TEMPORARY VARIABLES */
   SC_STRINGDEF(255)          _SCT_t_4 = {255,0,""};
   SC_BOOL                    _SCT_B0;
   /* literal strings: */
   static const SC_STRINGDEF(1) _SCT_t_1 = {1,1,"-"};
   static const SC_STRINGDEF(1) _SCT_t_2 = {1,1,":"};
   static const SC_STRINGDEF(0) _SCT_t_3 = {0,0,""};

   PRG_ENTRY(43, ip);
/******************* Network *************************/
{
   R_TRIG_TYP* __lpt0 = (R_TRIG_TYP*)__OD(ip->R_TRIG_1);
   (*__lpt0).CLK = (*(MODBUSBOOL_RW_T_TYP*)__OD(22)).XSETDT;
   R_TRIG((__lpt0));
   _SCT_B0 =  (!((1 & (*__lpt0).Q)));
   if((1 & _SCT_B0))goto SCL__GETTIME;

}
/******************* Network *************************/
{
   _SCT_B0 = ((1 & ip->SXMVBWEAK) &  (!((1 & ip->SXMVBSTRONG))));
   if((1 & _SCT_B0))goto SCL__WEAK;

}
/******************* Network *************************/
{
   MODBUSINT_RW_T_TYP* __lpt0 = (MODBUSINT_RW_T_TYP*)__OD(23);
   ip->IERRORSETDT = TIME_SET_RTC_DT(CNV_WATCH_TO_DT(INT_TO_UINT((*__lpt0).ISETYYYY),INT_TO_USINT(
   (*__lpt0).ISETMM),INT_TO_USINT((*__lpt0).ISETDD),INT_TO_USINT((*__lpt0).ISETHH),INT_TO_USINT(
   (*__lpt0).ISETMIN),INT_TO_USINT((*__lpt0).ISETSS)));

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0))goto SCL__GETTIME;

}
/******************* Network *************************/
SCL__WEAK:;
{
   ip->IERRORSETDT = TIME_SET_RTC_DT(CNV_WATCH_TO_DT(BYTE_TO_UINT(ip->IBYEAR_VCU),BYTE_TO_USINT(
   ip->IBMONTH_VCU),BYTE_TO_USINT(ip->IBDAY_VCU),BYTE_TO_USINT(ip->IBHOUR_VCU),BYTE_TO_USINT(
   ip->IBMIN_VCU),BYTE_TO_USINT(ip->IBSEC_VCU)));

}
/******************* Network *************************/
SCL__GETTIME:;
{
   TIME_GET_RTC_TYP* __lpt0 = (TIME_GET_RTC_TYP*)__OD(ip->TIME_GET_RTC_1);
   TIME_GET_RTC((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).DTDATETIME = (*__lpt0).DTACTUALDT;
   ip->UIMS = (*__lpt0).UIMILLISEC;

}
/******************* Network *************************/
{
   CNV_DT_TO_STRING_TYP* __lpt0 = (CNV_DT_TO_STRING_TYP*)__OD(ip->CNV_DT_TO_STRING_1);
   (*__lpt0).DTACTUALDT = (*(CALENDAR_T_TYP*)__OD(28)).DTDATETIME;
   (*__lpt0).USMODE_DT_D_T = SC_C_USINT(0);
   (*__lpt0).XYEARIN4DIG = SC_C_BOOL(1);
   (*__lpt0).XHRSMINSEC = SC_C_BOOL(1);
   (*__lpt0).X12HRS = SC_C_BOOL(0);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SDATESEPCHAR),(SC_STRING*)&(_SCT_t_1));
   MOVE_STRING((SC_STRING*)&((*__lpt0).STIMESEPCHAR),(SC_STRING*)&(_SCT_t_2));
   (*__lpt0).USCOUNTRY = SC_C_USINT(0);
   CNV_DT_TO_STRING((__lpt0));
   MOVE_STRING((SC_STRING*)&(ip->SDATETIME),(SC_STRING*)&((*__lpt0).SSTRING));
   ip->IERRORCNVDTTOSTR = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CNV_DT_TO_STRING_TYP* __lpt0 = (CNV_DT_TO_STRING_TYP*)__OD(ip->CNV_DT_TO_STRING_3);
   CNV_DT_TO_STRING_TYP* __lpt1 = (CNV_DT_TO_STRING_TYP*)__OD(ip->CNV_DT_TO_STRING_2);
   CALENDAR_T_TYP* __lpt2 = (CALENDAR_T_TYP*)__OD(28);
   (*__lpt1).DTACTUALDT = (*__lpt2).DTDATETIME;
   (*__lpt1).USMODE_DT_D_T = SC_C_USINT(1);
   (*__lpt1).XYEARIN4DIG = SC_C_BOOL(0);
   (*__lpt1).XHRSMINSEC = SC_C_BOOL(1);
   (*__lpt1).X12HRS = SC_C_BOOL(0);
   MOVE_STRING((SC_STRING*)&((*__lpt1).SDATESEPCHAR),(SC_STRING*)&(_SCT_t_3));
   MOVE_STRING((SC_STRING*)&((*__lpt1).STIMESEPCHAR),(SC_STRING*)&(_SCT_t_3));
   (*__lpt1).USCOUNTRY = SC_C_USINT(2);
   CNV_DT_TO_STRING((__lpt1));
   (*__lpt0).DTACTUALDT = (*__lpt2).DTDATETIME;
   (*__lpt0).USMODE_DT_D_T = SC_C_USINT(2);
   (*__lpt0).XYEARIN4DIG = SC_C_BOOL(0);
   (*__lpt0).XHRSMINSEC = SC_C_BOOL(1);
   (*__lpt0).X12HRS = SC_C_BOOL(0);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SDATESEPCHAR),(SC_STRING*)&(_SCT_t_3));
   MOVE_STRING((SC_STRING*)&((*__lpt0).STIMESEPCHAR),(SC_STRING*)&(_SCT_t_3));
   (*__lpt0).USCOUNTRY = SC_C_USINT(2);
   CNV_DT_TO_STRING((__lpt0));
   MOVE_STRING((SC_STRING*)&((*__lpt2).SDATEANDTIME),CONCAT_STR((SC_STRING*)&_SCT_t_4,2,(SC_STRING*)&(
   (*__lpt1).SSTRING),(SC_STRING*)&((*__lpt0).SSTRING)));

}
/******************* Network *************************/
{
   CNV_STRING_TO_DINT_TYP* __lpt0 = (CNV_STRING_TO_DINT_TYP*)__OD(ip->CNV_STRING_TO_DINT_1);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SSTRING),LEFT((SC_STRING*)&_SCT_t_4,(SC_STRING*)&(ip->SDATETIME),
   SC_C_INT(2)));
   CNV_STRING_TO_DINT((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).USDD = DINT_TO_USINT((*__lpt0).DIDINT);
   ip->IERRORCNVDD = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CNV_STRING_TO_DINT_TYP* __lpt0 = (CNV_STRING_TO_DINT_TYP*)__OD(ip->CNV_STRING_TO_DINT_2);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SSTRING),MID((SC_STRING*)&_SCT_t_4,(SC_STRING*)&(ip->SDATETIME),
   SC_C_INT(2),SC_C_INT(4)));
   CNV_STRING_TO_DINT((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).USMM = DINT_TO_USINT((*__lpt0).DIDINT);
   ip->IERRORCNVMM = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CNV_STRING_TO_DINT_TYP* __lpt0 = (CNV_STRING_TO_DINT_TYP*)__OD(ip->CNV_STRING_TO_DINT_3);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SSTRING),MID((SC_STRING*)&_SCT_t_4,(SC_STRING*)&(ip->SDATETIME),
   SC_C_INT(4),SC_C_INT(7)));
   CNV_STRING_TO_DINT((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).UIYYYY = DINT_TO_UINT((*__lpt0).DIDINT);
   ip->IERRORCNVYYYY = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CNV_STRING_TO_DINT_TYP* __lpt0 = (CNV_STRING_TO_DINT_TYP*)__OD(ip->CNV_STRING_TO_DINT_4);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SSTRING),MID((SC_STRING*)&_SCT_t_4,(SC_STRING*)&(ip->SDATETIME),
   SC_C_INT(2),SC_C_INT(12)));
   CNV_STRING_TO_DINT((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).USHH = DINT_TO_USINT((*__lpt0).DIDINT);
   ip->IERRORCNVHH = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CNV_STRING_TO_DINT_TYP* __lpt0 = (CNV_STRING_TO_DINT_TYP*)__OD(ip->CNV_STRING_TO_DINT_5);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SSTRING),MID((SC_STRING*)&_SCT_t_4,(SC_STRING*)&(ip->SDATETIME),
   SC_C_INT(2),SC_C_INT(15)));
   CNV_STRING_TO_DINT((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).USMIN = DINT_TO_USINT((*__lpt0).DIDINT);
   ip->IERRORCNVMIN = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CNV_STRING_TO_DINT_TYP* __lpt0 = (CNV_STRING_TO_DINT_TYP*)__OD(ip->CNV_STRING_TO_DINT_6);
   MOVE_STRING((SC_STRING*)&((*__lpt0).SSTRING),MID((SC_STRING*)&_SCT_t_4,(SC_STRING*)&(ip->SDATETIME),
   SC_C_INT(2),SC_C_INT(18)));
   CNV_STRING_TO_DINT((__lpt0));
   (*(CALENDAR_T_TYP*)__OD(28)).USSS = DINT_TO_USINT((*__lpt0).DIDINT);
   ip->IERRORCNVSS = (*__lpt0).IERROR;

}
/******************* Network *************************/
{
   CALENDAR_T_TYP* __lpt0 = (CALENDAR_T_TYP*)__OD(28);
   (*__lpt0).IDAYOFWK = USINT_TO_INT(CNV_GET_DTDYOFWK((*__lpt0).DTDATETIME));

}

   PRG_EXIT(43, ip);
}
