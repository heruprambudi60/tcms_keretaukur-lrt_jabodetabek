#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XTRAINWASH_ON;
}TRAIN_WASH_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_TRAIN_WASH_MODE_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_TRAIN_WASH_MODE_00101(P_TRAIN_WASH_MODE_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_TRAIN_WASH_MODE_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_TRAIN_WASH_MODE_00039(void)
{
   P_TRAIN_WASH_MODE_TYP *ip = (P_TRAIN_WASH_MODE_TYP*) __OD(102);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(39, ip);
/******************* Network *************************/
{
   CABIN_T_TYP* __lpt0 = (CABIN_T_TYP*)__OD(11);
   (*(TRAIN_WASH_MODE_T_TYP*)__OD(31)).XTRAINWASH_ON = (((1 & LD_I_X(3,1,2,28)) & (1 & (*__lpt0).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,1,2,28)) & (1 & (*__lpt0).XCAB2ACTIVE)));

}

   PRG_EXIT(39, ip);
}
