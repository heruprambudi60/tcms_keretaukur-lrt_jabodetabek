#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XENGINERUNNING;
   SC_BOOL                    XENGINESTOP;
   SC_BOOL                    XENGINEEMERGENCYSTOP;
}ENGINE_STATUS_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
}P_ENGINE_STATUS_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_ENGINE_STATUS_00099(P_ENGINE_STATUS_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_ENGINE_STATUS_TYP, STRUCT__SIZE) + sizeof(ip->STRUCT__SIZE);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_ENGINE_STATUS_00038(void)
{
   P_ENGINE_STATUS_TYP *ip = (P_ENGINE_STATUS_TYP*) __OD(100);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(38, ip);
   (*(ENGINE_STATUS_T_TYP*)__OD(33)).XENGINERUNNING = (((1 & LD_I_X(3,1,2,8)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,2,19)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(ENGINE_STATUS_T_TYP*)__OD(33)).XENGINESTOP = (((1 & LD_I_X(3,1,2,18)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((1 & LD_I_X(3,2,2,20)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(ENGINE_STATUS_T_TYP*)__OD(33)).XENGINEEMERGENCYSTOP = (((1 & LD_I_X(3,1,2,30)) & (1 
   & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) | ((1 & LD_I_X(3,2,2,21)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));

   PRG_EXIT(38, ip);
}
