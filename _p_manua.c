#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XMANUALMODE;
   SC_BOOL                    XTCMSFAULT;
   SC_BOOL                    XTCMSOK;
}MANUALMODE_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSET_1;
   SC_BOOL                    XRESET_1;
   SC__INSTC                  RS_1;
}P_MANUALMODE_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_MANUALMODE_00131(P_MANUALMODE_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_MANUALMODE_TYP, RS_1) + sizeof(ip->RS_1);
   /* non retains: */
   ip->XSET_1 = SC_C_BOOL(0);
   ip->XRESET_1 = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_MANUALMODE_00054(void)
{
   P_MANUALMODE_TYP *ip = (P_MANUALMODE_TYP*) __OD(132);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(54, ip);
   (*(MANUALMODE_T_TYP*)__OD(13)).XTCMSOK =  (!((1 & (*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE)));
   ip->XSET_1 = (((1 & LD_I_X(3,1,1,2)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) | (
   (1 & LD_I_X(3,2,2,2)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XRESET_1 = (( (!((1 & LD_I_X(3,1,1,2)))) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ( (!((1 & LD_I_X(3,2,2,2)))) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   (*(RS_TYP*)__OD(ip->RS_1)).SET = ip->XSET_1;
   (*(RS_TYP*)__OD(ip->RS_1)).RESET1 = ip->XRESET_1;
   RS(((RS_TYP*)__OD(ip->RS_1)));
   (*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE = (*(RS_TYP*)__OD(ip->RS_1)).Q1;
   (*(MANUALMODE_T_TYP*)__OD(13)).XTCMSFAULT =  (!(((1 & (*(MANUALMODE_T_TYP*)__OD(13)).XTCMSOK) 
   & (1 & (*(MANUALMODE_T_TYP*)__OD(13)).XMANUALMODE))));

   PRG_EXIT(54, ip);
}
