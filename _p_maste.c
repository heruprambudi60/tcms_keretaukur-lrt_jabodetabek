#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XFWD;
   SC_BOOL                    XREV;
   SC_INT                     IPOWERING;
   SC_INT                     IBRAKING;
   SC_BOOL                    XFASTBRAKE;
   SC_BOOL                    XEB;
   SC_REAL                    RPOWERING;
   SC_REAL                    RPOWER_MAX;
}MASTERCTRL_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XP2;
   SC_BOOL                    XP5;
   SC_BOOL                    XB1;
   SC_BOOL                    XB2;
   SC_BOOL                    XB3;
   SC_BOOL                    XB4;
   SC_BOOL                    XB5;
   SC_BOOL                    XB6;
   SC_BOOL                    XB7;
}P_MASTERCTRL_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_MASTERCTRL_00133(P_MASTERCTRL_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_MASTERCTRL_TYP, XB7) + sizeof(ip->XB7);
   /* non retains: */
   ip->XP2 = SC_C_BOOL(0);
   ip->XP5 = SC_C_BOOL(0);
   ip->XB1 = SC_C_BOOL(0);
   ip->XB2 = SC_C_BOOL(0);
   ip->XB3 = SC_C_BOOL(0);
   ip->XB4 = SC_C_BOOL(0);
   ip->XB5 = SC_C_BOOL(0);
   ip->XB6 = SC_C_BOOL(0);
   ip->XB7 = SC_C_BOOL(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_MASTERCTRL_00055(void)
{
   P_MASTERCTRL_TYP *ip = (P_MASTERCTRL_TYP*) __OD(134);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(55, ip);
   (*(MASTERCTRL_T_TYP*)__OD(12)).XFWD = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (
   1 & LD_I_X(3,1,1,3))) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & LD_I_X(3,2,
   2,3))));
   (*(MASTERCTRL_T_TYP*)__OD(12)).XREV = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) & (
   1 & LD_I_X(3,1,1,4))) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & LD_I_X(3,2,
   2,4))));
   ip->XP2 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(6)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(6)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XP5 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(7)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(7)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));

   if((1 & ip->XP2))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IPOWERING = SC_C_INT(2);
   }
   else if((1 & ip->XP5))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IPOWERING = SC_C_INT(5);
   }
   else
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IPOWERING = SC_C_INT(0);
   }

   ip->XB1 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(6)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(6)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XB2 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(5)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(5)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XB3 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(4)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(4)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XB4 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(3)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(3)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XB5 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(2)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(2)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XB6 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(1)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(1)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));
   ip->XB7 = (((SHR_BYTE(LD_I_B(3,1,1,1),SC_C_USINT(5)) == SC_C_BYTE(0)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE)) 
   | ((SHR_BYTE(LD_I_B(3,2,2,1),SC_C_USINT(5)) == SC_C_BYTE(0)) & (1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE)));

   if((1 & ip->XB1))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(1);
   }
   else if((1 & ip->XB2))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(2);
   }
   else if((1 & ip->XB3))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(3);
   }
   else if((1 & ip->XB4))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(4);
   }
   else if((1 & ip->XB5))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(5);
   }
   else if((1 & ip->XB6))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(6);
   }
   else if((1 & ip->XB7))
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(7);
   }
   else
   {
      (*(MASTERCTRL_T_TYP*)__OD(12)).IBRAKING = SC_C_INT(0);
   }

   (*(MASTERCTRL_T_TYP*)__OD(12)).XFASTBRAKE = (((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB1ACTIVE) 
   & (1 & LD_I_X(3,1,1,6))) | ((1 & (*(CABIN_T_TYP*)__OD(11)).XCAB2ACTIVE) & (1 & LD_I_X(3,
   2,2,6))));

   PRG_EXIT(55, ip);
}
