BEGIN_ATTRIBUTES
	SaveVersion=1
	Name=P_OO_HMI
	ClassId=15
	BodyClassId=26
END_ATTRIBUTES
PROGRAM P_OO_HMI
(**)
(**)
(*GroupPath='POUs'*)
	VAR_EXTERNAL 
		gDiscreteInputs_t: MODBUSBOOL_R_T;
		gCabses_t: CABIN_T;
		gMasterCtrl_t: MASTERCTRL_T;
		gInputRegisters_t: MODBUSINT_R_T;
		gBatt_t: BATT_T;
		gManualMode_t: MANUALMODE_T;
		gDeadman_t: DEADMAN_T;
		gDoors_t: DOORS_T;
		gTraction_M_t: TRACTION_MNGMNT;
		gbrake_t: BRAKE_T;
		gCompressor_t: COMPRESSOR_T;
		gSignalLamp_t: SIGNAL_LAMP_T;
		gHeadLamp_t: HEAD_LAMP_T;
		gComTest_t: COMPRESOR_TEST_T;
		gUltrason_t: ULTRASON_MODE_T;
		gTrainwsh_t: TRAIN_WASH_MODE_T;
		gEnginestatus_t: ENGINE_STATUS_T;
		IxDynBrake_1: BOOL:=FALSE;
			(*DI 19*)
		IxDynBrake_2: BOOL:=FALSE;
			(*DI 22*)
		IxSmokeDect_1: BOOL:=FALSE;
			(*DI 26*)
		IxSmokeDect_2: BOOL:=FALSE;
			(*DI 31*)
	END_VAR
'ST'
BODY
(********************************************************************************
                             CABIN SELECTION
*********************************************************************************)
gDiscreteInputs_t.xCab1Front	:= gCabses_t.xCab1Active;
gDiscreteInputs_t.xCab2Front	:= gCabses_t.xCab2Active;
gDiscreteInputs_t.xCabError		:= gCabses_t.xCabError;

(********************************************************************************
                             	MANUAL MODE
*********************************************************************************)
(*MODE*)
gDiscreteInputs_t.xManualMode	:= gManualMode_t.xManualMode;

(********************************************************************************
                            MASTER CONTROLLER
*********************************************************************************)
gDiscreteInputs_t.xCab1Dir	:= (gCabses_t.xCab1Active AND gMasterCtrl_t.xFwd) OR (gCabses_t.xCab2Active AND gMasterCtrl_t.xRev);
gDiscreteInputs_t.xCab2Dir	:= (gCabses_t.xCab1Active AND gMasterCtrl_t.xRev) OR (gCabses_t.xCab2Active AND gMasterCtrl_t.xFwd);
gInputRegisters_t.iPowering	:= gMasterCtrl_t.iPowering;
gInputRegisters_t.iBraking	:= gMasterCtrl_t.iBraking;
gDiscreteInputs_t.xFastBrake	:= gMasterCtrl_t.xFastBrake;

(********************************************************************************
                                 BATTERY
*********************************************************************************)
gDiscreteInputs_t.xBattShutDown	:= gBatt_t.xBattOFF_Cmd;
gInputRegisters_t.rBattVoltage   := gBatt_t.rBattV;
gDiscreteInputs_t.xBattLow		:= gBatt_t.xBattLow;

(********************************************************************************
                                 DEADMAN
*********************************************************************************)
gDiscreteInputs_t.xDeadmanActive	:= gDeadman_t.xDeadBuzzer;
gDiscreteInputs_t.xDeadmanTrCutOff	:= gDeadman_t.xDeadTrCutOFF;
gDiscreteInputs_t.xDeadmanEB		:= gDeadman_t.xDeadEB;

(********************************************************************************
                                 DOORS
*********************************************************************************)
gDiscreteInputs_t.xD1Open	:= gDoors_t.xD1_Open;
gDiscreteInputs_t.xD2Open	:= gDoors_t.xD2_Open;
gDiscreteInputs_t.xD1Locked	:= gDoors_t.xD1_Locked;
gDiscreteInputs_t.xD2Locked	:= gDoors_t.xD2_Locked;

(********************************************************************************
                                 Brake
*********************************************************************************)
gDiscreteInputs_t.xEBSw := gbrake_t.xEBSw;
gDiscreteInputs_t.xEBBypass := gbrake_t.xEBBypass;
gDiscreteInputs_t.xParkingBrakeApplied := gbrake_t.xParkingBrk;
 
gDiscreteInputs_t.xAllBrakeRelease := gbrake_t.xAllBrakeRelease;
gDiscreteInputs_t.xBrakeCylinder := gbrake_t.xBrakePres_SW;
gDiscreteInputs_t.xBrakePipePresure := gbrake_t.xBrakePipePres_SW;
gDiscreteInputs_t.xHoldingBrake := gbrake_t.xHoldBrake;
gDiscreteInputs_t.xDynamicBrakeSW := IxDynBrake_1 OR IxDynBrake_2;

(********************************************************************************
                                 Traction Management
*********************************************************************************)
gDiscreteInputs_t.xTractionReady := gTraction_M_t.xTrReady ;

(********************************************************************************
                                 Compressor Management
*********************************************************************************)
gDiscreteInputs_t.xCom1_ON := gCompressor_t.xComp1_ACTV;
gDiscreteInputs_t.xCom2_ON := gCompressor_t.xComp2_ACTV;
gDiscreteInputs_t.xComFault := gCompressor_t.xCompFault;
gDiscreteInputs_t.xCompressor_Error := gCompressor_t.xCompErr ;
gDiscreteInputs_t.xCom_OFF := gCompressor_t.xCompOff;
gDiscreteInputs_t.xComEmg := gCompressor_t.xEmgComp;
gDiscreteInputs_t.xComBypass := gCompressor_t.xCompBypass;
gInputRegisters_t.iCmp1_hh := dint_to_int (gCompressor_t.diCmpTime_1/60);
gInputRegisters_t.iCmp2_hh := dint_to_int (gCompressor_t.diCmpTime_2/60);



gInputRegisters_t.iFillingTime := gComTest_t.xFillingTime;
gInputRegisters_t.iLeakageTime := gComTest_t.xLeakageCommand ;

(********************************************************************************
                                 Signal Lamp
*********************************************************************************)
gDiscreteInputs_t.xFrWhite		:= gSignalLamp_t.xFrontR_Wht1_HMI; 
gDiscreteInputs_t.xRrWhite		:= gSignalLamp_t.xRearR_Wht2_HMI;
gDiscreteInputs_t.xFrRed		:= gSignalLamp_t.xFrontR_Red1_HMI;
gDiscreteInputs_t.xRrRed		:= gSignalLamp_t.xRearR_Red2_HMI;
gDiscreteInputs_t.xFrSideRed	:= gSignalLamp_t.xFrontR_Side1_HMI;
gDiscreteInputs_t.xRrSideRed	:= gSignalLamp_t.xRearR_Side2_HMI;
gDiscreteInputs_t.xFrGreen		:= gSignalLamp_t.xFrontR_Green_HMI;
gDiscreteInputs_t.xRrGreen		:= gSignalLamp_t.xRearR_Green_HMI;


(********************************************************************************
                                 Head Lamp
*********************************************************************************)
(*ACTIVE CABIN*)
gDiscreteInputs_t.xHead_Lamp1_ON	:= (gHeadLamp_t.xHeadLp_Stat_1 AND gCabses_t.xCab1Active) OR (gHeadLamp_t.xHeadLp_Stat_2 AND gCabses_t.xCab2Active);
gDiscreteInputs_t.xHead_Lamp1_Fault	:= (gHeadLamp_t.xFaultLp_Stat_1 AND gCabses_t.xCab1Active) OR (gHeadLamp_t.xFaultLp_Stat_2 AND gCabses_t.xCab2Active); 

(*PASSIVE CABIN*)
gDiscreteInputs_t.xHead_Lamp2_ON	:= (gHeadLamp_t.xHeadLp_Stat_2 AND gCabses_t.xCab1Active) OR (gHeadLamp_t.xHeadLp_Stat_1 AND gCabses_t.xCab2Active);
gDiscreteInputs_t.xHead_Lamp2_Fault	:= (gHeadLamp_t.xFaultLp_Stat_2 AND gCabses_t.xCab1Active) OR (gHeadLamp_t.xFaultLp_Stat_1 AND gCabses_t.xCab2Active);
gDiscreteInputs_t.xHead_Lamp_Notif_ON := gHeadLamp_t.xHeadLp_Notif;


(********************************************************************************
                                 Smoke Detector
*********************************************************************************)

gDiscreteInputs_t.xSmokeDetectorCab1 := IxSmokeDect_1;
gDiscreteInputs_t.xSmokeDetectorCab2 := IxSmokeDect_2; 

(********************************************************************************
                                 Ultrasonik Mode
*********************************************************************************)
gDiscreteInputs_t.xModeUltrasonik := gUltrason_t.xUltrsn_ON;

(********************************************************************************
                                 Train Wash
*********************************************************************************)
gDiscreteInputs_t.xModeTrainWash := gTrainwsh_t.xTrainWash_ON;


(********************************************************************************
                                 Engine Status
*********************************************************************************)
gDiscreteInputs_t.xEngine_Running := gEnginestatus_t.xEngineRunning;
gDiscreteInputs_t.xEngine_Stop := gEnginestatus_t.xEngineStop;
gDiscreteInputs_t.xEngine_Emg_Stop := gEnginestatus_t.xEngineEmergencyStop;


(********************************************************************************
                                 IO List
*********************************************************************************)
END_BODY
END_PROGRAM

