#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XTEMPSENSORERROR_1;
   SC_BOOL                    XAC_COMMAND_ON;
   SC_BOOL                    XAC1;
   SC_BOOL                    XAC2;
   SC_INT                     ISETTEMP_HUNDREDS;
   SC_BOOL                    XACSTARTUP;
   SC_BOOL                    XEVAP1_ON;
   SC_BOOL                    XEVAP2_ON;
   SC_BOOL                    XEVAP1FAULT_1;
   SC_BOOL                    XEVAP2FAULT_1;
   SC_BOOL                    XCD1FAULT_1;
   SC_BOOL                    XCD2FAULT_1;
   SC_BOOL                    XCD3FAULT_1;
   SC_BOOL                    XCD4FAULT_1;
   SC_BOOL                    XCP1FAULT_1;
   SC_BOOL                    XCP2FAULT_1;
   SC_BOOL                    XCP3FAULT_1;
   SC_BOOL                    XCP4FAULT_1;
   SC_BOOL                    XACSTARTUP2;
   SC_BOOL                    XCD_AC1_ON;
   SC_BOOL                    XCD_AC2_ON;
   SC_BOOL                    XCP_AC1_ON;
   SC_BOOL                    XCP_AC2_ON;
   SC_INT                     ITEMP_1;
   SC_BOOL                    XHALFMODE_1;
   SC_BOOL                    XFULLMODE_1;
   SC_BOOL                    XACERROR_1;
   SC_DINT                    DICP1TIME_1;
   SC_DINT                    DICP2TIME_1;
}AC_T_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XSTARTUP;
   SC_INT                     ITEMP;
   SC_BOOL                    XERRORTEMPSENSOR1;
   SC_BOOL                    XAC1ERROR;
   SC_BOOL                    XAC2ERROR;
   SC_BOOL*                   XFULL;
   SC_BOOL*                   XHALF;
   SC_INT                     IT2;
   SC_INT                     IT3;
   SC_INT                     IT4;
}FB_AC_1_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC_BOOL                    XENGINEOFF;
   SC_BOOL                    XENGINE_OFF;
}__GLOBALS_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_COPY__FB_AC_1_00082(FB_AC_1_TYP* ip1, FB_AC_1_TYP* ip2)
{
   MOVE_ANY(&(ip1->XSTARTUP), &(ip2->XSTARTUP), (long) (&(ip1->XFULL)) - (long) (&(ip1->XSTARTUP)));
   MOVE_ANY(&(ip1->IT2), &(ip2->IT2), (long) (ip1) + sizeof(FB_AC_1_TYP) - (long) (&(ip1->IT2)));
}

void SC_INIT__FB_AC_1_00083(FB_AC_1_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(FB_AC_1_TYP, IT4) + sizeof(ip->IT4);
   /* non retains: */
   ip->XSTARTUP = SC_C_BOOL(0);
   ip->ITEMP = SC_C_INT(0);
   ip->XERRORTEMPSENSOR1 = SC_C_BOOL(0);
   ip->XAC1ERROR = SC_C_BOOL(0);
   ip->XAC2ERROR = SC_C_BOOL(0);
   ip->XFULL = NULL;
   ip->XHALF = NULL;
   ip->IT2 = SC_C_INT(0);
   ip->IT3 = SC_C_INT(0);
   ip->IT4 = SC_C_INT(0);

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* FUNCTION BLOCK BODY: */
void SC_BODY__FB_AC_1_00058(FB_AC_1_TYP* ip)
{
   __GLOBALS_TYP* __lpgvl = (__GLOBALS_TYP*)__OD(4);

   /* TEMPORARY VARIABLES */
   SC_BOOL                    _SCT_B0;
   SC_BOOL                    _SCT_B1;

   FB_ENTRY(58, ip);
#undef __OG
#define __OG(member) (__lpgvl->member)
/******************* Network *************************/
{
   ip->IT4 = ((*(AC_T_TYP*)__OD(34)).ISETTEMP_HUNDREDS - SC_C_INT(10));

}
/******************* Network *************************/
{
   ip->IT3 = ((*(AC_T_TYP*)__OD(34)).ISETTEMP_HUNDREDS - SC_C_INT(10));

}
/******************* Network *************************/
{
   ip->IT2 = ((*(AC_T_TYP*)__OD(34)).ISETTEMP_HUNDREDS + SC_C_INT(20));

}
/******************* Network *************************/
SCL__ENGINE_OFF:;
{
   _SCT_B0 = __OG(XENGINEOFF);
   if((1 & _SCT_B0))goto SCL__ENGINE_OFF;

}
/******************* Network *************************/
{
   _SCT_B0 = ip->XERRORTEMPSENSOR1;
   if((1 & _SCT_B0))goto SCL__ERROR;

}
/******************* Network *************************/
{
   _SCT_B0 =  (!((1 & ip->XSTARTUP)));
   if((1 & _SCT_B0))goto SCL__NONSTARTUP;

}
/******************* Network *************************/
{
   (*ip->XFULL) = (ip->ITEMP >= ip->IT2);
   (*ip->XHALF) = ((ip->ITEMP < ip->IT2) & (ip->ITEMP > ip->IT4));

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(58, ip);
      return;
   }

}
/******************* Network *************************/
SCL__NONSTARTUP:;
{
   _SCT_B0 = ((1 & (*ip->XHALF)) &  (!((1 & (*ip->XFULL)))));
   _SCT_B1 = ( (!((1 & (*ip->XHALF)))) & (1 & (*ip->XFULL)));
   if((1 & _SCT_B0))goto SCL__HALFMODE;
   if((1 & _SCT_B1))goto SCL__FULLMODE;

}
/******************* Network *************************/
{
   (*ip->XHALF) = (ip->ITEMP >= (*(AC_T_TYP*)__OD(34)).ISETTEMP_HUNDREDS);

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(58, ip);
      return;
   }

}
/******************* Network *************************/
SCL__HALFMODE:;
{
   _SCT_B0 = (ip->ITEMP >= ip->IT2);
   (*ip->XFULL) = _SCT_B0;
   (*ip->XHALF) = (( (!((1 & _SCT_B0))) &  (!((ip->ITEMP <= ip->IT3)))) &  (!(((1 & ip->XAC1ERROR) 
   & (1 & ip->XAC2ERROR)))));

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(58, ip);
      return;
   }

}
/******************* Network *************************/
SCL__FULLMODE:;
{
   _SCT_B0 = (ip->ITEMP <= ip->IT3);
   (*ip->XFULL) = NOT_BOOL(_SCT_B0);
   (*ip->XHALF) = ((1 & _SCT_B0) &  (!(((1 & ip->XAC1ERROR) & (1 & ip->XAC2ERROR)))));

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(58, ip);
      return;
   }

}
/******************* Network *************************/
SCL__ERROR:;
{
   (*ip->XFULL) = SC_C_BOOL(1);
   (*ip->XHALF) = SC_C_BOOL(0);

}
/******************* Network *************************/
{
   _SCT_B0 = SC_C_BOOL(1);
   if((1 & _SCT_B0)){
      FB_EXIT(58, ip);
      return;
   }

}
/******************* Network *************************/
SCL__1ENGINEOFF:;
{
   (*ip->XFULL) = SC_C_BOOL(0);
   (*ip->XHALF) = SC_C_BOOL(1);

}

   FB_EXIT(58, ip);
}
