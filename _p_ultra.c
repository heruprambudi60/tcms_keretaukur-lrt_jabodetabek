#include    "cap1131.h"

#include    "mwrapper.h"
#include    "mcopx.h"
#include    "mcopmx.h"
#include    "mcopsx.h"
#include    "mcanx.h"
#include    "msio.h"
#include    "msiox.h"
#include    "mmvbx.h"
#include    "mfile.h"
#include    "mfilex.h"
#include    "mpnx.h"
#include    "msystemx.h"
#include    "mscomx.h"
#include    "mtrdpx.h"
#include    "mtrdpmx.h"
#include    "meipax.h"
#include    "mnet.h"
#include    "msnmp.h"
#include    "mftpc.h"
#include    "mdns.h"
#include    "methx.h"
#include    "msntp.h"
#include    "mtimex.h"
#include    "mmodulex.h"
#include    "mcnvx.h"
#include    "mgenx.h"
#include    "mcdl.h"
#include    "mconfigx.h"
#include    "sgeneral.h"
#include    "sconvert.h"
#include    "msim1xx.h"

#include    "hw_conf.h"
#include    "cg_types.h"


/* STRING TYPE DEFINITIONS: */
/*   <non>   */


/* FUNCTION BLOCK AND DUT STRUCTURES: */
typedef struct
{
   SC_BOOL                    XEMGULTRSN;
   SC_BOOL                    XULTRSN_ON;
   SC_INT                     XLIMITULTRSN;
   SC_BOOL                    XTRULTRSN_RDY;
}ULTRASON_MODE_T_TYP;

typedef struct
{
   SC_BOOL                    XCAB1ACTIVE;
   SC_BOOL                    XCAB2ACTIVE;
   SC_BOOL                    XCABERROR;
   SC_BOOL                    XNEUTRAL;
}CABIN_T_TYP;

typedef struct
{
   SC_INT                     ISPEED;
   SC_INT                     ILIMITSPEED;
   SC_BOOL                    XOVERSPEED;
   SC_BOOL                    XOVERSPEED_DO;
   SC_BOOL                    XZEROSPEED;
   SC_BOOL                    XALARMBUZZER;
   SC_BOOL                    XTRCUTTOFF;
}SPEED_T_TYP;

typedef struct
{
   SC_BOOL                    XTRREADY;
   SC_BOOL                    XTRCUTTOUT;
   SC_BOOL                    XTRCUTOFF_DO;
   SC_BOOL                    XVTDCOK;
}TRACTION_MNGMNT_TYP;

typedef struct
{
   SC_UDINT                   STRUCT__SIZE;
   SC__INSTC                  SR_1;
}P_ULTRASON_MODE_TYP;


/* FUNCTION PROTOTYPES: */
/*   <non>   */

void SC_INIT__P_ULTRASON_MODE_00103(P_ULTRASON_MODE_TYP* ip, int iRetain)
{
   ip->STRUCT__SIZE = OFFSET(P_ULTRASON_MODE_TYP, SR_1) + sizeof(ip->SR_1);
   /* non retains: */
   /* <non> */

   /* retains: */
   /* <non> */
   iRetain = iRetain;
}


/* PROGRAM BODY: */
void SC_BODY__P_ULTRASON_MODE_00040(void)
{
   P_ULTRASON_MODE_TYP *ip = (P_ULTRASON_MODE_TYP*) __OD(104);

   /* TEMPORARY VARIABLES */
   /* <non> */

   PRG_ENTRY(40, ip);
/******************* Network *************************/
{
   CABIN_T_TYP* __lpt0 = (CABIN_T_TYP*)__OD(11);
   (*(ULTRASON_MODE_T_TYP*)__OD(30)).XULTRSN_ON = (((1 & (*__lpt0).XCAB1ACTIVE) & (1 & LD_I_X(
   3,1,2,27))) | ((1 & (*__lpt0).XCAB2ACTIVE) & (1 & LD_I_X(3,2,3,30))));

}
/******************* Network *************************/
{
   ULTRASON_MODE_T_TYP* __lpt0 = (ULTRASON_MODE_T_TYP*)__OD(30);
   (*__lpt0).XEMGULTRSN = ((1 & (*__lpt0).XULTRSN_ON) & (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTOFF_DO));

}
/******************* Network *************************/
{
   SR_TYP* __lpt0 = (SR_TYP*)__OD(ip->SR_1);
   ULTRASON_MODE_T_TYP* __lpt1 = (ULTRASON_MODE_T_TYP*)__OD(30);
   (*__lpt0).SET1 = ((*(SPEED_T_TYP*)__OD(15)).ISPEED <= SC_C_INT(40));
   (*__lpt0).RESET = ((1 & (*__lpt1).XEMGULTRSN) & (1 & (*(TRACTION_MNGMNT_TYP*)__OD(24)).XTRCUTOFF_DO));
   SR((__lpt0));
   (*__lpt1).XTRULTRSN_RDY = (*__lpt0).Q1;

}

   PRG_EXIT(40, ip);
}
