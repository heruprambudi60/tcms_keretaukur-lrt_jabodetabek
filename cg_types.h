/* globally used types for c-code gen */
#ifndef __CGTYPES_H
#define __CGTYPES_H

#define OFFSET(type,member) (size_t)&(((type*)0)->member)

/* definitions for object table: */
typedef long SC__INSTC;
typedef struct{
   int  __ARRSIZE;
   char __ARRDATA[1];
}__ARRTYPE;

extern void* OCO__PT[];
#define __OF(id)              (OCO__PT[id])
#define __OD(id)              (OCO__PT[id])
#define __OG(member)          (((__GLOBALS_TYP*)__OD(4))->member)
#define __OA(id)              (((__ARRTYPE*)__OD(id))->__ARRDATA)
typedef void (*__PFN_INIT)(void *data, int retain);
typedef void (*__PFN_COPY)(void *ip1, void *ip2);


/* predefined RTI for all simple types: */
extern const RTI_TYPE RTI_T_BOOL;
extern const RTI_TYPE RTI_T_BYTE;
extern const RTI_TYPE RTI_T_WORD;
extern const RTI_TYPE RTI_T_DWORD;

extern const RTI_TYPE RTI_T_SINT;
extern const RTI_TYPE RTI_T_INT;
extern const RTI_TYPE RTI_T_DINT;

extern const RTI_TYPE RTI_T_USINT;
extern const RTI_TYPE RTI_T_UINT;
extern const RTI_TYPE RTI_T_UDINT;

extern const RTI_TYPE RTI_T_REAL;

extern const RTI_TYPE RTI_T_TIME;
extern const RTI_TYPE RTI_T_DT;

/* helper macro to define RTI for DUTs: */
#define RTI_CG_MEMBER_ARRAY_TYPE_(n) const struct{ULONG nMember; const char* pszCmt; RTI_MEMBER members[n];}
#define RTI_CG_MEMBER_ARRAY_(name, n, cmt)\
   RTI_CG_MEMBER_ARRAY_TYPE_(n) rti_ma__##name=\
   {n, cmt,{ 


static __inline__ BYTE* RTI_Marshal_BE_BOOL(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0];
    return pbDest + sizeof(BYTE);
}

static __inline__ BYTE* RTI_Marshal_LE_BOOL(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0];
    return pbDest + sizeof(BYTE);
}

static __inline__ BYTE* RTI_Marshal_BE_BYTE(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0];
    return pbDest + sizeof(BYTE);
}

static __inline__ BYTE* RTI_Marshal_LE_BYTE(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0];
    return pbDest + sizeof(BYTE);
}

static __inline__ BYTE* RTI_Marshal_BE_WORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[0] = pbSource[1]; 
    pbDest[1] = pbSource[0]; 
    return pbDest + sizeof(WORD);
}

static __inline__ BYTE* RTI_Marshal_LE_WORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[0] = pbSource[0]; 
    pbDest[1] = pbSource[1]; 
    return pbDest + sizeof(WORD);
}

static __inline__ BYTE* RTI_Marshal_BE_DWORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[0] = pbSource[3]; 
    pbDest[1] = pbSource[2]; 
    pbDest[2] = pbSource[1]; 
    pbDest[3] = pbSource[0]; 
    return pbDest + sizeof(DWORD);
}

static __inline__ BYTE* RTI_Marshal_LE_DWORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[0] = pbSource[0]; 
    pbDest[1] = pbSource[1]; 
    pbDest[2] = pbSource[2]; 
    pbDest[3] = pbSource[3]; 
    return pbDest + sizeof(DWORD);
}

static __inline__ BYTE* RTI_Marshal_BE_STRING(const BYTE* pbSource, BYTE* pbDest) 
{
    const SC_STRING* pStr = (SC_STRING*)pbSource;

    memcpy(pbDest, pStr->Contents, pStr->Length);
    if(pStr->Length < pStr->Size)
    {
        pbDest[pStr->Length] = 0;
    }
    return pbDest + pStr->Size;
}

static __inline__ BYTE* RTI_Marshal_LE_STRING(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest = RTI_Marshal_BE_STRING(pbSource, pbDest);
    return pbDest;
}

static __inline__ BYTE* RTI_Marshal_BE_BOOL_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{
    memcpy(pbDest, pbSource, ulArraySize);
    return pbDest + ulArraySize;
}

static __inline__ BYTE* RTI_Marshal_LE_BOOL_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{
    memcpy(pbDest, pbSource, ulArraySize);
    return pbDest + ulArraySize;
}

static __inline__ BYTE* RTI_Marshal_BE_BYTE_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{
    memcpy(pbDest, pbSource, ulArraySize);
    return pbDest + ulArraySize;
}

static __inline__ BYTE* RTI_Marshal_LE_BYTE_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{
    memcpy(pbDest, pbSource, ulArraySize);
    return pbDest + ulArraySize;
}

static __inline__ BYTE* RTI_Marshal_BE_WORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbDest = RTI_Marshal_BE_WORD(pbSource + (ulIndex * sizeof(WORD)), pbDest);
    }

    return pbDest;
}

static __inline__ BYTE* RTI_Marshal_LE_WORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    memcpy(pbDest, pbSource, ulArraySize * sizeof(WORD));
    return pbDest + (ulArraySize * sizeof(WORD));
}

static __inline__ BYTE* RTI_Marshal_BE_DWORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbDest = RTI_Marshal_BE_DWORD(pbSource + (ulIndex * sizeof(DWORD)), pbDest);
    }

    return pbDest;
}

static __inline__ BYTE* RTI_Marshal_LE_DWORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    memcpy(pbDest, pbSource, ulArraySize * sizeof(DWORD));
    return pbDest + (ulArraySize * sizeof(DWORD));
}

static __inline__ BYTE* RTI_Marshal_BE_STRING_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulSizeOfScStringDef, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbDest = RTI_Marshal_BE_STRING(pbSource + (ulIndex * ulSizeOfScStringDef), pbDest);
    }

    return pbDest;
}

static __inline__ BYTE* RTI_Marshal_LE_STRING_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulSizeOfScStringDef, ULONG ulArraySize) 
{
    pbDest = RTI_Marshal_BE_STRING_Array(pbSource, pbDest, ulSizeOfScStringDef, ulArraySize); 
    return pbDest;	
}

static __inline__ const BYTE* RTI_UnMarshal_BE_BOOL(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0] != 0 ? 0x01 : 0x00; 
    return pbSource + sizeof(BYTE);
}

static __inline__ const BYTE* RTI_UnMarshal_LE_BOOL(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0] != 0 ? 0x01 : 0x00; 
    return pbSource + sizeof(BYTE);
}

static __inline__ const BYTE* RTI_UnMarshal_BE_BYTE(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0]; 
    return pbSource + sizeof(BYTE);
}

static __inline__ const BYTE* RTI_UnMarshal_LE_BYTE(const BYTE* pbSource, BYTE* pbDest)
{
    pbDest[0] = pbSource[0]; 
    return pbSource + sizeof(BYTE);
}

static __inline__ const BYTE* RTI_UnMarshal_BE_WORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[1] = pbSource[0]; 
    pbDest[0] = pbSource[1]; 
    return pbSource + sizeof(WORD);
}

static __inline__ const BYTE* RTI_UnMarshal_LE_WORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[0] = pbSource[0]; 
    pbDest[1] = pbSource[1]; 
    return pbSource + sizeof(WORD);
}

static __inline__ const BYTE* RTI_UnMarshal_BE_DWORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[3] = pbSource[0]; 
    pbDest[2] = pbSource[1]; 
    pbDest[1] = pbSource[2]; 
    pbDest[0] = pbSource[3]; 
    return pbSource + sizeof(DWORD);
}

static __inline__ const BYTE* RTI_UnMarshal_LE_DWORD(const BYTE* pbSource, BYTE* pbDest) 
{
    pbDest[0] = pbSource[0]; 
    pbDest[1] = pbSource[1]; 
    pbDest[2] = pbSource[2]; 
    pbDest[3] = pbSource[3]; 
    return pbSource + sizeof(DWORD);
}

static __inline__ const BYTE* RTI_UnMarshal_BE_STRING(const BYTE* pbSource, BYTE* pbDest) 
{
    SC_STRING* pStr = (SC_STRING*)pbDest;
    ULONG ulIndex;
    
    // string is terminated by 0-char, it is part of the marshalled data, except if pStr->Length == pStr->Size
    for(ulIndex = 0; (ulIndex < pStr->Size) && (pbSource[ulIndex] != '\0'); ulIndex++)
    {
        pStr->Contents[ulIndex] = pbSource[ulIndex];
    }

    pStr->Length = ulIndex;
    //SC_STRING types are 0-terminated, there is always one byte more reserved for it in Contents 
    pStr->Contents[pStr->Length] = '\0';

    return pbSource + pStr->Size;
}

static __inline__ const BYTE* RTI_UnMarshal_LE_STRING(const BYTE* pbSource, BYTE* pbDest)
{
    pbSource = RTI_UnMarshal_BE_STRING(pbSource, pbDest);
    return pbSource;
}

static __inline__ const BYTE* RTI_UnMarshal_BE_BOOL_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{  
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        *pbDest++ = *pbSource++ != 0 ? 0x01 : 0x00; 
    }
    return pbSource;
}

static __inline__ const BYTE* RTI_UnMarshal_LE_BOOL_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{  
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        *pbDest++ = *pbSource++ != 0 ? 0x01 : 0x00; 
    }
    return pbSource;
}

static __inline__ const BYTE* RTI_UnMarshal_BE_BYTE_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{  
    memcpy(pbDest, pbSource, ulArraySize);
    return pbSource + ulArraySize;
}

static __inline__ const BYTE* RTI_UnMarshal_LE_BYTE_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize)
{  
    memcpy(pbDest, pbSource, ulArraySize);
    return pbSource + ulArraySize;
}

static __inline__ const BYTE* RTI_UnMarshal_BE_WORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbSource = RTI_UnMarshal_BE_WORD(pbSource, pbDest + (ulIndex * sizeof(WORD)));
    }

    return pbSource;
}

static __inline__ const BYTE* RTI_UnMarshal_LE_WORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbSource = RTI_UnMarshal_LE_WORD(pbSource, pbDest + (ulIndex * sizeof(WORD)));
    }

    return pbSource;
}

static __inline__ const BYTE* RTI_UnMarshal_BE_DWORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbSource = RTI_UnMarshal_BE_DWORD(pbSource, pbDest + (ulIndex * sizeof(DWORD)));
    }

    return pbSource;	
}

static __inline__ const BYTE* RTI_UnMarshal_LE_DWORD_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbSource = RTI_UnMarshal_LE_DWORD(pbSource, pbDest + (ulIndex * sizeof(DWORD)));
    }

    return pbSource;	
}

static __inline__ const BYTE* RTI_UnMarshal_BE_STRING_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulSizeOfScStringDef, ULONG ulArraySize) 
{
    ULONG ulIndex;
    for(ulIndex = 0; ulIndex < ulArraySize; ulIndex++)
    {
        pbSource = RTI_UnMarshal_BE_STRING(pbSource, pbDest + (ulIndex * ulSizeOfScStringDef));
    }

    return pbSource;
}

static __inline__ const BYTE* RTI_UnMarshal_LE_STRING_Array(const BYTE* pbSource, BYTE* pbDest, ULONG ulSizeOfScStringDef, ULONG ulArraySize) 
{
    pbSource = RTI_UnMarshal_BE_STRING_Array(pbSource, pbDest, ulSizeOfScStringDef, ulArraySize); 
    return pbSource;
}

#endif
