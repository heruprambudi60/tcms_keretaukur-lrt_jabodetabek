# set compile options:
SC_COMPILEOPTION0=-D__IEC_WATCHDOG__
SC_COMPILEOPTION1=-DCODE_SC30412=1

# set compile mode:
ONL_CHG = 0
SELECC=

# definition of macro MANUFACTURERLIBS:
MANUFACTURERLIBS = \
-l:mWRAPPER.A \
-l:mCOPx.A \
-l:mCOPMx.A \
-l:mCOPSx.A \
-l:mCANx.A \
-l:mSIO.A \
-l:mSIOx.A \
-l:mMVBx.A \
-l:mFILE.A \
-l:mFILEx.A \
-l:mPNx.A \
-l:mSYSTEMx.A \
-l:mSCOMx.A \
-l:mTRDPx.A \
-l:mTRDPMx.A \
-l:mEIPAx.A \
-l:mNET.A \
-l:mSNMP.A \
-l:mFTPC.A \
-l:mDNS.A \
-l:mETHx.A \
-l:mSNTP.A \
-l:mTIMEx.A \
-l:mMODULEx.A \
-l:mCNVx.A \
-l:mGENx.A \
-l:mCDL.A \
-l:mCONFIGx.A \
-l:sGeneral.A \
-l:sConvert.A \
-l:mSIM1xx.A \

# definition of macro APPLICATION_OBJS:
APPLICATION_OBJS = \
ocdata.o\
ocmlinit.o\
_p_ac_ca.o\
_fb_ac_g.o\
_fb_gett.o\
_fb_filt.o\
_fb_ac_h.o\
_fb_time.o\
_fb_ac_2.o\
_fb_ac_1.o\
_fb_ac_e.o\
_1fb_ac_.o\
_0fb_ac_.o\
_fb_ac_c.o\
_p_ac_mo.o\
_p_compr.o\
_fb_pres.o\
_p_engin.o\
_p_train.o\
_p_ultra.o\
_p_com_t.o\
_p_head_.o\
_p_calen.o\
_p_signa.o\
_p_tract.o\
_p_oo_hm.o\
_p_oo_do.o\
_p_brake.o\
_p_deadm.o\
_p_lamp.o\
_p_speed.o\
_p_doors.o\
_p_batt.o\
_p_manua.o\
_p_maste.o\
_p_cabse.o\
_my_task.o\
_project.o\
hw_conf.o\
hw_conf_l0.o\
hw_conf_l1.o\
hw_conf_l2.o\
hw_conf_l3.o\
hw_conf_n0.o\
StrLst.o\

# BASE is the Base path for the system files
BASE=C:\Selectron\MOS\MOS83x_V740.EC3C_SIM
LOC_EXTENSION=
MAP_EXTENSION=.MAP

# Before invoking 'make' Seleccontrol removes the file
# success.sem.If build 'all' fails, the file success.sem
# is not created, If softcontrol does not find it an error
# message will displayed.
# target 'all' is defined in custcomp.mk:

success.sem: all
	$(CHECK_RES_AND_SUCCESS) #if exist res fecho success.sem OK

# Special Settings for CPU Dependencies
# Cpu Type for Compilation
BOARD_TYP = CPU833

# Compilation Model
MODEL      = -mlarge

# Common Rules of Compilation
include $(BASE)\vorlagen\ips.mk

# Standard Definitions for OTHER_OBJ and all
include $(BASE)\vorlagen\custdef.mk

# Standard Rules for Specific targets
include $(BASE)\vorlagen\custcomp.mk
